package uk.co.nickthecoder.fizzy

import javafx.application.Application

class Fizzy {
    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(FizzyApp::class.java, *args)
        }
    }
}
