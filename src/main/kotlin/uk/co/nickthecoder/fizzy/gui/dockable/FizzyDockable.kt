/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.dockable

import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.Image
import uk.co.nickthecoder.fizzy.gui.ApplicationAction
import uk.co.nickthecoder.harbourfx.AbstractTabbedDockable
import uk.co.nickthecoder.harbourfx.Dockable

abstract class FizzyDockable(applicationAction: ApplicationAction) : Dockable {

    override val dockableId: String = applicationAction.name

    override val dockableIcon: Image? = applicationAction.findImage()

    override val dockableTitle = SimpleStringProperty(applicationAction.label)

    override val dockablePrefix = SimpleStringProperty()

    init {
        applicationAction.shortcutNumber()?.let { dockablePrefix.set("${it}: ") }
    }
}

abstract class FizzyTabbedDockable(applicationAction: ApplicationAction)
    : AbstractTabbedDockable() {

    override val dockableId: String = applicationAction.name

    override val dockableIcon: Image? = applicationAction.findImage()

    override val dockableTitle = SimpleStringProperty(applicationAction.label)

    override val dockablePrefix = SimpleStringProperty()

    init {
        applicationAction.shortcutNumber()?.let { dockablePrefix.set("${it}: ") }
    }
}
