/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import uk.co.nickthecoder.fizzy.FizzyApp
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.tools.DeleteTool
import uk.co.nickthecoder.fizzy.controller.tools.EditGeometryTool
import uk.co.nickthecoder.fizzy.controller.tools.EditTextTool
import uk.co.nickthecoder.fizzy.controller.tools.SelectTool
import uk.co.nickthecoder.fizzy.gui.controls.*
import uk.co.nickthecoder.fizzy.model.Color
import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.PrimitiveShapes
import uk.co.nickthecoder.fizzy.model.Templates
import uk.co.nickthecoder.fizzy.util.FizzyJsonReader

class FToolBar(val mainWindow: MainWindow)
    : BuildableNode {

    val toolBar = ToolBar()

    val document: Document?
        get() = mainWindow.document

    val drawingArea: DrawingArea?
        get() = mainWindow.documentTab?.drawingArea

    val controller: Controller?
        get() = drawingArea?.controller

    val sh = mainWindow.shortcutHelper


    val primitives2d = arrayOf(

            ShapePickerItem("primitive-box", "Box", PrimitiveShapes.box),

            ShapePickerItem("primitive-poly3", "Triangle", PrimitiveShapes.poly3),
            ShapePickerItem("primitive-poly4", "Diamond", PrimitiveShapes.poly4),
            ShapePickerItem("primitive-poly5", "Pentagon", PrimitiveShapes.poly5),
            ShapePickerItem("primitive-poly6", "Hexagon", PrimitiveShapes.poly6),
            ShapePickerItem("primitive-poly7", "Heptagon", PrimitiveShapes.poly7),
            ShapePickerItem("primitive-poly8", "Octagon", PrimitiveShapes.poly8),

            ShapePickerItem("primitive-star3", "3 Pointed Star", PrimitiveShapes.star3),
            ShapePickerItem("primitive-star4", "4 Pointed Star", PrimitiveShapes.star4),
            ShapePickerItem("primitive-star5", "5 Pointed Star", PrimitiveShapes.star5),
            ShapePickerItem("primitive-star6", "6 Pointed Star", PrimitiveShapes.star6)
    )

    val primitives1d = arrayOf(
            ShapePickerItem("primitive-line", "Line", PrimitiveShapes.line)
    )

    val lineWidths = arrayOf(
            "0.5 pt",
            "1 pt",
            "2 pt",
            "3 pt",
            "4 pt"
    )

    val openButton = ApplicationActions.DOCUMENT_OPEN.createButton(sh) { mainWindow.documentOpen() }
    val newButton = ApplicationActions.DOCUMENT_NEW.createSplitMenuButton(sh) { mainWindow.documentNew() }
    val saveButton = ApplicationActions.DOCUMENT_SAVE.createSplitMenuButton(sh) { mainWindow.documentSave() }
    //val saveAsButton = ApplicationActions.DOCUMENT_SAVE_AS.createButton(sh) { mainWindow.documentSaveAs() }

    val undoButton = ApplicationActions.EDIT_UNDO.createButton(sh) { onUndo() }
    val redoButton = ApplicationActions.EDIT_REDO.createButton(sh) { onRedo() }

    val selectToolButton = ApplicationActions.TOOL_SELECT.createButton(sh) { controller?.let { it.tool = SelectTool(it) } }
    val editGeometryToolButton = ApplicationActions.TOOL_EDIT_GEOMETRY.createButton(sh) { controller?.let { it.tool = EditGeometryTool(it) } }
    val editTextToolButton = ApplicationActions.TOOL_EDIT_TEXT.createButton(sh) { controller?.let { it.tool = EditTextTool(it) } }

    val primitive1dButton = ApplicationActions.TOOL_PRIMITIVE1D.create(sh, ShapePicker(mainWindow, primitives1d))
    val primitive2dButton = ApplicationActions.TOOL_PRIMITIVE2D.create(sh, ShapePicker(mainWindow, primitives2d))

    val deleteToolButton = ApplicationActions.TOOL_DELETE.createButton(sh) { controller?.let { it.tool = DeleteTool(it) } }

    val debugButton = ApplicationActions.DEV_DEBUG.createButton(sh) { mainWindow.debug() }

    val lineWidthPicker = LineWidthPicker(mainWindow, lineWidths)
    val strokeColorPicker = FColorPicker(mainWindow, "stroke", isStroke = true, defaultColor = Color.BLACK)
    val fillColorPicker = FColorPicker(mainWindow, "fill", isStroke = false, defaultColor = Color.WHITE)
    val strokeJoinPicker = StrokeJoinPicker(mainWindow)
    val strokeCapPicker = StrokeCapPicker(mainWindow)
    val fontNamePicker = FontNamePicker(mainWindow)
    val themePicker = ThemePicker(mainWindow)

    val flipXButton = ApplicationActions.EDIT_FLIP_X.createButton(sh) { controller?.flip(true, false) }
    val flipYButton = ApplicationActions.EDIT_FLIP_Y.createButton(sh) { controller?.flip(false, true) }

    override fun build(): Node {

        toolBar.items.addAll(
                newButton, openButton, saveButton,
                Separator(),
                undoButton, redoButton, flipXButton, flipYButton,
                Separator(),
                selectToolButton, primitive1dButton, primitive2dButton, deleteToolButton, editGeometryToolButton, editTextToolButton,
                Separator(),
                fillColorPicker.build(), strokeColorPicker.build(),
                lineWidthPicker.build(), strokeJoinPicker.build(), strokeCapPicker.build(),
                Separator(),
                fontNamePicker.build(),
                themePicker.build(),
                Separator(),
                debugButton
        )

        sh.actions.add(ApplicationActions.CLOSE_TAB to { mainWindow.closeTab() })

        sh.actions.add(ApplicationActions.ZOOM_IN to { mainWindow.zoomIn() })
        sh.actions.add(ApplicationActions.ZOOM_IN2 to { mainWindow.zoomIn() })
        sh.actions.add(ApplicationActions.ZOOM_OUT to { mainWindow.zoomOut() })
        sh.actions.add(ApplicationActions.ZOOM_RESET to { mainWindow.resetZoom() })
        sh.actions.add(ApplicationActions.EDIT_REDO2 to { onRedo() })
        sh.actions.add(ApplicationActions.EDIT_DELETE to { onDelete() })

        saveButton.items.addAll(
                ApplicationActions.DOCUMENT_SAVE.createMenuItem(sh) { mainWindow.documentSave() },
                ApplicationActions.DOCUMENT_SAVE_AS.createMenuItem(sh) { mainWindow.documentSaveAs() },
                ApplicationActions.EXPORT_PNG.createMenuItem(sh) { mainWindow.exportAsPNG() },
                ApplicationActions.EXPORT_SVG.createMenuItem(sh) { mainWindow.exportAsSVG() }
        )

        val menuButton = MenuButton()
        menuButton.graphic = FizzyApp.graphic("icons/menu.png")

        val editItems = Menu("Edit")
        editItems.items.addAll(
                ApplicationActions.EDIT_COPY.createMenuItem { controller?.copy() },
                ApplicationActions.EDIT_PASTE.createMenuItem { controller?.paste() },
                ApplicationActions.EDIT_CUT.createMenuItem { controller?.cut() },
                ApplicationActions.EDIT_DUPLICATE.createMenuItem { controller?.duplicate() },

                ApplicationActions.EDIT_ZORDER_TOP.createMenuItem { controller?.zOrderTop() },
                ApplicationActions.EDIT_ZORDER_UP.createMenuItem { controller?.zOrderUp() },
                ApplicationActions.EDIT_ZORDER_DOWN.createMenuItem { controller?.zOrderDown() },
                ApplicationActions.EDIT_ZORDER_BOTTOM.createMenuItem { controller?.zOrderBottom() }
        )

        val toolsItem = Menu("Windows")
        toolsItem.items.addAll(
                ApplicationActions.WINDOW_STENCILS.createMenuItem { mainWindow.toggleStencils() },
                ApplicationActions.WINDOW_DOCUMENT_PROPERTIES.createMenuItem { mainWindow.toggleDocumentProperties() },
                ApplicationActions.WINDOW_SHAPE_PROPERTIES.createMenuItem { mainWindow.toggleShapeProperties() },
                ApplicationActions.WINDOW_SHAPE_TREE.createMenuItem { mainWindow.toggleShapeTree() },
                ApplicationActions.WINDOW_HELP_REFERENCE.createMenuItem { mainWindow.helpReference() }
        )

        menuButton.items.addAll(editItems, toolsItem)
        toolBar.items.add(menuButton)

        newButton.focusedProperty().addListener { _, _, _ -> updateNewMenu() }

        return toolBar
    }

    private fun updateNewMenu() {
        newButton.items.clear()
        Templates.templates.forEach { template ->
            template.file?.let { file ->
                val item = MenuItem(file.nameWithoutExtension)
                item.onAction = EventHandler {
                    val doc = FizzyJsonReader(file).load()
                    doc.file = null
                    mainWindow.editDocument(doc)
                }
                newButton.items.add(item)
            }
        }
    }

    fun clearSelection() {
        controller?.selection?.clear()
        controller?.handles?.clear()
        controller?.dirty?.let { it.value++ }
    }

    private fun onUndo() {
        document?.history?.undo()
        clearSelection()
    }

    private fun onRedo() {
        document?.history?.redo()
        clearSelection()
    }

    private fun onDelete() {
        controller?.selection?.let { selection ->
            controller?.deleteShapes(selection)
        }
    }
}
