/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.TextField
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula

abstract class StringEditor(private val prop: Prop<String>)
    : TextField(prop.value), PropListener {

    init {
        prefColumnCount = 15

        textProperty().addListener { _, _, _ -> checkAndUpdate() }
        prop.addListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        text = this.prop.value
    }

    private fun checkAndUpdate() {
        if (prop.value != text) {
            updateProp(text)
        }
    }

    protected abstract fun updateProp(value: String)

}


class StringVariableEditor(private val variable: PropVariable<String>)
    : StringEditor(variable) {

    init {
        styleClass.add("variable")
    }

    override fun updateProp(value: String) {
        variable.value = text
    }
}


class StringExpressionValueEditor(private val expression: PropExpression<String>)
    : StringEditor(expression) {

    init {
        styleClass.add("value")
    }

    override fun updateProp(value: String) {
        expression.formula = value.toFormula()
    }
}
