/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.dockable

import javafx.scene.Node
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.util.StringConverter
import uk.co.nickthecoder.fizzy.gui.ApplicationActions
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.gui.editor.createValueEditor
import uk.co.nickthecoder.fizzy.gui.editor.createVariableEditor
import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.Page
import uk.co.nickthecoder.fizzy.model.PageSize

class DocumentProperties(val mainWindow: MainWindow)
    : FizzyTabbedDockable(ApplicationActions.WINDOW_DOCUMENT_PROPERTIES) {

    override val noTabContent = Label("No Document Selected")

    private val sizeTab = Tab("Page Sizes")
    private val customPropertiesTab = Tab("Custom Properties")


    init {
        mainWindow.documentProperty.addListener { _, _, _ -> rebuild() }
        rebuild()
    }


    override fun rebuildTabs() {
        val document = mainWindow.document ?: return

        sizeTab.content = createSizeTab(document)
        customPropertiesTab.content = createCustomPropertiesTab(document)

        for (tab in listOf(sizeTab, customPropertiesTab)) {
            if (tab.content != null) {
                tabs.add(tab)
            }
        }
    }

    private fun createSizeTab(document: Document): Node {

        val pageNumber = ComboBox<Page>()
        pageNumber.converter = object : StringConverter<Page>() {
            override fun fromString(str: String): Page {
                return document.pages[str.toInt() - 1]
            }

            override fun toString(obj: Page): String {
                return (document.pages.indexOf(obj) + 1).toString()
            }
        }
        document.pages.forEach { page ->
            pageNumber.items.add(page)
        }

        val heading = GridPane()
        heading.addRow(0, Label("Page #"), pageNumber)

        val sizeGrid = GridPane()
        sizeGrid.styleClass.add("properties-grid")

        pageNumber.valueProperty().addListener { _, _, page ->
            sizeGrid.children.clear()
            sizeGrid.addRow(0, Label("Paper Size"), createValueEditor(page.pageSize, page.pageSize.paperSize))
            sizeGrid.addRow(1, Label("Document Size"), createValueEditor(page.pageSize, page.pageSize.documentSize))
        }

        pageNumber.value = document.pages[0]

        val borderPane = BorderPane()
        borderPane.top = heading
        borderPane.center = sizeGrid
        return borderPane
    }

    private fun createCustomPropertiesTab(document: Document): Node {

        val customPropertiesGrid = GridPane()

        customPropertiesGrid.styleClass.add("properties-grid")
        document.customProperties.forEachIndexed { index, customProperty ->
            val label = Label(customProperty.label.value)
            val data = createVariableEditor(PageSize(), customProperty.data)
            customPropertiesGrid.addRow(index, label, data)
        }

        return customPropertiesGrid
    }

}
