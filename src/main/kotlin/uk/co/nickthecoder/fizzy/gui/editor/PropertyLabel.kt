/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.TextField
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener

class PropertyLabel(val property: Prop<*>, expressionEditor: ExpressionEditor? = null)
    : TextField(property.valueString()), PropListener {

    init {
        property.addListener(this)
        isEditable = false
        update()

        if (property is PropExpression<*> && expressionEditor != null) {
            addEventFilter(MouseEvent.MOUSE_PRESSED) { event ->
                if (event.clickCount == 2) {
                    expressionEditor.editExpression(property, this)
                    event.consume()
                }
            }
        }
    }

    fun update() {
        text = property.valueString()
    }

    override fun dirty(prop: Prop<*>) {
        update()
    }
}
