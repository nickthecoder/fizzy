/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.controls

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.MenuButton
import javafx.scene.control.MenuItem
import uk.co.nickthecoder.fizzy.FizzyApp
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.model.Color
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions
import uk.co.nickthecoder.fizzy.prop.DimensionExpression


class LineWidthPicker(val mainWindow: MainWindow, val widthFormulas: Array<String>)
    : BuildableNode {

    val button = MenuButton()

    /**
     * Holds the last line width to be picked. New shapes will use this line width.
     * Note that this is a DimensionExpression, and not a Dimension, so that its FORMULA can be
     * passed when creating new shapes. In this way point sizes won't go wrong when moving between documents
     * with different scale factors for their DynamicUnits.
     */
    val lineWidthProp = DimensionExpression("1pt")

    override fun build(): Node {

        button.graphic = FizzyApp.graphic("icons/format-stroke-width.png")
        val noStrokeMenuItem = MenuItem("None")
        noStrokeMenuItem.onAction = EventHandler {
            noStroke()
        }
        button.items.add(noStrokeMenuItem)

        widthFormulas.forEach { widthFormula ->
            val menuItem = MenuItem(widthFormula)
            menuItem.onAction = EventHandler {
                pickWidth(widthFormula)
            }
            button.items.add(menuItem)
        }

        return button
    }

    fun noStroke() {
        val history = mainWindow.document?.history ?: return

        history.beginBatch()
        mainWindow.controller?.selection?.forEach { shape ->
            history.makeChange(ChangeExpressions(listOf(
                    shape.strokeColor to Color.TRANSPARENT.toFormula()
            )))
        }
        history.endBatch()

    }

    fun pickWidth(widthFormula: String) {

        lineWidthProp.formula = widthFormula

        val history = mainWindow.document?.history ?: return

        history.beginBatch()
        mainWindow.controller?.selection?.forEach { shape ->
            history.makeChange(ChangeExpressions(listOf(
                    shape.lineWidth to widthFormula
            )))
        }
        history.endBatch()
    }

}
