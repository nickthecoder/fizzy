/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.PageSize
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula


abstract class Dimension2Editor(pageSize: PageSize, private val prop: Prop<Dimension2>)
    : HBox(), PropListener {

    private val xTextField = TextField(prop.value.x.inUnits(prop.value.x.units).toFormula())
    private val yTextField = TextField(prop.value.y.inUnits(prop.value.y.units).toFormula())

    private val xUnitsComboBox = UnitsComboBox(pageSize, prop.value.x.units)
    private val yUnitsComboBox = UnitsComboBox(pageSize, prop.value.x.units)

    init {
        children.addAll(xTextField, xUnitsComboBox, Label(","), yTextField, yUnitsComboBox)
        styleClass.addAll("hbox")
        xTextField.prefColumnCount = doubleColumnCount
        yTextField.prefColumnCount = doubleColumnCount

        xTextField.textProperty().addListener { _, _, _ -> checkAndUpdate() }
        yTextField.textProperty().addListener { _, _, _ -> checkAndUpdate() }

        xUnitsComboBox.valueProperty().addListener { _, _, _ -> checkAndUpdate() }
        yUnitsComboBox.valueProperty().addListener { _, _, _ -> checkAndUpdate() }

        prop.addListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        xTextField.text = this.prop.value.x.inUnits(this.prop.value.x.units).toFormula()
        yTextField.text = this.prop.value.y.inUnits(this.prop.value.y.units).toFormula()
        xUnitsComboBox.units = this.prop.value.x.units
        yUnitsComboBox.units = this.prop.value.y.units
    }

    private fun checkAndUpdate() {
        xTextField.styleClass.remove("error")
        yTextField.styleClass.remove("error")
        try {
            xTextField.text.toDouble()
        } catch (e: Exception) {
            xTextField.styleClass.add("error")
        }
        try {
            yTextField.text.toDouble()
        } catch (e: Exception) {
            yTextField.styleClass.add("error")
        }
        try {
            val x = Dimension(xTextField.text.toDouble(), xUnitsComboBox.units)
            val y = Dimension(yTextField.text.toDouble(), yUnitsComboBox.units)
            if (prop.value.x != x || prop.value.y != y) {
                updateProp(Dimension2(x, y))
            }
        } catch (e: Exception) {
        }
    }

    protected abstract fun updateProp(value: Dimension2)
}


class Dimension2VariableEditor(pageSize: PageSize, private val variable: PropVariable<Dimension2>)
    : Dimension2Editor(pageSize, variable) {

    init {
        styleClass.addAll("variable")
    }

    override fun updateProp(value: Dimension2) {
        variable.value = value
    }
}

class Dimension2ExpressionValueEditor(pageSize: PageSize, private val expression: PropExpression<Dimension2>)
    : Dimension2Editor(pageSize, expression) {

    init {
        styleClass.addAll("value")
    }

    override fun updateProp(value: Dimension2) {
        expression.formula = value.toFormula()
    }
}
