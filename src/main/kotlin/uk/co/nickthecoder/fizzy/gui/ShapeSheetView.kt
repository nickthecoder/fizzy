/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.ButtonBase
import javafx.scene.control.Label
import javafx.scene.control.TitledPane
import javafx.scene.layout.FlowPane
import javafx.scene.layout.VBox
import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.controlpoint.ControlPoint
import uk.co.nickthecoder.fizzy.model.geometry.GeometryPart

class ShapeSheetView(val mainWindow: MainWindow, val shape: Shape)
    : AbstractSheetView() {

    private val geometryPartsListener = ListListenerLambda<GeometryPart> { _, _, _, _ ->
        rebuildSection("Geometry")
    }

    private val connectionPointsListener = ListListenerLambda<ConnectionPoint> { _, _, _, _ ->
        rebuildSection("ConnectionPoint")
    }

    private val controlPointsListener = ListListenerLambda<ControlPoint> { _, _, _, _ ->
        rebuildSection("ControlPoint")
    }

    private val snapPointsListener = ListListenerLambda<SnapPoint> { _, _, _, _ ->
        rebuildSection("Snap")
    }

    private val scratchListener = ListListenerLambda<Scratch> { _, _, _, _ ->
        rebuildSection("Scratch")
    }

    private val customPropertiesListener = ListListenerLambda<CustomProperty> { _, _, _, _ ->
        rebuildSection("CustomProperty")
    }

    private val childrenListener = ListListenerLambda<Shape> { _, _, _, _ ->
        rebuildRelations()
    }

    private val relationsTitledPane = TitledPane("Relations", Label(""))


    init {
        if (shape is ShapeWithGeometry) {
            shape.geometry.parts.listeners.add(geometryPartsListener)
        }
        shape.connectionPoints.listeners.add(connectionPointsListener)
        shape.controlPoints.listeners.add(controlPointsListener)
        shape.snapPoints.listeners.add(snapPointsListener)
        shape.scratches.listeners.add(scratchListener)
        shape.customProperties.listeners.add(customPropertiesListener)
        shape.children.listeners.add(childrenListener)
    }

    override fun document() = shape.document()

    override fun title(): String {
        val result = StringBuffer("${shape.name.value}#${shape.id}")
        var parent = shape.parent
        while (parent is Shape) {
            result.append(" :: ${parent.name.value}#${parent.id}")
            parent = parent.parent
        }
        return result.toString()
    }

    override fun build(): Node {
        try {
            return super.build()
        } finally {
            relationsTitledPane.isExpanded = true
            rebuildRelations()
            vBox.children.add(relationsTitledPane)
        }
    }

    private fun createShapeButton(shape: Shape): ButtonBase {

        val button = Button("${shape.name.value} #${shape.id}")
        button.onAction = EventHandler { mainWindow.editShapeSheet(shape) }
        return button
    }

    fun rebuildRelations() {

        val container = VBox()

        var ancestor = shape.parent
        if (ancestor is Shape) {
            val flow = FlowPane()
            container.children.addAll(Label("Ancestors"), flow)

            while (ancestor is Shape) {
                flow.children.add(0, createShapeButton(ancestor))
                ancestor = ancestor.parent
            }
        }

        shape.linkedFrom?.let { master ->
            container.children.addAll(Label("Master"), createShapeButton(master))
        }

        if (shape.children.isNotEmpty()) {

            val flow = FlowPane()
            container.children.addAll(Label("Descendants"), flow)

            fun recurse(parent: Shape) {
                parent.children.forEach { child ->
                    flow.children.add(createShapeButton(child))
                    recurse(child)
                }
            }

            recurse(shape)
        }
        relationsTitledPane.content = container
    }

    override fun metaData() = shape.metaData()

    override fun pageSize() = shape.page().pageSize

    override fun topLevelSectionName() = "Shape #${shape.id} ${shape.name.value}"

}
