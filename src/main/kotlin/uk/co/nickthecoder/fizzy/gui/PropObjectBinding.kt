/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.beans.binding.ObjectBinding
import javafx.beans.property.Property
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener

class PropObjectBinding<T : Any>(val prop: Prop<T>)
    : ObjectBinding<T>(), PropListener {

    override fun computeValue() = prop.value

    init {
        prop.addListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        invalidate()
    }

}

class NotPropObjectBinding(val prop: Prop<Boolean>)
    : ObjectBinding<Boolean>(), PropListener {

    override fun computeValue() = !prop.value

    init {
        prop.addListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        invalidate()
    }
}

fun <T : Any> Property<T>.bind(prop: Prop<T>) {
    this.bind(PropObjectBinding(prop))
}

fun Property<Boolean>.bindNot(prop: Prop<Boolean>) {
    this.bind(NotPropObjectBinding(prop))
}