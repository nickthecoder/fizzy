/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula


abstract class AngleEditor(private val prop: Prop<Angle>)
    : HBox(), PropListener {

    private val textField = TextField(prop.value.degrees.toFormula())

    init {
        children.addAll(textField, Label("degrees"))

        styleClass.add("hbox")
        textField.prefColumnCount = doubleColumnCount

        textField.textProperty().addListener { _, _, _ -> checkAndUpdate() }
        prop.addListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        textField.text = this.prop.value.degrees.toFormula()
    }

    private fun checkAndUpdate() {
        textField.styleClass.remove("error")
        try {
            val value = textField.text.toDouble()
            if (prop.value.degrees != value) {
                updateProp(value)
            }
        } catch (e: Exception) {
            textField.styleClass.add("error")
        }
    }

    protected abstract fun updateProp(value: Double)
}


class AngleVariableEditor(private val variable: PropVariable<Angle>)
    : AngleEditor(variable) {

    init {
        styleClass.add("variable")
    }

    override fun updateProp(value: Double) {
        variable.value = Angle.degrees(value)
    }
}


class AngleExpressionValueEditor(private val expression: PropExpression<Angle>)
    : AngleEditor(expression) {

    init {
        styleClass.add("variable")
    }

    override fun updateProp(value: Double) {
        expression.formula = Angle.degrees(value).toFormula()
    }
}
