/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.event.EventHandler
import javafx.scene.control.TextField
import javafx.scene.control.Tooltip
import javafx.scene.input.KeyCode
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.fizzy.model.history.ChangeExpression
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener

class ExpressionField(val expression: PropExpression<*>, editor: ExpressionEditor)
    : TextField(expression.formula), PropListener {

    init {
        styleClass.add("expression")
        expression.addListener(this)

        focusedProperty().addListener { _, _, newValue ->
            if (newValue) {
                editor.showExpression(expression, this)
            }
        }

        addEventFilter(MouseEvent.MOUSE_PRESSED) { event ->
            if (event.clickCount == 2) {
                editor.editExpression(expression, this)
                event.consume()
            }
        }

        onKeyPressed = EventHandler { event ->
            if (event.code == KeyCode.ENTER) {
                event.consume()
                editor.editExpression(expression, this)
            }
        }

        prefColumnCount = 10

        textProperty().addListener { _, _, newFormula ->
            if (expression.formula != newFormula) {
                expression.formula = newFormula
                update()
            }
        }

        focusedProperty().addListener { _, _, hasFocus ->
            expression.let { expression ->
                if (!hasFocus) {
                    if (expression.formula != text) {
                        editor.history.makeChange(ChangeExpression(expression, text))
                        expression.value
                        update()
                    }
                }
            }
        }
        update()
    }

    private fun update() {
        if (text != expression.formula) {
            text = expression.formula
        }
        tooltip = Tooltip(expression.valueString())

        styleClass.remove("error")
        styleClass.remove("linked")

        if (expression.isLinked()) {
            styleClass.add("linked")
        }
        if (expression.isError()) {
            styleClass.add("error")
        }
    }

    override fun dirty(prop: Prop<*>) {
        update()
    }
}
