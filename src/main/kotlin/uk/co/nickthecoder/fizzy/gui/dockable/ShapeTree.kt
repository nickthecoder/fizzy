/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.dockable

import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import uk.co.nickthecoder.fizzy.gui.ApplicationActions
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.model.ShapeParent


class ShapeTree(val mainWindow: MainWindow)
    : FizzyDockable(ApplicationActions.WINDOW_SHAPE_TREE) {

    private val tree = TreeView<ShapeParent>()

    private val noDocumentLabel = Label("No Document")

    override val dockableContent = SimpleObjectProperty<Node>(noDocumentLabel)

    init {
        dockableContent.value = buildTree()
        mainWindow.pageProperty.addListener { _, _, _ -> dockableContent.value = buildTree() }
    }

    private fun buildTree(): Node? {

        tree.root?.children?.clear()
        val page = mainWindow.page ?: return noDocumentLabel

        tree.isShowRoot = false
        tree.root = ShapeItem(page)
        tree.root.isExpanded = true

        tree.selectionModel.selectedItemProperty().addListener { _, _, item ->
            val shape = item?.value
            if (shape is Shape) {
                mainWindow.documentTab?.let { docTab ->
                    if (docTab.page === item.value.page()) {
                        docTab.drawingArea.controller.selection.clear()
                        docTab.drawingArea.controller.parent = shape.parent
                        docTab.drawingArea.controller.selection.add(shape)
                    }
                }
            }
        }

        tree.onMousePressed = EventHandler {
            if (it.isPopupTrigger) {
                onPopup()
            }
        }
        tree.onMouseReleased = EventHandler {
            if (it.isPopupTrigger) {
                onPopup()
            }
        }

        return tree
    }

    private fun onPopup() {
        val shape = tree.selectionModel.selectedItem?.value
        if (shape is Shape) {
            val menu = ContextMenu()
            val ss = MenuItem("Edit Shape Sheet")
            ss.onAction = EventHandler { mainWindow.editShapeSheet(shape) }
            menu.items.add(ss)
            tree.contextMenu = menu
        }
    }

    class ShapeItem(val shape: ShapeParent) : TreeItem<ShapeParent>(shape) {

        private var opened = false

        // TODO Listen to the children, and add/remove when changed.

        override fun getChildren(): ObservableList<TreeItem<ShapeParent>> {
            if (!opened) {
                opened = true

                super.getChildren().setAll(shape.children.map { ShapeItem(it) })
            }
            return super.getChildren()
        }

        override fun isLeaf() = false

        override fun toString(): String {
            val v = value
            return if (v is Shape) v.name.value else ""
        }

    }
}
