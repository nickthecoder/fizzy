/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.Node
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropVariable

fun createValueEditor(pageSize: PageSize, prop: Prop<*>): Node {
    return when (prop) {
        is PropVariable<*> -> createVariableEditor(pageSize, prop)
        is PropExpression<*> -> createExpressionValueEditor(pageSize, prop)
        else -> Label(prop.valueString())
    }
}

@Suppress("UNCHECKED_CAST")
fun createVariableEditor(pageSize: PageSize, prop: PropVariable<*>): Node {
    return when (prop.value) {
        is Angle -> AngleVariableEditor(prop as PropVariable<Angle>)
        is Boolean -> BooleanVariableEditor(prop as PropVariable<Boolean>)
        is Dimension -> DimensionVariableEditor(pageSize, prop as PropVariable<Dimension>)
        is Dimension2 -> Dimension2VariableEditor(pageSize, prop as PropVariable<Dimension2>)
        is Double -> DoubleVariableEditor(prop as PropVariable<Double>)
        is String -> StringVariableEditor(prop as PropVariable<String>)
        is Vector2 -> Vector2VariableEditor(prop as PropVariable<Vector2>)
        else -> Label(prop.valueString())
    }
}

@Suppress("UNCHECKED_CAST")
fun createExpressionValueEditor(pageSize: PageSize, prop: PropExpression<*>): Node {
    return when (prop.value) {
        is Angle -> AngleExpressionValueEditor(prop as PropExpression<Angle>)
        is Boolean -> BooleanExpressionValueEditor(prop as PropExpression<Boolean>)
        is Dimension -> DimensionExpressionValueEditor(pageSize, prop as PropExpression<Dimension>)
        is Dimension2 -> Dimension2ExpressionValueEditor(pageSize, prop as PropExpression<Dimension2>)
        is Double -> DoubleExpressionValueEditor(prop as PropExpression<Double>)
        is String -> StringExpressionValueEditor(prop as PropExpression<String>)
        is Vector2 -> Vector2ExpressionValueEditor(prop as PropExpression<Vector2>)
        else -> Label(prop.valueString())
    }
}

internal const val doubleColumnCount = 8

class UnitsComboBox(val size: PageSize, value: Units)
    : ComboBox<String>() {

    var units: Units
        get() = size.getUnits(value)
        set(v) {
            value = v.name
        }

    init {
        for (u in size.units()) {
            items.add(u.name)
        }
        this.value = value.name
    }
}
