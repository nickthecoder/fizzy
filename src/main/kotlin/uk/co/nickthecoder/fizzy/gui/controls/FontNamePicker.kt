/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.controls

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.Menu
import javafx.scene.control.MenuButton
import javafx.scene.control.MenuItem
import javafx.scene.text.Font
import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.model.ShapeText
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions
import uk.co.nickthecoder.fizzy.util.toFormula
import java.util.regex.Pattern

val fontFamilyBlacklist = MutableFList<String>().apply { addAll("Noto .*", "cmex10", "cmsy10", "esint10", "msam10", "stmary10", "wasy10") }

class FontNamePicker(val mainWindow: MainWindow)
    : BuildableNode {

    val button = MenuButton()

    private val blacklist = fontFamilyBlacklist.map { Pattern.compile(it) }

    private fun accept(family: String): Boolean {
        blacklist.forEach {
            if (it.matcher(family).matches()) {
                return false
            }
        }
        return true
    }


    override fun build(): Node {

        Font.getFamilies().filter { accept(it) }.forEach { fontFamily ->
            val names = Font.getFontNames(fontFamily).filter { accept(it) }

            if (names.size != 0) {
                if (names.size == 1) {

                    val menuItem = MenuItem(names.first())
                    menuItem.graphic = graphic(names.first())
                    menuItem.onAction = EventHandler {
                        pick(fontFamily, names.first())
                    }
                    button.items.add(menuItem)

                } else {
                    val sub = Menu(fontFamily)
                    sub.graphic = graphic(names.first())
                    button.items.add(sub)
                    names.filter { accept(it) }.forEach { fontName ->
                        val menuItem = MenuItem(fontName)
                        menuItem.graphic = graphic(fontName)
                        menuItem.onAction = EventHandler {
                            pick(fontFamily, fontName)
                        }
                        sub.items.add(menuItem)
                    }
                }
            }
        }
        mainWindow.shapeSelectionProperty.addListener { _, _, _ -> onShapeSelectionChanged() }

        button.graphic = graphic("Courier New")

        return button
    }


    fun pick(fontFamily: String, fontName: String) {
        val variant = if (fontName.startsWith(fontFamily)) {
            fontName.substring(fontFamily.length).trim()
        } else {
            "Regular"
        }

        val history = mainWindow.document?.history ?: return

        history.beginBatch()
        mainWindow.controller?.selection?.forEach { shape ->
            if (shape is ShapeText) {
                history.makeChange(ChangeExpressions(
                        listOf(shape.fontFamily to fontFamily.toFormula(),
                                shape.fontVariant to variant.toFormula())
                ))
            }
        }
        history.endBatch()
        button.graphic = graphic(fontName)
    }

    private fun onShapeSelectionChanged() {
        val shape = mainWindow.shapeSelectionProperty.value
        if (shape is ShapeText) {
            button.graphic = graphic(shape.fontFamily.value)
        }
    }


    fun graphic(fontName: String): Label {
        val label = Label("Aa")
        label.font = Font(fontName, 12.0)
        return label
    }
}
