/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.model.CustomProperty
import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.PageSize

class DocumentSheetView(val mainWindow: MainWindow, val document: Document)
    : AbstractSheetView() {

    private val customPropertiesListener = ListListenerLambda<CustomProperty> { _, _, _, _ ->
        rebuildSection("CustomProperty")
    }

    init {
        document.customProperties.listeners.add(customPropertiesListener)
    }

    override fun document() = document

    override fun metaData() = document.metaData()

    override fun pageSize() = PageSize()

    override fun title() = "Document ${document.name}"

    override fun topLevelSectionName(): String? = null

}
