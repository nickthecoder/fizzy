/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.PageSize
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula


abstract class DimensionEditor(val pageSize: PageSize, private val prop: Prop<Dimension>)
    : HBox(), PropListener {

    private val textField = TextField(prop.value.inUnits(prop.value.units).toFormula())

    private val unitsComboBox = UnitsComboBox(pageSize, prop.value.units)

    init {
        children.addAll(textField, unitsComboBox)

        prop.addListener(this)
        styleClass.addAll("hbox")
        textField.prefColumnCount = doubleColumnCount

        textField.textProperty().addListener { _, _, _ -> checkAndUpdate() }
        unitsComboBox.valueProperty().addListener { _, _, _ -> checkAndUpdate() }
    }

    override fun dirty(prop: Prop<*>) {
        textField.text = this.prop.value.inUnits(this.prop.value.units).toFormula()
        unitsComboBox.value = this.prop.value.units.name
    }

    private fun checkAndUpdate() {
        textField.styleClass.remove("error")
        try {
            val newVal = Dimension(textField.text.toDouble(), pageSize.getUnits(unitsComboBox.value))
            if (newVal != prop.value) {
                updateProp(newVal)
            }
        } catch (e: Exception) {
            textField.styleClass.add("error")
        }
    }

    protected abstract fun updateProp(value: Dimension)
}

class DimensionVariableEditor(pageSize: PageSize, private val variable: PropVariable<Dimension>)
    : DimensionEditor(pageSize, variable) {

    init {
        styleClass.addAll("variable")
    }

    override fun updateProp(value: Dimension) {
        variable.value = value
    }
}

class DimensionExpressionValueEditor(pageSize: PageSize, private val expression: PropExpression<Dimension>)
    : DimensionEditor(pageSize, expression) {

    init {
        styleClass.addAll("value")
    }

    override fun updateProp(value: Dimension) {
        expression.formula = value.toFormula()
    }
}
