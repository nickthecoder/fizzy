/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula

abstract class DoubleEditor(private val prop: Prop<Double>)
    : HBox(), PropListener {

    private val textField = TextField(prop.value.toFormula())

    init {
        children.add(textField)
        textField.prefColumnCount = doubleColumnCount

        prop.addListener(this)
        textField.textProperty().addListener { _, _, _ -> checkAndUpdate() }
    }

    override fun dirty(prop: Prop<*>) {
        textField.text = this.prop.value.toFormula()
    }

    private fun checkAndUpdate() {
        styleClass.remove("error")
        try {
            val value = textField.text.toDouble()
            if (prop.value != value) {
                updateProp(value)
            }
        } catch (e: Exception) {
            styleClass.add("error")
        }
    }

    protected abstract fun updateProp(value: Double)
}


class DoubleVariableEditor(private val variable: PropVariable<Double>)
    : DoubleEditor(variable) {

    init {
        styleClass.add("variable")
    }

    override fun updateProp(value: Double) {
        variable.value = value
    }
}


class DoubleExpressionValueEditor(private val expression: PropExpression<Double>)
    : DoubleEditor(expression) {

    init {
        styleClass.add("value")
    }

    override fun updateProp(value: Double) {
        expression.formula = value.toFormula()
    }
}
