/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.scene.Node
import javafx.scene.canvas.Canvas
import uk.co.nickthecoder.fizzy.gui.controls.BuildableNode
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.util.runLater

abstract class AbstractCanvas(protected val drawingArea: DrawingArea)
    : BuildableNode {

    val canvas = Canvas(100.0, 100.0)

    protected val dc = CanvasContext(canvas).rounded()


    var dirty: Boolean = false
        set(v) {
            if (field != v) {
                field = v
                if (v) {
                    runLater("PageOutline Dirty") { draw() }
                }
            }
        }

    protected val dirtyListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            dirty = true
        }
    }

    init {
        drawingArea.controller.dirty.addListener(dirtyListener)
    }

    override fun build(): Node {
        runLater("AbstractCanvas initial draw") {
            draw()
        }
        return canvas
    }

    abstract fun draw()

}
