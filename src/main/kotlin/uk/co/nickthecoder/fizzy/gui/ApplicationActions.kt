/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.scene.input.KeyCode

object ApplicationActions {

    val DOCUMENT_NEW = ApplicationAction("document-new", "Create a New Document", KeyCode.N, control = true)
    val DOCUMENT_OPEN = ApplicationAction("document-open", "Open a Document", KeyCode.O, control = true)
    val DOCUMENT_SAVE = ApplicationAction("document-save", "Save", KeyCode.S, control = true)
    val DOCUMENT_SAVE_AS = ApplicationAction("document-save-as", "Save As", KeyCode.S, control = true, shift = true)
    val EXPORT_PNG = ApplicationAction("export-png", "Export as PNG", KeyCode.E, control = true)
    val EXPORT_SVG = ApplicationAction("export-svg", "Export as SVG", KeyCode.E, control = true, shift = true)

    val EDIT_COPY = ApplicationAction("edit-copy", "Copy", KeyCode.C, control = true)
    val EDIT_CUT = ApplicationAction("edit-cut", "Cut", KeyCode.X, control = true)
    val EDIT_PASTE = ApplicationAction("edit-paste", "Paste", KeyCode.V, control = true)
    val EDIT_DUPLICATE = ApplicationAction("edit-duplicate", "Duplicate", KeyCode.D, control = true)

    val EDIT_UNDO = ApplicationAction("edit-undo", "Undo", KeyCode.Z, control = true)
    val EDIT_REDO = ApplicationAction("edit-redo", "Redo", KeyCode.Z, control = true, shift = true)
    val EDIT_REDO2 = ApplicationAction("edit-redo", "Redo", KeyCode.Y, control = true)
    val EDIT_DELETE = ApplicationAction("edit-delete", "Delete", KeyCode.DELETE)

    val EDIT_FLIP_X = ApplicationAction("edit-flip-x", "Flip Horizontally", KeyCode.H, control = true)
    val EDIT_FLIP_Y = ApplicationAction("edit-flip-y", "Flip Vertically", KeyCode.J, control = true)

    val EDIT_ZORDER_UP = ApplicationAction("edit-zorder-up", "Move Up through Z-Order", KeyCode.PAGE_UP, control = true, shift = true)
    val EDIT_ZORDER_DOWN = ApplicationAction("edit-zorder-down", "Move Up through Z-Order", KeyCode.PAGE_DOWN, control = true, shift = true)
    val EDIT_ZORDER_TOP = ApplicationAction("edit-zorder-top", "Move Up through Z-Order", KeyCode.HOME, control = true, shift = true)
    val EDIT_ZORDER_BOTTOM = ApplicationAction("edit-zorder-bottom", "Move Up through Z-Order", KeyCode.END, control = true, shift = true)

    val TOOL_SELECT = ApplicationAction("tool-select", "Select", KeyCode.F1)
    val TOOL_PRIMITIVE1D = ApplicationAction("tool-primitive1d", "Create Lines", KeyCode.F2)
    val TOOL_PRIMITIVE2D = ApplicationAction("tool-primitive2d", "Create Basic Shapes", KeyCode.F3)
    val TOOL_DELETE = ApplicationAction("tool-delete", "Delete", KeyCode.F4)
    val TOOL_EDIT_GEOMETRY = ApplicationAction("tool-edit-geometry", "Edit Geometry", KeyCode.F5)
    val TOOL_EDIT_TEXT = ApplicationAction("tool-edit-text", "Edit Text", KeyCode.F8)

    val APPLY_THEME = ApplicationAction("apply-theme", "Apply Theme", KeyCode.T, control = true)

    val DEV_DEBUG = ApplicationAction("dev-debug", "Debug", KeyCode.F12, control = true)

    val CLOSE_TAB = ApplicationAction("close-tab", "Close Tab", KeyCode.W, control = true)

    val ZOOM_IN = ApplicationAction("zoom-in", "Zoom In", KeyCode.PLUS, control = true)
    val ZOOM_IN2 = ApplicationAction("zoom-in", "Zoom In", KeyCode.EQUALS, control = true)
    val ZOOM_OUT = ApplicationAction("zoom-out", "Zoom Out", KeyCode.MINUS, control = true)
    val ZOOM_RESET = ApplicationAction("zoom-reset", "Reset Zoom", KeyCode.DIGIT0, control = true)

    val WINDOW_STENCILS = ApplicationAction("window-stencils", "Stencils", KeyCode.DIGIT1, alt = true)
    val WINDOW_SHAPE_PROPERTIES = ApplicationAction("window-shape-properties", "Shape Properties", KeyCode.DIGIT2, alt = true)
    val WINDOW_DOCUMENT_PROPERTIES = ApplicationAction("window-document-properties", "Document Properties", KeyCode.DIGIT3, alt = true)
    val WINDOW_SHAPE_TREE = ApplicationAction("window-shape-tree", "Shape Tree", KeyCode.DIGIT4, alt = true)
    val WINDOW_HELP_REFERENCE = ApplicationAction("window-help-reference", "Help Reference", KeyCode.F1, control = true)
}
