/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.scene.control.Tab
import javafx.scene.web.WebView
import uk.co.nickthecoder.fizzy.FizzyApp

class WebViewTab(url: String)
    : Tab() {

    private val webView = WebView()

    init {

        webView.engine.load(url)
        content = webView
        graphic = FizzyApp.graphic("icons/window-help-reference.png")

    }

}
