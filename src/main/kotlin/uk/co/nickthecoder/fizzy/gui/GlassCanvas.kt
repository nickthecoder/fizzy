/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.event.EventHandler
import javafx.scene.Cursor
import javafx.scene.canvas.Canvas
import javafx.scene.control.CheckMenuItem
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuItem
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.controller.CKeyEvent
import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.ShapeHighlight
import uk.co.nickthecoder.fizzy.controller.handle.Handle
import uk.co.nickthecoder.fizzy.controller.tools.DragHandleTool
import uk.co.nickthecoder.fizzy.controller.tools.ToolCursor
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType

/**
 * A Canvas above the drawing, where things such as bounding boxes and control handles are drawn.
 * By drawing these onto a different [Canvas], the [Canvas] displaying the the document does not need
 * to be redrawn when the selection changes, or while dragging a bounding box.
 */
class GlassCanvas(val page: Page, drawingArea: DrawingArea)
    : AbstractCanvas(drawingArea) {

    /**
     * Listen to the currently selected shapes. When they change, we need to draw their handles and bounding box etc.
     */
    val shapeListener = object : ChangeListener<Shape> {
        override fun changed(item: Shape, changeType: ChangeType, obj: Any?) {
            dirty = true
        }
    }

    /**
     * When the selection changes, we need to draw, to update the bounding boxes and handles on display.
     */
    private val selectionListener = ListListenerLambda<Shape> { _, item, _, added ->
        if (added) item.changeListeners.add(shapeListener) else item.changeListeners.remove(shapeListener)
        dirty = true
    }

    private var previousPoint = Dimension2.ZERO_mm

    private var panning = false

    private var prePanCursor = Cursor.DEFAULT

    /**
     * Use to ignore the spurious onMouseClicked event at the end of a drag.
     * It seems that PRESS, MOVE RELEASE also produces an onMouseClicked event at the end.
     * IMHO, this is NOT a click, and so I want to ignore it.
     * Set inside [onDragDetected], reset inside [onMousePressed] and [onMouseClicked].
     */
    private var dragging = false

    private val cursorListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            canvas.cursor = when (drawingArea.controller.cursorProp.value) {
                ToolCursor.DEFAULT -> Cursor.DEFAULT
                ToolCursor.STAMP -> Cursor.HAND // Replace this with a better one!
                ToolCursor.DELETE -> Cursor.OPEN_HAND // Replace this with a better one!
                ToolCursor.GROW -> Cursor.CROSSHAIR
                ToolCursor.MOVE -> Cursor.CLOSED_HAND
            }
        }
    }

    init {
        drawingArea.controller.cursorProp.addListener(cursorListener)

        drawingArea.controller.showConnectionPoints.addListener(dirtyListener)
        drawingArea.controller.highlightShape.addListener(dirtyListener)

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED) { onMousePressed(it) }
        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED) { onMouseClicked(it) }
        canvas.addEventHandler(MouseEvent.DRAG_DETECTED) { onDragDetected(it) }
        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED) { onMouseDragged(it) }
        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED) { onMouseReleased(it) }
        canvas.addEventHandler(MouseEvent.MOUSE_MOVED) { onMouseMoved(it) }
        canvas.addEventHandler(KeyEvent.KEY_TYPED) { onKeyTyped(it) }
        canvas.addEventHandler(KeyEvent.KEY_PRESSED) { onKeyPressed(it) }

        drawingArea.controller.selection.listeners.add(selectionListener)
    }

    private fun toPage(event: MouseEvent) = drawingArea.toPage(event)

    private fun convertEvent(event: MouseEvent) = CMouseEvent(
            toPage(event),
            event.button.ordinal,
            event.isShiftDown,
            event.isControlDown,
            drawingArea.controller.scale)

    private fun convertEvent(event: KeyEvent) = CKeyEvent(event.code.name, event.character)

    private fun onKeyTyped(event: KeyEvent) {
        if (event.character.none { it.isISOControl() }) {
            val fEvent = convertEvent(event)
            drawingArea.controller.onKeyTyped(fEvent)
            if (fEvent.isConsumed()) {
                event.consume()
            }
        }
    }

    private fun onKeyPressed(event: KeyEvent) {
        val fEvent = convertEvent(event)
        drawingArea.controller.onKeyPressed(fEvent)
        if (fEvent.isConsumed()) {
            event.consume()
        }
    }

    fun onMousePressed(event: MouseEvent) {
        canvas.requestFocus()
        dragging = false

        if (event.button == MouseButton.SECONDARY || event.isMiddleButtonDown || event.isMetaDown || event.isAltDown) {
            panning = true
            prePanCursor = canvas.cursor
            canvas.cursor = Cursor.CLOSED_HAND
            previousPoint = toPage(event)
        } else if (event.clickCount == 2) {

            drawingArea.zoomByPixel(if (convertEvent(event).isAdjust) 1.0 / 1.4 else 1.4, event.x, event.y)

        } else {
            drawingArea.controller.onMousePressed(convertEvent(event))
        }

        if (event.isPopupTrigger) {
            popup(event)
        }

        event.consume()
    }

    fun onMouseReleased(event: MouseEvent) {
        if (panning) {
            canvas.cursor = prePanCursor
            panning = false
        } else {
            drawingArea.controller.onMouseReleased(convertEvent(event))
        }
        if (event.isPopupTrigger) {
            popup(event)
        }
        event.consume()
    }

    private fun popup(event: MouseEvent) {
        val items = drawingArea.controller.tool.onContextMenu(convertEvent(event))
        if (items.isNotEmpty()) {
            val menu = ContextMenu()
            items.forEach { item ->
                val toggle = item.toggle
                if (toggle == null) {
                    val menuItem = MenuItem(item.label)
                    menuItem.onAction = EventHandler { item.action() }
                    menu.items.add(menuItem)
                } else {
                    val menuItem = CheckMenuItem(item.label)
                    menuItem.isSelected = toggle.value
                    menuItem.onAction = EventHandler { item.action() }
                    menu.items.add(menuItem)
                }
            }
            menu.show(canvas.scene.window, event.screenX, event.screenY)
        }
    }

    private fun onMouseMoved(event: MouseEvent) {
        if (!panning) {
            drawingArea.controller.onMouseMoved(convertEvent(event))
        }
        event.consume()
    }

    private fun onDragDetected(event: MouseEvent) {
        dragging = true

        if (panning) {
            event.consume()
        } else {
            drawingArea.controller.onDragDetected(convertEvent(event))
        }
        event.consume()
    }

    fun onMouseDragged(event: MouseEvent) {
        if (panning) {
            val newPoint = toPage(event)
            drawingArea.panBy(newPoint - previousPoint)
            previousPoint = toPage(event)
        } else {
            drawingArea.controller.onMouseDragged(convertEvent(event))
        }
        event.consume()
    }

    private fun onMouseClicked(event: MouseEvent) {
        // Ignore the spurious onMouseClicked event that JavaFX spits out at the end of a drag. Grr.
        if (dragging) {
            event.consume()
            dragging = false
            return
        }

        if (panning) {
            panning = false
        } else {
            drawingArea.controller.onMouseClicked(convertEvent(event))
        }
        event.consume()
    }

    override fun draw() {
        dc.clear()
        dc.use {
            dc.scale(drawingArea.controller.scale)
            dc.translate(drawingArea.pan)

            val parent = drawingArea.controller.parent
            if (parent is Shape) {
                highlightShape(ShapeHighlight.create(parent, "PARENT"))
            }

            drawingArea.controller.selection.forEach { shape ->
                drawBoundingBox(shape)
            }
            drawingArea.controller.handles.forEach { handle ->
                if (handle.isVisible()) {
                    val tool = drawingArea.controller.tool
                    val draggingHandle: Handle? = if (tool is DragHandleTool) tool.handle else null
                    dc.use {
                        dc.translate(handle.position())
                        beginHandle(handle)
                        handle.draw(dc, draggingHandle === handle)
                    }
                }
            }
            drawingArea.controller.tool.draw(dc)


            if (drawingArea.controller.showConnectionPoints.value) {
                val singleShape = drawingArea.controller.singleShape
                if (singleShape == null) {
                    drawingArea.controller.parent.children.forEach { shape ->
                        shape.connectionPoints.forEach { drawConnectionPoint(it) }
                    }
                } else {
                    singleShape.connectionPoints.forEach { drawConnectionPoint(it) }
                }
            }

            if (drawingArea.controller.highlightShape.value !== ShapeHighlight.NO_SHAPE_HIGHLIGHT) {
                highlightShape(drawingArea.controller.highlightShape.value)
            }
        }

        dirty = false
    }

    private fun highlightShape(highlight: ShapeHighlight) {
        val shape = highlight.shape

        if (shape is ShapeWithGeometry) {
            dc.use {
                dc.lineWidth(4.0 / drawingArea.controller.scale)
                dc.lineColor(highlight.lineColor)
                dc.fillColor(highlight.fillColor)

                fun transform(s: Shape) {
                    val parent = s.parent
                    if (parent is Shape) {
                        transform(parent)
                    }
                    shape.transform.apply(dc)
                }
                transform(shape)

                dc.beginPath()
                shape.geometry.parts.filter { it.visible.value }.forEach { it.draw(dc) }
                dc.endPath(highlight.lineColor.isVisible(), highlight.fillColor.isVisible())
            }
        }
    }


    private fun drawConnectionPoint(connectionPoint: ConnectionPoint) {
        val shape = connectionPoint.shape ?: return
        val pagePoint = shape.localToPage(connectionPoint.point.value)
        val pageAngle = shape.localToPage(connectionPoint.angle.value)

        dc.use {
            dc.translate(pagePoint)
            dc.scale(4.0 / drawingArea.controller.scale)
            dc.lineColor(Controller.BLUE_BASE)
            dc.lineWidth(0.5)

            dc.beginPath()
            dc.moveTo(Controller.SQUARE[0])
            dc.lineTo(Controller.SQUARE[2])
            dc.moveTo(Controller.SQUARE[1])
            dc.lineTo(Controller.SQUARE[3])
            dc.moveTo(Dimension2.ZERO_mm)
            dc.lineTo(pageAngle.unitDimension() * 4.0)
            dc.endPath(stroke = true, fill = false)
        }
    }

    private fun drawBoundingBox(shape: Shape) {
        dc.use {
            beginSelection()
            if (shape is Shape1d) {
                dc.beginPath()
                dc.moveTo(shape.parent.localToPage(shape.start.value))
                dc.lineTo(shape.parent.localToPage(shape.end.value))
                dc.endPath(true, false)
            } else {
                val corners = drawingArea.controller.shapeCorners(shape)
                dc.polygon(true, false, *corners)
                if (shape is Shape2d) {
                    val r1 = (corners[0] + corners[1]) / 2.0
                    val r2 = r1 + (corners[1] - corners[2]).normalise() * Dimension(Controller.ROTATE_DISTANCE / drawingArea.controller.scale, Units.defaultUnits)
                    dc.beginPath()
                    dc.moveTo(r1)
                    dc.lineTo(r2)
                    dc.endPath(true, false)
                }
            }
        }
    }

    private fun beginSelection() {
        dc.lineColor(Controller.BLUE_BASE)
        dc.lineWidth(2.0 / drawingArea.controller.scale)
        dc.lineDashes(5.0 / drawingArea.controller.scale)
    }

    private fun beginHandle(handle: Handle) {
        dc.lineColor(handle.strokeColor())
        dc.fillColor(handle.fillColor())
        dc.scale(4.0 / drawingArea.controller.scale)
        dc.lineWidth(0.25)
    }


    companion object {

    }
}
