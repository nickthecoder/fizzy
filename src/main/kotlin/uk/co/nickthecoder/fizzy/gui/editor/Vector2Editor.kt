/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import uk.co.nickthecoder.fizzy.model.Vector2
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula


abstract class Vector2Editor(private val prop: Prop<Vector2>)
    : HBox(), PropListener {

    private val xTextField = TextField(prop.value.x.toFormula())
    private val yTextField = TextField(prop.value.y.toFormula())

    init {
        styleClass.addAll("hbox")
        children.addAll(xTextField, Label(","), yTextField)
        yTextField.prefColumnCount = doubleColumnCount
        xTextField.prefColumnCount = doubleColumnCount

        prop.addListener(this)
        xTextField.textProperty().addListener { _, _, _ -> checkAndUpdate() }
        yTextField.textProperty().addListener { _, _, _ -> checkAndUpdate() }
    }

    override fun dirty(prop: Prop<*>) {
        xTextField.text = this.prop.value.x.toFormula()
        yTextField.text = this.prop.value.y.toFormula()
    }

    private fun checkAndUpdate() {
        xTextField.styleClass.remove("error")
        yTextField.styleClass.remove("error")

        try {
            xTextField.text.toDouble()
        } catch (e: Exception) {
            xTextField.styleClass.add("error")
        }
        try {
            yTextField.text.toDouble()
        } catch (e: Exception) {
            yTextField.styleClass.add("error")
        }

        try {
            val x = xTextField.text.toDouble()
            val y = yTextField.text.toDouble()
            if (prop.value.x != x || prop.value.y != y) {
                updateProp(Vector2(x, y))
            }
        } catch (e: Exception) {
        }
    }

    protected abstract fun updateProp(value: Vector2)
}


class Vector2VariableEditor(private val variable: PropVariable<Vector2>)
    : Vector2Editor(variable) {

    init {
        styleClass.add("variable")
    }

    override fun updateProp(value: Vector2) {
        variable.value = value
    }
}

class Vector2ExpressionValueEditor(private val expression: PropExpression<Vector2>)
    : Vector2Editor(expression) {

    init {
        styleClass.add("value")
    }

    override fun updateProp(value: Vector2) {
        expression.formula = value.toFormula()
    }
}
