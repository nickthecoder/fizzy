/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import uk.co.nickthecoder.fizzy.gui.controls.BuildableNode
import uk.co.nickthecoder.fizzy.gui.editor.ExpressionEditor
import uk.co.nickthecoder.fizzy.gui.editor.ExpressionField
import uk.co.nickthecoder.fizzy.gui.editor.PropertyLabel
import uk.co.nickthecoder.fizzy.gui.editor.createVariableEditor
import uk.co.nickthecoder.fizzy.model.HasDocument
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.model.MetaDataCell
import uk.co.nickthecoder.fizzy.model.PageSize
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropVariable

abstract class AbstractSheetView : HasDocument, BuildableNode {

    protected val vBox = VBox()

    protected lateinit var expressionEditor: ExpressionEditor

    protected val scroll = ScrollPane(vBox)

    protected val borderPane = BorderPane()

    protected val sectionNameToTitledPane = mutableMapOf<String, TitledPane>()


    override fun build(): Node {

        expressionEditor = ExpressionEditor(document().history)

        val top = VBox()
        borderPane.center = scroll
        val title = Label(title())
        title.styleClass.add("heading")
        top.children.addAll(title, expressionEditor.build())
        borderPane.top = top

        borderPane.styleClass.add("shape-sheet")

        val metaData = metaData()
        topLevelSectionName()?.let { sectionTitle ->
            buildSection(sectionTitle, metaData)
        }
        metaData.sections.forEach { name, section ->
            buildSection(name, section)
        }

        return borderPane
    }

    protected abstract fun title(): String

    protected abstract fun topLevelSectionName(): String?

    protected abstract fun pageSize(): PageSize

    protected abstract fun metaData(): MetaData

    protected fun buildSection(titledPane: TitledPane, metaData: MetaData) {

        val namedCellsAndRows = VBox()
        namedCellsAndRows.styleClass.addAll("namedCellsAndRows")

        if (metaData.cells.isNotEmpty()) {
            val namedCells = GridPane()
            namedCells.styleClass.add("named-cells")
            var index = 0
            metaData.cells.forEach { cell ->
                val control = createControl(cell)
                val label = Label(cell.cellName)
                namedCells.addRow(index, label, control)
                index++
            }
            namedCellsAndRows.children.add(namedCells)
        }

        if (metaData.rows.isNotEmpty()) {
            val rowCells = GridPane()
            rowCells.styleClass.add("row-cells")
            val columnIndices = mutableMapOf<String, Int>()
            val columnTitles = mutableMapOf<String, MutableSet<String>>()

            metaData.rows.forEachIndexed { rowIndex, row ->
                val rowControls = mutableMapOf<Int, Node>()

                val type = row.type
                if (type != null) {
                    if (!columnIndices.containsKey("type")) {
                        columnIndices["type"] = columnIndices.size
                    }
                    rowControls[columnIndices["type"]!!] = Label(row.label ?: type)
                }


                row.cells.filter { it.display }.forEach { cell ->
                    val columnName = cell.columnKey ?: cell.cellName
                    if (columnTitles[columnName] == null) {
                        columnTitles[columnName] = mutableSetOf<String>()
                    }
                    columnTitles[columnName]!!.add(cell.cellName)

                    if (!columnIndices.containsKey(columnName)) {
                        columnIndices[columnName] = columnIndices.size
                    }
                    val control = createControl(cell)
                    rowControls[columnIndices[columnName]!!] = control
                }
                val controls = Array<Node>(columnIndices.size + 1) { Label() }
                val indexMenu = MenuButton("${rowIndex + 1}")
                if (metaData.rowFactories.size == 1) {
                    val insertMI = MenuItem("Insert ${metaData.rowFactories[0].label}")
                    insertMI.onAction = EventHandler {
                        metaData.rowFactories[0].create(rowIndex)
                    }
                    indexMenu.items.add(insertMI)
                } else if (metaData.rowFactories.isNotEmpty()) {
                    metaData.rowFactories.forEach { factory ->
                        val insertMI = MenuItem("Insert ${factory.label}")
                        insertMI.onAction = EventHandler {
                            factory.create(rowIndex)
                        }
                        indexMenu.items.add(insertMI)
                    }
                }
                val deleteMI = MenuItem("Delete Row")
                deleteMI.onAction = EventHandler {
                    metaData.rowRemoval?.invoke(rowIndex)
                }
                indexMenu.items.add(deleteMI)


                controls[0] = indexMenu
                rowControls.forEach { i, control -> controls[i + 1] = control }
                rowCells.addRow(rowIndex + 1, * controls)
            }
            val headers = Array<Node>(columnIndices.size + 1) { Label() }
            columnIndices.forEach { columnName, columnIndex ->
                val title = columnTitles[columnName] ?: emptySet<String>()
                val label = Label(title.joinToString(separator = " / "))
                label.styleClass.add("header")
                headers[columnIndex + 1] = label
            }
            rowCells.addRow(0, * headers)

            namedCellsAndRows.children.add(rowCells)
        }
        if (metaData.rowFactories.isNotEmpty()) {
            if (metaData.rowFactories.size == 1) {
                val button = Button(metaData.rowFactories[0].label)
                button.onAction = EventHandler {
                    metaData.rowFactories[0].create(metaData.rows.size)
                }
                namedCellsAndRows.children.add(button)
            } else {
                val button = MenuButton("New Row")
                metaData.rowFactories.forEach { factory ->
                    val menuItem = MenuItem(factory.label)
                    menuItem.onAction = EventHandler {
                        factory.create(metaData.rows.size)
                    }
                    button.items.add(menuItem)
                }
                namedCellsAndRows.children.add(button)
            }
        }

        titledPane.content = namedCellsAndRows
    }


    protected fun buildSection(name: String, metaData: MetaData) {

        val titledPane = TitledPane(name, Label())
        val expressionToggle = ToggleButton("=")
        expressionToggle.selectedProperty().addListener { _, _, newValue ->
            titledPane.styleClass.remove("hideExpressions")
            titledPane.styleClass.remove("hideValues")
            titledPane.styleClass.add(if (newValue) "hideValues" else "hideExpressions")
        }
        expressionToggle.isSelected = true

        titledPane.graphic = expressionToggle
        titledPane.styleClass.add("sheet-section")
        sectionNameToTitledPane[name] = titledPane

        buildSection(titledPane, metaData)
        vBox.children.add(titledPane)
    }

    fun rebuildSection(sectionName: String) {
        val metaData = metaData()

        metaData.sections.forEach { name, section ->
            if (name == sectionName) {
                sectionNameToTitledPane[name]?.let { titledPane ->
                    buildSection(titledPane, section)
                }
            }
        }
    }

    private fun createControl(cell: MetaDataCell): Node {
        val expressionControl = createEditControl(cell)
        expressionControl.styleClass.add("expression")

        val valueControl = createValueControl(cell)
        valueControl.styleClass.add("value")

        return StackPane(expressionControl, valueControl)
    }

    private fun createValueControl(cell: MetaDataCell): Node {
        return PropertyLabel(cell.cellProp, expressionEditor)
    }

    private fun createEditControl(cell: MetaDataCell): Node {
        val prop = cell.cellProp
        if (prop is PropExpression<*>) {
            return ExpressionField(prop, expressionEditor)
        } else if (prop is PropVariable<*>) {
            return createVariableEditor(pageSize(), prop)
        } else {
            return Label(cell.cellProp.valueString())
        }
    }
}