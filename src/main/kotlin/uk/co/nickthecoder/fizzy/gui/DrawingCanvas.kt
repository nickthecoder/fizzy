/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import uk.co.nickthecoder.fizzy.model.Page
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.view.PageView
import uk.co.nickthecoder.fizzy.view.ShapeView

/**
 *
 */
class DrawingCanvas(page: Page, singleShape: Shape?, drawingArea: DrawingArea)
    : AbstractCanvas(drawingArea), ChangeListener<Page> {

    private var pageView = if (singleShape == null) {
        PageView(page, dc)
    } else {
        ShapeView(singleShape, dc)
    }

    var page: Page = page
        set(v) {
            if (pageView is PageView) {
                field = v
                pageView = PageView(v, dc)
                pageView.draw()
            } else {
                throw IllegalStateException("Cannot change the page when viewing a single shape.")
            }
        }

    init {
        page.changeListeners.add(this)
    }

    override fun changed(item: Page, changeType: ChangeType, obj: Any?) {
        dirty = true
    }

    override fun draw() {
        pageView.dc.clear()
        pageView.dc.use {
            pageView.dc.scale(drawingArea.controller.scale)
            pageView.dc.translate(drawingArea.pan)
            pageView.draw()
            dirty = false
        }
    }
}
