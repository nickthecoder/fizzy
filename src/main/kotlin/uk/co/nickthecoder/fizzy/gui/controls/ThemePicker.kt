/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.controls

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.MenuButton
import javafx.scene.control.MenuItem
import javafx.scene.control.SeparatorMenuItem
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.model.DefaultTheme
import uk.co.nickthecoder.fizzy.model.Themes

class ThemePicker(val mainWindow: MainWindow)
    : BuildableNode {

    val button = MenuButton("Theme")


    override fun build(): Node {
        val defaultItem = MenuItem("Default Theme")
        defaultItem.onAction = EventHandler {
            mainWindow.page?.let { page ->
                page.theme.value = DefaultTheme(page)
            }
        }
        button.items.add(defaultItem)

        Themes.themesData.forEach { (name, _) ->
            val item = MenuItem(name)
            item.onAction = EventHandler {
                mainWindow.page?.let { page ->
                    Themes.useTheme(name, page)
                }
            }
            button.items.add(item)
        }
        val applyTheme = MenuItem("Apply Theme to Selected Shapes")
        applyTheme.onAction = EventHandler { mainWindow.applyTheme() }

        button.items.addAll(SeparatorMenuItem(), applyTheme)

        return button
    }

}
