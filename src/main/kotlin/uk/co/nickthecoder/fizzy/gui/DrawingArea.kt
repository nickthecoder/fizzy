/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.input.MouseEvent
import javafx.scene.layout.StackPane
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.gui.controls.BuildableNode
import uk.co.nickthecoder.fizzy.model.*

/**
 * Contains a GlassCanvas on top of a DrawingCanvas.
 * Later, this may also contain other GUI elements such as rulers.
 *
 */
class DrawingArea(mainWindow: MainWindow, val page: Page, private val singleShape: Shape? = null)

    : BuildableNode {

    val controller = Controller(page, singleShape, mainWindow)


    private val glassCanvas = GlassCanvas(page, this)

    private val drawingCanvas = DrawingCanvas(page, singleShape, this)

    private val pageOutlineCanvas = PageOutlineCanvas(page, this)

    private val canvases = listOf<AbstractCanvas>(pageOutlineCanvas, drawingCanvas, glassCanvas)

    var pan: Dimension2 = Dimension2.ZERO_mm

    private val stackPane = StackPane()

    init {
        stackPane.widthProperty().addListener { _, _, _ -> onResized() }
        stackPane.heightProperty().addListener { _, _, _ -> onResized() }
    }

    override fun build(): Node {
        assert(stackPane.children.size == 0)
        canvases.forEach {
            stackPane.children.add(it.build())
        }

        if (singleShape != null) {
            panBy(singleShape.size.value)
        }
        Platform.runLater {
            resetZoomAndPan()
        }
        return stackPane
    }

    private fun resetZoomAndPan() {
        pan = Dimension2.ZERO_mm
        controller.scale = Controller.DEFAULT_SCALE / page.pageSize.scale
    }

    private fun onResized() {
        canvases.forEach {
            it.canvas.width = stackPane.width
            it.canvas.height = stackPane.height
            it.draw()
        }
    }

    fun panBy(by: Dimension2) {
        pan += by
        controller.dirty.value++
        drawingCanvas.dirty = true
    }

    fun resetZoom() {
        adjustZoom(Controller.DEFAULT_SCALE / controller.scale)
    }

    fun adjustZoom(by: Double) {
        val selectedShape = controller.selection.firstOrNull()
        if (selectedShape == null) {
            // Zoom into the center of the visible area
            zoomByPixel(by, stackPane.width / 2.0, stackPane.height / 2.0)
        } else {
            // Zoom into the currently selected object
            val pagePoint = selectedShape.transform.fromLocalToPage.value * selectedShape.transform.center()
            zoomBy(by, pagePoint)
        }
    }

    fun zoomByPixel(by: Double, atPixelX: Double, atPixelY: Double) {
        val pagePoint = pixelsToPage(atPixelX, atPixelY)
        controller.scale *= by
        pan += pixelsToPage(atPixelX, atPixelY) - pagePoint

        controller.dirty.value++
        drawingCanvas.dirty = true
    }

    fun zoomBy(by: Double, at: Dimension2) {
        val (pixelX, pixelY) = pageToPixels(at)
        zoomByPixel(by, pixelX, pixelY)
    }

    fun pixelsToPage(x: Double, y: Double): Dimension2 = Dimension2(
            Dimension(x / controller.scale - pan.x.inDefaultUnits, Units.defaultUnits),
            Dimension(y / controller.scale - pan.y.inDefaultUnits, Units.defaultUnits))

    fun pageToPixels(point: Dimension2): Pair<Double, Double> = Pair((point.x + pan.x).inDefaultUnits * controller.scale, (point.y + pan.y).inDefaultUnits * controller.scale)

    fun toPage(event: MouseEvent) = pixelsToPage(event.x, event.y)

}
