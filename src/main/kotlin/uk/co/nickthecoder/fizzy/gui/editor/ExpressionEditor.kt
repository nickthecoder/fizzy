/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.TextField
import javafx.scene.control.Tooltip
import javafx.scene.input.KeyCode
import javafx.scene.layout.VBox
import uk.co.nickthecoder.fizzy.gui.controls.BuildableNode
import uk.co.nickthecoder.fizzy.model.history.ChangeExpression
import uk.co.nickthecoder.fizzy.model.history.History
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener

class ExpressionEditor(val history: History)
    : BuildableNode, PropListener {

    private var returnNode: TextField? = null

    val vBox = VBox()

    val textField = TextField()

    val errorMessage = TextField()

    var expression: PropExpression<*>? = null
        set(v) {
            field?.removeListener(this)
            field = v
            field?.addListener(this)
            textField.text = v?.formula ?: ""
            update()
        }

    init {
        textField.styleClass.add("expression")

        textField.textProperty().addListener { _, _, newFormula ->
            expression?.let { expression ->
                if (expression.formula != newFormula) {
                    expression.formula = newFormula
                    update()
                }
            }
        }

        textField.focusedProperty().addListener { _, _, hasFocus ->
            expression?.let { expression ->
                if (!hasFocus) {
                    if (expression.formula != textField.text) {
                        history.makeChange(ChangeExpression(expression, textField.text))
                        expression.value
                        update()
                    }
                }
            }
        }

        textField.onKeyPressed = EventHandler { event ->
            if (event.code == KeyCode.ENTER || event.code == KeyCode.TAB) {
                returnNode?.let {
                    it.requestFocus()
                    returnNode = null
                    event.consume()
                }
            }
        }
        update()
    }

    override fun build(): Node {
        errorMessage.styleClass.add("error")
        errorMessage.isEditable = false
        vBox.children.addAll(textField, errorMessage)
        return vBox
    }

    fun editExpression(expression: PropExpression<*>, textField: TextField) {
        Platform.runLater {
            val pos = if (textField.text == expression.formula) textField.caretPosition else 0
            returnNode = textField
            this.expression = expression
            this.textField.requestFocus()
            this.textField.positionCaret(pos)
        }
    }

    fun showExpression(expression: PropExpression<*>, textField: TextField) {
        returnNode = textField
        this.expression = expression
    }

    private fun update() {
        expression?.let { expression ->
            if (textField.text != expression.formula) {
                textField.text = expression.formula
            }
            textField.tooltip = Tooltip(expression.valueString())

            textField.styleClass.remove("error")
            textField.styleClass.remove("linked")

            if (expression.isLinked()) {
                textField.styleClass.add("linked")
            }
            if (expression.isError()) {
                textField.styleClass.add("error")
                errorMessage.text = expression.exception()?.toString()
            } else {
                errorMessage.text = ""
            }
        }

    }

    override fun dirty(prop: Prop<*>) {
        update()
    }
}
