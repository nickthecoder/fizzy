/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.dockable

import javafx.event.EventHandler
import javafx.scene.canvas.Canvas
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.scene.layout.FlowPane
import javafx.scene.layout.VBox
import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.controller.Clipboard
import uk.co.nickthecoder.fizzy.controller.tools.GrowShape1dTool
import uk.co.nickthecoder.fizzy.controller.tools.SelectTool
import uk.co.nickthecoder.fizzy.controller.tools.StampShape2dTool
import uk.co.nickthecoder.fizzy.gui.CanvasContext
import uk.co.nickthecoder.fizzy.gui.controls.BuildableNode
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.history.CreateShape
import uk.co.nickthecoder.fizzy.model.history.DeleteShape
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.view.ShapeView

class StencilView(val stencilsView: StencilsView, val stencil: Document, val localMasters: Boolean = false)
    : BuildableNode {

    private val titledPane = TitledPane()

    private val buttons = FlowPane()

    private val pageChangeListener = ListListenerLambda<Shape> { _, _, _, _ -> rebuild() }


    override fun build(): TitledPane {
        titledPane.isExpanded = false
        titledPane.content = buttons

        rebuild()

        titledPane.addEventHandler(MouseEvent.MOUSE_PRESSED) { checkMenu(it) }
        titledPane.addEventHandler(MouseEvent.MOUSE_RELEASED) { checkMenu(it) }

        findStencilPage().children.listeners.add(pageChangeListener)

        return titledPane
    }

    private fun findStencilPage(): Page = if (localMasters) stencil.localMasterShapes else stencil.pages.firstOrNull()
            ?: Page(stencil)

    fun rebuild() {
        buttons.children.forEach { button ->
            button as StencilButton
            button.destroy()
        }
        buttons.children.clear()
        if (localMasters) {
            titledPane.text = "Local"
        } else {
            titledPane.text = stencil.name.value
        }

        findStencilPage().children.filter { !it.getStencilShapeInfo().isHidden.value }.sortedBy { it.getStencilShapeInfo().order.value }.forEach { shape ->
            buttons.children.add(StencilButton(this, shape))
        }
    }

    private fun checkMenu(event: MouseEvent) {
        if (event.isPopupTrigger) {
            event.consume()
            val menu = ContextMenu()
            addMenuItems(menu)
            menu.show(titledPane.scene.window, event.screenX, event.screenY)
        }
    }

    internal fun addMenuItems(menu: ContextMenu) {

        if (!stencil.unchanged.value) {
            stencil.file?.let { file ->
                val saveItem = MenuItem("Save Stencil")
                saveItem.onAction = EventHandler { stencil.save(file) }
                menu.items.add(saveItem)
            }
        }
        val newShape1dMI = MenuItem("New 1D Shape")
        newShape1dMI.onAction = EventHandler { createNewShape1d() }

        val newShape2dMI = MenuItem("New 2D Shape")
        newShape2dMI.onAction = EventHandler { createNewShape2d() }

        val testPage = MenuItem("Test Page")
        testPage.onAction = EventHandler { editTestPage() }


        if (!Clipboard.isEmpty()) {
            menu.items.add(MenuItem("Paste").apply { onAction = EventHandler { paste() } })
        }

        menu.items.addAll(newShape1dMI, newShape2dMI, SeparatorMenuItem(), testPage)

    }


    private fun createNewShape1d() {
        val shape = PrimitiveShapes.createLine(stencil.pages[0], end = "(100mm, 0mm)")
        stencil.history.makeChange(CreateShape(shape, stencil.pages[0]))
    }

    private fun createNewShape2d() {
        val shape = PrimitiveShapes.createBox(stencil.pages[0])
        stencil.history.makeChange(CreateShape(shape, stencil.pages[0]))
    }

    private fun editTestPage() {
        val testPage = stencil.pages.elementAtOrNull(1) ?: Page(stencil)
        stencilsView.mainWindow.editPage(testPage)
    }

    private fun paste() {
        stencil.history.beginBatch()
        Clipboard.forEach { shape ->
            val copy = shape.copyInto(stencil.pages[0], false)
            stencil.history.makeChange(CreateShape(copy, stencil.pages[0]))
        }
        stencil.history.endBatch()
    }

}

class StencilButton(private val stencilView: StencilView, val shape: Shape)
    : VBox(), PropListener {

    private val mainWindow = stencilView.stencilsView.mainWindow
    private val button = ToggleButton()
    private val nameLabel = Label(shape.name.value)

    private val rebuildListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            stencilView.rebuild()
        }
    }

    init {
        styleClass.add("stencil")

        val info = shape.getStencilShapeInfo()
        info.order.addListener(rebuildListener)
        info.isHidden.addListener(rebuildListener)

        info.isLineMarking.addListener(this)
        shape.name.addListener(this)

        button.graphic = ShapeGraphic(shape, BUTTON_SIZE)
        button.toggleGroup = stencilView.stencilsView.toggleGroup

        button.onAction = EventHandler {
            mainWindow.controller?.let { controller ->
                if (button.isSelected) {
                    if (shape is Shape2d) {
                        controller.tool = StampShape2dTool(controller, shape) { button.isSelected = false }
                    } else if (shape is Shape1d) {
                        controller.tool = GrowShape1dTool(controller, shape)
                    }
                    // TODO Handle ShapeText too
                } else {
                    controller.tool = SelectTool(controller)
                }
            }
            button.parent.requestFocus()
        }

        button.addEventFilter(MouseEvent.MOUSE_PRESSED) { checkMenu(it) }
        button.addEventFilter(MouseEvent.MOUSE_RELEASED) { checkMenu(it) }

        nameLabel.maxWidth = MAX_WIDTH
        this.minWidth = MAX_WIDTH + 6.0
        this.maxWidth = MAX_WIDTH + 6.0

        children.addAll(button, nameLabel)
    }

    fun destroy() {
        val info = shape.getStencilShapeInfo()
        info.order.removeListener(rebuildListener)
        info.isHidden.removeListener(rebuildListener)
        info.isLineMarking.removeListener(this)
        shape.name.removeListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        nameLabel.text = shape.name.value
    }

    private fun checkMenu(event: MouseEvent) {
        if (event.isPopupTrigger) {
            event.consume()
            val menu = ContextMenu()

            val editMaster = MenuItem("Edit")
            editMaster.onAction = EventHandler { mainWindow.editMaster(shape) }

            val editShapeSheet = MenuItem("Edit Shape Sheet")
            editShapeSheet.onAction = EventHandler { mainWindow.editShapeSheet(shape) }

            val duplicateShape = MenuItem("Duplicate Shape")
            duplicateShape.onAction = EventHandler { shape.duplicate() }

            val deleteMaster = MenuItem("Delete")
            deleteMaster.onAction = EventHandler {
                val history = shape.document().history
                history.makeChange(DeleteShape(shape))
            }

            val copyMaster = MenuItem("Copy")
            copyMaster.onAction = EventHandler { Clipboard.copy(listOf(shape)) }

            menu.items.addAll(editMaster, editShapeSheet, duplicateShape, deleteMaster, SeparatorMenuItem(), copyMaster)
            stencilView.addMenuItems(menu)
            menu.show(button.scene.window, event.screenX, event.screenY)
        }
    }

    companion object {
        val MAX_WIDTH = 80.0
        val BUTTON_SIZE = 50.0
        val MARGIN = 3.0
    }
}

class ShapeGraphic(val shape: Shape, val size: Double, val margin: Double = StencilButton.MARGIN)
    : Canvas(size, size), ChangeListener<Shape> {

    init {
        redraw()
        shape.changeListeners.add(this)
    }

    override fun changed(item: Shape, changeType: ChangeType, obj: Any?) {
        redraw()
    }

    private fun redraw() {
        val dc = CanvasContext(this).rounded()
        dc.clear()

        dc.use {
            val width = shape.size.value.x.inDefaultUnits
            val height = shape.size.value.y.inDefaultUnits
            dc.translate(Dimension(size / 2.0, Units.defaultUnits), Dimension(size / 2.0, Units.defaultUnits))
            if (width > height) {
                dc.scale((size - margin * 2) / width)
            } else {
                dc.scale((size - margin * 2) / height)
            }
            val transform = shape.transform
            if (transform is Shape1dTransform) {
                dc.translate((transform.start.value - transform.end.value) / 2.0 - transform.start.value)
            } else if (transform is Shape2dTransform) {
                dc.translate(transform.locPin.value - transform.pin.value - shape.size.value * 0.5)
            }
            ShapeView(shape, dc).draw()
        }
    }
}
