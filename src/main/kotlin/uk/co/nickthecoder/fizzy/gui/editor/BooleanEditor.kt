/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.editor

import javafx.scene.control.CheckBox
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.toFormula

abstract class BooleanEditor(private val prop: Prop<Boolean>)
    : CheckBox(""), PropListener {

    init {
        isSelected = prop.value

        selectedProperty().addListener { _, _, _ -> checkAndUpdate() }
        prop.addListener(this)
    }

    override fun dirty(prop: Prop<*>) {
        isSelected = this.prop.value
    }

    private fun checkAndUpdate() {
        if (prop.value != isSelected) {
            updateProp(isSelected)
        }
    }

    protected abstract fun updateProp(value: Boolean)

}


class BooleanVariableEditor(private val variable: PropVariable<Boolean>)
    : BooleanEditor(variable) {

    init {
        styleClass.add("variable")
    }

    override fun updateProp(value: Boolean) {
        variable.value = value
    }
}


class BooleanExpressionValueEditor(private val expression: PropExpression<Boolean>)
    : BooleanEditor(expression) {

    init {
        styleClass.add("value")
    }

    override fun updateProp(value: Boolean) {
        expression.formula = value.toFormula()
    }
}
