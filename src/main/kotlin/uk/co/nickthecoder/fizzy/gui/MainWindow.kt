/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import javafx.beans.property.Property
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.embed.swing.SwingFXUtils
import javafx.scene.Scene
import javafx.scene.SnapshotParameters
import javafx.scene.canvas.Canvas
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.image.WritableImage
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import javafx.stage.FileChooser
import javafx.stage.Stage
import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.OtherActions
import uk.co.nickthecoder.fizzy.gui.dockable.DocumentProperties
import uk.co.nickthecoder.fizzy.gui.dockable.ShapeProperties
import uk.co.nickthecoder.fizzy.gui.dockable.ShapeTree
import uk.co.nickthecoder.fizzy.gui.dockable.StencilsView
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.util.FizzyJsonReader
import uk.co.nickthecoder.fizzy.util.toFormula
import uk.co.nickthecoder.fizzy.view.PageView
import uk.co.nickthecoder.fizzy.view.SVGContext
import uk.co.nickthecoder.harbourfx.Dockable
import uk.co.nickthecoder.harbourfx.DockableFactory
import uk.co.nickthecoder.harbourfx.Harbour
import javax.imageio.ImageIO

class MainWindow(val stage: Stage)
    : OtherActions, DockableFactory {

    private val vBox = BorderPane()

    internal val shortcutHelper = ShortcutHelper("MainWindow", vBox)

    private val tabs = TabPane()

    internal val harbour = Harbour(tabs, this)


    internal val toolBar = FToolBar(this)


    val documentProperty = SimpleObjectProperty<Document?>(null)

    val document: Document?
        get() = documentProperty.value


    val pageProperty = SimpleObjectProperty<Page?>(null)

    val page: Page?
        get() = pageProperty.value


    val tab: Tab?
        get() = tabs.selectionModel.selectedItem

    val documentTab: DocumentTab?
        get() {
            val tab = tabs.selectionModel.selectedItem
            if (tab is DocumentTab) {
                return tab
            }
            return null
        }


    val controller: Controller?
        get() {
            return documentTab?.drawingArea?.controller
        }


    val shapeSelectionProperty: Property<Shape?> = SimpleObjectProperty<Shape?>()

    private var selectionListener = ListListenerLambda<Shape> { _, _, _, _ ->
        onSelectionChanged()
    }

    private val stencils = StencilsView(this)

    /**
     * Custom colors are shared by all FColorPickers, so this is the place to store them all.
     */
    val customColors: ObservableList<Color> = FXCollections.observableArrayList<Color>()

    private var documentProperties = DocumentProperties(this)

    private var shapeProperties = ShapeProperties(this)

    private var shapeTree = ShapeTree(this)

    init {
        stage.title = "Fizzy OJ"

        stage.scene = Scene(vBox, 1200.0, 800.0)

        vBox.top = toolBar.build()
        vBox.center = harbour

        tabs.selectionModel.selectedItemProperty().addListener { _, oldTab, newTab -> onTabChanged(oldTab, newTab) }
    }

    private fun onTabChanged(oldTab: Tab?, newTab: Tab?) {

        if (oldTab is DocumentTab) {
            oldTab.drawingArea.controller.selection.listeners.remove(selectionListener)
            pageProperty.unbind()
        }

        if (newTab is DocumentTab) {
            newTab.drawingArea.controller.selection.listeners.add(selectionListener)
            pageProperty.bind(newTab.pageProperty)
        } else {
            pageProperty.value = null
        }

        if (newTab is HasDocument) {
            documentProperty.value = newTab.document()
        } else {
            documentProperty.value = null
        }

        controller?.selection?.let {
            onSelectionChanged()
        }

        val doc = document
        if (doc == null) {
            toolBar.undoButton.disableProperty().unbind()
            toolBar.redoButton.disableProperty().unbind()
        } else {
            toolBar.undoButton.disableProperty().bindNot(doc.history.canUndoProp)
            toolBar.redoButton.disableProperty().bindNot(doc.history.canRedoProp)
        }
    }

    private fun onSelectionChanged() {
        shapeSelectionProperty.value = controller?.selection?.lastOrNull()
    }

    fun editDocument(doc: Document) {
        doc.pages.firstOrNull()?.let { editPage(it) }
    }

    fun editPage(page: Page) {
        val doc = page.document
        val file = doc.file
        val tab = DocumentTab(this, doc, file?.nameWithoutExtension ?: "New Document", page = page)
        tabs.tabs.add(tab)
        tabs.selectionModel.select(tab)
    }

    /**
     * Changes all shapes (including descendants), so that lineWidth, colors etc all use the Page's theme.
     */
    fun applyTheme() {
        val changes = mutableListOf<Pair<PropExpression<*>, String>>()
        fun applyTheme(shape: Shape) {
            changes.add(Pair(shape.lineWidth, "Theme.lineWidth(Class)"))
            changes.add(Pair(shape.strokeColor, "Theme.strokeColor(Class)"))
            changes.add(Pair(shape.fillColor, "Theme.fillColor(Class)"))
            shape.children.forEach { applyTheme(it) }

            if (shape is ShapeWithGeometry) {
                changes.add(Pair(shape.strokeCap, "Theme.strokeCap(Class)"))
                changes.add(Pair(shape.strokeJoin, "Theme.strokeJoin(Class)"))
                changes.add(Pair(shape.cornerRadius, "Theme.cornerRadius(Class)"))
                changes.add(Pair(shape.dashes, "Theme.dashes(Class)"))
            }

            when (shape) {
                is Shape1d -> {
                    changes.add(Pair(shape.startMargin, "Theme.startMargin(Class)"))
                    changes.add(Pair(shape.endMargin, "Theme.endMargin(Class)"))

                    if (shape.themeClass.formula == "".toFormula()) {
                        changes.add(Pair(shape.themeClass, "line".toFormula()))
                    }
                }
                is Shape2d -> {
                    if (shape.themeClass.formula == "".toFormula()) {
                        changes.add(Pair(shape.themeClass, "box".toFormula()))
                    }
                }
                is ShapeText -> {
                    if (shape.themeClass.formula == "".toFormula()) {
                        changes.add(Pair(shape.themeClass, "text".toFormula()))
                    }
                }
            }
        }

        controller?.selection?.forEach { applyTheme(it) }
        val change = ChangeExpressions(changes)
        controller?.page?.document?.history?.beginBatch()
        controller?.page?.document?.history?.makeChange(change)
        controller?.page?.document?.history?.endBatch()
    }

    fun debug() {
        controller?.selection?.firstOrNull()?.let {
            val size = it.size.value
            println("Size = $size")
        }
    }

    fun editShape(shape: Shape) {
        val tab = DocumentTab(this, shape.document(), "${shape.name.value} #${shape.id}", shape.page(), shape)
        tabs.tabs.add(tab)
        tabs.selectionModel.select(tab)
    }

    fun editMaster(shape: Shape) {
        val tab = DocumentTab(this, shape.document(), "Master", shape.page(), shape)
        tabs.tabs.add(tab)
        tabs.selectionModel.select(tab)
    }

    override fun editShapeSheet(shape: Shape) {
        val tab = ShapeSheetTab(this, shape)
        tabs.tabs.add(tab)
        tabs.selectionModel.select(tab)
    }

    override fun editDocumentSheet(document: Document) {
        val tab = DocumentSheetTab(this, document)
        tabs.tabs.add(tab)
        tabs.selectionModel.select(tab)
    }


    fun toggleStencils() {
        harbour.toggle(stencils)
    }

    fun toggleDocumentProperties() {
        harbour.toggle(documentProperties)
    }

    fun toggleShapeProperties() {
        harbour.toggle(shapeProperties)
    }

    fun toggleShapeTree() {
        harbour.toggle(shapeTree)
    }

    fun helpReference() {
        val tab = WebViewTab("https://gitlab.com/nickthecoder/fizzy/wikis/ref/Index")
        tabs.tabs.add(tab)
        tabs.selectionModel.select(tab)
    }

    fun documentNew() {
        val doc = Document()
        Page(doc)
        editDocument(doc)
    }


    fun documentOpen() {
        val chooser = FileChooser()
        chooser.extensionFilters.addAll(fizzyExtensionFilter, stencilExtensionFilter, templateExtensionFilter, allFizzyExtensionFilter, allExtensionFilter)
        val file = chooser.showOpenDialog(stage)
        if (file != null) {
            editDocument(FizzyJsonReader(file).load())
        }
    }

    fun documentSave() {
        val doc = document ?: return
        val file = doc.file
        if (file == null) {
            documentSaveAs()
        } else {
            doc.save(file)
        }
    }

    fun documentSaveAs() {

        val doc = document ?: return
        val chooser = FileChooser()
        chooser.extensionFilters.addAll(fizzyExtensionFilter, stencilExtensionFilter, templateExtensionFilter, allFizzyExtensionFilter, allExtensionFilter)
        val file = chooser.showSaveDialog(stage)
        if (file != null) {
            doc.save(file)
        }
    }

    fun exportAsPNG() {
        val page = pageProperty.value ?: return
        val chooser = FileChooser()
        chooser.extensionFilters.addAll(pngExtensionFilter, allExtensionFilter)
        val file = chooser.showSaveDialog(stage)
        if (file != null) {
            val canvas = Canvas(600.0, 600.0)
            val dc = CanvasContext(canvas).rounded()
            val pv = PageView(page, dc)
            pv.draw()
            val writableImage = WritableImage(canvas.width.toInt(), canvas.height.toInt())
            val snapParams = SnapshotParameters().also { it.fill = Color.TRANSPARENT }
            canvas.snapshot(snapParams, writableImage)
            val renderedImage = SwingFXUtils.fromFXImage(writableImage, null)
            ImageIO.write(renderedImage, "png", file)
        }
    }

    fun exportAsSVG() {
        val page = pageProperty.value ?: return
        val chooser = FileChooser()
        chooser.extensionFilters.addAll(svgExtensionFilter, allExtensionFilter)
        val file = chooser.showSaveDialog(stage)
        if (file != null) {
            val dc = SVGContext()
            val pv = PageView(page, dc.rounded())
            dc.begin()
            pv.draw()
            file.writeText(dc.end())
        }
    }

    fun closeTab() {
        tab.let { tabs.tabs.remove(it) }
    }

    fun zoomIn() {
        val tab = documentTab ?: return
        tab.drawingArea.adjustZoom(2.0)
    }

    fun zoomOut() {
        val tab = documentTab ?: return
        tab.drawingArea.adjustZoom(0.5)
    }

    fun resetZoom() {
        val tab = documentTab ?: return
        tab.drawingArea.resetZoom()
    }

    fun defaultDockPositions() {
        harbour.clear()

        harbour.left.mainHalf.dockables.addAll(stencils, shapeTree)
        harbour.bottom.mainHalf.dockables.addAll(shapeProperties)
        harbour.bottom.otherHalf.dockables.addAll(documentProperties)
    }

    override fun createDockable(dockableId: String): Dockable? {
        return when (dockableId) {
            ApplicationActions.WINDOW_DOCUMENT_PROPERTIES.name -> documentProperties
            ApplicationActions.WINDOW_SHAPE_PROPERTIES.name -> shapeProperties
            ApplicationActions.WINDOW_SHAPE_TREE.name -> shapeTree
            ApplicationActions.WINDOW_STENCILS.name -> stencils
            else -> null
        }
    }

    companion object {
        val fizzyExtensionFilter = FileChooser.ExtensionFilter("Fizzy Document (*.fizzy)", "*.fizzy")
        val stencilExtensionFilter = FileChooser.ExtensionFilter("Fizzy Stencil (*.fizzys)", "*.fizzys")
        val templateExtensionFilter = FileChooser.ExtensionFilter("Fizzy Template (*.fizzyt)", "*.fizzyt")
        val themeExtensionFilter = FileChooser.ExtensionFilter("Fizzy Theme", "*.fizzytheme", "*.fizzytheme")
        val allFizzyExtensionFilter = FileChooser.ExtensionFilter("Fizzy Files", "*.fizzy", "*.fizzys", "*.fizzyt", "*.fizzytheme")

        val pngExtensionFilter = FileChooser.ExtensionFilter("PNG Image (*.png)", "*.png")
        val svgExtensionFilter = FileChooser.ExtensionFilter("SVG (*.svg)", "*.svg")
        val allExtensionFilter = FileChooser.ExtensionFilter("All Files", "*.*")
    }

}
