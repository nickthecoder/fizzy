/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui.dockable

import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.layout.GridPane
import uk.co.nickthecoder.fizzy.gui.ApplicationActions
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.gui.editor.createValueEditor
import uk.co.nickthecoder.fizzy.gui.editor.createVariableEditor
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.model.Shape1d
import uk.co.nickthecoder.fizzy.model.Shape2d
import uk.co.nickthecoder.fizzy.model.ShapeText

class ShapeProperties(val mainWindow: MainWindow)
    : FizzyTabbedDockable(ApplicationActions.WINDOW_SHAPE_PROPERTIES) {

    override val noTabContent = Label("No Shape Selected")

    private val shape: Shape?
        get() = mainWindow.shapeSelectionProperty.value

    private val customPropertiesTab = Tab("Custom Properties")
    private val sizeAndPositionTab = Tab("Size and Position")
    private val lockTab = Tab("Lock")


    init {
        mainWindow.shapeSelectionProperty.addListener { _, _, _ -> rebuild() }
        rebuild()
    }

    override fun rebuildTabs() {

        sizeAndPositionTab.content = createSizeAndPositionTab()
        customPropertiesTab.content = createCustomPropertiesTab()
        lockTab.content = createLockTab()

        for (tab in listOf(sizeAndPositionTab, customPropertiesTab, lockTab)) {
            if (tab.content != null) tabs.add(tab)
        }
    }

    private fun createSizeAndPositionTab(): Node? {

        val shape = shape ?: return null

        val sizeAndPositionGrid = GridPane()
        sizeAndPositionGrid.styleClass.add("properties-grid")

        when (shape) {
            is Shape2d -> {
                sizeAndPositionGrid.addRow(0, Label("Position"), createValueEditor(shape.page().pageSize, shape.transform.pin))
                sizeAndPositionGrid.addRow(1, Label("Size"), createValueEditor(shape.page().pageSize, shape.size))
            }
            is ShapeText -> {
                sizeAndPositionGrid.addRow(0, Label("Position"), createValueEditor(shape.page().pageSize, shape.transform.pin))
                sizeAndPositionGrid.addRow(1, Label("Font Size"), createValueEditor(shape.page().pageSize, shape.fontSize))
            }
            is Shape1d -> {
                sizeAndPositionGrid.addRow(0, Label("Start"), createValueEditor(shape.page().pageSize, shape.start))
                sizeAndPositionGrid.addRow(1, Label("End"), createValueEditor(shape.page().pageSize, shape.end))
            }
        }
        return sizeAndPositionGrid
    }

    private fun createCustomPropertiesTab(): Node? {

        val shape = shape ?: return null

        val customPropertiesGrid = GridPane()

        customPropertiesGrid.styleClass.add("properties-grid")
        shape.customProperties.filter { it.visible.value }.forEachIndexed { index, customProperty ->
            val label = Label(customProperty.label.value)
            val data = createVariableEditor(shape.page().pageSize, customProperty.data)
            customPropertiesGrid.addRow(index, label, data)
        }
        return customPropertiesGrid
    }

    private fun createLockTab(): Node? {
        val shape = shape ?: return null

        val lockGrid = GridPane()
        lockGrid.styleClass.add("properties-grid")
        lockGrid.addRow(0, Label("Size"), createValueEditor(shape.page().pageSize, shape.locks.size))
        lockGrid.addRow(1, Label("Rotation"), createValueEditor(shape.page().pageSize, shape.locks.rotation))

        return lockGrid
    }

}
