/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.gui

import uk.co.nickthecoder.fizzy.model.*

class PageOutlineCanvas(val page: Page, drawingArea: DrawingArea)
    : AbstractCanvas(drawingArea) {


    override fun draw() {
        dc.clear()
        dc.use {
            dc.lineWidth(0.1)
            dc.strokeCap(StrokeCap.SQUARE)

            dc.scale(drawingArea.controller.scale)
            dc.translate(drawingArea.pan)

            dc.lineColor(Color.BLACK)
            dc.fillColor(Color.WHITE)

            dc.beginPath()
            dc.moveTo(Dimension2.ZERO_mm)
            dc.lineTo(Dimension2(page.pageSize.documentSize.value.x, Dimension.ZERO_mm))
            dc.lineTo(page.pageSize.documentSize.value)
            dc.lineTo(Dimension2(Dimension.ZERO_mm, page.pageSize.documentSize.value.y))
            dc.lineTo(Dimension2.ZERO_mm)
            dc.endPath(true, true)
        }
        dirty = false
    }
}
