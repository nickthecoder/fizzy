/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy

import javafx.application.Application
import javafx.application.Platform
import javafx.event.EventHandler
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.stage.Stage
import org.apache.logging.log4j.LogManager
import uk.co.nickthecoder.fizzy.gui.JavaFXFFonDetails
import uk.co.nickthecoder.fizzy.gui.MainWindow
import uk.co.nickthecoder.fizzy.model.fontDetailsFactory
import uk.co.nickthecoder.fizzy.util.FizzyJsonReader
import uk.co.nickthecoder.fizzy.util.runLaterHandler
import java.io.File
import java.util.prefs.Preferences

private val logger = LogManager.getLogger(FizzyApp::class.java)

/**
 * The JavaFX [Application] (i.e. the entry point for the program when using a gui.
 */
class FizzyApp : Application() {

    fun preferences(): Preferences = Preferences.userNodeForPackage(FizzyApp::class.java)

    override fun start(primaryStage: Stage) {

        primaryStage.icons.add(Image(FizzyApp::class.java.getResource("icons/fizzy.png") !!.toExternalForm()))

        runLaterHandler = { Platform.runLater(it) }
        fontDetailsFactory = { family, variant -> JavaFXFFonDetails(family, variant) }

        private_mainWindow = MainWindow(primaryStage)

        parameters.raw.forEach { arg ->
            val doc = FizzyJsonReader(File(arg)).load()
            mainWindow.editDocument(doc)
        }

        // Showing the stage before adding the style sheet causes some toolbar buttons to be collapsed in the ">>" extra button.
        // However this bug : https://bugs.openjdk.java.net/browse/JDK-8132900
        // demands that the stage is shown FIRST. Hmm.
        // I've chosen to work around the bug that I can reproduce (the bug above has been fixed in my version of JavaFX).
        //primaryStage.show()

        //Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA)
        primaryStage.scene.stylesheets.add(FizzyApp::class.java.getResource("fizzy.css") !!.toExternalForm())
        primaryStage.show()

        if (!mainWindow.harbour.load(preferences())) mainWindow.defaultDockPositions()

        primaryStage.onHiding = EventHandler { mainWindow.harbour.save(preferences()) }
    }

    companion object {

        private lateinit var private_mainWindow: MainWindow

        val mainWindow
            get() = private_mainWindow

        private val imageMap = mutableMapOf<String, Image?>()


        fun imageResource(name: String): Image? {
            val image = imageMap[name]
            if (image == null) {
                val imageStream = FizzyApp::class.java.getResourceAsStream(name)
                if (imageStream == null) {
                    logger.info("Didn't find image '$name'")
                }
                val newImage = if (imageStream == null) null else Image(imageStream)
                imageMap[name] = newImage
                return newImage
            }
            return image
        }

        fun graphic(name: String): ImageView? = imageResource(name)?.let { ImageView(it) }

    }
}

