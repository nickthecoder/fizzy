## [Home](Home.md)

**[Expression Reference](ref/Index.md)**

**Basic Data Types**

* [Angle](ref/Angle.md)
* [Area](ref/Area.md)
* [Boolean](ref/Boolean.md)
* [Color](ref/Color.md)
* [Dimension](ref/Dimension.md)
* [Dimension2](ref/Dimension2.md)
* [Double](ref/Double.md)
* [Paint](ref/Paint.md)
* [String](ref/String.md)
* [StrokeCap](ref/StrokeCap.md)
* [StrokeJoin](ref/StrokeJoin.md)
* [Vector2](ref/Vector2.md)

**Objects**

* [ControlPoint](ref/ControlPoint.md)
* [CustomProperty](ref/CustomProperty.md)
* [Document](ref/Document.md)
* [Expression](ref/Expression.md)
* [Page](ref/Page.md)
* [Transform](ref/Transform.md)
* [Scratch](ref/Scratch.md)
* [Shape](ref/Shape.md)

**Other**

* [List of Functions](ref/Functions.md)
* [List of Constants](ref/Constants.md)
* [Special Constructs](ref/Special.md)
