# AngleControlPoint

A [ControlPoint](ControlPoint.md), which is constrained around the circumference of a circle.

Defined by the center of a circle (`Start`), the circle's `Radius`, and an `Angle`.

This is useful when you want the user to choose an angle.

`Point` is rarely of importance, and `Radius` is often arbitrary.

## Fields

|Name|Type|Comments|
|----|----|--------|
|Point|[Dimension2](Dimension2)| Start + Angle.unitVector() * Radius
|Start|[Dimension2](Dimension2)| Was called `A` (deprecated)
|Radius|[Dimension](Dimension)|
|Angle|[Angle](Angle.md)| Changes when the control handle is dragged.
|Min|[Angle](Angle.md)|The minimum value for `Angle`. Often 0 deg
|Max|[Angle](Angle.md)|The maximum value for `Angle`. Often 360 deg
