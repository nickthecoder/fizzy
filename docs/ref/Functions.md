# Functions

Standard Maths functions :

|Name|Arguments|Return Type|
|----|---------|-----------|
| abs |[Double](Double.md)|[Double](Double.md)|
| ceil |[Double](Double.md)|[Double](Double.md)|
| exp |[Double](Double.md)|[Double](Double.md)|
| floor |[Double](Double.md)|[Double](Double.md)|
| ln |[Double](Double.md)|[Double](Double.md)|
| log |[Double](Double.md)|[Double](Double.md)|
| round |[Double](Double.md)|[Double](Double.md)|
| sqrt |[Double](Double.md)|[Double](Double.md)|

Trig functions :

|Name|Arguments|Return Type|Comment|
|----|---------|-----------|-------|
| sin |[Angle](Angle.md)|[Double](Double.md)|
| cos |[Angle](Angle.md)|[Double](Double.md)|
| tan |[Angle](Angle.md)|[Double](Double.md)|
| asin |[Double](Double.md)|[Angle](Angle.md)|Inverse(arc) sin
| acos |[Double](Double.md)|[Angle](Angle.md)|Inverse(arc) cos
| atan |[Double](Double.md)|[Angle](Angle.md)|Inverse(arc) tan
| sinh |[Angle](Angle.md)|[Double](Double.md)|Hyperbolic
| cosh |[Angle](Angle.md)|[Double](Double.md)|Hyperbolic
| tanh |[Angle](Angle.md)|[Double](Double.md)|Hyperbolic

There is also the [if](if.md) function.
