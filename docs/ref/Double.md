# Double

All numbers are stored as Doubles; there is no Integer data type in Fizzy.

## Fields

none

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|toString|none|[String](String.md)|Converts to a string, but may be ugly for non integer values.
|format|[String](String.md)|[String](String.md)|Uses Java's [DecimalFormat](https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html)
|clamp|min : [Double](Double.md), max : [Double](Double.md)|[Double](Double.md)|
|min|other : [Double](Double.md)|[Double](Double.md)|There is also a global function, which takes two doubles.
|max|other : [Double](Double.md)|[Double](Double.md)|There is also a global function, which takes two doubles.
