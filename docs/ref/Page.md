# Page

## Fields

|Name|Type|Comments|
|----|----|--------|
|PaperSize|[Dimension2](Dimension2.md)|
|DocumentSize|[Dimension2](Dimension2.md)|
|Scale|[Dimension2](Dimension2.md)|
|Shape.`foo`|[Shape](Shape.md)|Replace `foo` with the name of the child shape. See note below.

`Shape.foo` Only finds top-level shapes.
To search for a shape where the name is only known at run-time, use `findShape( name )` (see below).

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|findShape|name : [String](String.md)|[Shape](Shape.md)|Finds any shape with the given name (the whole tree is searched, not just the top-level shapes).

## Sections

none
