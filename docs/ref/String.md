# String

## Creation

Use double quotes :

    "Hello World"

## Fields

|Name|Type|Comments|
|----|----|--------|
|Length|[Double](Double.md)

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|contains|String|[Boolean](Boolean.md)|
|endsWith|String|[Boolean](Boolean.md)|
|head|[Double](Double.md)|String|See note below
|indexOf|String|[Double](Double.md)|-1 if not found
|isBlank|none|[Boolean](Boolean.md)|Returns `true` if this string is empty or consists solely of whitespace characters.
|isEmpty|none|[Boolean](Boolean.md)|Returns `true` if this string contains no characters.
|startsWith|String|[Boolean](Boolean.md)|
|substring|startIndex [Double](Double.md), endIndex [Double](Double.md)|String|See note below.
|tail|[Double](Double.md)|String|See note below

Note for `tail` and `head` :

* If the argument > length, then the whole string is returned
* If the argument < 0, then an empty string is returned.

Note for `startWith( startIndex, endIndex )` :

* Indices are zero based.
* startIndex is inclusive, and endIndex is exclusive.
* If the startIndex < 0, then it is treated as if it were 0.
* If the endIndex > length, then it is treated as if it were length
* If endIndex <= startIndex, then an empty string is returned.

