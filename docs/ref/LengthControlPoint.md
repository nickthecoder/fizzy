# LengthControlPoint

A [ControlPoint](ControlPoint.md), which is constrained to sit along a line.

The line is defined by a `Start` point, and an `Angle`.

An alternative is [BetweenControlPoint](BetweenControlPoint.md) which is also constrained
along a line, but defined using a start and end point, rather than an angle.


## Fields

|Name|Type|Comments|
|----|----|--------|
|Point|[Dimension2](Dimension2)|Start + Angle.unitVector() * Length
|Start|[Dimension2](Dimension2.md)| Was called `A` (deprecated)
|Angle|[Angle](Angle.md)
|Length|[Dimension](Dimension.md)| Changes when the control handle is dragged.
|Min|[Dimension](Dimension.md)|The minimum value for `Length`. Often `MIN_DIMENSION` or 0mm
|Max|[Dimension](Dimension.md)|The maximum value for `Length`. Often `MAX_DIMENSION`
