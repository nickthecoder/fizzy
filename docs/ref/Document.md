# Document

## Fields

|Name|Type|Comments|
|----|----|--------|
|Name|[String](String.md)|The document's filename, without the extension
|Shape`N`|[Shape](Shape.md)|See note below|

You can access a shape, using the syntax .Shape`N` where `N` is the ID of the shape.
For example, to access shape with an ID of 10 :

    theDocument.Shape10

## Methods

none

## Sections

* [CustomProperty](CustomProperty.md)
