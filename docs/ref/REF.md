# REF (reference)

REF is a special construct, which looks like a function taking one parameter.
It's return value is the same as its parameter.

So at first sight it does nothing at all!

Imagine you have a [LengthControlPoint](LengthControlPoint.md), and its
`Length` expression is defined as :

    CustomProperty.specialLength

If you were to move the control point in the GUI, then the expressions
would change to a constant value. So maybe `Length` would now be:

    100 mm

The `CustomProperty.specialLength` wouldn't be affected when you dragged
the control point.

Now, if we changed the `Length` expression to :

    REF( CustomProperty.specialLength )

Its value would be the same as before. However, now when we move the
control point, instead of replacing the expression with a new value,
it updates the expression being referenced.

So after dragging,  `Length`'s formula will be unchanged :

    REF( CustomProperty.specialLength )

but `CustomProperty.specialLength` will have changed.

This works for any expressions that can be changed via the GUI, not only
ControlPoints.
