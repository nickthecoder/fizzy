# Local Coordinates

Almost all points specified in a Shape's Shape Sheet are in Local Coordinates,
NOT [Page Coordinates](PageCoordinates.md).

Therefore, when a shape is moved, rotated, flipped or scaled, all of the
[Dimension2](Dimension2) data in the shape sheet remains the same, with the
exception of Shape.Pin (and also Shape.Start and Shape.End for 1D shapes).

For example, if we have a box of size (20mm, 10mm), then the Geometry section
may have points : (0mm,0mm) (20mm,0mm) (20mm,10mm) (0mm,10mm) and back to (0mm,0mm).

No matter where we move this box on the page, the geometry will remain the same.
The only thing that will change is [Shape](Shape.md)'s `Pin` field.
The `Pin` field is relative to the Parent (which is either a [Page](Page.md),
or a parent [Shape](Shape.md).

If we set Pin to (50mm, 50mm), where are the corners of the box in the Page's
coordinate system?
That depends on where, within the Shape, the box's `Pin` refers to.
Often we want `Pin` to refer to the middle of the shape, but not always.

This is where [Shape](Shape.md)'s `LocPin` comes in. `LocPin` is the Local version
of Pin. So the most common value for LocPin is `Size / 2`, which means that `Pin`
will refer to the center of the shape.

So, setting Pin to (50mm, 50mm) our box's corners will be at
(40mm, 45mm) (60mm, 45mm) (60mm, 55mm) and (40mm, 55mm).

To convert from local to page coordinates we can use the method
[Shape](Shape.md)'s method `localToPage`. There are three other transformations :
`localToParent`, `pageToLocal` and `parentToLocal`.


Whenever you are designing a Shape1d, imagine its start point is at (0,0),
and the line pointing to the right.

So a simple line's Size will be :

    ( (Start - End).Length, LineWidth )

To be clear Size.Y will always be LineWidth, no matter where the line is pointing
on the page.
