# Dimension2

Many values in a Shape Sheet define a 2D Point, and rather than define
the X and Y values in separate expressions, Fizzy uses one expression.

This makes many tasks simpler, for example, the center of a shape is
given by the expression

    Size / 2

## Creation

The most verbose way e.g. :

    Dimension2( 1mm, 2mm )

However, you can also simply use :

    ( 1mm, 2mm )

It is possible to leave the brackets off too, but that isn't recommended (for clarity).

The units do not have to be the same. e.g.

    (10mm, 2 pt)

The X and Y values don't have to be constants. e.g. :

    (Size.X / 2, Size.Y / 4)

## Operators

Mathematical operators work as you might expect :

| Left |Operator|Right|=|Comments|
|------|--------|-----|-----------|--------|
|Dimension2|+ or -|Dimension2|Dimension2|
|Dimension2|* or /|[Double](Double.md)|Dimension2|
|Dimension2|* or /|[Vector2](Vector2.md)|Dimension2
|Dimension2| / |Dimension2|[Vector2](Vector2.md)|

## Fields

|Name|Type|Comments|
|----|----|--------|
|X|[Dimension](Dimension.md)|
|Y|[Dimension](Dimension.md)|
|Length|[Dimension](Dimension.md)|
|Angle|[Angle](Angle.md)|The angle of this point from the origin.


## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|normalise|none|[Vector2](Vector2.md)|A unit vector in the same direction as the Dimension2.
|rotate|[Angle](Angle.md)|[Dimension2](Dimension2.md)|
|toString|none|[String](String.md)|
|format|[String](String.md)|[String](String.md)|Uses Java's [DecimalFormat](https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html)
|toUnits|[Units](Units.md)|[Dimension2](Dimension2.md)|Converts to different units.
|clamp|min : [Dimension2](Dimension2.md), max : [Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)
|min|other : [Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|Dimension2(min( this.X, other.X) , min ( this.y, other.Y))
|max|other : [Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|
