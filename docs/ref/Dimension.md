# Dimension

Contains a [Double](Double.md), and units of measure.

The basic metric units are : mm, cm, m and km.
There are also imperial units : inch, ft, yard.

There are also "Dymanic" units "pt" (points), which are used for font sizes, and
line widths. Points are dynamic, because if you change the scale factor for a document,
then the point sizes will change in respect to the other units.

This dynamic nature is important when creating scale diagrams, such as floor plans.

## Creation
To create a Dimension, just add the units after a number. e.g.

100mm or 10cm or 0.1m etc

## Operators

Mathematical operators work as you might expect :

| Left |Operator|Right|=|Comments|
|------|--------|-----|-----------|--------|
|Dimension|+ or -|Dimension|Dimension|
|Dimension|* or /|[Double](Double.md)|Dimension|
|Dimension| * |Dimension|[Area](Area.md)|
|Dimension| / |Dimension|[Double](Double.md)|

## Field

|Name|Type|Comments|
|----|----|--------|
|mm|[Double](Double.md)|The number of millimeters
|cm|[Double](Double.md)|The number of centimeters
|m|[Double](Double.md)|The number of meters
|km|[Double](Double.md)|The number of kilometers

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|toString|none|[String](String.md)|Converts to a string, but may be ugly for non integer values.
|format|[String](String.md)|[String](String.md)|Uses Java's [DecimalFormat](https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html)
|units|none|[Units](Units.md)|
|toUnits|[Units](Units.md)|[Dimension](Dimension.md)|Converts to different units, keeping the same length.
|clamp|min : [Dimension](Dimension.md), max : [Dimension](Dimension.md)| [Dimension](Dimension.md)|
|min|other : [Dimension](Dimension.md)|[Dimension](Dimension.md)|The minimum of `this` and `other`
|max|other : [Dimension](Dimension.md)|[Dimension](Dimension.md)|The maximum of `this` and `other`
