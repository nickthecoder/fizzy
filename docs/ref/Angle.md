# Angle

Fizzy has a special type devoted to angles.
This has certain advantages over the use a plain old Doubles

* Improved type safety (cannot incorrectly mix and match number and angles)
* No confusion between the use of degrees and radians

Fizzy chooses 0° to point East (along the positive x axis),
and progress anticlockwise. i.e. North is 90° and South is -90°.
This is the normal mathematical convention.

This makes it easy for mathematically minded people to visualise the angles
in the normal way.

However, because the origin is in the top left of the page, and the
Y axis point **downwards**, angles can still be a little confusing.

If you stick to using Fizzy's in-built [Angle](Angle.md),
[Vector2](Vector2.md) and [Dimension2](Dimension2.md) objects, then the
'weird' Y axis won't be a problem.

However, if you use trig functions (sin, cos, tan etc),
then you must ensure you negate the y values appropriately.

## Creation

Used the "rad" or "deg" suffix operators. e.g.

    10 deg
    10 °
    3 rad

## Constants

Note, `PI` and `TAU` are Doubles, not Angles.

But you can do the following :

    PI rad
    180 deg

    TAU rad
    360 deg

## Operators

Mathematical operators work as you might expect :

| Left |Operator|Right|=|Comments|
|------|--------|-----|-----------|--------|
|Angle|+ or -|Angle|Angle|
|Angle|* or /|[Double](Double.md)|Angle|

## Fields

|Name|Type|Comments|
|----|----|--------|
|Degrees|[Double](Double.md)|
|Radians|[Double](Double.md)|

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|unitVector|none|[Vector2](Vector2.md)|A vector of length 1, at the given angle.
|format|[String](String.md)|[String](String.md)|Uses Java's [DecimalFormat](https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html)
|sin|none|[Double](Double.md)
|cos|none|[Double](Double.md)
|tan|none|[Double](Double.md)
|sinh|none|[Double](Double.md)
|cosh|none|[Double](Double.md)
|tanh|none|[Double](Double.md)
|clamp|min : [Angle](Angle.md), max : [Angle](Angle.md)|[Angle](Angle.md)|
