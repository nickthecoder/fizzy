# Scratch

A section of [Shape](Shape.md).

Holds intermediate calculations, without any special purpose.

The following returns the value of the current shape's scratch with name "foo"

    CustomProperty.foo
