# WithinControlPoint

A [ControlPoint](ControlPoint.md), which is free to be positioned anywhere.

The control point defined by two opposite corners of a rectangle
(named `A` and `B`), and a Vector2 (named `Position`)
defining how close the control point is to A or B.

A `Position` of (0,0) means at A, and (1,1) means at B.

`Position` can have -ve values, and values over 1, if the point is
outside of the rectangle defined by `A`and `B`.
But is `ConstrainX` / `ConstrainY` are set to `true`, then the values
are forced within the range 0..1.


Consider [StandardControlPoint](StandardControlPoint.md) as an alternative, which is simpler,
but cannot adjust itself when the shape is scaled.


## Fields

|Name|Type|Comments|
|----|----|--------|
|Point|[Dimension2](Dimension2)| A + (B - A) * Position
|A|[Dimension2](Dimension2)|
|B|[Dimension2](Dimension2)|
|Position|[Vector2](Vector2.md)| Changed when the control handle is dragged. Values in the range 0..1 if ConstrainX and ConstrainY are true.
|Min|[Vector2](Vector2.md)| The minimum value for `Position`. Often `MIN_VECTOR2` or (0,0)
|Max|[Vector2](Vector2.md)| The maximum value for `Position`. Often `MAX_VECTOR2` or (1,1)
