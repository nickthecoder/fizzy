# BetweenControlPoint

A [ControlPoint](ControlPoint.md), which is constrained to sit along a line.

The line is defined by `Start` and `End` points.

An alternative is [LengthControlPoint](LengthControlPoint.md) which is also constrained
along a line, but defined using a start point and an angle.

## Fields

|Name|Type|Comments|
|----|----|--------|
|Point|[Dimension2](Dimension2)| Start + (End - Start) * Length
|Start|[Dimension2](Dimension2.md)| Was called `A` (deprecated)
|End|[Dimension2](Dimension2.md)| Was called `B` (deprecated)
|Along|[Double](Double.md)| Changes when the control handle is dragged.
|Min|[Double](Double.md)|The minimum value for `Along`. Often `MIN_VALUE` or 0
|Max|[Double](Double.md)|The maximum value for `Along`. Often `MAX_VALUE` or 1

