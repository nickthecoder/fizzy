# Vector2

A vector of two doubles. Unlike [Dimension2](Dimension2.md), a Vector2 has no units,
and it therefore not used to store points.

# Creation

The long form :

    Vector2( 10, 20 )

The short form :

    ( 10, 10 )

The brackets are actually optional, but adding them adds clarity.
When passed as an argument to a function or method, the brackets become
mandatory :

    foo( ( 10, 10 ) ) // Correct
    foo( 10, 10 ) // Wrong - two parameters of type Double, not a Vector2.

## Fields

|Name|Type|Comments|
|----|----|--------|
|X|[Double](Double.md)|
|Y|[Double](Double.md)|
|Angle|[Angle](Angle.md)|
|Length|[Double](Double.md)|

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|format|[String](String.md)|[String](String.md)|Uses Java's [DecimalFormat](https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html)
|normalise|none|Vector2|Creates a vector of length 1, in the same direction.
|rotate|[Angle](Angle.md)|Vector2|Rotates the vector about the origin.
|clamp|min : Vector2, max : Vector2| Vector2|
