# Paint

Used to fill area, or color stroked lines.
Currently, the only kind of Paint is a solid [Color](Color.md), but future versions of
Fizzy may have gradient fills of various kinds.
