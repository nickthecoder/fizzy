# Shape

Shape is the basic building block, and comes in three flavours :

* [Shape1d](Shape1d.md) - Used for lines
* [Shape2d](Shape2d.md) - Used for everything that isn't a line, or text.
* [ShapeText](ShapeText.md)

## Fields

|Name|Type|Comments|
|----|----|--------|
|Document|[Document](Document.md)|
|Child.`foo`|[Shape](Shape.md)|Replace `foo` with the name of the child shape. See note below.
|FillColor|[Paint](Paint.md)|
|FlipX|[Boolean](Boolean.md)|
|FlipY|[Boolean](Boolean.md)|
|LineColor|[Paint](Paint.md)| An alias for StrokeColor
|LineWidth|[Dimension](Dimension.md)|
|LocPin|[Dimension2](Dimension2.md)|The position of Pin, relative to THIS shape.
|Name|[String](String.md)|
|Page|[Page](Page.md)|
|Parent|[Page](Page.md) or [Shape](Shape.md)| Shapes can be children of other shapes
|Pin|[Dimension2](Dimension2.md)|The Shape's position relative to it's parent|
|Rotation|[Angle](Angle.md)|
|Scale|[Vector2](Vector2.md)|
|Size|[Dimension](Dimension.md)|
|StrokeColor|[Paint](Paint.md)

`Child.foo` only finds immediate children (not grandchildren etc).
To search for a shape where the name is only known at run-time, use `findShape( name )` (see below).

### Additional fields of Shape1d and Shape2d (i.e. not ShapeText)

|Name|Type|Comments|
|----|----|--------|
|CornerRadius|[Dimension](Dimension.md)|Causes all joins between [LineTo](LineTo.md)s to be rounded.
|StrokeCap|[StrokeCap](StrokeCap.md)|
|StrokeJoin|[StrokeJoin](StrokeJoin.md)|


### Additional fields of Shape1d

|Name|Type|Comments|
|----|----|--------|
|Start|[Dimension2](Dimension2.md)|
|End|[Dimension2](Dimension2.md)|
|StartAngle|[Angle](Angle.md)|See note below
|EndAngle|[Angle](Angle.md)|See note below
|Length|[Dimension](Dimension.md)|
|IsStartConnected|[Boolean](Boolean.md)|Is the start point connected to another shape
|IsEndConnected|[Boolean](Boolean.md)|Is the end point connected to another shape

`StartAngle` and `EndAngle` are relative to their parent.
When not connected to anything, these are usually
`(End-Start).Angle` and `(Start-End).Angle`.
However, when the Shape1d is connected to another shape,
then it will give the "preferred" angle of the connection.
For [ConnectionPoint](ConnectionPoint.md)s, this is the angle of the ConnectionPoint.
When connecting along the Geometry of another Shape,
then it is the `normal` at that point of the Geometry.

### Additional fields of ShapeText

|Name|Type|Comments|
|----|----|--------|
|Text|[String](String.md)|
|FontSize|[Double](Double.md)|
|AlignX|[Double](Double.md)|0=Left 0.5=Center 1=Right
|AlignY|[Double](Double.md)|0=Top 0.5=Middle 1=Bottom
|Clip|[Boolean](Boolean.md)|Is the text clipped to the shape's size?
|MarginTop|[Dimension](Dimension.md)|
|MarginRight|[Dimension](Dimension.md)|
|MarginBottom|[Dimension](Dimension.md)|
|MarginLeft|[Dimension](Dimension.md)|
|TextSize|[Dimension2](Dimension2.md)|The size of the text, not including any margins


## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|findShape|name : [String](String.md)|[Shape](Shape.md)|Finds an ancestor with the given name.
|findCustomProperty|name : [String](String.md)|[CustomProperty](CustomProperty.md)|Rarely used
|findScratch|name : [String](String.md)|[Scratch](Scratch.md)|Rarely used
|findControlPoint|name : [String](String.md)|[ControlPoint](ControlPoint.md)|Rarely used
|localToParent|[Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|Converts local coordinates to those of the parent shape (or page)
|localToPage|[Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|Converts local coordinates to those of the page
|parentToLocal|[Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|Converts coordinates of the parent shape/page to local coordinates of this shape
|pageToLocal|[Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|Converts page coordinates to local coordinates of this shape

### Additional methods of Shape1d

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|connectTo|[ConnectionPoint](ConnectionPoint.md),<br> margin : [Dimension2](Dimension2.md)|[Dimension2](Dimension2.md)|Connects to a [ConnectionPoint](ConnectionPoint.md)
|connectToShape|shape :[Shape](Shape.md),<br> isStart : [Boolean](Boolean.md),<br> margin : [Dimension](Dimension.md)|[Dimension2](Dimension2.md)|Connects to a [Shape](Shape.md) (Smart)
|connectAlong|[Geometry](Geometry.md),<br>along : [Double](Double.md),<br> margin : [Dimension](Dimension.md)|[Dimension2](Dimension2.md)|Connects along the outline ([Geometry](Geometry.md) of a shape

`connectTo`, `connectToShape` and `connectAlong` are used on the `start` and `end` points
of a line are connected to another shape.
These methods are rarely used manually, but are created by the GUI when dragging line ends.

### Additional methods of ShapeText

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|textSize|[Dimension2](Dimension2.md)|The size of text, not including margins.

## Sections

* [ControlPoint](ControlPoint.md)
* [ConnectionPoint](ConnectionPoint.md)
* [Snap](Snap.md)
* [Scratch](Scratch.md)
* [CustomProperty](CustomProperty.md)
* [Transform](Transform.md)

Additional sections used by Shape1d and Shape2d :

* [Geometry](Geometry.md)
