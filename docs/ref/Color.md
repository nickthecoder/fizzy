# Color

A sub-class of [Paint](Paint.md), providing a solid color.
Fizzy currently has no other sub-classes of Paint, but later versions may have gradients.

## Creation

There are three constructors :

    RGB( 0, 1, 0 )
    RGBA( 0,1,0,1 )
    WebColor( "#rrggbb" )

RGB creates fully opaque colors. The parameters are red, green, blue in the range 0..1.

RGBA has an additional alpha parameter, also in the range 0..1.

WebColor uses HTML style colors in the form #rrggbb or #rrggbbaa, where each pair is a
two digit hexadecimal number.

## Constants

The only named colors are BLACK, WHITE and TRANSPARENT.

## Fields

|Name|Type|Comments|
|----|----|--------|
|Red|[Double](Double.md)|
|Green|[Double](Double.md)|
|Blue|[Double](Double.md)|
|Alpha|[Double](Double.md)|
|Opacity|[Double](Double.md)|An alias for Alpha

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|brighter|none|Color|A brighter version of the source color (by 1 / 0.7)
|darker|none|Color|A darker version of the source color (by 0.7)
|brighterBy|[Double](Double.md)|Color|Scales each of the RBG channels by the given amount. > 1 for brighter, < 1 for darker.

## Methods
