# Transform

When editing a Shape's Shape Sheet, you can see a section called "Transform".

However, within expressions, there is no `Shape.Transform` field.
Instead you can access the fields directly. e.g. :

    Pin
    LocPin

    myShape.Pin
    myShape.LocPin

