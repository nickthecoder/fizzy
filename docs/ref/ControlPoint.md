# ControlPoint

Control Points are draggable handles within a shape that can be used to control
any aspect of the shape that you want.

Some uses include :

* Changing line widths, margins or other measurements of your shape
* Moving elements, such as text labels

There are different kind of ControlPoints :

* [StandardControlPoint](StandardControlPoint.md)
* [WithinControlPoint](WithinControlPoint.md)
* [LengthControlPoint](LengthControlPoint.md)
* [BetweenControlPoint](BetweenControlPoint.md)
* [AngleControlPoint](AngleControlPoint.md)

The remainder of this page is the common fields/methods shared by all ControlPoints.

## Access

To access a control point, using its name :

    ControlPoint.theName.Point

## Fields

|Name|Type|Comments|
|----|----|--------|
|Point|[Dimension2](Dimension2.md)| The position of the control point in [Local Coordinates](LocalCoordinates.md).

## Methods

none
