# Area

Rarely used, but if you multiply a [Dimension](Dimension.md) by a [Dimension](Dimension.md), you get an Area.

## Creation

You cannot create them directly, only via multiplication of Dimensions.

## Operators

| Left |Operator|Right|=|Comments|
|------|--------|-----|-----------|--------|
|Area|+ or -|Area|Area|
|Area|* or /|[Double](Double.md)|Area|
|Area| / |[Dimension](Dimension.md)|[Dimension](Dimension.md)|

## Fields

|Name|Type|Comments|
|----|----|--------|
|mm|[Double](Double.md)|The number of square millimeters
|cm|[Double](Double.md)|The number of square centimeters
|m|[Double](Double.md)|The number of square meters
|km|[Double](Double.md)|The number of square kilometers

## Methods

|Name|Parameters|Return Type|Comments|
|----|----------|-----------|--------|
|toString|none|[String](String.md)|Converts to a string, but may be ugly for non integer values.
|format|[String](String.md)|[String](String.md)|Uses Java's [DecimalFormat](https://docs.oracle.com/javase/7/docs/api/java/text/DecimalFormat.html)
|units|none|[Units](Units.md)|
|toUnits|[Units](Units.md)|[Dimension](Dimension.md)|Converts to different units, keeping the same area.

