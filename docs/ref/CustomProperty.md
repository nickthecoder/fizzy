# CustomProperty

A section of [Shape](Shape.md) and [Document](Document.md).

Custom properties can be edited from their own tab in the Shape Property
dockable tool window.

For example, in the Electronics Stencil, CustomProperties are used to
store component ids, as well as resistor values etc.

The following returns the value of the current shape's custom property with name "foo"

    CustomProperty.foo

If you want access to other fields, then you need to use `CustomProperties`
instead of `CustomProperty`

# CustomProperties

Refers to the same section as CustomProperty, but allows access to
all of the fields, not only the `Value`.

## Fields

|Name|Type|Comments|
|----|----|--------|
|Value|Any|
|Label|[String](String.md)|
|IsMenu|[Boolean](Boolean.md)|

e.g. If you have a CustomProperty named `foo` :

    CustomeProperties.foo.Value
    // Is the same as
    CustomProperty.foo
