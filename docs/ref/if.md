# if

The "if" is a function, with three parameters :

`if` ( condition : [Boolean](Boolean.md),  trueResult : `Any`, falseResult  : `Any` )

The condition is evaluated, and if it is true, then `trueResult` is returned,
otherwise `falseResult` is returned.

The types for `trueResult` and `falseResult` should be the same.

e.g.

    if ( CustomProperty.foo < Size.X / 2, 0mm, Size.X )

Can be read as : If `foo` is less than Size.X / 2, then return 0mm, otherwise return Size.X

And if that isn't clear enough, here's some equivalent pseudo code :

    if ( foo < Size.X / 2 ) {
        return 0mm
    } else {
        return Size.X
    }
