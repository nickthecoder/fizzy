# StandardControlPoint

A [ControlPoint](ControlPoint.md), which is free to be positioned anywhere.

Internally the position is stored as a distance `Delta` from a given position `RelativeTo`.

The advantage of having a delta from a relative position, is so that `RelativeTo` can be repositioned and
`Point` will follow appropriately.

For example, imagine you have a control point which determines the position of
some text within a box.
If `RelativeTo` = Size/2, and `Delta` is initially (0mm,0mm),
the the text will appear in the middle of the box, even if the box's size changes.

Consider [WithinControlPoint](WithinControlPoint.md) as an alternative,
which is useful when you want the control point to adjust in proportion
when the shape is scaled.

## Fields

|Name|Type|Comments|
|----|----|--------|
|Point|[Dimension2](Dimension2)| RelativeTo + Delta.
|RelativeTo|[Dimension2](Dimension2.md)|Often (0mm,0mm)
|Delta|[Dimension2](Dimension2.md)| The offset away from `RelativeTo`. Changed when the control handle is dragged.
|Min|[Dimension2](Dimension2.md)|The minimum value for `Delta`. Often `MIN_DIMENSION2` or (0mm, 0mm)
|Max|[Dimension2](Dimension2.md)|The maximum value for `Delta`. Often `MAX_DIMENSION2` or Size
