# Constants

|Type|Values|
|----|------|
| Boolean | **true**, **false**
| Double | **PI**, **TAU**, **E**, **MAX_DOUBLE**, **MIN_DOUBLE**, **SMALLEST_DOUBLE**, **NaN**
| Vector2 | **MIN_VECTOR2**, **MAX_VECTOR2**
| Dimension | **MIN_DIMENSION**, **MAX_DIMENSION**
| Dimension2 | **MIN_DIMENSION2**, **MAX_DIMENSION2**
| Color | **BLACK**, **WHITE**, **TRANSPARENT**
| StrokeCap | **STROKE_CAP_BUTT**, **STROKE_CAP_SQUARE**, **STROKE_CAP_ROUND**
| StrokeJoin | **STROKE_JOIN_BEVEL**, **STROKE_JOIN_ROUND**, **STROKE_JOIN_MITRE**
