# Shape Sheet Reference

Shape Sheets are like spreadsheets.
Each cell is an [Expression](Expression.md), which can make calculations based on
other cells in the Shape Sheet.

[Shape](Shape.md) is the starting point for all Shape Sheet expressions, and is
therefore the most important.

## Basic Data Types

* [Angle](Angle.md)
* [Area](Area.md)
* [Boolean](Boolean.md)
* [Dimension](Dimension.md)
* [Dimension2](Dimension2.md)
* [Double](Double.md)
* [Paint](Paint.md)
* [String](String.md)
* [StrokeCap](StrokeCap.md)
* [StrokeJoin](StrokeJoin.md)
* [Vector2](Vector2)

## Objects

* [ConnectionPoint](ConnectionPoint.md)
* [ControlPoint](ControlPoint.md)
* [CustomProperty](CustomProperty.md)
* [Document](Document.md)
* [Geometry](Geometry.md)
* [Page](Page.md)
* [Scratch](Scratch.md)
* [Shape](Shape.md)
* [Snap](Snap.md)
* [Transform](Transform.md)

## Other Stuff

* [List of functions](Functions.md)
* [List of constants](Constants.md)
* [Special Constructs](Special.md)
