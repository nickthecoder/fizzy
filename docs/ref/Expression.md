# Expression

Shape sheets are similar to regular spread sheets.
Each cell contains an expression, which can be a simple constant, or
something more complex, which uses other cells in its calculation.

Unlike regular spread sheets, Fizzy's Shape Sheets have data types
specially designed for 2D drawing.
Most importantly is [Dimension2](Dimension2.md), which hold a 2D point.
This means there is no need for separate X and Y cells for each point.
It also makes many calculations easier.
For example, the middle of a shape is given by :

    Size / 2

There's no need to do anything like :

    Size.X / 2
    Size.Y / 2

## Data Types

Cells are strongly typed, which means the result of a cell must be of
the correct data type.

The most common types are :

|Type|Comment|
|----|-------|
|[Angle](Angle.md)|Avoids confusion between degrees and radians.
|[Area](Area.md)|Rarely used. Results from multiplying two [Dimension](Dimension.md)s
|[Boolean](Boolean.md)|true or false
|[Dimension](Dimension.md)|A length
|[Dimension2](Dimension2.md)|A 2D point, including units of measure. This is the most common data type.
|[Double](Double.md)|Numbers. Note that there is no Integer type.
|[String](String.md)|Text
|[Vector2](Vector2)|A 2D Vector (without units of length)

## Syntax

Fields are accessed using the "." operator.
For example, to find the Size of the current shape :

    this.Size

However, "this" is optional, and so it can be written simply as :

    Size

We can chain dot operators together. For example :
    this.Size.X

## Referencing Sections

Each Shape has many sections, each section is a mini spread sheet, and the data from the rows
can be accessed like so :

    Geometry.foo.Point
    ControlPoint.bar.Point

It is also possible to use indices, but this is **not** recommended.
Assuming foo and bar are both the first row, then the example above could have been written like so :

    Geometry.Point1
    ControlPoint.Point1

Fizzy initially copied Visio's use of indices, but in hindsight, indices are a bad idea for two reasons :

* Names are much easier to understand
* Inserting/deleting rows becomes a nightmare when referencing everything by its index.

There is no significant performance difference between the two methods.

### Simple Sections

Sections [Scratch](Scratch.md),
[ConnectionPoint](ConnectionPoint.md),
[Snap](Snap.md) and
[CustomProperty](CustomProperty.md)
only have a single piece of data, we access the data like so :

    Scratch.myControlPoint
    ConnectionPoint.foo
    Snap.bar
    CustomProperty.baz

## Functions and Methods

Functions and methods work the same way as most computer languages :

    object.methodName( arg1, arg2, ... )

Note that the comma symbol is used to separate arguments,
but is also used to combine two Dimensions into a Dimension2
(and also to combine two Doubles into a Vector2).
Therefore, additional brackets may be needed :

    foo( 10mm, 10mm )   // A function taking two arguments for type Dimension
    bar( (10mm, 10 mm) )   // A function taking 1 argument of type Dimension2

See [Functions](Functions.md)

## Operators

|Name|Symbol|Applies To|
|----|------|----------|
|Plus|+|Double, Dimension, Dimension2, Vector2, String
|Minus|-|Double, Dimension, Dimension2, Vector2
|Multiply|*|Double, Dimension, Dimension2, Vector2
|Divide|/|Double, Dimension, Dimension2, Vector2
|Power|^|Double
|And|&&|Boolean
|Or| \|\||Boolean
|Not|!|Boolean
|Exclusive Or|xor|Boolean
|Equals|==|Any
|Not Equals|!=|Any
|Greater Than|>|Double, Dimension, Angle
|Less Than|<|Double, Dimension, Angle
|Greater or Equal|>=|Double, Dimension, Angle
|Less or Equal|<=|Double, Dimension, Angle

## Lengths

Fizzy doesn't use numbers to store X and Y values, instead it uses [Dimension](Dimension.md),
which includes the units, such as meters, inches, points etc.

To create a length, append the units to the end of a number. e.g.

    10 mm
    10 inch
    10 pt

The units are :

|Name|Comments|
|----|--------|
|mm|Millimeters|
|cm|Centimeters|
|m|meters|
|km|Kilometers|
|inch|Inches|
|ft|Feet|
|yard|Yards|
|pt|Points|

If you need to convert a [Dimension](Dimension.md) to a double, you could do something like this :

    myLength / 1mm

Which returns myLength in millimeters.

## Points

All X,Y coordinates are stored as [Dimension2](Dimension2).

To create them, use the syntax `( X , Y )` e.g.

    (1mm, 2mm)
    (Size.X + 10mm , Size.Y + 10mm)

Note. Fizzy doesn't actually need the brackets just `1mm,2mm` would suffice,
but using brackets makes things clearer.

If you need a 2D vector **without** length units, then use [Vector2](Vector2.md)

To create them, use the syntax `( X, Y )`, where X and Y are [Double](Double.md)s. e.g.

    ( 1, 2 )

If you multiply a Vector2 by a Dimension, the result is a Dimension2.

See [Dimension](Dimension.md), [Dimension2](Dimension2.md) and [Vector2](Vector2.md)
for more details on their operators and methods.

## Angles

Fizzy uses the [Angle](Angle.md) data type, it doesn't use plain [Double](Double.md) to store angles.

To create angles, add either a `deg` or `rad` suffix. e.g.

    180 deg
    180 °
    3.14159 rad

If you need to convert an Angle back to a Double, then use :

    myAngle.Degrees
    myAngle.Radians

Note. `PI` and `TAU` are built in constants of type [Double](Double.md), they are NOT Angles!

## Flow Control

There are no loops of any kind.

There is an [if](if.md) though.
