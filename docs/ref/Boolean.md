# Boolean

See the [if](if.md) function.

## Creation

Use either of the two constants : true, false

## Operators

|Name|Symbol|
|----|------|
|And| &&
| Or|\|\|
|Exclusive Or|xor
| Not|!

