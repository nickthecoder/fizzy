// fizzy (top-level)

plugins {
    kotlin("jvm") version "1.9.10"
    // id("org.jetbrains.dokka") version "1.9.10"
    id("org.openjfx.javafxplugin") version "0.1.0"
    application
}

javafx {
    version = "13"
    javafx {
        modules("javafx.controls", "javafx.swing", "javafx.web")
    }
}

val fizzyVersion: String by project
val jsonVersion: String by project
val log4jVersion: String by project
val junitVersion: String by project
val harbourfxVersion: String by project

allprojects {

    version = fizzyVersion
    group = "uk.co.nickthecoder"

    repositories {
        mavenCentral()
        // My other projects hosted on gitlab.com
        maven {
            name = "harbourfx"
            url = uri("https://gitlab.com/api/v4/projects/11515658/packages/maven")
        }
    }
}

application {
    mainClass.set("uk.co.nickthecoder.fizzy.Fizzy")
}

dependencies {
    implementation("org.apache.logging.log4j:log4j-api:${log4jVersion}")
    implementation("org.apache.logging.log4j:log4j-core:${log4jVersion}")
    implementation(project(":fizzy-core"))
    implementation( "uk.co.nickthecoder:harbourfx:${harbourfxVersion}" )
    testImplementation("junit:junit:${junitVersion}")
}

task<Exec>("publishZip") {
    dependsOn(":distZip")
    commandLine("cp", "build/distributions/fizzy-${fizzyVersion}.zip", "../download/")
}

task<Exec>("ntc") {
    dependsOn(":publishZip")
    commandLine("echo", "Done")
}

defaultTasks("installDist")
