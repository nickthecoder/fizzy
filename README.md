# Fizzy

Draw diagrams using pre-defined "Stencils" (or create your own stencils).
Inspired by Visio.

For example, an organisation chart has boxes with structured content (a name, and role etc.).
The boxes can be connected by lines.
If you move a box, the connecting lines are automatically adjusted.

Stencils are defined using something similar to a spreadsheet,
where each cell contains an expression, which can reference other parts of the spreadsheet.
This allows the shapes to behave intelligently.
For example, a box may resize itself to ensure its contents fit
in nicely.

![Screenshot](docs/screenshots/FloorPlan.png)

[More Screenshots](docs/screenshots/index.md)

## Documentation

If you want to create your own stencils, then have a look at the
[Shape Sheet Reference](docs/ref/Index.md).

## Progress

It works, you can now create and edit diagrams, as well as Stencils, and Templates.
A few stencils have been created.

The GUI is still rough around the edges in many places.

Editing shapes isn't as easy as I'd like.
(This isn't a problem for those people who just want to use existing stencils).

The file format isn't documented, but it has already shown itself to support
backwards compatibility, when objects are renamed, or become deprecated during development.
However, I have made a few file format changes without regard for backwards compatibility.

Documentation is sparse.

Unit test coverage is sparse. I only have around 120 test functions, with just over
800 asserts in total.
I could easily multiple those figures by 10 (if I had the time and inclination).

## Features

* Pre-made Stencils (only basic shapes, flow charts, electronics and floor plans so far).
* Smart shapes, can adjust themselves when scaled, rotated etc.
* Custom Properties. e.g. an electronic circuit diagram can have component id, and values.
* Automatic attachment of lines to a shape (the line's ends will adjust intelligently).
* Themes. Change the look of your diagram with a single click, by selecting a different theme.
* Export as PNG and SVG

## Missing Features

* Printing (you'd have to convert to PNG or SVG, then print from another application!)
* No in-built editor for Themes.
* Many more stencils should be created.
* Dashed lines
* Displaying a line as double parallel lines (or more than two).
* Effects, such as drop shadows, and blurring.
* Limited support for elliptical arcs.
* Font handling is bad.
* No scroll bars (I always pan by dragging with the mouse).
* The dockable panels are a little awkward to use.
* Layers. All shapes are currently on a single layer.
* Only the 1st page of a document can be edited.
* Customisable events. e.g. Perform any action when a shape is double clicked.
* Compressed file format. Fizzy files are quite verbose. e.g. 500k for the Flowchart Stencil.
  22K if it were zipped.

## Compatibility with Visio

Fizzy cannot read any of Visio's files, and I have no intention of
attempting to write a conversion utility.
