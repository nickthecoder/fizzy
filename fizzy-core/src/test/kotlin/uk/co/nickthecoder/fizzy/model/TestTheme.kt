/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import org.junit.Test
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestTheme : MyTestCase() {

    @Test
    fun testTheme() {
        val doc = Document()
        val page = Page(doc)

        val box = createBox(page)
        box.lineWidth.formula = "2mm"
        assertEquals(2.0, box.lineWidth.value.mm)

        page.theme.value = DefaultTheme(page)
        box.lineWidth.formula = "Theme.lineWidth(Class)"
        assertEquals(1.0 / 72 * 25.4, box.lineWidth.value.mm) // 1pt

        Themes.useTheme("Chunky", page)
        assertEquals(3.0 / 72 * 25.4, box.lineWidth.value.mm, tiny) // 3 pt

    }

    @Test
    fun testInitialTheme() {
        val doc = Document()
        val page = Page(doc)

        val box = createBox(page)
        box.lineWidth.formula = "2mm"
        assertEquals(2.0, box.lineWidth.value.mm)

        box.lineWidth.formula = "Theme.lineWidth(Class)"
        assertEquals(1.0 / 72 * 25.4, box.lineWidth.value.mm, tiny) // 1 pt

    }
}