/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import junit.framework.TestCase
import org.junit.Test
import uk.co.nickthecoder.fizzy.model.controlpoint.BetweenControlPoint
import uk.co.nickthecoder.fizzy.model.controlpoint.WithinControlPoint
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestControlPoints
    : MyTestCase() {


    @Test
    fun testFree() {
        val doc = Document()
        val page = Page(doc)
        val box1 = createBox(page, "Dimension2(10mm,20mm)", "Dimension2(0mm,0mm)") // @ -5,-10 to 5, 10
        val free = WithinControlPoint("(0.5,0.5)")
        box1.controlPoints.add(free)

        TestCase.assertEquals(5.0, free.point.value.x.mm, tiny)
        TestCase.assertEquals(10.0, free.point.value.y.mm, tiny)

        free.dragTo(Vector2(6.0, 8.0) * onemm)
        TestCase.assertEquals(6.0, free.point.value.x.mm, tiny)
        TestCase.assertEquals(8.0, free.point.value.y.mm, tiny)

        box1.size.formula = (box1.size.value / 2.0).toFormula()
        TestCase.assertEquals(3.0, free.point.value.x.mm, tiny)
        TestCase.assertEquals(4.0, free.point.value.y.mm, tiny)

    }

    @Test
    fun testFree2() {
        val doc = Document()
        val page = Page(doc)
        val box1 = createBox(page, "Dimension2(10mm,20mm)", "Dimension2(0mm,0mm)") // @ -5,-10 to 5, 10
        val free = WithinControlPoint("(0.5,0.5)", "(4mm, 0mm)", "Size")
        box1.controlPoints.add(free)

        TestCase.assertEquals(7.0, free.point.value.x.mm, tiny)
        TestCase.assertEquals(10.0, free.point.value.y.mm, tiny)

        free.dragTo(Vector2(6.0, 8.0) * onemm)
        TestCase.assertEquals(6.0, free.point.value.x.mm, tiny)
        TestCase.assertEquals(8.0, free.point.value.y.mm, tiny)

        // Currently at (6mm,8mm). From 4mm,0mm -> 10mm,20mm. 2/6 , 8/20
        TestCase.assertEquals(2.0 / 6.0, free.position.value.x, tiny)
        TestCase.assertEquals(8.0 / 20.0, free.position.value.y, tiny)

        // After scaling, A remains the same (4mm,0mm) and B is now 5mm,10mm. Extent is 1mm,10mm
        // So point X should be 4 + 2/6 * 1
        // And Y = 0 + 8/20 * 10 = 4.0
        box1.size.formula = (box1.size.value / 2.0).toFormula()
        TestCase.assertEquals(4.0 + 2.0 / 6.0, free.point.value.x.mm, tiny)
        TestCase.assertEquals(4.0, free.point.value.y.mm, tiny)
    }

    @Test
    fun testLinear() {
        val doc = Document()
        val page = Page(doc)
        val box1 = createBox(page, "Dimension2(10mm,20mm)", "Dimension2(0mm,0mm)") // @ -5,-10 to 5, 10
        val linear = BetweenControlPoint("(0mm,0mm)", "(Size.X,0mm)", "0.5")
        box1.controlPoints.add(linear)

        TestCase.assertEquals(5.0, linear.point.value.x.mm, tiny)
        TestCase.assertEquals(0.0, linear.point.value.y.mm, tiny)

        // Note, Y is still 0mm, as we are dragging along a horizontal line.
        linear.dragTo(Vector2(6.0, 8.0) * onemm)
        TestCase.assertEquals(6.0, linear.point.value.x.mm, tiny)
        TestCase.assertEquals(0.0, linear.point.value.y.mm, tiny)
    }

}
