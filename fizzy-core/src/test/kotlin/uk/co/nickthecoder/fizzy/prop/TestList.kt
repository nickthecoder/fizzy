/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import org.junit.Test
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestList : MyTestCase() {

    @Test
    fun testList() {
        val empty = ListExpression("{}")
        assertEquals(0, empty.value.size)

        val size0 = DoubleExpression("{}.Size")
        assertEquals(0.0, size0.value)

        val size1 = DoubleExpression("{10}.Size")
        assertEquals(1.0, size1.value)
        assertEquals(10.0, DoubleExpression("{10,20}.get(0)").value)
        assertEquals(20.0, DoubleExpression("{10,20}.get(1)").value)

    }

}
