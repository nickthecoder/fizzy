/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.tools

import org.junit.Test
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.MockOtherActions
import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.Page


class TestBoundingBoxTool : TestTool() {

    val document = Document()
    val page = Page(document)
    val box1 = createBox(page, "Dimension2(4mm,4mm)", "Dimension2(6mm,10mm)")
    val box2 = createBox(page, "Dimension2(4mm,4mm)", "Dimension2(60mm,10mm)")

    init {
        page.children.add(box1)
        page.children.add(box2)
    }


    @Test
    fun testSimple() {
        // Select both boxes
        controller = Controller(page, null, MockOtherActions())
        clickAndDrag(0.0, 0.0, 200.0, 200.0)
        assertEquals("Selection Size", 2, controller.selection.size)
        assertEquals("Selection", box1, controller.selection[0])
        assertEquals("Selection", box2, controller.selection[1])

        // Select box1
        clickAndDrag(0.0, 0.0, 10.0, 20.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box1, controller.selection[0])

        // Select box2
        clickAndDrag(10.0, -10.0, 80.0, 20.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box2, controller.selection[0])

        // Select nothing
        clickAndDrag(0.0, 0.0, 5.0, 5.0)
        assertEquals("Selection Size", 0, controller.selection.size)

    }

    /**
     * This is the same as testSimple, but this time, we are only editing box2,
     * so while the mose movements are the same, the results will be different.
     */
    @Test
    fun testSingle() {
        // Around both boxes
        controller = Controller(page, box1, MockOtherActions())
        clickAndDrag(0.0, 0.0, 200.0, 200.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box1, controller.selection[0])

        // Around box1
        clickAndDrag(0.0, 0.0, 10.0, 20.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box1, controller.selection[0])

        // Around box2
        clickAndDrag(10.0, -10.0, 80.0, 20.0)
        assertEquals("Selection Size", 0, controller.selection.size)

        // Around nothing
        clickAndDrag(0.0, 0.0, 5.0, 5.0)
        assertEquals("Selection Size", 0, controller.selection.size)

    }


}
