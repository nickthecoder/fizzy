/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.tools

import org.junit.Test
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.MockOtherActions
import uk.co.nickthecoder.fizzy.controller.tools.DeleteTool
import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.Page


class TestDeleteTool : TestTool() {

    private val document = Document()
    private val page = Page(document)
    private val box1 = createBox(page, "Dimension2(4mm,4mm)", "Dimension2(6mm,10mm)")
    private val box2 = createBox(page, "Dimension2(4mm,4mm)", "Dimension2(60mm,10mm)")

    init {
        page.children.add(box1)
        page.children.add(box2)
    }


    @Test
    fun testSimple() {
        controller = Controller(page, null, MockOtherActions())
        controller.tool = DeleteTool(controller)

        // Select nothing
        click(0.0, 0.0)
        assertEquals("Shape Count", 2, page.children.size)

        // Select box1
        click(5.0, 11.0)
        assertEquals("Shape Count", 1, page.children.size)
        assertEquals("Shape", box2, page.children[0])

        // Select box2
        click(61.0, 11.0)
        assertEquals("Shape Count", 0, page.children.size)
    }

    /**
     * This is the same as testSimple, but this time, we are only editing box2.
     */
    @Test
    fun testSingle() {
        controller = Controller(page, box1, MockOtherActions())
        controller.tool = DeleteTool(controller)

        // Select nothing
        click(0.0, 0.0)
        assertEquals("Shape Count", 2, page.children.size)

        // Delete box1
        // Note. Maybe we shouldn't be able to delete the shape that we are editing???
        click(5.0, 11.0)
        assertEquals("Shape Count", 1, page.children.size)
        assertEquals("Shape", box2, page.children[0])

        // Select box2 - shouldn't delete it.
        click(61.0, 11.0)
        assertEquals("Shape Count", 1, page.children.size)

    }

    // TODO Test "entered" shapes.
}
