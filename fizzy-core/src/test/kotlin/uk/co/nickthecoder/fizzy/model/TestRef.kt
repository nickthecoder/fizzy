/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import org.junit.Test
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.DoubleExpression
import uk.co.nickthecoder.fizzy.prop.StringExpression
import uk.co.nickthecoder.fizzy.util.MyTestCase
import uk.co.nickthecoder.fizzy.util.toFormula

class TestRef : MyTestCase() {

    @Test
    fun testCustomProperty() {
        val doc = Document()
        val page = Page(doc)
        val box = createBox(page)

        val cp1 = CustomProperty("one", "One", CellType.STRING)
        val cp2 = CustomProperty("two", "Two", CellType.DOUBLE)
        box.customProperties.add(cp1)
        box.customProperties.add(cp2)

        cp1.data.value = "Hello"
        cp2.data.value = 3.0

        val scratch1 = Scratch("one", StringExpression("REF(CustomProperty.one)"), CellType.STRING)
        val scratch2 = Scratch("two", DoubleExpression("REF(CustomProperty.two)"), CellType.DOUBLE)
        box.scratches.add(scratch1)
        box.scratches.add(scratch2)

        assertEquals("CP1", "Hello", cp1.data.value as String)
        assertEquals("CP2", 3.0, cp2.data.value as Double)

        assertEquals("Scratch1", "Hello", scratch1.data.value)
        assertEquals("Scratch2", 3.0, scratch2.data.value)

        // Changing the referring expression should change the thing it is referring.

        (scratch1.data as StringExpression).value = "World"
        assertEquals("CP1", "World", cp1.data.value as String)
        assertEquals("Scratch1", "World", scratch1.data.value)

        (scratch2.data as DoubleExpression).value = 4.0
        assertEquals("CP2", 4.0, cp2.data.value as Double)
        assertEquals("Scratch2", 4.0, scratch2.data.value)

        // Changing the referred value should also change both.

        cp1.data.value = "Bye"
        assertEquals("CP1", "Bye", cp1.data.value as String)
        assertEquals("Scratch1", "Bye", scratch1.data.value)

        cp2.data.value = 5.0
        assertEquals("CP2", 5.0, cp2.data.value as Double)
        assertEquals("Scratch2", 5.0, scratch2.data.value)

    }

    @Test
    fun testScratch() {
        val doc = Document()
        val page = Page(doc)
        val box = createBox(page)

        val s1 = Scratch("one", StringExpression("Hello".toFormula()), CellType.STRING)
        val s2 = Scratch("two", DoubleExpression("3.0"), CellType.DOUBLE)
        box.scratches.add(s1)
        box.scratches.add(s2)

        val scratch1 = Scratch("refOne", StringExpression("REF(Scratch.one)"), CellType.STRING)
        val scratch2 = Scratch("refTwo", DoubleExpression("REF(Scratch.two)"), CellType.DOUBLE)
        box.scratches.add(scratch1)
        box.scratches.add(scratch2)

        assertEquals("S1", "Hello", s1.data.value as String)
        assertEquals("S2", 3.0, s2.data.value as Double)

        assertEquals("Scratch1", "Hello", scratch1.data.value)
        assertEquals("Scratch2", 3.0, scratch2.data.value)

        // Changing the referring expression should change the thing it is referring.

        (scratch1.data as StringExpression).value = "World"
        assertEquals("S1", "World", s1.data.value as String)
        assertEquals("Scratch1", "World", scratch1.data.value)

        (scratch2.data as DoubleExpression).value = 4.0
        assertEquals("S2", 4.0, s2.data.value as Double)
        assertEquals("Scratch2", 4.0, scratch2.data.value)

        // Changing the referred value should also change both.

        (s1.data as StringExpression).value = "Bye"
        assertEquals("S1", "Bye", s1.data.value as String)
        assertEquals("Scratch1", "Bye", scratch1.data.value)

        (s2.data as DoubleExpression).value = 5.0
        assertEquals("S2", 5.0, s2.data.value as Double)
        assertEquals("Scratch2", 5.0, scratch2.data.value)

    }


    @Test
    fun testGeometry() {
        val doc = Document()
        val page = Page(doc)
        val box = createBox(page)

        val scratch1 = Scratch("one", Dimension2Expression("REF(Geometry.Point1)"), CellType.STRING)
        val scratch2 = Scratch("two", Dimension2Expression("REF(Geometry.Point2)"), CellType.DOUBLE)
        box.scratches.add(scratch1)
        box.scratches.add(scratch2)

        assertEquals("G1", 0.0, box.geometry.parts[0].point.value.x.mm)
        assertEquals("G2", 10.0, box.geometry.parts[1].point.value.x.mm)

        assertEquals("Scratch1", 0.0, (scratch1.data.value as Dimension2).x.mm)
        assertEquals("Scratch2", 10.0, (scratch2.data.value as Dimension2).x.mm)

        // Changing the referring expression should change the thing it is referring.

        (scratch1.data as Dimension2Expression).value = Dimension2(Dimension(2.0, Units.mm), Dimension.ZERO_mm)
        assertEquals("G1", 2.0, box.geometry.parts[0].point.value.x.mm)
        assertEquals("Scratch1", 2.0, (scratch1.data as Dimension2Expression).value.x.mm)

        (scratch2.data as Dimension2Expression).value = Dimension2(Dimension(12.0, Units.mm), Dimension.ZERO_mm)
        assertEquals("G2", 12.0, box.geometry.parts[1].point.value.x.mm)
        assertEquals("Scratch2", 12.0, (scratch2.data as Dimension2Expression).value.x.mm)

        // Changing the referred value should also change both.

        (box.geometry.parts[0].point as Dimension2Expression).value = Dimension2(Dimension(1.0, Units.mm), Dimension.ZERO_mm)
        assertEquals("G1", 1.0, box.geometry.parts[0].point.value.x.mm)
        assertEquals("Scratch1", 1.0, (scratch1.data as Dimension2Expression).value.x.mm)

        (box.geometry.parts[1].point as Dimension2Expression).value = Dimension2(Dimension(11.0, Units.mm), Dimension.ZERO_mm)
        assertEquals("G2", 11.0, box.geometry.parts[1].point.value.x.mm)
        assertEquals("Scratch2", 11.0, (scratch2.data as Dimension2Expression).value.x.mm)

    }
}

