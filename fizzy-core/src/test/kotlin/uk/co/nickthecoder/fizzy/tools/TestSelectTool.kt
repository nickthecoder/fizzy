/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.tools

import org.junit.Test
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.MockOtherActions
import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.Page


class TestSelectTool : TestTool() {

    private val document = Document()
    private val page = Page(document)

    // Box1 goes from 40mm,80mm to 80mm,120mm
    private val box1 = createBox(page, "Dimension2(40mm,40mm)", "Dimension2(60mm,100mm)")
    private val box2 = createBox(page, "Dimension2(40mm,40mm)", "Dimension2(600mm,100mm)")

    // Box1a center is at 50mm,90mm. Extent is 46mm,86mm to 54mm,94mm
    private val box1a = createBox(box1, "Dimension2(8mm,8mm)", "Dimension2(10mm,10mm)")
    private val box1b = createBox(box1, "Dimension2(8mm,8mm)", "Dimension2(30mm,10mm)")

    init {
        page.children.add(box1)
        page.children.add(box2)

        box1.children.add(box1a)
        box1.children.add(box1b)
    }


    @Test
    fun testSimple() {
        controller = Controller(page, null, MockOtherActions())

        // Select nothing
        click(0.0, 0.0)
        assertEquals("Selection Size", 0, controller.selection.size)

        // Select box1
        click(50.0, 110.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box1, controller.selection[0])

        // Select box2
        click(610.0, 110.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box2, controller.selection[0])
    }

    /**
     * This is the same as testSimple, but this time, we are only editing box2,
     * so while the mose movements are the same, the results will be different.
     */
    @Test
    fun testSingle() {
        controller = Controller(page, box1, MockOtherActions())

        // Select nothing
        click(0.0, 0.0)
        assertEquals("Selection Size", 0, controller.selection.size)

        // Select box1
        click(50.0, 110.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box1, controller.selection[0])

        // Click box2 - should do nothing
        click(610.0, 110.0)
        assertEquals("Selection Size", 0, controller.selection.size)
    }

    fun testEnterShape() {
        controller = Controller(page, null, MockOtherActions())

        // Select box1
        click(50.0, 110.0)
        assertEquals("selected", 1, controller.selection.size)

        // Enter box 1
        val menu = menu(50.0, 110.0)
        val es = menu[0]
        assertEquals("Enter Shape", es.label)
        es.action()

        assertEquals("Nothing selected", 0, controller.selection.size)
        assertEquals("Parent", box1, controller.parent)

        // Box1a center is at 50mm,90mm. Extent is 46mm,86mm to 54mm,94mm
        click(50.0, 90.0)
        assertEquals("Selection Size", 1, controller.selection.size)
        assertEquals("Selection", box1a, controller.selection[0])

        // While entered, this should NOT select box1.
        click(40.0, 110.0)
        assertEquals("Selection Size", 0, controller.selection.size)
    }
}
