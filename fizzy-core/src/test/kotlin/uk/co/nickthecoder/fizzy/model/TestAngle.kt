/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import org.junit.Test
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestAngle : MyTestCase() {

    val zero = Angle.ZERO
    val ten = Angle.degrees(10.0)

    @Test
    fun testClamp() {

        assertEquals(4.0, Angle.degrees(4.0).clamp(zero, ten).degrees)
        assertEquals(6.0, Angle.degrees(6.0).clamp(zero, ten).degrees)
        assertEquals(0.0, Angle.degrees(-5.0).clamp(zero, ten).degrees)
        assertEquals(10.0, Angle.degrees(20.0).clamp(zero, ten).degrees)

        assertEquals(4.0, Angle.degrees(364.0).clamp(zero, ten).degrees, tiny)
        assertEquals(6.0, Angle.degrees(366.0).clamp(zero, ten).degrees, tiny)
        assertEquals(0.0, Angle.degrees(355.0).clamp(zero, ten).degrees, tiny)
        assertEquals(10.0, Angle.degrees(380.0).clamp(zero, ten).degrees, tiny)

        for (i in -350..-10 step 10) {
            assertEquals(i + 360.0, Angle.degrees(i.toDouble()).clamp(zero, Angle.TAU).degrees, tiny)
        }
        for (i in 0..350 step 10) {
            assertEquals(i.toDouble(), Angle.degrees(i.toDouble()).clamp(zero, Angle.TAU).degrees, tiny)
        }
    }

}