/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.tools

import uk.co.nickthecoder.fizzy.controller.CMenuItem
import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Units
import uk.co.nickthecoder.fizzy.util.MyTestCase

abstract class TestTool : MyTestCase() {

    lateinit var controller: Controller

    var dragStarted = false
    fun mm(x: Double, y: Double): Dimension2 = Dimension2(Dimension(x, Units.mm), Dimension(y, Units.mm))

    fun press(x: Double, y: Double) {
        controller.tool.onMousePressed(CMouseEvent(mm(x, y)))
    }

    fun drag(x: Double, y: Double) {
        if (!dragStarted) {
            dragStarted = true
            controller.tool.onDragDetected(CMouseEvent(mm(x, y)))
        }
        controller.tool.onMouseDragged(CMouseEvent(mm(x, y)))
    }

    fun release(x: Double, y: Double) {
        dragStarted = false
        controller.tool.onMouseReleased(CMouseEvent(mm(x, y)))
    }

    fun clickAndDrag(x1: Double, y1: Double, x2: Double, y2: Double, steps: Int = 1) {
        press(x1, y1)
        for (i in 1..steps) {
            drag(x1 + (x2 - x1) * i / steps, y1 + (y2 - y2) * i / steps)
        }
        release(x2, y2)
    }

    fun click(x: Double, y: Double) {
        press(x, y)
        release(x, y)
        controller.tool.onMouseClicked(CMouseEvent(mm(x, y)))
    }

    fun menu(x: Double, y: Double): List<CMenuItem> {
        return controller.tool.onContextMenu(CMouseEvent(mm(x, y), button = 2))
    }

}
