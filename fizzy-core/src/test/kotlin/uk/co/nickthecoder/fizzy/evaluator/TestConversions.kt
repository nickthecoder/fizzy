/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.evaluator

import org.junit.Test
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.util.MyTestCase

/**
 * Tests the conversion methods localToParent, localToPage, parentToLocal and pageToLocal.
 */
class TestConversions : MyTestCase() {

    @Test
    fun testTranslate() {
        val doc = Document()
        val page = Page(doc)
        val box = createBox(page, "Dimension2(10mm,10mm)", "Dimension2(15mm,25mm)")

        box.scratches.add(Scratch("parentTopLeft", Dimension2Expression("localToParent(Geometry.Point1)"), CellType.DIMENSION2))
        box.scratches.add(Scratch("pageTopLeft", Dimension2Expression("localToPage(Geometry.Point1)"), CellType.DIMENSION2))
        box.scratches.add(Scratch("localTopLeft", Dimension2Expression("parentToLocal(Scratch.parentTopLeft)"), CellType.DIMENSION2))
        box.scratches.add(Scratch("localTopLeft2", Dimension2Expression("pageToLocal(Scratch.pageTopLeft)"), CellType.DIMENSION2))

        val parentTopLeft = box.scratches[0].data.value as Dimension2
        assertEquals(10.0, parentTopLeft.x.mm, tiny)
        assertEquals(20.0, parentTopLeft.y.mm, tiny)

        val pageTopLeft = box.scratches[1].data.value as Dimension2
        assertEquals(10.0, pageTopLeft.x.mm, tiny)
        assertEquals(20.0, pageTopLeft.y.mm, tiny)

        val localTopLeft = box.scratches[2].data.value as Dimension2
        assertEquals(0.0, localTopLeft.x.mm, tiny)
        assertEquals(0.0, localTopLeft.y.mm, tiny)

        val localTopLeft2 = box.scratches[3].data.value as Dimension2
        assertEquals(0.0, localTopLeft2.x.mm, tiny)
        assertEquals(0.0, localTopLeft2.y.mm, tiny)

    }

    // TODO Add tests with a box inside another box, so that page and parent are different.
    // TODO Add tests with a scaled shape???
}