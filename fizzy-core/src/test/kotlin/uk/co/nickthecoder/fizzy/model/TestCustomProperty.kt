/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import org.junit.Test
import uk.co.nickthecoder.fizzy.prop.DoubleExpression
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestCustomProperty : MyTestCase() {

    @Test
    fun testScratch() {
        val doc = Document()
        val page = Page(doc)
        val box = createBox(page)

        box.customProperties.add(CustomProperty("foo", "Foo", CellType.DOUBLE))
        box.customProperties.add(CustomProperty("bar", "Bar", CellType.DOUBLE))

        box.customProperties[0].data.value = 10.0
        box.customProperties[1].data.value = 20.0

        // Check the values directly
        assertEquals(10.0, box.customProperties[0].data.value)
        assertEquals(20.0, box.customProperties[1].data.value)

        box.scratches.add(Scratch("ref1a", DoubleExpression("CustomProperty.foo"), CellType.DOUBLE))
        box.scratches.add(Scratch("ref1b", DoubleExpression("CustomProperties.foo.Value"), CellType.DOUBLE))

        // Check the values via a Scratch
        assertEquals(10.0, box.scratches[0].data.value)
        assertEquals(10.0, box.scratches[1].data.value)

        // Swap the names of foo and bar
        box.customProperties[0].name.value = "bar"
        box.customProperties[1].name.value = "foo"

        assertEquals(10.0, box.customProperties[0].data.value)
        assertEquals(20.0, box.customProperties[1].data.value)

        assertEquals(20.0, box.scratches[0].data.value)
        // The following assert used to fail, but forcing a re-evaluation of the expression made it work.
        assertEquals(20.0, box.scratches[1].data.value)

        // Change the values
        box.customProperties[0].data.value = 1.0
        box.customProperties[1].data.value = 2.0

        assertEquals(1.0, box.customProperties[0].data.value)
        assertEquals(2.0, box.customProperties[1].data.value)

        assertEquals(2.0, box.scratches[0].data.value)
        assertEquals(2.0, box.scratches[1].data.value)

    }

}
