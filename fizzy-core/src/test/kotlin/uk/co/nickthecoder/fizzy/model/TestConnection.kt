/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import org.junit.Test
import uk.co.nickthecoder.fizzy.model.geometry.LineTo
import uk.co.nickthecoder.fizzy.model.geometry.MoveTo
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestConnection : MyTestCase() {

    fun createConnectedBox(parent: ShapeParent, size: String, at: String): Shape2d {
        val box = createBox(parent, size, at)
        box.connectionPoints.add(ConnectionPoint("(Size.X/2, 0mm)").apply { angle.formula = "90 deg" })
        box.connectionPoints.add(ConnectionPoint("(Size.X/2, Size.Y)").apply { angle.formula = "-90 deg" })
        box.connectionPoints.add(ConnectionPoint("(0mm, Size.Y/2)").apply { angle.formula = "180 deg" })
        box.connectionPoints.add(ConnectionPoint("(Size.X, Size.Y/2)").apply { angle.formula = "0 deg" })

        return box
    }

    private fun createSmartLine(parent: ShapeParent): Shape1d {
        val line = createLine(parent, "(0mm, 0mm)", "(10mm,0mm)")
        (line.geometry.parts[0] as MoveTo).point.formula = "SmartStart"
        (line.geometry.parts[1] as LineTo).point.formula = "EndStart"

        line.scratches.add(Scratch(CellType.ANGLE).apply { data.formula = "StartAngle" })
        line.scratches.add(Scratch(CellType.ANGLE).apply { data.formula = "EndAngle" })

        return line
    }

    @Test
    fun testConnectTo() {
        val doc = Document()
        val page = Page(doc)

        val box1 = createConnectedBox(page, "(10mm,10mm)", "(100mm,100mm)")
        val box2 = createConnectedBox(page, "(10mm,10mm)", "(150mm,100mm)")

        page.children.add(box1)
        page.children.add(box2)

        val line = createSmartLine(page)
        line.start.formula = "connectTo( Page.Shape${box1.id}.ConnectionPoint4, StartMargin )"
        line.end.formula = "connectTo( Page.Shape${box2.id}.ConnectionPoint3, EndMargin )"

        page.children.add(line)

        // Make sure the simple connection works.
        assertEquals(105.0, box1.localToPage(box1.connectionPoints[3].point.value).x.mm)
        assertEquals(100.0, box1.localToPage(box1.connectionPoints[3].point.value).y.mm)

        assertEquals(105.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(145.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.startAngle.value.degrees, tiny)
        assertEquals(180.0, line.endAngle.value.degrees, tiny)

        // smart start/end should be not in effect (i.e. 0 and (Length, 0))
        assertEquals(0.0, line.smartStart.value.x.mm)
        assertEquals(40.0, line.smartEnd.value.x.mm)

        // Move box1 right by 20mm (the line's start and smartEnd should change), everything else stays the same.
        box1.transform.pin.formula = "(120mm, 100mm)"

        assertEquals(125.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(145.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.smartStart.value.x.mm)
        assertEquals(20.0, line.smartEnd.value.x.mm) // The line is shorter now

        // Move box2 right by 30mm (the line's end and smartEnd should change), everything else stays the same.
        box2.transform.pin.formula = "(180mm, 100mm)"

        assertEquals(125.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(175.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.smartStart.value.x.mm)
        assertEquals(50.0, line.smartEnd.value.x.mm) // The line is longer now
    }


    /**
     * The connects to the same place as [testConnectTo], but connects to the geometry, rather than the control points.
     * The positions will be different, as the start and ends will be the middle of the boxes, and the smartStart/end
     * will be non-trivial.
     */
    @Test
    fun testConnectAround() {
        val doc = Document()
        val page = Page(doc)

        val box1 = createConnectedBox(page, "(10mm,10mm)", "(100mm,100mm)")
        val box2 = createConnectedBox(page, "(10mm,10mm)", "(150mm,100mm)")

        page.children.add(box1)
        page.children.add(box2)

        val line = createSmartLine(page)
        line.start.formula = "connectAround( Page.Shape${box1.id}.Geometry, 0.25 + 0.125, StartMargin )"
        line.end.formula = "connectAround( Page.Shape${box2.id}.Geometry, 0.75 + 0.125, EndMargin )"

        page.children.add(line)

        assertEquals(105.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(145.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.startAngle.value.degrees, tiny)
        assertEquals(180.0, line.endAngle.value.degrees, tiny)

        // smart start/end should be not in effect (i.e. 0 and (Length, 0))
        assertEquals(0.0, line.smartStart.value.x.mm)
        assertEquals(40.0, line.smartEnd.value.x.mm)

        // Move box1 right by 20mm (the line's start and smartEnd should change), everything else stays the same.
        box1.transform.pin.formula = "(120mm, 100mm)"

        assertEquals(125.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(145.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.smartStart.value.x.mm)
        assertEquals(20.0, line.smartEnd.value.x.mm) // The line is shorter now

        // Move box2 right by 30mm (the line's end and smartEnd should change), everything else stays the same.
        box2.transform.pin.formula = "(180mm, 100mm)"

        assertEquals(125.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(175.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.smartStart.value.x.mm)
        assertEquals(50.0, line.smartEnd.value.x.mm) // The line is longer now
    }


    /**
     * The connects to the same place as [testConnectTo], but uses connectToShape.
     * As it connects to the same point, the positions and angles are the same.
     */
    @Test
    fun testConnectToShape() {
        val doc = Document()
        val page = Page(doc)

        val box1 = createConnectedBox(page, "(10mm,10mm)", "(100mm,100mm)")
        val box2 = createConnectedBox(page, "(10mm,10mm)", "(150mm,100mm)")

        page.children.add(box1)
        page.children.add(box2)

        val line = createSmartLine(page)
        line.start.formula = "connectToShape( Page.Shape${box1.id}, true, StartMargin )"
        line.end.formula = "connectToShape( Page.Shape${box2.id}, false, EndMargin )"

        page.children.add(line)

        // Make sure the simple connection works.
        assertEquals(100.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(150.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.startAngle.value.degrees, tiny)
        assertEquals(180.0, line.endAngle.value.degrees, tiny)

        assertEquals(5.0, line.smartStart.value.x.mm)
        assertEquals(45.0, line.smartEnd.value.x.mm)

        // Move box1 right by 20mm (the line's start and smartEnd should change), everything else stays the same.
        box1.transform.pin.formula = "(120mm, 100mm)"

        assertEquals(120.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(150.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(5.0, line.smartStart.value.x.mm)
        assertEquals(25.0, line.smartEnd.value.x.mm) // The line is shorter now

        // Move box2 right by 30mm (the line's end and smartEnd should change), everything else stays the same.
        box2.transform.pin.formula = "(180mm, 100mm)"

        assertEquals(120.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(180.0, line.end.value.x.mm)
        assertEquals(100.0, line.end.value.y.mm)

        assertEquals(0.0, line.startAngle.value.degrees, tiny) // Still the same as before.
        assertEquals(180.0, line.endAngle.value.degrees, tiny)
        assertEquals(0.0, (line.scratches[0].data.value as Angle).degrees, tiny) // Same as above, but via a Scratch.
        assertEquals(180.0, (line.scratches[1].data.value as Angle).degrees, tiny)

        assertEquals(5.0, line.smartStart.value.x.mm)
        assertEquals(55.0, line.smartEnd.value.x.mm) // The line is longer now

        // Now move box2, such that the point 'along' changes, and also the angle of the connections should change.
        box2.transform.pin.formula = "(120mm,150mm)"
        // box2 is now below box1

        assertEquals(120.0, line.start.value.x.mm)
        assertEquals(100.0, line.start.value.y.mm)

        assertEquals(120.0, line.end.value.x.mm)
        assertEquals(150.0, line.end.value.y.mm)

        assertEquals(-90.0, line.startAngle.value.degrees, tiny)
        assertEquals(90.0, line.endAngle.value.degrees, tiny)
        assertEquals(-90.0, (line.scratches[0].data.value as Angle).degrees, tiny) // Same as above, but via a Scratch.
        assertEquals(90.0, (line.scratches[1].data.value as Angle).degrees, tiny)

        assertEquals(5.0, line.smartStart.value.x.mm, tiny)
        assertEquals(45.0, line.smartEnd.value.x.mm, tiny)

    }

}
