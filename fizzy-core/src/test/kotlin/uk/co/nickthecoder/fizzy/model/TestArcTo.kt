/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import org.junit.Test
import uk.co.nickthecoder.fizzy.model.geometry.ArcTo
import uk.co.nickthecoder.fizzy.model.geometry.MoveTo
import uk.co.nickthecoder.fizzy.util.MyTestCase

class TestArcTo : MyTestCase() {

    @Test
    fun testSemi() {
        val doc = Document()
        val page = Page(doc)
        val shape = Shape2d.create(page)

        shape.geometry.parts.add(MoveTo("(0mm,0mm)"))
        val arc = ArcTo("(10mm,0mm)", "5mm")
        shape.geometry.parts.add(arc)

        assertEquals("Center x", 5.0, arc.center.value.x.mm)
        assertEquals("Center y", 0.0, arc.center.value.y.mm)

        assertEquals("Radius", 5.0, arc.radius.value.mm)
        assertEquals("Sweep", 180.0, arc.sweep.value.degrees, tiny)
        assertEquals("from", 180.0, Math.abs(arc.fromAngle.value.degrees), tiny)
        assertEquals("to", 0.0, arc.toAngle.value.degrees, tiny)
    }


    @Test
    fun testQuarter() {
        val doc = Document()
        val page = Page(doc)
        val shape = Shape2d.create(page)
        val ONE = Dimension(1.0, Units.mm)

        shape.geometry.parts.add(MoveTo("(0mm,10mm)"))
        val arc = ArcTo("(10mm,0mm)", "(10 - sqrt(50)) mm")
        shape.geometry.parts.add(arc)

        assertEquals("Radius", 10.0, arc.radius.value.mm, tiny)

        assertEquals("Center x", 0.0, arc.center.value.x.mm, tiny)
        assertEquals("Center y", 0.0, arc.center.value.y.mm, tiny)

        assertEquals("from", -90.0, arc.fromAngle.value.degrees, tiny)
        assertEquals("to", 0.0, arc.toAngle.value.degrees, tiny)
        assertEquals("Sweep", 90.0, arc.sweep.value.degrees, tiny)

        assertEquals("isAt", true, arc.isAlong(Dimension2(Dimension(9.99, Units.mm), Dimension.ZERO_mm), ONE, ONE))
        assertEquals("isAt", true, arc.isAlong(Dimension2(Dimension.ZERO_mm, Dimension(9.99, Units.mm)), ONE, ONE))
    }

}
