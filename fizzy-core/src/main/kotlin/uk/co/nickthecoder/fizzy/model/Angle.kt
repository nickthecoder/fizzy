/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.util.terse
import uk.co.nickthecoder.fizzy.util.toFormula

class Angle private constructor(val radians: Double) : FizzyType {

    val degrees: Double
        get() = Math.toDegrees(radians)

    operator fun plus(other: Angle) = Angle.radians(radians + other.radians)

    operator fun minus(other: Angle) = Angle.radians(radians - other.radians)

    operator fun unaryMinus() = Angle.radians(-radians)

    operator fun times(other: Double) = Angle.radians(radians * other)

    operator fun div(other: Double) = Angle.radians(radians / other)

    operator fun div(other: Angle) = radians / other.radians

    fun unitVector(): Vector2 = Vector2(1.0, 0.0).rotate(this)

    fun unitDimension(): Dimension2 = Dimension2(Dimension.ONE_mm, Dimension.ZERO_mm).rotate(this)

    fun normalUnitVector(): Vector2 = Vector2(0.0, -1.0).rotate(this)

    fun normalUnitDimension(): Dimension2 = (Vector2(0.0, -1.0) * Dimension.ONE_mm).rotate(this)

    /**
     * Returns an angle in the range -PI (exclusive) to PI (inclusive).
     */
    fun normalise(): Angle {
        return when {
            radians <= Math.PI -> {
                val rad = (radians.rem(Math.PI * 2.0) + Math.PI * 2.0).rem(Math.PI * 2.0)
                Angle(if (rad > Math.PI) rad - Math.PI * 2 else rad)
            }
            radians > Math.PI -> {
                val rad = radians.rem(Math.PI * 2.0)
                Angle(if (rad > Math.PI) rad - Math.PI * 2 else rad)
            }
            else -> this
        }
    }

    override fun hashCode() = 23 * radians.hashCode()

    override fun equals(other: Any?): Boolean {
        return other is Angle && other.radians == this.radians
    }

    override fun toFormula(): String = "${degrees.toFormula()} deg"


    fun clamp(min: Angle, max: Angle): Angle {
        if (min.radians <= radians && max.radians >= radians) return this
        // Clamping an angle isn't as simple as doing min and max, because of the cyclic nature of angles.

        // The angle relative to the minimum angle in the range 0..360°
        val rel = radians0ToTau(radians - min.radians)
        val range = max.radians - min.radians

        return if (rel < range) {
            // We are within the range given.
            Angle.radians(min.radians + rel)

        } else {
            val maxRel = rel - range
            if (Math.abs(radiansNegPiToPi(maxRel)) > Math.abs(radiansNegPiToPi(rel))) { // Are we closer to min than max?
                min
            } else {
                max
            }
        }
    }

    fun zeroToTau(): Angle = Angle.radians(radians0ToTau(radians))

    fun negPiToPi(): Angle = Angle.radians(radiansNegPiToPi(radians))

    fun isWithin(fromAngle: Angle, toAngle: Angle): Boolean = isRadiansWithin(radians, fromAngle.radians, toAngle.radians)

    override fun toString(): String = "${degrees.terse()} deg"

    companion object {

        fun degrees(d: Double) = Angle(Math.toRadians(d))

        fun radians(r: Double) = Angle(r)

        val ZERO = Angle(0.0)
        val PI = Angle(Math.PI)
        val NEG_PI = Angle(-Math.PI)
        val TAU = Angle(Math.PI * 2.0)

        /**
         * Returns the value so that it is in the range 0..TAU.
         * e.g. normaliseRadians(-1.0) == TAU - 1
         * This does NOT clamp the value.
         */
        fun radians0ToTau(radians: Double): Double {
            val n = radians.rem(Math.PI * 2)
            return if (n < 0) Math.PI * 2 + n else n
        }

        /**
         * Returns the value so that it is in the range -PI..PI
         * This does NOT clamp the value.
         */
        fun radiansNegPiToPi(radians: Double): Double {
            val a = radians0ToTau(radians)
            return if (a > Math.PI) a - Math.PI * 2.0 else a
        }


        fun isRadiansWithin(angle: Double, fromAngle: Double, toAngle: Double): Boolean {
            //println("If ${angle * 180 / Math.PI} within ${fromAngle * 180 / Math.PI} .. ${toAngle * 180 / Math.PI}")
            if (fromAngle <= angle && toAngle >= angle) return true

            val rel = radians0ToTau(angle - fromAngle)
            val range = radians0ToTau(toAngle - fromAngle)

            //println("rel=${rel * 180 / Math.PI} range=${range * 180 / Math.PI} Therefore ${rel <= range}")
            return rel <= range
        }
    }

}
