/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

abstract class PropDependency<T : Any>(
        val description: String)

    : AbstractProp<T>(), PropListener {

    var dirty: Boolean = true
        set(v) {
            if (field != v) {
                field = v
                if (v) {
                    notifyListeners()
                }
            }
        }

    private var foundProp: Prop<T>? = null
        set(v) {
            if (v !== field) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    abstract fun findProp(): Prop<T>

    private fun eval(): T {
        val v = findProp()
        foundProp = v
        dirty = false
        return v.value
    }

    override val value: T
        get() = foundProp?.value ?: eval()

    override fun dirty(prop: Prop<*>) {
        dirty = true
    }

    override fun toString() = description

}

class PropDependency1<T : Any, A : Any>(
        description: String,
        val a: Prop<A>,
        val lambda: (A) -> Prop<T>)

    : PropDependency<T>(description) {

    init {
        a.addListener(this)
    }

    override fun findProp(): Prop<T> {
        return lambda(a.value)
    }

}

class PropDependency2<T : Any, A : Any, B : Any>(
        description: String,
        val a: Prop<A>,
        val b: Prop<B>,
        val lambda: (A, B) -> Prop<T>)

    : PropDependency<T>(description) {

    init {
        a.addListener(this)
        b.addListener(this)
    }

    override fun findProp(): Prop<T> {
        return lambda(a.value, b.value)
    }

}
