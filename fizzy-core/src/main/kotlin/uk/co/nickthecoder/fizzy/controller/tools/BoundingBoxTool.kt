/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.tools

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Page
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * Drags a rectangle, and selects all shapes wholly contained within it.
 */
class BoundingBoxTool(controller: Controller, event: CMouseEvent, private val startPoint: Dimension2)
    : Tool(controller) {

    var endPoint = startPoint

    val selection = controller.selection

    init {
        if (!event.isAdjust) {
            selection.clear()
        }
    }

    override fun onMouseDragged(event: CMouseEvent) {
        endPoint = event.point
        controller.dirty.value++
    }

    override fun onMouseReleased(event: CMouseEvent) {
        val singleShape = controller.singleShape
        if (singleShape == null || controller.parent !is Page) {
            controller.parent.children.filter { controller.isWithin(it, startPoint, event.point) }.forEach {
                selection.add(it)
            }
        } else {
            if (controller.isWithin(singleShape, startPoint, event.point)) {
                selection.add(singleShape)
            }
        }
        controller.tool = SelectTool(controller)
        controller.dirty.value++
    }

    override fun draw(dc: DrawContext) {
        dc.use {
            dc.lineColor(Controller.BOUNDING_STROKE)
            dc.fillColor(Controller.BOUNDING_FILL)
            dc.lineWidth(2.0 / controller.scale)
            dc.lineDashes(5.0 / controller.scale)
            dc.rectangle(true, true, startPoint, endPoint)
        }
    }
}
