/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.geometry.Geometry
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation1
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType

class Connect2dAround(prop: Prop<ConnectionPoint>)
    : TypedMethod2<ConnectionPoint, Geometry, Double, Dimension2>("Connect2dAround", prop, Geometry::class.java, Double::class.java),
        Connection,
        ChangeListener<Geometry> {

    private var along: Double = 0.0

    private var myShape: Shape? = null
        set(v) {
            if (field !== v) {
                //field?.transform?.fromLocalToPage?.removeListener(this)
                field = v
                //v?.transform?.fromLocalToPage?.addListener(this)
            }
        }

    private var connectTo: Geometry? = null
        set(v) {
            if (field !== v) {
                field?.changeListeners?.remove(this)
                field?.shape?.transform?.fromPageToLocal?.removeListener(this)
                field?.shape?.transform?.pin?.removeListener(this)

                field = v

                v?.changeListeners?.add(this)
                v?.shape?.transform?.fromPageToLocal?.addListener(this)
                // Why is this needed? The above should be sufficient?
                // Without it, the connection only works sporadically while dragging the box that the line is connected to.
                v?.shape?.transform?.pin?.addListener(this)

                // Force evaluation. I'm not sure why this is needed, but removing causes TestConnections.testConnectAlong to fail.
                v?.shape?.transform?.fromPageToLocal?.value
            }
        }

    override val angle = PropCalculation1("ConnectAround.angle", true, this) {
        val normal = connectTo?.normalAlong(along)
        val otherShape = connectTo?.shape

        if (normal == null || otherShape == null) {
            Angle.ZERO
        } else {
            otherShape.localToPage(normal).normalise()
        }
    }


    init {
        selfDependants(1) // angle
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()

        angle.noLongerNeeded()

        connectTo = null
        myShape = null
    }

    override fun changed(item: Geometry, changeType: ChangeType, obj: Any?) {
        dirty = true
    }

    override fun eval(a: Geometry, b: Double): Dimension2 {
        myShape = prop.value.shape
        connectTo = a
        along = b
        val pagePoint = a.shape.localToPage(a.pointAlong(b))

        val offset = prop.value.point.value - myShape!!.transform.locPin.value

        // Adjust offset for rotation, scale, flip etc. Cannot use localToParent, or localToPage, because
        // that would result in recursion, as it uses pin
        var matrix = Matrix33.identity
        val t = myShape!!.transform
        when (t) {
            is Shape2dTransform -> {
                if (t.flipX.value || t.flipY.value) matrix *= Matrix33.flip(t.flipX.value, t.flipY.value)
                if (t.rotation.value.radians != 0.0) matrix *= Matrix33.rotate(t.rotation.value)
                if (t.scale.value.x != 1.0 || t.scale.value.y != 1.0) matrix *= Matrix33.scale(t.scale.value)
            }
            is Shape1dTransform -> {
                if (t.rotation.value.radians != 0.0) matrix *= Matrix33.rotate(t.rotation.value)
            }
        }

        return myShape!!.parent.pageToLocal(pagePoint - matrix * offset)
    }

    override fun connectedTo(): Shape {
        value // Force evaluation
        return connectTo?.shape ?: myShape!!
    }

}
