/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.util

/**
 * Quick and dirty XML output.
 */
class SimpleXML {

    private val buffer = StringBuffer()

    private var tagDepth = 0

    private var tagNeedsClosing = false

    private var tightTag = false

    fun beginTag(tag: String, tight: Boolean = false) {
        if (tagNeedsClosing) {
            closeTag()
        }
        if (!tightTag) {
            buffer.append("  ".repeat(tagDepth))
        }
        tightTag = tight
        buffer.append("<$tag ")
        tagNeedsClosing = true
        tagDepth++
    }

    fun closeTag() {
        buffer.append(">")
        if (!tightTag) {
            buffer.append("\n")
        }
        tagNeedsClosing = false
    }

    fun attribute(name: String, value: String) {
        if (!tightTag) {
            buffer.append("\n").append("  ".repeat(tagDepth + 1))
        }
        buffer.append(name).append("=\"").append(escapeAttribute(value)).append("\" ")
    }

    fun endTag() {
        if (!tagNeedsClosing) throw RuntimeException("Tag didn't need closing")

        tagDepth--
        tagNeedsClosing = false
        buffer.append("/>")
        if (!tightTag) {
            buffer.append("\n")
        }
    }

    fun endTag(tag: String, tight: Boolean = false) {
        if (tagNeedsClosing) closeTag()
        tagDepth--

        if (!tightTag) {
            buffer.append("  ".repeat(tagDepth))
        }

        buffer.append("</$tag>")

        tightTag = tight
        if (!tightTag) {
            buffer.append("\n")
        }
    }

    fun text(str: String) {
        if (tagNeedsClosing) closeTag()
        // TODO Escape
        buffer.append(str)
    }

    // TODO
    private fun escapeAttribute(value: String): String = value

    override fun toString() = buffer.toString()
}