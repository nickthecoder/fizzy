/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.ArgList

/**
 * T is the type of the receiver object.
 * R is the type of the return value from the method.
 */
abstract class PropMethod<T : Any, R : Any>(description: String, val prop: Prop<T>)
    : PropCalculation<R>(description, false) {

    protected var arg: Prop<*>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    init {
        prop.addListener(this)
    }

    override fun isConstant() = prop.isConstant() && arg?.isConstant() == true

    fun applyArgs(arg: Prop<*>) {
        if (this.arg !== arg) {
            this.arg = arg
        }
    }

    override fun eval(): R {

        arg?.let {
            return eval(it)
        }
        throw RuntimeException("Arguments not supplied")
    }

    abstract fun eval(arg: Prop<*>): R

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        prop.removeListener(this)
        arg?.removeListener(this)
    }

}

/**
 * A method, which has no arguments.
 * T is the type of the receiver object.
 */
open class PropMethod0<T : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val lambda: () -> R)

    : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {
        if (arg is ArgList && arg.value.isEmpty()) {
            return lambda()
        }
        throw RuntimeException("Expected no arguments, but found $arg")
    }
}

/**
 * A method which has 1 argument.
 * T is the type of the receiver object.
 * A is the type of the method's argument.
 */
open class PropMethod1<T : Any, A : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val klassA: Class<A>,
        private val lambda: (A) -> R)

    : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {

        klassA.assertIsInstance(arg.value, description)

        @Suppress("UNCHECKED_CAST")
        return lambda(arg.value as A)
    }
}

/**
 * A method which has 2 arguments.
 * T is the type of the receiver object.
 * A and B are the types of the method's arguments.
 */
open class PropMethod2<T : Any, A : Any, B : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val klassA: Class<A>,
        private val klassB: Class<B>,
        private val lambda: (A, B) -> R)

    : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {
        if (arg is ArgList && arg.value.size == 2) {
            val a = arg.value[0].value
            val b = arg.value[1].value

            klassA.assertIsInstance(a, description, "Argument #1")
            klassB.assertIsInstance(b, description, "Argument #2")

            @Suppress("UNCHECKED_CAST")
            return lambda(a as A, b as B)
        }
        throw RuntimeException("Expected 2 arguments")
    }
}

/**
 * A method which has 2 arguments.
 * T is the type of the receiver object.
 * A and B are the types of the method's arguments.
 */
open class PropMethod3<T : Any, A : Any, B : Any, C : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val klassA: Class<A>,
        private val klassB: Class<B>,
        private val klassC: Class<C>,
        private val lambda: (A, B, C) -> R)

    : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {
        if (arg is ArgList && arg.value.size == 3) {
            val a = arg.value[0].value
            val b = arg.value[1].value
            val c = arg.value[2].value

            klassA.assertIsInstance(a, description, "Argument #1")
            klassB.assertIsInstance(b, description, "Argument #2")
            klassC.assertIsInstance(c, description, "Argument #3")

            @Suppress("UNCHECKED_CAST")
            return lambda(a as A, b as B, c as C)
        }
        throw RuntimeException("Expected 3 arguments")
    }
}

/**
 * A method which has 2 arguments.
 * T is the type of the receiver object.
 * A and B are the types of the method's arguments.
 */
open class PropMethod4<T : Any, A : Any, B : Any, C : Any, D : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val klassA: Class<A>,
        private val klassB: Class<B>,
        private val klassC: Class<C>,
        private val klassD: Class<D>,
        private val lambda: (A, B, C, D) -> R)

    : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {
        if (arg is ArgList && arg.value.size == 4) {
            val a = arg.value[0].value
            val b = arg.value[1].value
            val c = arg.value[2].value
            val d = arg.value[3].value

            klassA.assertIsInstance(a, description, "Argument #1")
            klassB.assertIsInstance(b, description, "Argument #2")
            klassC.assertIsInstance(c, description, "Argument #3")
            klassD.assertIsInstance(d, description, "Argument #4")

            @Suppress("UNCHECKED_CAST")
            return lambda(a as A, b as B, c as C, d as D)
        }
        throw RuntimeException("Expected 4 arguments")
    }
}
