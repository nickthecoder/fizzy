/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.PropCalculation2

/**
 * A control point, which is free to be positioned anywhere.
 * Internally the position is stored as a distance [delta] from a given position [relativeTo].
 * Therefore [point] = [relativeTo] + [delta].
 *
 * The advantage of having a delta from a relative position, is so that [relativeTo] can be repositioned and
 * [point] will follow appropriately.
 *
 * However, [relativeTo] is often (0mm, 0mm)
 *
 * Consider [WithinControlPoint] as an alternative, which is useful when you want the control point to adjust
 * in proportion when the shape is scaled.
 */
class StandardControlPoint(
        val relativeTo: Dimension2Expression,
        val delta: Dimension2Expression)

    : ControlPoint(ControlPointType.STANDARD) {

    constructor(relativeTo: String, delta: String = "(0mm,0mm)") : this(Dimension2Expression(relativeTo), Dimension2Expression(delta))

    constructor(relativeTo: Dimension2 = Dimension2.ZERO_mm, delta: Dimension2 = Dimension2.ZERO_mm) : this(Dimension2Expression(relativeTo), Dimension2Expression(delta))


    override val point = PropCalculation2<Dimension2, Dimension2, Dimension2>("StandardControlPoint.point", true, relativeTo, delta) { vRel, vDelta ->
        vRel + vDelta
    }

    val min = Dimension2Expression("MIN_DIMENSION2")

    val max = Dimension2Expression("MAX_DIMENSION2")

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        relativeTo.context = context
        delta.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("RelativeTo", relativeTo, "A")
        metaData.newCell("Delta", delta, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        delta.value = (draggedTo - relativeTo.value).clamp(min.value, max.value)
    }

    override fun copy(link: Boolean): StandardControlPoint {
        val result = StandardControlPoint(relativeTo.copy(link), delta.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
