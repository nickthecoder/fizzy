/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.util.ChangeListeners
import uk.co.nickthecoder.fizzy.util.HasChangeListeners

/**
 * An empty class definition, subclassing MutableFList<CustomProperty>.
 * See [CustomPropertyListPropType] for details on why this exists.
 */
class CustomPropertyList : MutableFList<CustomProperty>()

class CustomProperty(name: String, label: String = name, cellType: CellType)
    : HasChangeListeners<CustomProperty>, PropListener, MetaDataAware, NamedWithData {

    val label = PropVariable(label)

    val type = PropVariable(cellType)

    override val data = cellType.variable()

    override val changeListeners = ChangeListeners<CustomProperty>()

    override val name = PropVariable(name)

    val visible = BooleanExpression(true)

    /**
     * For Boolean types only. Should this custom property appear in the shape's context menu?
     */
    val isMenu = BooleanExpression(false)

    constructor(name: String, label: String, cellType: CellType, value: Any) : this(name, label, cellType) {
        data.value = value
    }

    private val typeListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            data.value = type.value.variable().value
        }
    }

    init {
        type.addListener(typeListener)

        this.name.addListener(this)
        this.label.addListener(this)
        this.data.addListener(this)
        this.visible.addListener(this)
    }

    fun setContext(context: EvaluationContext) {
        isMenu.context = context
        visible.context = context
    }

    override fun metaData(): MetaData {
        val md = MetaData(null)
        addMetaData(md)
        return md
    }

    fun addMetaData(metaData: MetaData) {
        metaData.newCell("Name", name)
        metaData.newCell("Type", type)
        metaData.newCell("Value", data)
        metaData.newCell("Label", label)
        if (type.value == CellType.BOOLEAN) {
            metaData.newCell("IsMenu", isMenu)
        }
        metaData.newCell("Visible", visible)
    }

    override fun dirty(prop: Prop<*>) {
        changeListeners.fireChanged(this)
    }

    fun copy(link: Boolean): CustomProperty {
        val copy = CustomProperty(name = name.value, label = label.value, cellType = type.value)
        @Suppress("UNCHECKED_CAST")
        copy.data.value = this.data.value
        copy.isMenu.copyFrom(isMenu, link)
        copy.visible.copyFrom(this.visible, link)
        return copy
    }
}
