/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.history.AddGeometryPart
import uk.co.nickthecoder.fizzy.model.history.RemoveGeometryPart
import uk.co.nickthecoder.fizzy.prop.BooleanExpression
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.clamp0To1
import uk.co.nickthecoder.fizzy.util.ChangeAndListListener
import uk.co.nickthecoder.fizzy.util.ChangeListeners
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.util.HasChangeListeners

class Geometry(val shape: ShapeWithGeometry)

    : HasChangeListeners<Geometry>, PropListener, MetaDataAware {

    var parts = MutableFList<GeometryPart>()

    override val changeListeners = ChangeListeners<Geometry>()

    private val visibleListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            resetPrevParts()
        }
    }

    private val geometryPartsListener = ChangeAndListListener(this, parts,
            onAdded = { part, _ ->
                part.geometry = this
                part.setContext(shape.context)

                part.visible.addListener(visibleListener)
                resetPrevParts()
            },
            onRemoved = { part, _ ->
                part.geometry = null
                part.setContext(constantsContext)

                part.visible.removeListener(visibleListener)
                resetPrevParts()
            }
    )


    internal fun resetPrevParts() {
        // Do NOT reset while the shape is being loaded, as this will cause expressions to be evaluated before
        // their dependencies have been created, causing errors.
        if (shape.loaded) {
            if (parts.isEmpty()) return
            var moveTo: MoveTo? = null

            var prev = parts[0]
            parts.forEach { part ->
                if (part.visible.value && part is MoveTo) {
                    moveTo = part
                }
                if (part is Close) {
                    part.point.value = moveTo?.point?.value ?: Dimension2.ZERO_mm
                }

                part.prevPartProp.value = prev
                if (part.visible.value) prev = part
            }
        }
    }

    override fun dirty(prop: Prop<*>) {
        changeListeners.fireChanged(this, ChangeType.CHANGE, prop)
    }

    override fun createRow(type: String?, dataType: CellType?): Pair<MetaDataAware, MetaData> {
        val part = when (type) {
            "MoveTo" -> MoveTo()
            "LineTo" -> LineTo()
            "RoundedCorner" -> RoundedCorner()
            "RoundedLinesTo" -> RoundedCorner() // For backwards compatibility
            "BezierCurveTo" -> BezierCurveTo()
            "ArcTo" -> ArcTo()
            "Close" -> Close()
            else -> null
        }
        if (part == null) {
            throw IllegalStateException("Geometry has no rows of type $type")
        } else {
            parts.add(part)
            return Pair(part, part.metaData())
        }
    }

    override fun metaData(): MetaData {
        val md = MetaData(null)
        addMetaData(md)
        return md
    }

    fun addMetaData(metaData: MetaData) {
        parts.forEach { part ->
            val row = metaData.newRow(part.javaClass.simpleName)
            part.addEarlyMetaData(row)
            part.addLateMetaData(row)
        }
        metaData.rowFactories.add(RowFactory("MoveTo") { index ->
            shape.document().history.makeChange(AddGeometryPart(shape, index, MoveTo()))
        })
        metaData.rowFactories.add(RowFactory("LineTo") { index ->
            shape.document().history.makeChange(AddGeometryPart(shape, index, LineTo()))
        })
        metaData.rowFactories.add(RowFactory("RoundedCorner", listOf("RoundedLinesTo")) { index ->
            shape.document().history.makeChange(AddGeometryPart(shape, index, RoundedCorner()))
        })
        metaData.rowFactories.add(RowFactory("BezierCurveTo") { index ->
            shape.document().history.makeChange(AddGeometryPart(shape, index, BezierCurveTo()))
        })
        metaData.rowFactories.add(RowFactory("ArcTo") { index ->
            shape.document().history.makeChange(AddGeometryPart(shape, index, ArcTo()))
        })
        metaData.rowFactories.add(RowFactory("Close") { index ->
            shape.document().history.makeChange(AddGeometryPart(shape, index, Close()))
        })
        metaData.rowRemoval = { index ->
            shape.document().history.makeChange(RemoveGeometryPart(shape, index))
        }
    }

    fun isAt(localPoint: Dimension2, lineWidth: Dimension, minDistance: Dimension): Boolean {

        if (parts.isEmpty()) return false

        // Adapted from : https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
        var crossCount = 0
        var moveTo = parts[0]
        var prev = parts[0]
        var foundMoveTo = false

        if (shape.fillColor.value.isVisible()) {
            parts.filter { it.visible.value }.forEach { part ->
                if (part is MoveTo) {
                    if (foundMoveTo) {
                        // We've completed one section of the Geometry

                        // If the Geometry is not closed. i.e. if the first point isn't the same as the last point,
                        // then add an extra line to close the shape.
                        if (prev.point.value != moveTo.point.value) {
                            crossCount += LineTo.crossCount(localPoint, prev.point.value, moveTo.point.value)
                        }
                    }
                    foundMoveTo = true
                    moveTo = part
                } else {
                    crossCount += part.crossCount(localPoint)
                }
                prev = part
            }

            // If the Geometry is not closed. i.e. if the first point isn't the same as the last point,
            // then add an extra line to close the shape.
            if (prev.point.value != moveTo.point.value) {
                crossCount += LineTo.crossCount(localPoint, prev.point.value, moveTo.point.value)
            }
            if (crossCount.rem(2) == 1) {
                return true
            }
        }

        if (shape.strokeColor.value.isVisible()) {
            parts.filter { it.visible.value }.forEach { part ->
                if (part.isAlong(localPoint, lineWidth, minDistance)) {
                    return true
                }
            }
        }

        return false
    }

    /**
     * @param here The point to test, in local coordinate system
     * @return distance in page coordinates , how far along the line in the range 0..1
     */
    fun checkAlong(here: Dimension2): Pair<Double, Double>? {

        var minDist = Double.MAX_VALUE
        var partAlong: Double = 0.0
        var bestPartIndex = -1

        parts.filter { it.visible.value && it !is MoveTo }.forEachIndexed { index, part ->
            val pair = part.checkAlong(here)
            if (pair != null && pair.first < minDist) {
                minDist = pair.first
                partAlong = pair.second
                bestPartIndex = index
            }
        }
        if (bestPartIndex >= 0) {
            val partCount = parts.count { it.visible.value && it !is MoveTo }
            val along = (bestPartIndex + partAlong) / partCount
            return Pair(minDist, along)
        } else {
            return null
        }
    }

    private fun findVisiblePart(n: Int): GeometryPart? {

        var partIndex = n

        for (part in parts) {
            if (part.visible.value && part !is MoveTo) {
                if (partIndex == 0) {
                    return part
                }
                partIndex--
            }
        }
        return null
    }

    /**
     * Find the point part way along this geometry. 0 will be the start of the geometry,
     * 1 will be the end.
     */
    fun pointAlong(along: Double): Dimension2 {

        val alongClipped = clamp0To1(along)
        val nonMoveCount = parts.count { it.visible.value && it !is MoveTo }

        val partIndex = Math.floor(nonMoveCount * alongClipped).toInt()
        val part = findVisiblePart(partIndex) ?: return Dimension2.ZERO_mm
        val partAlong = (alongClipped - partIndex.toDouble() / nonMoveCount) * nonMoveCount

        return part.pointAlong(partAlong)
    }

    fun tangentAlong(along: Double): Angle {

        val alongClipped = clamp0To1(along)
        val nonMoveCount = parts.count { it.visible.value && it !is MoveTo }

        val partIndex = Math.floor(nonMoveCount * alongClipped).toInt()
        val part = findVisiblePart(partIndex) ?: return Angle.ZERO
        val partAlong = (alongClipped - partIndex.toDouble() / nonMoveCount) * nonMoveCount

        return part.tangentAlong(partAlong)
    }

    fun normalAlong(along: Double): Angle = tangentAlong(along) + Angle.radians(Math.PI / 2.0)

    /**
     * Finds the end of a path. If the geometry only has one MoveTo, then this will be the item in [parts].
     * However, if there are more than one MoveTo, then it will return item with index >= startIndex and
     * parts[index + 1] is a MoveTo.
     */
    fun findEnd(startIndex: Int): GeometryPart {
        var result = parts[startIndex]
        for (i in startIndex + 1..parts.size - 1) {
            if (parts[i].visible.value) {
                if (parts[i] is MoveTo) break
                result = parts[i]
            }
        }
        return result
    }

    fun copyInto(newShape: ShapeWithGeometry, link: Boolean) {
        val newGeometry = newShape.geometry
        //newGeometry.connect.copyFrom(connect, link)
        parts.forEach { part ->
            newGeometry.parts.add(part.copy(link))
        }
    }

}
