/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.model.geometry.GeometryPart
import uk.co.nickthecoder.fizzy.model.geometry.MoveTo
import uk.co.nickthecoder.fizzy.prop.*

enum class CellType(val label: String, val klass: Class<*>, val expression: () -> PropExpression<*>, val variable: () -> PropVariable<Any>) {

    ANGLE("Angle", Angle::class.java, { AngleExpression() }, { PropVariable(Angle.radians(0.0)) }),
    BOOLEAN("Boolean", Boolean::class.java, { BooleanExpression() }, { PropVariable(false) }),
    DIMENSION("Dimension", Dimension::class.java, { DimensionExpression() }, { PropVariable(Dimension.ZERO_mm) }),
    DIMENSION2("Dimension2", Dimension2::class.java, { Dimension2Expression() }, { PropVariable(Dimension2.ZERO_mm) }),
    DOUBLE("Double", Double::class.java, { DoubleExpression() }, { PropVariable(0.0) }),
    PAINT("Paint", Paint::class.java, { PaintExpression() }, { PropVariable(Color.BLACK) }),
    STRING("String", String::class.java, { StringExpression() }, { PropVariable("") }),
    STROKE_CAP("StrokeCap", StrokeCap::class.java, { StrokeCapExpression() }, { PropVariable(StrokeCap.BUTT) }),
    STROKE_JOIN("StrokeJoin", StrokeJoin::class.java, { StrokeJoinExpression() }, { PropVariable(StrokeJoin.BEVEL) }),
    VECTOR2("Vector2", Vector2::class.java, { Vector2Expression() }, { PropVariable(Vector2.ZERO) }),
    GEOMETRY_PART("GeometryPart", GeometryPart::class.java, { GeometryPartExpression() }, { PropVariable(MoveTo()) });
}

fun findCellType(klass: Class<*>): CellType? {
    return when (klass) {
        Angle::class.java -> CellType.ANGLE
    //Area::class.java -> AreaExpression()
        java.lang.Boolean::class.java -> CellType.BOOLEAN
        Boolean::class.java -> CellType.BOOLEAN
        Dimension::class.java -> CellType.DIMENSION
        Dimension2::class.java -> CellType.DIMENSION2
        java.lang.Double::class.java -> CellType.DOUBLE
        Double::class.java -> CellType.DOUBLE
        Paint::class.java -> CellType.PAINT
        String::class.java -> CellType.STRING
        StrokeCap::class.java -> CellType.STROKE_CAP
        StrokeJoin::class.java -> CellType.STROKE_JOIN
        Vector2::class.java -> CellType.VECTOR2
        else -> null
    }
}

fun <T : Any> createExpression(klass: Class<T>): PropExpression<*> {
    return findCellType(klass)?.expression?.invoke()
            ?: throw IllegalArgumentException("Unexpected type for an Expression : '$klass'")

}
