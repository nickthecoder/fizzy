/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.collection.MutableFList
import java.io.File

object Themes {
    /**
     * The list of directories in which themes are stored.
     */
    val directories = MutableFList<File>()


    private val directoriesListener = ListListenerLambda<File> { _, item, _, added ->
        if (added) addThemes(item) else removeThemes(item)
    }

    val themesData = mutableMapOf<String, StandardThemeData>()


    init {
        directories.listeners.add(directoriesListener)

        directories.add(File(File(System.getProperty("user.home"), "fizzy"), "themes"))
        findStockDirectory()?.let { directories.add(File(it, "themes")) }
    }

    fun useTheme(name: String, page: Page) {
        val data = themesData[name]
        page.theme.value = if (data == null) DefaultTheme(page) else StandardTheme(data, page)
    }

    fun addThemes(dir: File) {
        dir.listFiles { _, name -> name.endsWith(".fizzytheme") }?.forEach { file ->
            try {
                themesData[file.nameWithoutExtension] = StandardThemeData(file)
            } catch (e: Exception) {
            }
        }
    }

    fun removeThemes(dir: File) {
        themesData.filter { it.value.file.parentFile == dir }.keys.forEach { key ->
            themesData.remove(key)
        }
    }

}
