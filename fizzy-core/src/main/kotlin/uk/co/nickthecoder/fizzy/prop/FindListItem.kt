/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListener
import uk.co.nickthecoder.fizzy.model.Named
import uk.co.nickthecoder.fizzy.prop.methods.FindListItemMethod

/**
 * Looks through an [FList], looking for the item with a particular name.
 * If found, then return the item from the list.
 *
 * This is very similar to [FindListItemData], but returns the item in the list, rather than the item's data.
 *
 * Used in expressions like so :
 *
 *     ConnectionPoint.foo.Point
 *     CustomProperties.foo.Data  // Note, this is equivalent to CustomProperty.foo
 *     Geometry.foo.Point
 *     Child.foo.Size
 *
 * where 'foo' is the name of the list item to search for.
 *
 * Also see [FindListItemMethod], which does a similar job, but unlike this, it can be passed the name
 * as an argument. (This item name is fixed at creation time).
 */
class FindListItem<T : Named>(private val listName: String, private val list: FList<T>, val name: String)
    : PropCalculation<Any>("FindListItem", false), ListListener<T> {

    /**
     * Listen to the name, so that if it changes, we become dirty (and will throw when re-evaluated,
     * unless another item is given the name)
     */
    private var itemName: Prop<String>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    init {
        list.listeners.add(this)
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        list.listeners.remove(this)
        itemName?.removeListener(this)
    }

    /**
     * If the list item is removed, we need to become dirty!
     * If the name has been added back again, become dirty (probably not needed, but it won't hurt).
     */
    override fun listChanged(list: FList<T>, item: T, index: Int, added: Boolean) {
        if (item.name.value == name) {
            dirty = true
        }
    }


    override fun eval(): T {
        list.forEach { item ->
            if (item.name.value == name) {
                itemName = item.name
                return item
            }
        }
        throw RuntimeException("$listName $name not found")
    }

}
