/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.model.geometry.MoveTo

interface ShapeParent {

    val children: ShapeChildren

    fun page(): Page

    fun document(): Document

    fun findChild(name: String): Shape?

    fun findShape(name: String): Shape?

    fun findShapesAt(pagePoint: Dimension2, minDistance: Dimension) = children.filter { it.isAt(pagePoint, minDistance) }

    /**
     * Finds the nearest [ConnectionPoint] to the given point (in page coordinates).
     *
     * @param exclude Do not consider 'exclude', nor shapes that are connected to 'exclude'
     * @param allowOpposite When false, do not consider shapes that 'exclude' is connected to.
     * This prevents a line being connected to a shape which is already connected to the line,
     * thus avoiding a mutual dependency.
     */
    fun findNearestConnectionPoint(atPagePoint: Dimension2, exclude: Shape, allowOpposite: Boolean): Pair<ConnectionPoint, Double>? {
        var nearest: ConnectionPoint? = null
        var distance = Double.MAX_VALUE

        children.forEach { child ->
            if (child !== exclude && !child.isConnectedTo(exclude) && (allowOpposite || !exclude.isConnectedTo(child))) {
                child.connectionPoints.forEach { cp ->
                    val d = (child.localToPage(cp.point.value) - atPagePoint).length().inDefaultUnits
                    if (d < distance) {
                        nearest = cp
                        distance = d
                    }
                }
            }
        }

        return if (nearest == null) {
            null
        } else {
            Pair(nearest!!, distance)
        }
    }

    /**
     * Finds the nearest geometry that a point can be connected to.
     * Returns the Geometry, the distance and the amount along.
     *
     * @param exclude Do not consider 'exclude', nor shapes that are connected to 'exclude'
     * @param allowOpposite When false, do not consider shapes that 'exclude' is connected to.
     * This prevents a line being connected to a shape which is already connected to the line,
     * thus avoiding a mutual dependency.
     */
    fun findNearestConnectionGeometry(atPagePoint: Dimension2, exclude: Shape, allowOpposite: Boolean): Triple<ShapeWithGeometry, Double, Double>? {
        var nearest: ShapeWithGeometry? = null
        var nearestDistance = Double.MAX_VALUE
        var nearestAlong = 0.0

        fun scanChildren(shapeParent: ShapeParent) {
            shapeParent.children.filterIsInstance<ShapeWithGeometry>().forEach { child ->

                if (child !== exclude && !child.isConnectedTo(exclude) && (allowOpposite || !exclude.isConnectedTo(child))) {
                    val localPoint = child.pageToLocal(atPagePoint)
                    // Ignore geometries that cannot be connected to.
                    if (child.connectAround.value) {

                        val nonMoveCount = child.geometry.parts.count { it.visible.value && it !is MoveTo }
                        val interval = 1.0 / nonMoveCount
                        var moveToCount = 0

                        child.geometry.parts.filter { it.visible.value }.forEachIndexed { index, part ->

                            if (part is MoveTo) {
                                moveToCount++
                            } else {

                                part.checkAlong(localPoint)?.let { (dist, along) ->
                                    if (dist < nearestDistance) {
                                        nearest = child
                                        nearestDistance = dist
                                        nearestAlong = ((index - moveToCount) + along) * interval
                                    }
                                }
                            }

                        }
                    }
                    scanChildren(child)
                }
            }
        }
        scanChildren(this)

        return if (nearest == null) {
            null
        } else {
            Triple(nearest!!, nearestDistance, nearestAlong)
        }
    }

    fun localToParent(localPoint: Dimension2): Dimension2

    fun parentToLocal(parentPoint: Dimension2): Dimension2

    fun localToPage(localPoint: Dimension2): Dimension2

    fun pageToLocal(pagePoint: Dimension2): Dimension2


    fun localToParent(localAngle: Angle): Angle

    fun localToPage(localAngle: Angle): Angle

    fun parentToLocal(parentAngle: Angle): Angle

    fun pageToLocal(pageAngle: Angle): Angle

}
