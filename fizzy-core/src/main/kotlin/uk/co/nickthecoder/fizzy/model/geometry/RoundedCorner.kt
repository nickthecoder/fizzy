/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.DimensionExpression
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * A pair of lines from the previous path's point to [mid] and then to [point].
 * The corner where they meet ([mid]) is rounded to a given [radius], so [mid] is only
 * touched if [radius] = 0.
 *
 * Note, this doesn't do quite the same as [DrawContext.roundedCorner], which doesn't actually
 * visit the end point. This means that [RoundedCorner] isn't as easy to chain together to
 * make a succession of rounded corners.
 *
 * To chain a two or more [RoundedCorner] together, it is common for the end point of the
 * first [RoundedCorner] to be the mid point of the where you actually want to go.
 * For example, to create a rounded triangle, [mid] would be the points of the triangle, and
 * [point] would be half way between pairs of the triangle's points.
 *
 * Note. If the whole [Shape] is to be rounded, then you can simply use [LineTo], and set a radius
 * using [ShapeWithGeometry.cornerRadius].
 *
 * Currently neither this, nor [ShapeWithGeometry.cornerRadius] offer full support for the rounded corner.
 * Both ignore the rounded corner, and treat it like two straight lines. Therefore selection this shape using the mouse,
 * and connecting a point along the shape's outline will be increasingly wrong as radius increases.
 * In particular [pointAlong], [tangentAlong], [isAlong], [checkAlong] and [crossCount] ignore the rounded part,
 * and treat this the same as two straight lines.
 */
class RoundedCorner(
        val mid: Dimension2Expression,
        point: Dimension2Expression,
        val radius: DimensionExpression
) : AbstractGeometryPart(point) {

    constructor(mid: String, point: String, radius: String) : this(Dimension2Expression(mid), Dimension2Expression(point), DimensionExpression(radius))

    constructor(mid: Dimension2, point: Dimension2, radius: Dimension) : this(mid.toFormula(), point.toFormula(), radius.toFormula())

    constructor() : this(Dimension2.ZERO_mm, Dimension2.ZERO_mm, Dimension.ZERO_mm)

    init {
        mid.addListener(this)
        radius.addListener(this)
    }

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        mid.context = context
        radius.context = context
    }

    override fun addEarlyMetaData(metaData: MetaData) {
        super.addEarlyMetaData(metaData)
        metaData.cells.add(MetaDataCell("Mid", mid))
        metaData.cells.add(MetaDataCell("Radius", radius))
    }

    // TODO The following methods do NOT take the curved section into consideration.

    override fun pointAlong(along: Double): Dimension2 {
        return if (along < 0.5) {
            prevPart.point.value + (mid.value - prevPart.point.value) * along * 2.0
        } else {
            mid.value + (point.value - mid.value) * (along - 0.5) * 2.0
        }
    }

    override fun tangentAlong(along: Double): Angle {
        return if (along < 0.5) {
            (mid.value - prevPart.point.value).angle()
        } else {
            (point.value - mid.value).angle()
        }
    }

    override fun crossCount(here: Dimension2): Int {
        return LineTo.crossCount(here, prevPart.point.value, mid.value) +
                LineTo.crossCount(here, mid.value, point.value)
    }

    override fun isAlong(here: Dimension2, lineWidth: Dimension, minDistance: Dimension): Boolean {
        return LineTo.isAlong(shape, here, prevPart.point.value, mid.value, lineWidth, minDistance) ||
                LineTo.isAlong(shape, here, mid.value, point.value, lineWidth, minDistance)
    }

    override fun checkAlong(here: Dimension2): Pair<Double, Double>? {
        val shape = this.shape ?: return null

        val a = LineTo.checkAlong(shape, here, prevPart.point.value, mid.value)
        val b = LineTo.checkAlong(shape, here, mid.value, point.value)
        return if (a == null) {
            if (b == null) {
                null
            } else {
                Pair(b.first, b.second / 2.0 + 0.5)
            }
        } else if (b == null || (Math.abs(a.first) < Math.abs(b.first) && a.second <= 1.0)) {
            Pair(a.first, a.second / 2.0)
        } else if (b.second < 0.0) {
            null
        } else {
            Pair(b.first, b.second / 2.0 + 0.5)
        }
    }

    // TODO The previous methods do not take the curved section into consideration.

    override fun copy(link: Boolean): GeometryPart {
        return RoundedCorner(mid.copy(link), point.copy(link), radius.copy(link))
    }

    override fun draw(dc: DrawContext) {
        dc.roundedCorner(mid.value, point.value, radius.value)
        dc.lineTo(point.value)
    }

    override fun toString() = "RoundedCorner mid=${mid.value} point=${point.value} radius=${radius.value}"

}
