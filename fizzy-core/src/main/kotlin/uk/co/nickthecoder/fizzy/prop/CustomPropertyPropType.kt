/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.model.CustomProperty
import uk.co.nickthecoder.fizzy.model.CustomPropertyList

class CustomPropertyPropType private constructor()
    : PropType<CustomProperty>(CustomProperty::class.java) {

    override fun findField(prop: Prop<CustomProperty>, name: String): Prop<*>? {
        return when (name) {
            "Value" -> PropField("CustomProperty.Value", prop) { prop.value.data }
            "Label" -> PropField("CustomProperty.Label", prop) { prop.value.label }
            "IsMenu" -> PropField("CustomProperty.IsMenu", prop) { prop.value.isMenu }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = CustomPropertyPropType()
    }
}

class CustomPropertyShortcut(val customPropertyList: CustomPropertyList)

/**
 * Allows for the following syntax :
 *     CustomProperty.Foo
 * Where "Foo" is the name of a [CustomProperty].
 */
class CustomPropertyShortcutPropType private constructor()
    : PropType<CustomPropertyShortcut>(CustomPropertyShortcut::class.java) {

    override fun findField(prop: Prop<CustomPropertyShortcut>, name: String): Prop<*>? {
        // Lets us access a CustomProperty data value using : CustomProperty.TheName
        prop.value.customPropertyList.forEach { property ->
            if (property.name.value == name) {
                return FindListItemData("CustomProperty", prop.value.customPropertyList, name)
            }
        }
        return super.findField(prop, name)
    }

    companion object {
        val instance = CustomPropertyShortcutPropType()
    }
}

/**
 * As CustomProperty.foo give access directly to the CustomProperty's value, we can't
 * get the other data about the CustomProperty (e.g. label, isMenu etc).
 * So this call allow us to do :
 *
 *     CustomProperties.foo.Label
 */
class CustomPropertyListPropType private constructor()
    : PropType<CustomPropertyList>(CustomPropertyList::class.java) {

    override fun findField(prop: Prop<CustomPropertyList>, name: String): Prop<*>? {
        // Lets us access a CustomProperty data value using : CustomProperty.TheName
        prop.value.forEach { property ->
            if (property.name.value == name) {
                return FindListItem("CustomProperties", prop.value, name)
            }
        }
        return super.findField(prop, name)
    }

    companion object {
        val instance = CustomPropertyListPropType()
    }
}
