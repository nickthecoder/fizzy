/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.evaluator.ThisContext
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.prop.methods.ConnectToShape
import uk.co.nickthecoder.fizzy.prop.methods.Connection
import uk.co.nickthecoder.fizzy.util.toFormula

class Shape1d private constructor(
        parent: ShapeParent,
        linkedFrom: Shape?,
        id: Int = parent.document().generateShapeId())
    : ShapeWithGeometry(parent, linkedFrom, id) {

    override val context = createContext(ThisContext(this, Shape1dPropType.instance))

    override val transform = Shape1dTransform(this)

    override val locks = Locks(this, context)

    val start = transform.start

    val end = transform.end

    val startLineMarker = StringExpression()

    val midLineMarker = StringExpression()

    val endLineMarker = StringExpression()

    val startLineMarkerShape = FindLineMarker(true, startLineMarker)

    val midLineMarkerShape = FindLineMarker(true, midLineMarker)

    val endLineMarkerShape = FindLineMarker(true, endLineMarker)

    /**
     * When connected to another shape, how much space to "back off".
     * For thin lines, this is often 0, but for lines with large arrow heads, may be slightly
     * bigger than the arrow head, so as to leave clear space between the arrow and the box it is pointing to.
     */
    val startMargin = DimensionExpression("0mm")

    /**
     * Like [startMargin], but for the end of this line, rather than the start.
     */
    val endMargin = DimensionExpression("0mm")


    /**
     * Used by ConnectToShape, to allow the direction to be adusted from the straight line path from
     * the line's start to end.
     * This is particularly useful when connecting more than one line between the same pair of boxes,
     * as each line can take join the box at a different place.
     */
    val extraStartAngle = AngleExpression(Angle.ZERO)

    val extraEndAngle = AngleExpression(Angle.ZERO)

    override val size = Dimension2Expression("((End-Start).Length,LineWidth)")

    val length = PropCalculation2("Shape1d.length", true, start, end) { sv, ev ->
        (ev - sv).length()
    }

    init {
        listenTo(extraStartAngle, extraEndAngle, startLineMarker, midLineMarker, endLineMarker, startMargin, endMargin)
        fillColor.formula = "TRANSPARENT"
    }

    override fun postInit() {
        super.postInit()
        themeClass.formula = "line".toFormula()
    }

    override fun copyInto(parent: ShapeParent, link: Boolean): Shape1d {
        val newShape = Shape1d(parent, if (link) this else null)
        newShape.postInit()
        populateShape(newShape, link)
        return newShape
    }

    override fun addMetaData(metaData: MetaData) {
        super.addMetaData(metaData)

        metaData.newCell("Start", start, save = false)
        metaData.newCell("End", end, save = false)

        metaData.newCell("ExtraStartAngle", extraStartAngle)
        metaData.newCell("ExtraEndAngle", extraEndAngle)

        metaData.newCell("StartMargin", startMargin)
        metaData.newCell("EndMargin", endMargin)


        metaData.newCell("StartLineMarker", startLineMarker)
        metaData.newCell("MidLineMarker", midLineMarker)
        metaData.newCell("EndLineMarker", endLineMarker)
    }

    val isStartConnected: Prop<Boolean> = PropCalculation1("Shape1d.isStartConnected", true, start) {
        start.calculatedProperty() is Connection
    }

    val isEndConnected: Prop<Boolean> = PropCalculation1("Shape1d.isEndConnected", true, end) {
        end.calculatedProperty() is Connection
    }

    val startConnectedShape: Prop<Shape> = PropCalculation1("Shape1d.startConnectedShape", true, start) {
        val cp = start.calculatedProperty() as? Connection
        if (cp == null) {
            this
        } else {
            cp.connectedTo()
        }
    }

    val endConnectedShape: Prop<Shape> = PropCalculation1("Shape1d.endConnectedShape", true, end) {
        val cp = end.calculatedProperty() as? Connection
        if (cp == null) {
            this
        } else {
            cp.connectedTo()
        }
    }

    fun startConnection(): Connection? {
        val result = start.calculatedProperty()
        return if (result is Connection) result else null
    }

    fun endConnection(): Connection? {
        val result = end.calculatedProperty()
        return if (result is Connection) result else null
    }

    val smartStart: Prop<Dimension2> by lazy {
        PropCalculation3("Shape1d.smartStart", true, start, end, startMargin) { _, _, _ ->
            start.value
            val cp = start.calculatedProperty()
            if (cp is ConnectToShape) {
                cp.smartPosition().value
            } else {
                Dimension2.ZERO_mm
            }
        }
    }

    val smartEnd: Prop<Dimension2> by lazy {
        PropCalculation3("Shape1d.smartEnd", true, start, end, endMargin) { _, _, _ ->
            end.value
            val cp = end.calculatedProperty()
            if (cp is ConnectToShape) {
                cp.smartPosition().value
            } else {
                parentToLocal(end.value)
            }
        }
    }

    val startAngle: Prop<Angle> = PropCalculation2("Shape1d.startAngle", true, start, end) { _, _ ->
        val startCP = start.calculatedProperty()
        if (startCP is Connection) {
            startCP.angle.value
        } else {
            Angle.ZERO
        }
    }

    val endAngle: Prop<Angle> = PropCalculation2("Shape1d.endAngle", true, start, end) { _, _ ->
        val endCP = end.calculatedProperty()
        if (endCP is Connection) {
            endCP.angle.value
        } else {
            Angle.ZERO
        }
    }

    companion object {

        fun create(parent: ShapeParent): Shape1d {
            val result = Shape1d(parent, null)
            result.postInit()
            return result
        }

        internal fun create(parent: ShapeParent, linkedFrom: Shape?, id: Int): Shape1d {
            val result = Shape1d(parent, linkedFrom, id)
            result.postInit()
            return result
        }

    }
}
