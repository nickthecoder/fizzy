/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

class Dimension2(val x: Dimension, val y: Dimension) : FizzyType {

    operator fun plus(other: Dimension2): Dimension2 = Dimension2(x + other.x, y + other.y)

    operator fun minus(other: Dimension2) = Dimension2(x - other.x, y - other.y)

    operator fun unaryMinus() = Dimension2(-x, -y)

    operator fun times(scale: Double) = Dimension2(x * scale, y * scale)

    // operator fun times(other: Dimension2) = Dimension2(x * other.x, y * other.y)

    operator fun times(other: Vector2) = Dimension2(x * other.x, y * other.y)

    operator fun div(scale: Double) = Dimension2(x / scale, y / scale)

    operator fun div(other: Dimension2) = Vector2(x / other.x, y / other.y)

    operator fun div(other: Dimension) = Vector2(x / other, y / other)

    operator fun div(other: Vector2) = Dimension2(x / other.x, y / other.y)

    /**
     * Returns the dot product of the two Dimension2s in default units.
     */
    fun dot(other: Dimension2) =
            x.inDefaultUnits * other.x.inDefaultUnits + y.inDefaultUnits * other.y.inDefaultUnits

    fun dot(other: Vector2) = x * other.x + y * other.y

    fun ratio(other: Dimension2): Vector2 {
        return Vector2(x.ratio(other.x), y.ratio(other.y))
    }

    fun max(other: Dimension2) = Dimension2(
            if (this.x.inDefaultUnits > other.x.inDefaultUnits) this.x else other.x,
            if (this.y.inDefaultUnits > other.y.inDefaultUnits) this.y else other.y
    )

    fun min(other: Dimension2) = Dimension2(
            if (this.x.inDefaultUnits > other.x.inDefaultUnits) other.x else this.x,
            if (this.y.inDefaultUnits > other.y.inDefaultUnits) other.y else this.y
    )

    /**
     * Finds the length of the 2D Dimension.
     */
    fun length(): Dimension {
        return Dimension(Math.sqrt(x.inDefaultUnits * x.inDefaultUnits + y.inDefaultUnits * y.inDefaultUnits) / x.units.scale, x.units)
    }

    /**
     * Returns a Vector of length 1, in the same direction as this 2D Dimension.
     * If you need a Dimension2 of unit length, then simply multiply this result by a Dimension of 1 (in whichever units you wish).
     */
    fun normalise(): Vector2 {
        val l = length().inDefaultUnits
        return Vector2(x.inDefaultUnits / l, y.inDefaultUnits / l)
    }

    fun normalise2(): Dimension2 {
        val l = length().inDefaultUnits
        return Dimension2(Dimension(x.inDefaultUnits / l), Dimension(y.inDefaultUnits / l))
    }

    fun aspectRatio() = if (x.inDefaultUnits == 0.0 && y.inDefaultUnits == 0.0) 1.0 else x.inDefaultUnits / y.inDefaultUnits

    fun keepAspectRatio(aspectRatio: Double): Dimension2 {
        val ratio = x.inDefaultUnits / y.inDefaultUnits
        return if (ratio > aspectRatio) {
            Dimension2(x / ratio * aspectRatio, y)
        } else {
            Dimension2(x, y * ratio / aspectRatio)
        }
    }

    /**
     * The angle of the vector. 0° is along the x axis, and +ve values are anticlockwise if the y axis is pointing downwards.
     */
    fun angle(): Angle = Angle.radians(Math.atan2(-y.inDefaultUnits, x.inDefaultUnits))

    /**
     * Rotates anticlockwise about the origin.
     *
     * The units of both x and y will be in the units of x. i.e. if x is in cm and y is in m,
     * then the result will be in cm for both x and y
     */
    fun rotate(angle: Angle) = rotate(angle.radians)

    /**
     * Rotates anticlockwise about the origin.
     *
     * The units of both x and y will be in the units of x. i.e. if x is in cm and y is in m,
     * then the result will be in cm for both x and y
     */
    fun rotate(radians: Double): Dimension2 {
        val sin = Math.sin(radians)
        val cos = Math.cos(radians)
        return Dimension2(x * cos + y * sin, -x * sin + y * cos)
    }

    fun isNear(other: Dimension2) = x.isNear(other.x) && y.isNear(other.y)


    fun clamp(min: Dimension2, max: Dimension2): Dimension2 =
            Dimension2(x.clamp(min.x, max.x), y.clamp(min.y, max.y))

    override fun hashCode(): Int {
        return x.hashCode() + 17 * y.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (other is Dimension2) {
            return x == other.x && y == other.y
        }
        return false
    }

    override fun toFormula() = "(${x.toFormula()},${y.toFormula()})"

    fun format(format: String) = "( ${x.format(format)},${y.format(format)} )"

    override fun toString() = "Dimension2($x , $y)"

    companion object {
        val ZERO_mm = Dimension2(Dimension.ZERO_mm, Dimension.ZERO_mm)
    }

}
