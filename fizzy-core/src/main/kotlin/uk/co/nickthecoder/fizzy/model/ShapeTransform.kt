/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.prop.methods.Connection
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * Holds the position of a shape within a parent (which is either another [Shape] or a [Page].
 *
 * Used by [Shape].
 */
interface ShapeTransform
    : MetaDataAware {

    val pin: Prop<Dimension2>
    val locPin: Prop<Dimension2>

    val rotation: Prop<Angle>

    val fromParentToLocal: Prop<Matrix33>
    val fromPageToLocal: Prop<Matrix33>
    val fromLocalToPage: Prop<Matrix33>
    val fromLocalToParent: Prop<Matrix33>

    fun addMetaData(metaData: MetaData)

    fun center(): Dimension2

    fun apply(dc: DrawContext)

    fun isConnectedTo(other: Shape): Boolean
}

class Shape2dTransform(val shape: Shape)
    : ShapeTransform {

    /**
     * The position of this object relative to the parent (which is either a [Shape] or a [Page]).
     */
    override val pin = Dimension2Expression("(0mm, 0mm)", shape.context)

    /**
     * The local position of this object, which corresponds to the [pin] within the parent.
     * It is also used as the center of rotation.
     * (0,0) would be the top-left of the shape, and [Shape2d.size] would be the bottom right.
     * The default value is [Shape2d.size] / 2, which is the center of the shape.
     */
    override val locPin = Dimension2Expression("Size / 2", shape.context)

    // Should we have a scale? A scale would scale the line widths, the fonts etc
    val scale = Vector2Expression("(1, 1)", shape.context)

    override val rotation = AngleExpression("0 deg", shape.context)

    val flipX = BooleanExpression(false, shape.context)

    val flipY = BooleanExpression(false, shape.context)

    override fun center(): Dimension2 = locPin.value

    /**
     * A matrix which can transform a point from the coordinate system of the parent to local coordinates.
     * The inverse is [fromLocalToParent].
     * This is based on [pin], [locPin], [scale] and [rotation].
     * Whenever any of those become dirty, then this also becomes dirty.
     */
    override val fromParentToLocal =
            PropCalculation6("ShapeTransform.fromLocalToParent", true, pin, locPin, scale, rotation, flipX, flipY) { vPin, vLocPin, vScale, vRotation, vFlipX, vFlipY ->

                var result = Matrix33.translate(vLocPin.x.inDefaultUnits, vLocPin.y.inDefaultUnits)
                if (vScale.x != 1.0 || vScale.y != 1.0) result *= Matrix33.scale(1.0 / vScale.x, 1.0 / vScale.y)
                if (vRotation.radians != 0.0) result *= Matrix33.rotate(-vRotation)
                if (vFlipX || vFlipY) result *= Matrix33.flip(vFlipX, vFlipY)
                if (vPin.x.inDefaultUnits != 0.0 || vPin.y.inDefaultUnits != 0.0) result *= Matrix33.translate(-vPin.x.inDefaultUnits, -vPin.y.inDefaultUnits)

                result
            }

    /**
     * A matrix which can transform a point from a local coordinate into a coordinate in the parent's coordinate system.
     * The inverse is [fromParentToLocal].
     * This is based on [pin], [locPin], [scale] and [rotation].
     * Whenever any of those become dirty, then this also becomes dirty.
     */
    override val fromLocalToParent =
            PropCalculation6("ShapeTransform.fromLocalToParent", true, pin, locPin, scale, rotation, flipX, flipY) { vPin, vLocPin, vScale, vRotation, vFlipX, vFlipY ->

                var result = Matrix33.translate(vPin.x.inDefaultUnits, vPin.y.inDefaultUnits)
                if (vFlipX || vFlipY) result *= Matrix33.flip(vFlipX, vFlipY)
                if (vRotation.radians != 0.0) result *= Matrix33.rotate(vRotation)
                if (vScale.x != 1.0 || vScale.y != 1.0) result *= Matrix33.scale(vScale)
                if (vLocPin.x.inDefaultUnits != 0.0 || vLocPin.y.inDefaultUnits != 0.0) result *= Matrix33.translate(-vLocPin.x.inDefaultUnits, -vLocPin.y.inDefaultUnits)

                result
            }

    /**
     * A matrix which can transform a local coordinate into a coordinate of the page.
     */
    override val fromLocalToPage: Prop<Matrix33> = if (shape.parent is Shape) {
        PropCalculation2("ShapeTransform.fromLocalToPage", true, (shape.parent as Shape).transform.fromLocalToPage, fromLocalToParent) { parentLocalToPage, localToParent ->
            parentLocalToPage * localToParent
        }
    } else {
        fromLocalToParent
    }

    /**
     * A matrix which can transform a page coordinate into a local coordinate
     */
    override val fromPageToLocal: Prop<Matrix33> = if (shape.parent is Shape) {
        PropCalculation2("ShapeTransform.fromPageToLocal", true, fromParentToLocal, (shape.parent as Shape).transform.fromPageToLocal) { parentToLocal, parentPageToLocal ->
            parentToLocal * parentPageToLocal
        }
    } else {
        fromParentToLocal
    }

    init {
        shape.listenTo(pin, locPin, scale, rotation, flipX, flipY)
    }

    override fun metaData(): MetaData {
        val result = MetaData(null, null)
        addMetaData(result)
        return result
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Pin", pin)
        metaData.newCell("LocPin", locPin)
        metaData.newCell("FlipX", flipX)
        metaData.newCell("FlipY", flipY)
        metaData.newCell("Scale", scale)
        metaData.newCell("Rotation", rotation)
    }

    override fun apply(dc: DrawContext) {
        dc.translate(pin.value)
        dc.scale(scale.value)
        if (flipX.value) {
            dc.scale(Vector2(-1.0, 1.0))
        }
        if (flipY.value) {
            dc.scale(Vector2(1.0, -1.0))
        }
        dc.rotate(rotation.value)
        dc.translate(-locPin.value)
    }

    override fun isConnectedTo(other: Shape): Boolean {
        pin.value
        return (pin.calculatedProperty() as? Connection)?.connectedTo() === other
    }
}

class Shape1dTransform(val shape: Shape1d)
    : ShapeTransform {

    val start = Dimension2Expression("(0mm,0mm)")

    val end = Dimension2Expression("(1mm,1mm)")

    override val pin: Prop<Dimension2>
        get() = start

    override val locPin = PropConstant(Dimension2.ZERO_mm)

    override val rotation: Prop<Angle> = PropCalculation2("ShapeTransform.rotation", true, start, end) { s, e ->
        (e - s).angle()
    }

    /**
     * A matrix which can transform a point from the coordinate system of the parent to local coordinates.
     * The inverse is [fromLocalToParent].
     * Whenever any of those become dirty, then this also becomes dirty.
     */
    override val fromParentToLocal =
            PropCalculation2("ShapeTransform.fromParentToLocal", true, start, rotation) { vStart, vRotation ->
                Matrix33.rotate(-vRotation) *
                        Matrix33.translate(-vStart.x.inDefaultUnits, -vStart.y.inDefaultUnits)
            }

    /**
     * A matrix which can transform a point from a local coordinate into a coordinate in the parent's coordinate system.
     * The inverse is [fromParentToLocal].
     */
    override val fromLocalToParent =
            PropCalculation2("ShapeTransform.fromLocalToParent", true, start, rotation) { vStart, vRotation ->
                Matrix33.translate(vStart.x.inDefaultUnits, vStart.y.inDefaultUnits) *
                        Matrix33.rotate(vRotation)
            }

    /**
     * A matrix which can transform a local coordinate into a coordinate of the page.
     */
    override val fromLocalToPage: Prop<Matrix33> = if (shape.parent is Shape) {
        PropCalculation2("ShapeTransform.fromLocalToPage", true, (shape.parent as Shape).transform.fromLocalToPage, fromLocalToParent) { parentLocalToPage, localToParent ->
            parentLocalToPage * localToParent
        }
    } else {
        fromLocalToParent
    }

    /**
     * A matrix which can transform a page coordinate into a local coordinate
     */
    override val fromPageToLocal: Prop<Matrix33> = if (shape.parent is Shape) {
        PropCalculation2("ShapeTransform.fromPageToLocal", true, fromParentToLocal, (shape.parent as Shape).transform.fromPageToLocal) { parentToLocal, parentPageToLocal ->
            parentToLocal * parentPageToLocal
        }
    } else {
        fromParentToLocal
    }


    init {
        shape.listenTo(start, end)
    }

    override fun center(): Dimension2 = Dimension2((start.value - end.value).length() * 0.5, Dimension.ZERO_mm)

    override fun metaData(): MetaData {
        val result = MetaData(null, null)
        addMetaData(result)
        return result
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Start", start)
        metaData.newCell("End", end)
    }

    override fun apply(dc: DrawContext) {
        dc.translate(start.value)
        dc.rotate(rotation.value)
    }


    override fun isConnectedTo(other: Shape): Boolean {
        start.value
        end.value
        //println( "Is 1d connected ${this.shape} to $other ? ${}")
        return (start.calculatedProperty() as? Connection)?.connectedTo() === other ||
                (end.calculatedProperty() as? Connection)?.connectedTo() === other
    }
}
