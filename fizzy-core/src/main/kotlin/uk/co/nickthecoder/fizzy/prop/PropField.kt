/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

/**
 * Dynamically evaluates a field value of a Prop.
 * This is a dynamically calculated value. Therefore, if the underlying Prop changes its
 * value, the the [PropField] will also update.
 */
class ConstantPropField<T : Any, R : Any>(
        description: String, // This is only to aid debugging.
        private val prop: Prop<T>,
        private val lambda: (Prop<T>) -> R)

    : PropCalculation<R>(description, false) {

    init {
        prop.addListener(this)
    }

    override fun isConstant() = prop.isConstant()

    override fun eval(): R = lambda(prop)

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        prop.removeListener(this)
    }

    override fun toString(): String {
        return "ConstantPropField $description of $prop"
    }
}

class PropField<T : Any>(
        description: String, // This is only to aid debugging.
        private val prop: Prop<T>,
        private val lambda: (Prop<T>) -> Prop<Any>)

    : PropCalculation<Any>(description, false) {

    private var resultProp: Prop<Any>? = null
        set(v) {
            if (field != v) {
                field?.removeListener(this)
                field = v
                field?.addListener(this)
            }
        }

    init {
        prop.addListener(this)
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        prop.removeListener(this)
        resultProp?.removeListener(this)
    }

    override fun isConstant() = prop.isConstant() && resultProp?.isConstant() == true

    override fun eval(): Any {
        val result = lambda(prop)
        resultProp = result
        return result.value
    }


    override fun toString(): String {
        return "PropField $description of $prop"
    }
}
