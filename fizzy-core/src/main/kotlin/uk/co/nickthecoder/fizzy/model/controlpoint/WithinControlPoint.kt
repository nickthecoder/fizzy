/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.model.Vector2
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation3
import uk.co.nickthecoder.fizzy.prop.Vector2Expression
import uk.co.nickthecoder.fizzy.util.clamp

/**
 * A control point defined by two opposite corners of a rectangle (named [a] and [b]), and a Vector2 (named [position])
 * defining how close to A or B (0,0) means at A, and (1,1) means at B.
 * The [position] can have -ve values, and values over 1, if the point is outside of the rectangle defined by [a] and [b].
 *
 * Consider [StandardControlPoint] as an alternative, which is simpler, but cannot adjust itself when the shape is scaled.
 */
class WithinControlPoint(
        val position: Vector2Expression,
        val a: Dimension2Expression,
        val b: Dimension2Expression)

    : ControlPoint(ControlPointType.WITHIN) {

    constructor(position: String, a: String = "(0mm,0mm)", b: String = "Size") : this(Vector2Expression(position), Dimension2Expression(a), Dimension2Expression(b))

    constructor(position: Vector2, a: Dimension2, b: Dimension2) : this(Vector2Expression(position), Dimension2Expression(a), Dimension2Expression(b))

    constructor() : this("(0,0)")

    override val point: Prop<Dimension2> = PropCalculation3<Dimension2, Dimension2, Dimension2, Vector2>("WithinControlPoint.point", true, a, b, position)
    { va, vb, vPosition -> va + (vb - va) * vPosition }

    val min = Vector2Expression("MIN_VECTOR2")
    val max = Vector2Expression("MAX_VECTOR2")

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        position.context = context
        a.context = context
        b.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("A", a, "A")
        metaData.newCell("B", b, "B")
        metaData.newCell("Position", position, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        val size = b.value - a.value
        val foo = draggedTo - a.value
        val x = (foo.x / size.x).clamp(min.value.x, max.value.x)
        val y = (foo.y / size.y).clamp(min.value.y, max.value.y)

        position.value = Vector2(x, y)
    }

    override fun copy(link: Boolean): WithinControlPoint {
        val result = WithinControlPoint(position.copy(link), a.copy(link), b.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
