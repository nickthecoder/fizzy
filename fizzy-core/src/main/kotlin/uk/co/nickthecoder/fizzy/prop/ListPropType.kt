/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.FizzyType
import uk.co.nickthecoder.fizzy.util.toFormula

/**
 * NOTE. Any Prop<List> should be an immutable list,
 * because method "get" and field "Size" do NOT recalculate if the list were to change.
 *
 * If the list is mutable, then use an FMutableList instead.
 */
class ListPropType : PropType<List<Prop<*>>>(ArrayList::class.java) {

    override fun findField(prop: Prop<List<Prop<*>>>, name: String): Prop<*>? {
        return when (name) {
            "Size" -> ConstantPropField("List.Size", prop) { it.value.size.toDouble() }
            else -> super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<List<Prop<*>>>, name: String): PropMethod<in List<Prop<*>>, *>? {
        return when (name) {
            "get" -> PropMethod1("List.get", prop, Double::class.java) { index -> prop.value[index.toInt()].value }
            else -> super.findMethod(prop, name)
        }
    }

    companion object {
        val instance = ListPropType()
    }
}

/**
 * All list expressions are a list of [Prop]<*>.
 * This means that the type of each items does NOT have to be the same, and no type checking is done on them.
 */
class ListExpression : PropExpression<List<Prop<*>>> {

    constructor(argList: List<Prop<*>> = emptyList<Prop<Any>>(), context: EvaluationContext = constantsContext) : this(argList.toFormula(), context)

    @Suppress("UNCHECKED_CAST")
    constructor(expression: String, context: EvaluationContext = constantsContext) : super(expression, List::class.java as Class<List<Prop<*>>>, context)

    constructor(other: ListExpression) : super(other)

    override val defaultValue = emptyList<Prop<Any>>()

    override fun toFormula(value: List<Prop<*>>) = value.toFormula()

    override fun copy(link: Boolean) = if (link) ListExpression(this) else ListExpression(formula)

}

fun List<Prop<*>>.toFormula() = joinToString(separator = ",", prefix = "{", postfix = "}") {
    val value = it.value
    when (value) {
        is Boolean -> value.toFormula()
        is String -> value.toFormula()
        is Double -> value.toFormula()
        is FizzyType -> value.toFormula()
        else -> throw RuntimeException("ListExpression doesn't know how to handle type : ${value.javaClass.simpleName}")
    }
}

