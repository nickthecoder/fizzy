/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.model.controlpoint.ControlPoint
import uk.co.nickthecoder.fizzy.prop.AbstractFListPropType
import uk.co.nickthecoder.fizzy.prop.FindListItem
import uk.co.nickthecoder.fizzy.prop.Prop

class ControlPointList : MutableFList<ControlPoint>()

class ControlPointListPropType
    : AbstractFListPropType<ControlPointList>(ControlPointList::class.java) {


    override fun findField(prop: Prop<ControlPointList>, name: String): Prop<*>? {
        // Lets us access a ControlPoint value using : ControlPoint.TheName.Point,
        // rather than the old way : ControlPoint.Point1 (assuming "TheName" is the first control point).
        prop.value.forEach { cp ->
            if (cp.name.value == name) {
                return FindListItem("ControlPoint", prop.value, name)
            }
        }

        return super.findField(prop, name)
    }

    companion object {
        val instance = ControlPointListPropType()
    }

}
