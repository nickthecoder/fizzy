/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.util.clamp
import uk.co.nickthecoder.fizzy.util.toFormula
import java.text.DecimalFormat

class DoublePropType private constructor()
    : PropType<Double>(Double::class.java) {

    override fun findMethod(prop: Prop<Double>, name: String): PropMethod<in Double, *>? {
        return when (name) {
            "toString" -> PropMethod0("Double.toString", prop) { prop.value.toFormula() }
            "format" -> PropMethod1("Double.format", prop, String::class.java) { format -> DecimalFormat(format).format(prop.value) }
            "clamp" -> PropMethod2("Double.clamp", prop, Double::class.java, Double::class.java) { min, max -> prop.value.clamp(min, max) }
            "min" -> PropMethod1("Double.min", prop, Double::class.java) { other -> Math.min(prop.value, other) }
            "max" -> PropMethod1("Double.max", prop, Double::class.java) { other -> Math.max(prop.value, other) }

            else -> super.findMethod(prop, name)
        }
    }

    companion object {
        val instance = DoublePropType()
    }
}

class DoubleExpression
    : PropExpression<Double> {

    constructor(expression: String, context: EvaluationContext = constantsContext) : super(expression, Double::class.java, context)

    constructor(other: DoubleExpression) : super(other)

    constructor(value: Double = 0.0) : this(value.toFormula())

    override val defaultValue: Double = java.lang.Double.parseDouble(".0")

    override fun toFormula(value: Double) = value.toFormula()

    override fun copy(link: Boolean) = if (link) DoubleExpression(this) else DoubleExpression(formula, constantsContext)

    override fun valueString() = value.toFormula()
}
