/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropConstant

/**
 * Connects to a Shape's pinned position (the 'center' of a box).
 * This is an alternative to ConnectToShape, but without code to connect a line's end intelligently.
 *
 * Designed to connect 2d shape to the center of another 2d shape, and also to connect a control point
 * to a 2d shape's center.
 */
class ConnectToShapePin(prop: Prop<Shape>, val local: Boolean)
    : TypedMethod1<Shape, Shape, Dimension2>("ConnectToShapePin", prop, Shape::class.java),
        Connection {

    private var along: Double = 0.0

    override val angle = PropConstant(Angle.ZERO)

    override fun isConstant() = false

    private var myShape: Shape? = null
        set(v) {
            if (field !== v) {
                field?.transform?.fromLocalToParent?.removeListener(this)
                field = v
                v?.transform?.fromLocalToParent?.addListener(this)
            }
        }

    private var connectTo: Shape? = null
        set(v) {
            if (field !== v) {
                field?.transform?.fromLocalToParent?.removeListener(this)
                field = v
                v?.transform?.fromLocalToParent?.addListener(this)
            }
        }

    override fun noLongerNeeded() {
        super.noLongerNeeded()

        connectTo?.transform?.fromLocalToParent?.removeListener(this)
        myShape?.transform?.fromLocalToParent?.removeListener(this)
    }

    override fun eval(a: Shape): Dimension2 {

        myShape = prop.value
        connectTo = a

        val pagePoint = connectTo!!.parent.localToPage(connectTo!!.transform.pin.value)

        return if (local) {
            myShape!!.pageToLocal(pagePoint)
        } else {
            myShape!!.parent.pageToLocal(pagePoint)

        }
    }

    override fun connectedTo(): Shape {
        value // Force evaluation
        return connectTo ?: prop.value
    }

}
