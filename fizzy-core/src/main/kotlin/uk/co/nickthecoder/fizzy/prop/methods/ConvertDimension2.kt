/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Matrix33
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.prop.Prop

abstract class ConvertDimension2(prop: Prop<Shape>)
    : TypedMethod1<Shape, Dimension2, Dimension2>("ConvertDimension2", prop, Dimension2::class.java) {

    private var previousConversion: Prop<Matrix33>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    override fun eval(a: Dimension2): Dimension2 {
        previousConversion = conversion()
        return previousConversion!!.value * a
    }

    protected abstract fun conversion(): Prop<Matrix33>
}

class LocalToParent(prop: Prop<Shape>)
    : ConvertDimension2(prop) {

    override fun conversion(): Prop<Matrix33> = prop.value.transform.fromLocalToParent
}

class ParentToLocal(prop: Prop<Shape>)
    : ConvertDimension2(prop) {

    override fun conversion(): Prop<Matrix33> = prop.value.transform.fromParentToLocal
}

class PageToLocal(prop: Prop<Shape>)
    : ConvertDimension2(prop) {

    override fun conversion(): Prop<Matrix33> = prop.value.transform.fromPageToLocal
}

class LocalToPage(prop: Prop<Shape>)
    : ConvertDimension2(prop) {

    override fun conversion(): Prop<Matrix33> = prop.value.transform.fromLocalToPage
}
