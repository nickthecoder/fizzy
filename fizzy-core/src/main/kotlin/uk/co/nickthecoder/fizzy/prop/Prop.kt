/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import java.lang.ref.WeakReference

/**
 * The base class for all properties. Properties which have a value which can be changed should subclass
 * [PropConstant], whereas properties who's value changes dynamically (such as PropCalculation) do NOT
 * subclass [PropConstant], and therefore the value is a val.
 *
 * T should be an immutable type, so that the property value cannot change without the property's listeners being
 * notified.
 * Note. Mutable classes such as Shape are also used as [Prop] values, and their listeners never fire.
 * This is ok, because they only use a Prop<Shape> to get to the Prop's that they care about.
 * i.e. the are temporary items within an expression.
 */
interface Prop<out T : Any> {

    val value: T

    /**
     * For debugging purposes only.
     */
    fun whyAlive(): String = "?"

    /**
     * Properties that are contant may be rolled up together during expression evaliation.
     * e.g. the following will result in a single PropConstant<Dimension2>
     *
     *     (1mm + 1pt, 10mm)
     */
    fun isConstant() = false

    fun field(name: String): Prop<*>? = PropType.field(this, name)

    fun method(name: String): Prop<*>? = PropType.method(this, name)

    fun valueString() = value.toString()

    fun addListener(listener: PropListener)

    fun removeListener(listener: PropListener): Boolean

}

abstract class AbstractProp<out T : Any> : Prop<T> {

    protected val propListeners = PropListeners()

    var expectedMaxListeners = 50

    override fun toString(): String = "Prop: $value"


    override fun addListener(listener: PropListener) {
        propListeners.add(listener)
        if (propListeners.items.size > expectedMaxListeners) {
            println("Warning too many Listeners (${propListeners.items.size}) : $this (${this.javaClass.simpleName})")
            propListeners.items.mapNotNull { it.get() }.forEach {
                val extra = if (it is Prop<*>) {
                    it.whyAlive()
                } else {
                    ""
                }
                println("    ${it.javaClass.simpleName} : $it   $extra")
            }
            expectedMaxListeners *= 2
        }

    }

    override fun whyAlive(): String = "Listeners : ${propListeners.items.size}"

    override fun removeListener(listener: PropListener): Boolean {
        return propListeners.remove(listener)
    }

    open fun notifyListeners() {
        propListeners.fireDirty(this)
    }

    protected open fun dependantsCount(): Int = propListeners.items.size
}


abstract class CountingProp<out T : Any>(val permanent: Boolean) : AbstractProp<T>() {

    private val dependants = mutableListOf<WeakReference<*>>()

    private var ignorableCount = 0

    protected var noLongerNeeded = false

    override fun whyAlive(): String = super.whyAlive() + " dependants : ${dependants.size}  ignorableCount : $ignorableCount  perm? : $permanent  nln? : $noLongerNeeded  $1st Listener : ${propListeners.items.firstOrNull()?.get()}"

    /**
     * Some properties, such as ConnectToShape have dependencies that are part of themselves,
     * and as such should NOT be counted when deciding if this Prop is [noLongerNeeded].
     */
    fun selfDependants(n: Int) {
        ignorableCount += n
    }

    fun addUncountedListener(listener: PropListener) {
        ignorableCount++
        addListener(listener)
    }

    fun removeUncountedListener(listener: PropListener) {
        ignorableCount--
        removeListener(listener)
    }

    override fun removeListener(listener: PropListener): Boolean {
        val removed = super.removeListener(listener)
        check()
        //if (!removed && dependantsCount()) {
        //    println("Warning remove( $listener ). Wasn't in the list of Listeners.")
        //}
        return removed
    }

    fun addDependant(dependant: Any) {
        dependants.forEach {
            if (it.get() === dependant) {
                throw RuntimeException("Already dependant")
            }
        }
        dependants.add(WeakReference(dependant))
    }

    fun removeDependant(dependant: Any) {
        dependants.removeIf { it.get() === dependant }

        if (!permanent && dependantsCount() <= ignorableCount) {
            noLongerNeeded()
        }
    }

    override fun dependantsCount(): Int = super.dependantsCount() + dependants.size

    private fun check() {
        propListeners.tidy()
        if (!noLongerNeeded && !permanent && dependantsCount() <= ignorableCount) {
            noLongerNeeded()
        }
    }

    /**
     * Called when the dependant count drops to zero.
     */
    open fun noLongerNeeded() {
        noLongerNeeded = true
        propListeners.clear()
    }

}

fun throwExpectedType(type: String, found: Prop<*>): Prop<*> {
    throw RuntimeException("Expected a $type, but found ${found.value.javaClass.simpleName}")
}
