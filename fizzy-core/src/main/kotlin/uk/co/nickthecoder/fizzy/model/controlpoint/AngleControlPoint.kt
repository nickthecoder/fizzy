/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.model.Vector2
import uk.co.nickthecoder.fizzy.prop.AngleExpression
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.DimensionExpression
import uk.co.nickthecoder.fizzy.prop.PropCalculation3
import uk.co.nickthecoder.fizzy.view.DrawContext

class AngleControlPoint(
        val start: Dimension2Expression,
        val radius: DimensionExpression,
        val angle: AngleExpression
) : ControlPoint(ControlPointType.ANGLE) {

    constructor(a: String = "(0mm,0mm)", radius: String = "10mm", angle: String = "0deg") : this(Dimension2Expression(a), DimensionExpression(radius), AngleExpression(angle))

    override val point = PropCalculation3("AngleControlPoint.point", true, start, radius, angle) { vStart, vRadius, vAngle ->
        vStart + Vector2(Math.cos(vAngle.radians), -Math.sin(vAngle.radians)) * vRadius
    }

    val min = AngleExpression("0 deg")

    val max = AngleExpression("360 deg")

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        start.context = context
        radius.context = context
        angle.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Start", start, "A").addAliases("A")
        metaData.newCell("Radius", radius, "B")
        metaData.newCell("Angle", angle, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        angle.value = (draggedTo - start.value).angle().clamp(min.value, max.value)
    }


    override fun drawHandle(dc: DrawContext, dragging: Boolean) {
        shape?.let { shape ->
            if (dragging) {
                dc.beginPath()
                dc.moveTo(Dimension2.ZERO_mm)
                dc.lineTo(shape.localToPage(start.value) - shape.localToPage(point.value))
                dc.endPath(stroke = true, fill = false)
            }
        }
        Controller.drawHandle(dc, Controller.CIRCLE)
    }

    override fun copy(link: Boolean): AngleControlPoint {
        val result = AngleControlPoint(start.copy(link), radius.copy(link), angle.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
