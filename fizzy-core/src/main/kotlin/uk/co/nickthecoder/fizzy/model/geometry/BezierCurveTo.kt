/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * Cubic bezier curves are defined by four points. The start and end points, plus two control points, which
 * the line usually heads towards, but doesn't touch.
 * The Start point is the [GeometryPart.point] of the previous geometry part in the list.
 * The end point is this.[point], and the two control points are [a] and [b].
 *
 * https://en.wikipedia.org/wiki/B%C3%A9zier_curve
 * A point along the curve is given by :
 *
 * B(t) = (1-t)³ P0 + 3(1-t)²t P1 + 3(1-t)t² P2 + t³ P3
 *
 * Where t is in the range 0..1. P0 is the start, P1 and P2 are the two control points, and P3 is the end point.
 *
 * https://pomax.github.io/bezierinfo/
 */
class BezierCurveTo(val a: Dimension2Expression, val b: Dimension2Expression, point: Dimension2Expression)
    : AbstractGeometryPart(point) {

    constructor(aFormula: String, bFormula: String, pointFormula: String) :
            this(Dimension2Expression(aFormula), Dimension2Expression(bFormula), Dimension2Expression(pointFormula))

    constructor(a: Dimension2, b: Dimension2, point: Dimension2) :
            this(a.toFormula(), b.toFormula(), point.toFormula())

    constructor() : this(Dimension2.ZERO_mm, Dimension2.ZERO_mm, Dimension2.ZERO_mm)

    private val pointsCache = mutableListOf<Dimension2>()

    private val prevListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            pointsCache.clear()
        }
    }

    init {
        a.addListener(this)
        b.addListener(this)
        prevPoint.addListener(prevListener)
        a.addListener(prevListener)
        b.addListener(prevListener)
    }

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        a.context = context
        b.context = context
    }

    override fun addEarlyMetaData(metaData: MetaData) {
        super.addEarlyMetaData(metaData)
        metaData.cells.add(MetaDataCell("A", a))
        metaData.cells.add(MetaDataCell("B", b))
    }

    private fun ensurePointsCache() {
        if (pointsCache.isEmpty()) {
            for (i in 1..16) {
                val t = i.toDouble() / 16.0
                pointsCache.add(pointAlong(t))
            }
        }
    }

    /**
     * B(t) = (1-t)³ P0 + 3(1-t)²t P1 + 3(1-t)t² P2 + t³ P3
     *
     * If we use "along" as the value for t in the cubic bezier curve equation above,
     * then we can get a point along the curve.
     */
    override fun pointAlong(along: Double): Dimension2 {
        val prev = prevPart.point.value
        val oneMinusT = 1 - along
        val oneMinusT2 = oneMinusT * oneMinusT
        val oneMinusT3 = oneMinusT2 * oneMinusT
        return prev * oneMinusT3 +
                a.value * (3 * oneMinusT2 * along) +
                b.value * (3 * oneMinusT * along * along) +
                point.value * (along * along * along)
    }

    /**
     * Equation for the tangent taken from here :
     * https://stackoverflow.com/questions/4089443/find-the-tangent-of-a-point-on-a-cubic-bezier-curve
     *
     *     dP(t) / dt =  -3(1-t)^2 * P0         + 3(1-t)^2 * P1      - 6t(1-t) * P1    - 3t^2 * P2    + 6t(1-t) * P2    + 3t^2 * P3
     *
     *     Where P0 = prev, P1 = a, P2 = b, P3 = this.point and t = along.
     */
    override fun tangentAlong(along: Double): Angle {
        val prev = prevPart.point.value
        val oneMinusT = 1 - along
        val oneMinusT2 = oneMinusT * oneMinusT

        val tangent = prev * (-3 * oneMinusT2) +
                a.value * (3 * oneMinusT2) -
                a.value * (6 * along * oneMinusT) -
                b.value * (3 * along * along) +
                b.value * (6 * along * oneMinusT) +
                point.value * (3 * along * along)

        return tangent.angle()
    }

    override fun crossCount(here: Dimension2): Int {
        ensurePointsCache()

        var result = 0
        var prev = prevPart.point.value
        pointsCache.forEach { p ->
            result += LineTo.crossCount(here, prev, p)
            prev = p
        }
        return result
    }

    override fun isAlong(here: Dimension2, lineWidth: Dimension, minDistance: Dimension): Boolean {
        ensurePointsCache()

        var prev = prevPart.point.value
        pointsCache.forEach { p ->
            if (LineTo.isAlong(shape, here, prev, p, lineWidth, minDistance)) {
                return true
            }
            prev = p
        }
        return false
    }


    override fun checkAlong(here: Dimension2): Pair<Double, Double>? {
        val shape = this.shape ?: return null

        ensurePointsCache()

        var minDistance: Double = Double.MAX_VALUE
        var minAlong = 0.0
        var prev = prevPart.point.value
        val interval = 1.0 / (pointsCache.size)

        pointsCache.forEachIndexed { index, p ->
            LineTo.checkAlong(shape, here, prev, p)?.let { (dist, along) ->
                if (dist < minDistance) {
                    minDistance = dist
                    minAlong = interval * (index + along)
                }
            }
            prev = p
        }
        return minDistance to minAlong
    }

    override fun copy(link: Boolean): GeometryPart = BezierCurveTo(a.copy(link), b.copy(link), point.copy(link))

    override fun draw(dc: DrawContext) {
        dc.bezierCurveTo(a.value, b.value, point.value)
    }

    override fun toString() = "BezierTo a=${a.value} b=${b.value} point=${point.value}"
}
