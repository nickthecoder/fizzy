/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListener
import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.evaluator.CompoundEvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.ThisContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.controlpoint.ControlPointType
import uk.co.nickthecoder.fizzy.model.geometry.Geometry
import uk.co.nickthecoder.fizzy.model.history.*
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.util.*

class ShapeChildren : MutableFList<Shape>()

abstract class Shape internal constructor(var parent: ShapeParent, val linkedFrom: Shape?, val id: Int)
    : Named, FindsUnits, ShapeParent, PropListener, HasChangeListeners<Shape>, MetaDataAware {

    override val name = PropVariable("")

    internal var loaded = true
        set(v) {
            field = v
            if (v && this is ShapeWithGeometry) {
                geometry.resetPrevParts()
            }
        }


    abstract val size: Dimension2Expression

    val lineWidth = DimensionExpression("2mm")

    val strokeColor = PaintExpression("BLACK")

    val fillColor = PaintExpression("WHITE")

    val themeClass = StringExpression()


    // Note, this is abstract to avoid leaking 'this' in the constructor, so it is only instantiated in final sub-classes
    abstract val context: EvaluationContext

    // Note, this is abstract to avoid leaking 'this' in the constructor, so it is only instantiated in final sub-classes
    abstract val transform: ShapeTransform

    abstract val locks: Locks

    override var changeListeners = ChangeListeners<Shape>()

    final override val children = ShapeChildren()


    val connectionPoints = ConnectionPointList()

    val smartConnectionType = StringExpression("SMART_CONNECTION_" + SmartConnectionType.AROUND.name)


    val controlPoints = ControlPointList()

    val snapPoints = SnapPointList()

    val scratches = ScratchList()

    val customProperties = CustomPropertyList()

    private var stencilShapeInfo: StencilShapeInfo? = null

    /**
     * When child Shapes change, this causes that even to bubble up to listeners of this Shape.
     */
    private val shapeListener = object : ChangeListener<Shape>, ListListener<Shape> {

        override fun changed(item: Shape, changeType: ChangeType, obj: Any?) {
            changeListeners.fireChanged(this@Shape)
        }

        override fun listChanged(list: FList<Shape>, item: Shape, index: Int, added: Boolean) {
            if (added) {
                changeListeners.fireChanged(this@Shape, ChangeType.ADD, item)
                item.changeListeners.add(this)
                if (item.parent !== this@Shape) {
                    throw IllegalStateException("Added a shape to the wrong parent")
                }
            } else {
                changeListeners.fireChanged(this@Shape, ChangeType.REMOVE, item)
                item.changeListeners.add(this)
            }
        }
    }

    /**
     * Keeps references to [ChangeAndListListener], so that they are not gc'd.
     * They do not need to be referenced directly, they take care of everything themselves.
     */
    protected val collectionListeners = mutableListOf<Any>()

    /**
     * Shapes should not be created directly with a constructor, instead use a static 'create' method,
     * which calls [postInit]. This is to ensure that 'this' is not leaked from the constructor.
     */
    protected open fun postInit() {
        document().usedId(id)

        children.listeners.add(shapeListener)

        listenTo(name, lineWidth, strokeColor, fillColor, smartConnectionType, size, themeClass)

        collectionListeners.add(ChangeAndListListener(this, connectionPoints,
                onAdded = { item, _ -> item.shape = this },
                onRemoved = { item, _ -> item.shape = null }
        ))
        collectionListeners.add(ChangeAndListListener(this, controlPoints,
                onAdded = { item, _ -> item.shape = this },
                onRemoved = { item, _ -> item.shape = null }
        ))
        collectionListeners.add(ChangeAndListListener(this, snapPoints,
                onAdded = { item, _ -> item.shape = this },
                onRemoved = { item, _ -> item.shape = null }
        ))
        collectionListeners.add(ChangeAndListListener(this, scratches,
                onAdded = { item, _ -> item.setContext(context) },
                onRemoved = { item, _ -> item.setContext(constantsContext) }
        ))
        collectionListeners.add(ChangeAndListListener(this, customProperties,
                onAdded = { item, _ -> item.setContext(context) },
                onRemoved = { item, _ -> item.setContext(constantsContext) }
        ))

        metaData().cells.forEach { cell ->
            val exp = cell.cellProp
            if (exp is PropExpression<*>) {
                exp.context = context
            }
        }
    }

    protected fun createContext(thisContext: ThisContext<*>) = CompoundEvaluationContext(listOf(
            constantsContext, thisContext))

    private var dirty = false
        set(v) {
            if (field != v) {
                field = v
                dirtyProp.value = v
                if (v) {
                    runLater("Shape is dirty") {
                        changeListeners.fireChanged(this)
                        dirty = false
                        // NOTE. Placing dirty=false before the fireChange caused infinite recursion
                        // But as it was via runLater didn't blow the stack, but took 100% CPU, and app didn't end.
                    }
                }
            }
        }

    internal val dirtyProp = PropVariable(dirty)

    override fun dirty(prop: Prop<*>) {
        dirty = true
    }

    override fun document(): Document = parent.document()

    override fun page(): Page = parent.page()

    override fun findUnits(name: String) = parent.page().pageSize.findUnits(name)

    override fun pageToLocal(pagePoint: Dimension2) = transform.fromPageToLocal.value * pagePoint

    override fun localToPage(localPoint: Dimension2) = transform.fromLocalToPage.value * localPoint

    override fun parentToLocal(parentPoint: Dimension2) = transform.fromParentToLocal.value * parentPoint

    override fun localToParent(localPoint: Dimension2) = transform.fromLocalToParent.value * localPoint

    override fun localToPage(localAngle: Angle): Angle {
        val zero = localToPage(Dimension2.ZERO_mm)
        val unit = localToPage(localAngle.unitDimension())
        return (unit - zero).angle()
    }

    override fun localToParent(localAngle: Angle): Angle {
        val zero = localToParent(Dimension2.ZERO_mm)
        val unit = localToParent(localAngle.unitDimension())
        return (unit - zero).angle()
    }

    override fun pageToLocal(pageAngle: Angle): Angle {
        val zero = pageToLocal(Dimension2.ZERO_mm)
        val unit = localToParent(pageAngle.unitDimension())
        return (unit - zero).angle()
    }

    override fun parentToLocal(parentAngle: Angle): Angle {
        val zero = parentToLocal(Dimension2.ZERO_mm)
        val unit = localToParent(parentAngle.unitDimension())
        return (unit - zero).angle()
    }

    fun isConnectedTo(other: Shape) = transform.isConnectedTo(other)

    fun findShape(id: Int): Shape? {
        if (id == this.id) return this

        children.forEach { child ->
            val found = child.findShape(id)
            if (found != null) return found
        }
        return null
    }

    override fun findChild(name: String): Shape? = children.firstOrNull { it.name.value == name }

    override fun findShape(name: String): Shape? {
        if (name == this.name.value) return this

        children.forEach { child ->
            val found = child.findShape(name)
            if (found != null) return found
        }
        return null
    }

    fun findShapeText(): ShapeText? {
        if (this is ShapeText) return this

        children.forEach { child ->
            if (child is ShapeText) {
                return child
            }
        }
        return null
    }

    /**
     * point should be in units of the parent (i.e. in the same units as this.transform.pin).
     * It is NOT in the coordinates used by the [Geometry] sections.
     * Therefore we first need to convert it to this shapes coordinates, and then compare it to the
     * [Geometry].
     *
     * Returns true iff this geometry is close to the given point, or if any of the descendants isAt the point.
     *
     */
    open fun isAt(pagePoint: Dimension2, minDistance: Dimension): Boolean {

        children.forEach { child ->
            if (child.isAt(pagePoint, minDistance)) {
                return true
            }
        }
        return false
    }

    fun findScratch(name: String): Scratch? {
        scratches.forEach { scratch ->
            if (scratch.name.value == name) {
                return scratch
            }
        }

        return null
    }

    /**
     * Listens to the expression, so that when it changes, Shape's listeners are informed.
     * The expression's [EvaluationContext] is also set.
     */
    fun listenTo(vararg expressions: Prop<*>) {
        expressions.forEach { expression ->
            expression.addListener(this)
            if (expression is PropExpression<*>) {
                expression.context = context
            }
        }
    }

    abstract fun copyInto(parent: ShapeParent, link: Boolean): Shape

    fun duplicate(): Shape {
        val copy = copyInto(parent, false)
        parent.children.add(copy)
        return copy
    }

    protected open fun populateShape(newShape: Shape, link: Boolean) {

        connectionPoints.forEach { connectionPoint ->
            newShape.connectionPoints.add(connectionPoint.copy(link))
        }
        controlPoints.forEach { controlPoint ->
            newShape.controlPoints.add(controlPoint.copy(link))
        }
        snapPoints.forEach { snapPoint ->
            newShape.snapPoints.add(snapPoint.copy(link))
        }
        scratches.forEach { scratch ->
            newShape.scratches.add(scratch.copy(link))
        }
        customProperties.forEach { customProperty ->
            newShape.customProperties.add(customProperty.copy(link))
        }

        children.forEach { child ->
            val copy = child.copyInto(newShape, link)
            newShape.children.add(copy)
        }
        metaData().copyInto(newShape.metaData(), link)
    }

    override fun createRow(type: String?, dataType: CellType?): Pair<MetaDataAware, MetaData> {

        ControlPointType.values().forEach { cpt ->
            if (cpt.code == type || cpt.aliases.contains(type)) {
                val cp = cpt.factory()
                controlPoints.add(cp)
                return Pair(cp, cp.metaData())
            }
        }

        return when (type) {
            "ConnectionPoint" -> {
                val cp = ConnectionPoint()
                connectionPoints.add(cp)
                Pair(cp, cp.metaData())
            }
            "Snap" -> {
                val sp = SnapPoint()
                snapPoints.add(sp)
                Pair(sp, sp.metaData())
            }
            "Scratch" -> {
                val scratch = Scratch(dataType ?: CellType.DIMENSION2)
                scratches.add(scratch)
                Pair(scratch, scratch.metaData())
            }
            "CustomProperty" -> {
                val customProperty = CustomProperty("", "", dataType ?: CellType.STRING)
                customProperties.add(customProperty)
                Pair(customProperty, customProperty.metaData())
            }
            else -> throw IllegalStateException("Shape does not have any rows of type $type")
        }

    }

    override fun getSection(sectionName: String): Pair<Any, MetaData> {
        return when (sectionName) {
            "Transform" -> Pair(transform, metaData().sections[sectionName]!!)
            "Lock" -> Pair(locks, metaData().sections[sectionName]!!)
            "ConnectionPoint" -> Pair(connectionPoints, metaData().sections[sectionName]!!)
            "ControlPoint" -> Pair(controlPoints, metaData().sections[sectionName]!!)
            "Snap" -> Pair(snapPoints, metaData().sections[sectionName]!!)
            "Scratch" -> Pair(scratches, metaData().sections[sectionName]!!)
            "CustomProperty" -> Pair(customProperties, metaData().sections[sectionName]!!)
            "StencilShapeInfo" -> Pair(getStencilShapeInfo(), metaData().sections[sectionName]!!)

            else -> throw IllegalStateException("Shape does not have a section named $sectionName")
        }
    }

    override fun metaData(): MetaData {
        val result = MetaData(null, null)
        addMetaData(result)
        return result
    }

    protected open fun addMetaData(metaData: MetaData) {
        metaData.newCell("Name", name)
        metaData.newCell("LineWidth", lineWidth)
        metaData.newCell("Size", size)
        metaData.newCell("LineColor", strokeColor)
        metaData.newCell("FillColor", fillColor)
        metaData.newCell("Class", themeClass)

        val transformSection = metaData.newSection("Transform")
        transform.addMetaData(transformSection)

        locks.addMetaData(metaData)

        val connectionPointsSection = metaData.newSection("ConnectionPoint")
        if (this is ShapeWithGeometry) {
            connectionPointsSection.newCell("ConnectAround", connectAround)
        }
        connectionPointsSection.newCell("SmartConnectionType", smartConnectionType)

        connectionPointsSection.rowFactories.add(RowFactory("New Connection Point") { index ->
            document().history.makeChange(AddConnectionPoint(this, index))
        })
        connectionPointsSection.rowRemoval = { index ->
            document().history.makeChange(RemoveConnectionPoint(this, index))
        }

        connectionPoints.forEach { connectionPoint ->
            val connectionPointRow = connectionPointsSection.newRow(null)
            connectionPoint.addMetaData(connectionPointRow)
        }


        val controlPointsSection = metaData.newSection("ControlPoint")
        ControlPointType.values().forEach { cpt ->
            controlPointsSection.rowFactories.add(RowFactory("New ${cpt.label} Control Point") { index ->
                document().history.makeChange(AddControlPoint(this, index, cpt.factory()))
            })
        }
        controlPointsSection.rowRemoval = { index ->
            document().history.makeChange(RemoveControlPoint(this, index))
        }

        controlPoints.forEach { controlPoint ->
            controlPointsSection.newRow(controlPoint.metaData())
        }


        val snapPointsSection = metaData.newSection("Snap")
        snapPointsSection.rowFactories.add(RowFactory("New Snap Point") { index ->
            document().history.makeChange(AddSnapPoint(this, index))
        })
        snapPointsSection.rowRemoval = { index ->
            document().history.makeChange(RemoveSnapPoint(this, index))
        }

        snapPoints.forEach { snapPoint ->
            val snapPointRow = snapPointsSection.newRow(null)
            snapPoint.addMetaData(snapPointRow)
        }


        val scratchSection = metaData.newSection("Scratch")
        for (type in CellType.values()) {
            scratchSection.rowFactories.add(RowFactory(type.label) { index ->
                document().history.makeChange(AddScratch(this, index, Scratch(type)))
            })
        }
        scratchSection.rowRemoval = { index ->
            document().history.makeChange(RemoveScratch(this, index))
        }
        scratches.forEach { scratch ->
            val scratchRow = scratchSection.newRow(null)
            scratch.addMetaData(scratchRow)
        }


        val customPropertySection = metaData.newSection("CustomProperty")
        for (type in CellType.values()) {
            customPropertySection.rowFactories.add(RowFactory(type.label) { index ->
                document().history.makeChange(AddCustomProperty(customProperties, index, CustomProperty("", "", type)))
            })
        }
        customPropertySection.rowRemoval = { index ->
            document().history.makeChange(RemoveCustomProperty(customProperties, index))
        }
        customProperties.forEach { customProperty ->
            val customPropertyRow = customPropertySection.newRow(null)
            customProperty.addMetaData(customPropertyRow)
        }

        val ssi = stencilShapeInfo
        if (ssi != null) {
            val ssimd = metaData.newSection("StencilShapeInfo")
            ssi.addMetaData(ssimd)
        }
    }

    /**
     * Only call this, if the shape is being used in a Stencil!
     */
    fun getStencilShapeInfo(): StencilShapeInfo {
        stencilShapeInfo?.let { return it }
        val ssi = StencilShapeInfo()
        stencilShapeInfo = ssi
        return ssi
    }

    fun debugCheckStale(): Boolean {
        var foundStale = false
        metaData().cells.forEach { cell ->
            val cp = cell.cellProp
            if (cp is PropExpression<*>) {
                val value = cell.cellProp.value
                cp.forceRecalculation()
                if (cp.value != value) {
                    foundStale = true
                    println("Stale : $cell")
                }
            }
        }
        return foundStale
    }

    override fun toString(): String = "${name.value} #$id"

}

private val noDocument = Document()
private val noPage = Page(noDocument)
val NO_SHAPE = Shape2d.create(noPage)

abstract class ShapeWithGeometry(parent: ShapeParent, linkedFrom: Shape?, id: Int)
    : Shape(parent, linkedFrom, id) {

    val strokeCap = StrokeCapExpression(StrokeCap.ROUND.toFormula())

    val strokeJoin = StrokeJoinExpression(StrokeJoin.ROUND.toFormula())

    /**
     * A list of Dimension (items of any other type will be ignored).
     * Defines the dash pattern for lines.
     */
    val dashes = ListExpression()

    /**
     * Converts [dashes], who's value is a list of Prop<*> to a Prop who's value is a DoubleArray.
     * Each of [dashes] Dimensions are converted into default units (a Double).
     * This can then be passed directly to DrawContext.dashes.
     */
    val processedDashes = PropCalculation1("Shape.processedDashes", true, dashes) {
        it.mapNotNull { (it.value as? Dimension)?.inDefaultUnits }.toDoubleArray()
    }

    /**
     * While drawing the shape, all straight lines are rounded. This has no effect on other
     * type of geometry parts.
     * Note, currently this only affect the appearance, and has no affect when checking for mouse clicks,
     * nor when connecting another shape to this this shape's geometry. Therefore is only appropriate for
     * small radii.
     */
    val cornerRadius = DimensionExpression("0mm")

    val connectAround = BooleanExpression("false")


    val geometry = Geometry(this)


    val geometryListener = object : ChangeListener<Geometry> {
        override fun changed(item: Geometry, changeType: ChangeType, obj: Any?) {
            changeListeners.fireChanged(this@ShapeWithGeometry, ChangeType.CHANGE, item)
        }
    }

    override fun postInit() {
        super.postInit()
        listenTo(strokeCap, strokeJoin, dashes, cornerRadius, connectAround)
        geometry.changeListeners.add(geometryListener)
    }

    override fun getSection(sectionName: String): Pair<Any, MetaData> {
        return when (sectionName) {
            "Geometry" -> Pair(geometry, metaData().sections[sectionName]!!)

            else -> super.getSection(sectionName)
        }
    }

    /**
     * point should be in units of the parent (i.e. in the same units as this.transform.pin).
     * It is NOT in the coordinates used by the [Geometry] sections.
     * Therefore we first need to convert it to this shapes coordinates, and then compare it to the
     * [Geometry].
     *
     * Returns true iff this geometry is close to the given point, or if any of the descendants isAt the point.
     *
     */
    override fun isAt(pagePoint: Dimension2, minDistance: Dimension): Boolean {
        val localPoint = transform.fromPageToLocal.value * pagePoint

        if (geometry.isAt(localPoint, lineWidth.value, minDistance)) {
            return true
        }

        return super.isAt(pagePoint, minDistance)
    }

    override fun populateShape(newShape: Shape, link: Boolean) {
        if (newShape !is ShapeWithGeometry) throw IllegalArgumentException("Expected a ShapeWithGeometry")

        // This is weird, but works. We first copy the geometry WITHOUT linking,
        // and then the metaData will link the properties later.
        geometry.copyInto(newShape, false)
        super.populateShape(newShape, link)
    }

    override fun addMetaData(metaData: MetaData) {
        super.addMetaData(metaData)
        metaData.newCell("StrokeCap", strokeCap)
        metaData.newCell("StrokeJoin", strokeJoin)
        metaData.newCell("Dashes", dashes)
        metaData.newCell("CornerRadius", cornerRadius)
        // ConnectAround is in the Geometry section

        val geometrySection = metaData.newSection("Geometry")
        geometry.addMetaData(geometrySection)
    }

}
