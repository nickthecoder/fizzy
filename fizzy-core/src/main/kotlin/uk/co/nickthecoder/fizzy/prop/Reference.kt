/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

class Reference
    : PropMethod<Dummy, Any>("Reference", dummyInstance), MutableProp<Any> {

    override fun isConstant() = false

    override fun eval(arg: Prop<*>): Any {
        return arg.value
    }

    override var value: Any
        get() = super.value
        set(v) {
            val ref = arg
            if (ref is MutableProp<*>) {
                @Suppress("UNCHECKED_CAST")
                (ref as MutableProp<Any>).value = v
            } else {
                throw IllegalStateException("Expected a MutableProp, but found ${ref}")
            }
        }
}
