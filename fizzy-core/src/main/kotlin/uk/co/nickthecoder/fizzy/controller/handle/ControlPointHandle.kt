/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.handle

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.ShapeHighlight
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.model.controlpoint.ControlPoint
import uk.co.nickthecoder.fizzy.model.history.ChangeExpression
import uk.co.nickthecoder.fizzy.prop.methods.Connection
import uk.co.nickthecoder.fizzy.view.DrawContext

class ControlPointHandle(shape: Shape, val controller: Controller, private val controlPoint: ControlPoint)
    : ShapeHandle(shape) {

    private var wasConstant = controlPoint.dragTo.isConstant()
    private var preFormula = controlPoint.dragTo.formula
    private var preValue = controlPoint.point.value

    override fun position() = shape.localToPage(controlPoint.point.value)

    override fun beginDrag(startPoint: Dimension2) {
        wasConstant = controlPoint.dragTo.calculatedProperty() !is Connection
        preValue = controlPoint.point.value
        preFormula = controlPoint.dragTo.formula
        controller.showConnectionPoints(true)
    }

    override fun dragTo(event: CMouseEvent, dragPoint: Dimension2) {
        val connectionData = controller.connectionData(dragPoint, shape, event.scale, false, true)
        val formula = if (connectionData != null) {
            connectionData.connectionPointFormula
        } else {
            shape.pageToLocal(dragPoint).toFormula()
        }
        controlPoint.dragTo.formula = formula

        controller.highlightShape.value = ShapeHighlight.create(connectionData)
    }

    override fun endDrag() {
        val newFormula = controlPoint.dragTo.formula

        // Momentarily set to the current position to its pre-drag value, so that undo can revert it correctly.
        if (wasConstant) {
            controlPoint.dragTo.value = preValue
        } else {
            controlPoint.dragTo.formula = preFormula
        }
        controlPoint.shape?.page()?.document?.history?.makeChange(ChangeExpression(controlPoint.dragTo, newFormula))

        controller.highlightShape.value = ShapeHighlight.NO_SHAPE_HIGHLIGHT
        controller.showConnectionPoints(false)
    }

    override fun draw(dc: DrawContext, dragging: Boolean) {
        controlPoint.draw(dc, dragging)
    }

    override fun isVisible() = controlPoint.visible.value

    override fun strokeColor() = Controller.CONTROL_HANDLE_STROKE
    override fun fillColor() = Controller.CONTROL_HANDLE_FILL

}
