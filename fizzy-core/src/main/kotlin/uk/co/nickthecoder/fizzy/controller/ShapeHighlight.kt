/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller

import uk.co.nickthecoder.fizzy.model.Color
import uk.co.nickthecoder.fizzy.model.NO_SHAPE
import uk.co.nickthecoder.fizzy.model.Shape

data class ShapeHighlight(val shape: Shape, val lineColor: Color, val fillColor: Color) {

    companion object {
        val NO_SHAPE_HIGHLIGHT = ShapeHighlight(NO_SHAPE, Color.TRANSPARENT, Color.TRANSPARENT)

        fun create(cd: ConnectionData?) = if (cd == null) {
            NO_SHAPE_HIGHLIGHT
        } else {
            when (cd.type) {
                ConnectionType.AROUND -> ShapeHighlight(cd.connectedToShape, Controller.GREEN_BASE, Color.TRANSPARENT)
                ConnectionType.SMART -> ShapeHighlight(cd.connectedToShape, Controller.GREEN_BASE, Controller.GREEN_BASE.transparent(0.5))
                ConnectionType.PIN -> ShapeHighlight(cd.connectedToShape, Controller.GREEN_BASE, Controller.GREEN_BASE.transparent(0.5))
                else -> NO_SHAPE_HIGHLIGHT
            }
        }

        fun create(shape: Shape, name: String): ShapeHighlight {
            return when (name) {
                "DELETE" -> ShapeHighlight(shape, Controller.RED_BASE, Color.TRANSPARENT)
                "PARENT" -> ShapeHighlight(shape, Controller.PURPLE_BASE, Color.TRANSPARENT)
                else -> NO_SHAPE_HIGHLIGHT
            }
        }
    }
}

