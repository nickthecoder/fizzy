/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller

import uk.co.nickthecoder.fizzy.model.Document
import uk.co.nickthecoder.fizzy.model.Page
import uk.co.nickthecoder.fizzy.model.Shape

object Clipboard : Iterable<Shape> {

    private val document = Document()
    private val page = Page(document)

    private val copied = mutableListOf<Shape>()

    fun copy(selection: Collection<Shape>) {
        copied.clear()
        copied.addAll(selection.map { it.copyInto(page, false) })
    }

    override fun iterator() = copied.iterator()

    fun isEmpty() = copied.isEmpty()
}
