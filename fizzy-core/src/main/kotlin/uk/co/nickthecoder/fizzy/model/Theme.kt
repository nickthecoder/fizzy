/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.evaluator.CompoundEvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.ThisContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.prop.*
import java.io.File

interface Theme {

    val name: String


    fun lineWidth(themeClass: String): Dimension

    fun strokeColor(themeClass: String): Paint

    fun fillColor(themeClass: String): Paint

    fun strokeCap(themeClass: String): StrokeCap

    fun strokeJoin(themeClass: String): StrokeJoin

    fun dashes(themeClass: String): List<Prop<Dimension>>

    fun cornerRadius(themeClass: String): Dimension

    fun startMargin(themeClass: String): Dimension

    fun endMargin(themeClass: String): Dimension

    fun lineMarking(type: String, themeClass: String): String

}

class StandardThemeData(var file: File) {

    private val map = mutableMapOf<String, MutableMap<String, PropExpression<*>>>()

    init {
        parse(file.readText())
    }

    private fun parse(text: String) {

        var themeClass: String = "ANY"

        text.split("\n").forEach { line ->
            val trimmed = line.trim()

            if (trimmed.startsWith(".")) {
                themeClass = trimmed.substring(1)
            } else {
                val colon = trimmed.indexOf(":")
                if (colon > 0) {
                    val key = trimmed.substring(0, colon).trim()
                    val exp = createExpression(key)
                    if (exp == null) {
                        throw RuntimeException("Unexpected attribute : '$key'")
                    } else {
                        exp.formula = trimmed.substring(colon + 1).trim()
                        add(themeClass, key, exp)
                    }
                }
            }
        }
    }

    private fun createExpression(attribute: String): PropExpression<*>? {
        return when (attribute) {
            "lineWidth" -> DimensionExpression()
            "strokeColor" -> PaintExpression()
            "fillColor" -> PaintExpression()
            "strokeCap" -> StrokeCapExpression()
            "strokeJoin" -> StrokeJoinExpression()
            "cornerRadius" -> DimensionExpression()
            "startMargin" -> DimensionExpression()
            "endMargin" -> DimensionExpression()
            "lineMarking" -> StringExpression()
            "dashes" -> ListExpression()
            else -> null
        }
    }


    private fun add(themeClass: String, attribute: String, expression: PropExpression<*>) {
        val existing = map[attribute]
        if (existing == null) {
            map[attribute] = mutableMapOf(themeClass to expression)
        } else {
            existing[themeClass] = expression
        }
    }

    internal fun find(attribute: String, themeClasses: String): PropExpression<*>? {
        val m = map[attribute]
        if (m == null) {
            return null
        }

        themeClasses.split(" ").forEach { themeClass ->
            val r = m[themeClass]
            if (r != null) {
                return r
            }
        }
        return null
    }

    internal fun find(attribute: String, type: String, themeClasses: String): PropExpression<*>? {
        val m = map[attribute]
        if (m == null) {
            return null
        }

        themeClasses.split(" ").forEach { themeClass ->
            val full = "$themeClass:$type"
            val r = m[full]
            if (r != null) {
                return r
            }
        }
        return null
    }
}

class StandardTheme(val data: StandardThemeData, val page: Page)
    : Theme {

    override val name = data.file.nameWithoutExtension

    private var fallBack: Theme = DefaultTheme(page)


    private val context = CompoundEvaluationContext(listOf(ThisContext(page, PagePropType.instance), constantsContext))


    private inline fun <reified T> findTyped(attribute: String, themeClasses: String): T? {
        val exp = data.find(attribute, themeClasses) ?: return null
        exp.context = context
        exp.forceRecalculation()
        return exp.value as? T?
    }

    private inline fun <reified T> findTyped(attribute: String, type: String, themeClasses: String): T? {
        val exp = data.find(attribute, type, themeClasses) ?: return null
        exp.context = context
        exp.forceRecalculation()
        return exp.value as? T?
    }

    override fun lineWidth(themeClass: String) = findTyped("lineWidth", themeClass)
            ?: fallBack.lineWidth(themeClass)

    override fun strokeColor(themeClass: String) = findTyped("strokeColor", themeClass)
            ?: fallBack.strokeColor(themeClass)

    override fun fillColor(themeClass: String) = findTyped("fillColor", themeClass)
            ?: fallBack.fillColor(themeClass)

    override fun cornerRadius(themeClass: String) = findTyped("cornerRadius", themeClass)
            ?: fallBack.cornerRadius(themeClass)

    override fun strokeCap(themeClass: String) = findTyped("strokeCap", themeClass)
            ?: fallBack.strokeCap(themeClass)

    override fun strokeJoin(themeClass: String) = findTyped("strokeJoin", themeClass)
            ?: fallBack.strokeJoin(themeClass)

    override fun dashes(themeClass: String) = findTyped("dashes", themeClass)
            ?: fallBack.dashes(themeClass)

    override fun startMargin(themeClass: String) = findTyped("startMargin", themeClass)
            ?: fallBack.startMargin(themeClass)

    override fun endMargin(themeClass: String) = findTyped("endMargin", themeClass)
            ?: fallBack.endMargin(themeClass)

    override fun lineMarking(type: String, themeClass: String) = findTyped("lineMarking", type, themeClass)
            ?: fallBack.lineMarking(type, themeClass)

}

class DefaultTheme(val page: Page)
    : Theme {

    private val lineThickness = Dimension(1.0, page.findUnits("pt") ?: Units.mm)

    override val name = "DEFAULT"

    override fun lineWidth(themeClass: String) = if (themeClass.split(" ").contains("text")) Dimension.ZERO_mm else lineThickness

    override fun fillColor(themeClass: String) = when {
        themeClass.split(" ").contains("line") -> Color.TRANSPARENT
        themeClass.split(" ").contains("text") -> Color.BLACK
        else -> Color.WHITE
    }

    override fun strokeColor(themeClass: String) = if (themeClass.split(" ").contains("text")) Color.TRANSPARENT else Color.BLACK

    override fun cornerRadius(themeClass: String) = Dimension.ZERO_mm

    override fun strokeCap(themeClass: String) = StrokeCap.BUTT

    override fun strokeJoin(themeClass: String) = StrokeJoin.ROUND

    override fun dashes(themeClass: String): List<Prop<Dimension>> = emptyList()

    override fun startMargin(themeClass: String) = Dimension.ZERO_mm

    override fun endMargin(themeClass: String) = Dimension.ZERO_mm

    override fun lineMarking(type: String, themeClass: String) = when (type) {
        "arrow" -> "Arrow (Plain)"
        else -> ""
    }

}
