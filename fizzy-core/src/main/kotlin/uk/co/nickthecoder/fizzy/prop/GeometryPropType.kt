/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.geometry.*
import java.lang.RuntimeException


class GeometryPropType private constructor()
    : PropType<Geometry>(Geometry::class.java) {

    override fun findField(prop: Prop<Geometry>, name: String): Prop<*>? {

        // Lets us access a GeometryPart value using : Geometry.TheName.Point,
        // rather than the old way : Geometry.Point1 (assuming "TheName" is the first control point).
        prop.value.parts.forEach { part ->
            if (part.name.value == name) {
                return FindListItem("GeometryPart", prop.value.parts, name)
            }
        }

        // Allow access to any of the Geometries parts, without the hassle of ".parts.xxx"
        val partsField = ConstantPropField("Geometry.parts", prop) { prop.value.parts }

        return partsField.field(name) ?: super.findField(prop, name)
    }

    companion object {
        val instance = GeometryPropType()
    }
}

class GeometryPartExpression(formula: String = "", context: EvaluationContext = constantsContext)
    : PropExpression<GeometryPart>(formula, GeometryPart::class.java, context) {
    override val defaultValue: GeometryPart = MoveTo()
    override fun copy(link: Boolean): GeometryPartExpression = GeometryPartExpression(formula, context)
    override fun toFormula(value: GeometryPart): String {
        throw RuntimeException("Cannot produce a formula for a GeometryPart")
    }
}

abstract class GeometryPartPropType<T : GeometryPart>(klass: Class<*>)
    : PropType<T>(klass) {


    override fun findField(prop: Prop<T>, name: String): Prop<*>? {
        return when (name) {
            "Name" -> prop.value.name
            "Point" -> prop.value.point
            else -> null
        }
    }

    override fun findMethod(prop: Prop<T>, name: String): PropMethod<in GeometryPart, *>? {
        return when (name) {
            "pointAlong" -> PropMethod1("Geometry.pointAlong", prop, Double::class.java) { along: Double -> prop.value.pointAlong(along) }
            "tangentAlong" -> PropMethod1("Geometry.tangentAlong", prop, Double::class.java) { along: Double -> prop.value.tangentAlong(along) }
            else -> null
        }
    }
}

class MoveToPropType private constructor()
    : GeometryPartPropType<MoveTo>(MoveTo::class.java) {

    companion object {
        val instance = MoveToPropType()
    }
}

class LineToPropType private constructor()
    : GeometryPartPropType<LineTo>(LineTo::class.java) {

    companion object {
        val instance = LineToPropType()
    }
}

class BezierToPropType private constructor()
    : GeometryPartPropType<BezierCurveTo>(BezierCurveTo::class.java) {

    override fun findField(prop: Prop<BezierCurveTo>, name: String): Prop<*>? {
        return when (name) {
            "Point" -> prop.value.point
            "A" -> prop.value.a
            "B" -> prop.value.b
            else -> null
        }
    }

    companion object {
        val instance = BezierToPropType()
    }
}


class RoundedCornerPropType private constructor()
    : GeometryPartPropType<RoundedCorner>(RoundedCorner::class.java) {

    override fun findField(prop: Prop<RoundedCorner>, name: String): Prop<*>? {
        return when (name) {
            "Mid" -> prop.value.mid
            "Point" -> prop.value.point
            "Radius" -> prop.value.radius
            else -> null
        }
    }

    companion object {
        val instance = RoundedCornerPropType()
    }
}


class ArcToPropType private constructor()
    : GeometryPartPropType<ArcTo>(ArcTo::class.java) {

    override fun findField(prop: Prop<ArcTo>, name: String): Prop<*>? {
        return when (name) {
            "SegmentHeight" -> prop.value.segmentHeight
            "Radius" -> prop.value.radius
            "Center" -> prop.value.center
            "FromAngle" -> prop.value.fromAngle
            "ToAngle" -> prop.value.toAngle
            "Sweep" -> prop.value.sweep
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = ArcToPropType()
    }
}


class ClosePropType private constructor()
    : GeometryPartPropType<Close>(Close::class.java) {

    companion object {
        val instance = ClosePropType()
    }
}