/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.ArgList
import uk.co.nickthecoder.fizzy.model.*

/**
 * See [DummyPropType]
 */
class Dummy

/**
 * Functions are implemented by the same mechanism as methods, where the [Prop] that the methods refers to is of type
 * Prop<[Dummy]> (and is is just ignored!
 */
class DummyPropType private constructor()
    : PropType<Dummy>(Dummy::class.java) {

    override fun findField(prop: Prop<Dummy>, name: String): PropField<Dummy>? {
        return null
    }

    override fun findMethod(prop: Prop<Dummy>, name: String): PropMethod<Dummy, *>? {
        return when (name) {
            "abs" -> PropFunction1("abs", Double::class.java) { Math.abs(it) }
            "ceil" -> PropFunction1("ceil", Double::class.java) { Math.ceil(it) }
            "floor" -> PropFunction1("floor", Double::class.java) { Math.floor(it) }
            "exp" -> PropFunction1("exp", Double::class.java) { Math.exp(it) }
            "ln" -> PropFunction1("ln", Double::class.java) { Math.log(it) }
            "log" -> PropFunction1("log", Double::class.java) { Math.log10(it) }
            "REF" -> Reference()
            "round" -> PropFunction1("round", Double::class.java) { Math.round(it).toDouble() }
            "sqrt" -> PropFunction1("sqrt", Double::class.java) { Math.sqrt(it) }

            "min" -> PropFunction2("min", Double::class.java, Double::class.java) { a, b -> Math.min(a, b) }
            "max" -> PropFunction2("max", Double::class.java, Double::class.java) { a, b -> Math.max(a, b) }

            "sin" -> PropFunction1("sin", Angle::class.java) { Math.sin(it.radians) }
            "cos" -> PropFunction1("cos", Angle::class.java) { Math.cos(it.radians) }
            "tan" -> PropFunction1("tan", Angle::class.java) { Math.tan(it.radians) }
            "sinh" -> PropFunction1("sinh", Angle::class.java) { Math.sinh(it.radians) }
            "cosh" -> PropFunction1("cosh", Angle::class.java) { Math.cosh(it.radians) }
            "tanh" -> PropFunction1("tanh", Angle::class.java) { Math.tanh(it.radians) }

            "asin" -> PropFunction1("asin", Double::class.java) { Angle.radians(Math.asin(it)) }
            "acos" -> PropFunction1("acos", Double::class.java) { Angle.radians(Math.acos(it)) }
            "atan" -> PropFunction1("atan", Double::class.java) { Angle.radians(Math.atan(it)) }

            "if" -> IfFunction()

            "Vector2" -> PropFunction2("Vector2()", Double::class.java, Double::class.java) { x, y -> Vector2(x, y) }
            "Dimension2" -> PropFunction2("Dimension2()", Dimension::class.java, Dimension::class.java) { x, y -> Dimension2(x, y) }

            "WebColor" -> PropFunction1("WebColor()", String::class.java) { Color.web(it) }
            "RGB" -> PropFunction3("RGB()", Double::class.java, Double::class.java, Double::class.java) { r, g, b -> Color(clamp0To1(r), clamp0To1(g), clamp0To1(b)) }
            "RGBA" -> PropFunction4("RGBA()", Double::class.java, Double::class.java, Double::class.java, Double::class.java) { r, g, b, a -> Color(clamp0To1(r), clamp0To1(g), clamp0To1(b), clamp0To1(a)) }
            else -> null
        }
    }

    companion object {
        val instance = DummyPropType()
    }
}

fun clamp0To1(v: Double) = Math.min(1.0, Math.max(0.0, v))

/**
 * The one and only instance of a [Prop] of type [Dummy] used as the [PropMethod]'s value when the 'method' is really a
 * function (and applies to nothing).
 */
val dummyInstance = PropConstant(Dummy())

class PropFunction1<A : Any, R : Any>(description: String, klassA: Class<A>, lambda: (A) -> R)
    : PropMethod1<Dummy, A, R>(description, dummyInstance, klassA, lambda)

class PropFunction2<A : Any, B : Any, R : Any>(description: String, klassA: Class<A>, klassB: Class<B>, lambda: (A, B) -> R)
    : PropMethod2<Dummy, A, B, R>(description, dummyInstance, klassA, klassB, lambda)

class PropFunction3<A : Any, B : Any, C : Any, R : Any>(description: String, klassA: Class<A>, klassB: Class<B>, klassC: Class<C>, lambda: (A, B, C) -> R)
    : PropMethod3<Dummy, A, B, C, R>(description, dummyInstance, klassA, klassB, klassC, lambda)

class PropFunction4<A : Any, B : Any, C : Any, D : Any, R : Any>(description: String, klassA: Class<A>, klassB: Class<B>, klassC: Class<C>, klassD: Class<D>, lambda: (A, B, C, D) -> R)
    : PropMethod4<Dummy, A, B, C, D, R>(description, dummyInstance, klassA, klassB, klassC, klassD, lambda)

class IfFunction : PropMethod<Dummy, Any>("If", dummyInstance) {

    override fun eval(arg: Prop<*>): Any {
        if (arg is ArgList && arg.value.size == 3) {
            applyArgs(arg)
            val condition = arg.value[0].value
            if (condition is Boolean) {
                val a = arg.value[1]
                val b = arg.value[2]
                return if (condition) a.value else b.value
            }
        }
        throw RuntimeException("Expected (Boolean,Any,Any), but found ${arg.value}")
    }
}