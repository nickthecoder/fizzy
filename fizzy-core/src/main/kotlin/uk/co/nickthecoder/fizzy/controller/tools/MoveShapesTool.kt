/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.tools

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape1d
import uk.co.nickthecoder.fizzy.model.Shape2dTransform
import uk.co.nickthecoder.fizzy.model.history.ChangeExpression
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.methods.Connection


class MoveShapesTool(controller: Controller, var previousPoint: Dimension2)
    : Tool(controller) {

    override val cursor: ToolCursor = ToolCursor.MOVE

    val document = controller.page.document

    init {
        document.history.beginBatch()
    }

    override fun beginTool() {
        if (controller.selection.firstOrNull()?.transform is Shape2dTransform) {
            controller.showConnectionPoints(true)
        }
    }

    override fun endTool(replacement: Tool) {
        controller.showConnectionPoints(false)
        super.endTool(replacement)
    }

    private fun ignoreConnectedEnds(end: Dimension2Expression, delta: Dimension2): String? {
        end.value
        val cp = end.calculatedProperty()
        return if (cp is Connection && controller.selection.contains(cp.connectedTo())) {
            null
        } else {
            (end.value + delta).toFormula()
        }
    }

    override fun onMouseDragged(event: CMouseEvent) {

        val transformA = controller.selection.firstOrNull()?.transform
        if (controller.selection.size == 1 && transformA is Shape2dTransform) {

            val oldPin = transformA.pin.value
            val newPin = transformA.pin.value + (event.point - previousPoint)

            val connectionData = controller.connectionData(newPin, transformA, event.scale)
            if (connectionData != null) {

                document.history.makeChange(
                        ChangeExpression(transformA.pin, connectionData.connectionPointFormula)
                )

                previousPoint += transformA.pin.value - oldPin
                return
            }
        }


        val delta = if (event.isConstrain)
            event.point - previousPoint
        else
            controller.calculateSnap(controller.selection.lastOrNull(), controller.parent, event.point - previousPoint)

        val list = mutableListOf<Pair<Dimension2Expression, String>>()

        // 1d shapes
        controller.selection.filterIsInstance<Shape1d>().forEach { shape ->

            val newStart = ignoreConnectedEnds(shape.start, delta)
            val newEnd = ignoreConnectedEnds(shape.end, delta)

            if (newStart != null) list.add(shape.start to newStart)
            if (newEnd != null) list.add(shape.end to newEnd)

        }

        // 2d (including text) shapes
        controller.selection.forEach { shape ->
            val transform = shape.transform
            if (transform is Shape2dTransform) {
                val formula = ignoreConnectedEnds(transform.pin, delta)
                if (formula != null) {
                    list.add(transform.pin to (transform.pin.value + delta).toFormula())
                }
            }
        }

        if (list.isNotEmpty()) {
            document.history.makeChange(
                    ChangeExpressions(list)
            )
        }
        previousPoint += delta

    }

    override fun onMouseReleased(event: CMouseEvent) {

        /*
        // Connect any lines to nearby connection points
        controller.selection.filterIsInstance<Shape1d>().forEach { shape ->

            controller.connectionData(shape.parent.localToPage(shape.start.value), shape, event.scale, true)?.let {
                document.history.makeChange(ChangeExpression(shape.start, it.connectionPointFormula))
            }
            controller.connectionData(shape.parent.localToPage(shape.end.value), shape, event.scale, false)?.let {
                document.history.makeChange(ChangeExpression(shape.end, it.connectionPointFormula))
            }
        }
        */

        document.history.endBatch()
        controller.tool = SelectTool(controller)
    }

}
