/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * A circular arc travelling from the previous part's [point] to [point].
 * The curvature is determined by [segmentHeight]. If this is zero, then the result will be a straight line.
 *
 * [center], [radius], [fromAngle] and [toAngle] are calculated values.
 */
class ArcTo(point: Dimension2Expression, val segmentHeight: DimensionExpression)
    : AbstractGeometryPart(point) {

    constructor(pointFormula: String, heightFormula: String) : this(Dimension2Expression(pointFormula), DimensionExpression(heightFormula))

    constructor(point: Dimension2, height: Dimension) : this(point.toFormula(), height.toFormula())

    constructor() : this(Dimension2.ZERO_mm, Dimension.ZERO_mm)

    private val chordLength = PropCalculation2("ArcTo.chordLength", true, prevPoint, this.point) { vA, vB -> (vB - vA).length() }

    // See https://en.wikipedia.org/wiki/Circular_segment
    val radius: Prop<Dimension> = PropCalculation2("ArcTo.radisu", true, segmentHeight, chordLength) { vHeight, vC ->
        if (vHeight.inDefaultUnits > 0.0) {
            vHeight / 2.0 + vC * vC / (vHeight * 8.0)
        } else {
            vHeight / -2.0 + vC * vC / (vHeight * -8.0) // Ensures radius is +ve
        }
    }

    val center: Prop<Dimension2> = PropCalculation4("ArcTo.center", true, prevPoint, this.point, radius, segmentHeight) { vA, vB, vR, vH ->
        val mid = (vA + vB) / 2.0
        val foo = (vB - vA)
        val away = Dimension2(foo.y, -foo.x).normalise()
        if (vH.inDefaultUnits > 0.0) {
            mid + away * (vR - vH)
        } else {
            mid + away * (-vR - vH)
        }
    }

    val fromAngle: Prop<Angle> = PropCalculation2("ArcTo.fromAngle", true, prevPoint, center) { p, c -> (p - c).angle() }

    val toAngle: Prop<Angle> = PropCalculation2("ArcTo.toAngle", true, point, center) { p, c -> (p - c).angle() }

    val sweep: Prop<Angle> = PropCalculation4("ArcTo.sweep", true, fromAngle, toAngle, radius, segmentHeight) { from, to, r, h ->
        var radians = Angle.radians0ToTau(to.radians - from.radians)
        if (radians > Math.PI) {
            radians -= Math.PI * 2
        }
        // Now radians is in the range -PI..PI

        // Do we need to use the "bigger" version?
        if (Math.abs(h.inDefaultUnits) > r.inDefaultUnits) {
            radians = if (radians > 0) {
                radians - Math.PI * 2
            } else {
                Math.PI * 2 + radians
            }
        }
        Angle.radians(radians)
    }

    init {
        segmentHeight.addListener(this)
    }

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        segmentHeight.context = context
    }

    override fun addEarlyMetaData(metaData: MetaData) {
        super.addEarlyMetaData(metaData)
        metaData.cells.add(MetaDataCell("SegmentHeight", segmentHeight, "A").addAliases("Height"))
    }

    /**
     * Draws the arc using a series of bezier curves. The number of curves will vary from 1 to 4, depending
     * on the angle we need to trace out. This gives a reasonable approximation to a circular arc, but isn't perfect.
     */
    override fun draw(dc: DrawContext) {

        if (segmentHeight.value.inDefaultUnits == 0.0) {
            dc.lineTo(point.value)
            return
        }

        if (segmentHeight.value.inDefaultUnits == 0.0) {
            dc.lineTo(point.value)
            return
        }

        val angle1 = fromAngle.value

        //val angle2 = toAngle.value
        val diff = sweep.value.radians

        //var diff = angle2.radians + Math.PI * 2 - angle1.radians
        //if (diff > Math.PI * 2) diff -= Math.PI * 2.0
        //if (segmentHeight.value.inDefaultUnits < 0) diff = (diff - Math.PI * 2.0)

        // How many bezier curves shall we use to approximate the arc.
        // In general, the larger the angle, the more curves we need. 4 is ok for a full circle.
        val parts = (Math.abs(diff * 0.6) + 1).toInt()

        // Change in angle for each bezier curve
        val delta = diff / parts

        // The distance of the bezier curve's control point (c1) from the start point.
        // This will also be the distance of c2 from the end point.
        val controlDist = radius.value * (4.0 / 3.0 * Math.tan(delta / 4))

        // The tangent at bezier curve's start point.
        var tangent1 = angle1.normalUnitVector() * controlDist
        // The start of the bezier curve
        var p1 = prevPoint.value

        for (i in 1..parts) {
            // The angle around the circle
            val theta = angle1 + Angle.radians(diff * (i.toDouble() / parts))

            // The tangent at the bezier curve's end point
            val tangent2 = theta.normalUnitVector() * controlDist
            // The end of the bezier curve.
            val p2 = center.value + theta.unitVector() * radius.value

            dc.bezierCurveTo(p1 + tangent1, p2 - tangent2, p2)

            // Update for the next iteration
            tangent1 = tangent2
            p1 = p2
        }
    }

    override fun tangentAlong(along: Double): Angle {
        if (segmentHeight.value.inDefaultUnits == 0.0) {
            return (point.value - prevPart.point.value).angle()
        }

        return fromAngle.value + sweep.value * along + if (segmentHeight.value.inDefaultUnits < 0.0) Angle.degrees(-90.0) else Angle.degrees(90.0)
    }

    override fun pointAlong(along: Double): Dimension2 {
        if (segmentHeight.value.inDefaultUnits == 0.0) {
            return prevPart.point.value + (point.value - prevPart.point.value) * along
        }

        val angle = fromAngle.value + sweep.value * along
        return center.value + radius.value * angle.unitVector()
    }

    override fun crossCount(here: Dimension2): Int {
        if (segmentHeight.value.inDefaultUnits == 0.0) {
            return LineTo.crossCount(here, prevPart.point.value, point.value)
        }

        // Find the two points the horizontal line (Y = here.y) crosses the circle.
        // If it is above or below the circle, then return false.
        // For both points, if x < here.x, then ignore that solution.
        // Finally test if the angle of both points is within the range of angles.

        // The height the horizontal line crosses relative to the center of the circle
        val ty = here.y.inDefaultUnits - center.value.y.inDefaultUnits
        val r = radius.value.inDefaultUnits

        // If it is above or below the circle, then return false.
        if (ty < -r || ty > r) return 0

        var crossCount = 0

        // From Pythag. x = sqrt(r² - y²)
        val x = Math.sqrt(r * r - ty * ty)

        val from = if (sweep.value.radians > 0.0) fromAngle.value.radians else toAngle.value.radians
        val to = from + Math.abs(sweep.value.radians)

        // tx is the x value relative to the center of the circle
        fun test(tx: Double): Boolean {
            // If x < here.x, then ignore that solution
            if (center.value.x.inDefaultUnits + tx < here.x.inDefaultUnits) return false

            val angle = Math.atan2(-ty, tx)
            //println("Within? ${Angle.isRadiansWithin(angle, from, to)}")
            return Angle.isRadiansWithin(angle, from, to)
        }

        if (test(x)) crossCount++
        if (test(-x)) crossCount++

        //println("Cross count $crossCount RESULT : ${crossCount.rem(2) == 1}")
        return crossCount
    }

    override fun isAlong(here: Dimension2, lineWidth: Dimension, minDistance: Dimension): Boolean {
        if (segmentHeight.value.inDefaultUnits == 0.0) {
            return LineTo.isAlong(shape, here, prevPart.point.value, point.value, lineWidth, minDistance)
        }

        val angle = (here - center.value).angle()
        val rel = (angle - fromAngle.value).zeroToTau()

        if (sweep.value.radians < 0) {
            if (rel.radians < Math.PI * 2 + sweep.value.radians) return false
        } else {
            if (rel.radians > sweep.value.radians) return false
        }

        val dist = center.value + angle.unitVector() * radius.value - here
        return dist.length() < minDistance + lineWidth
    }

    override fun checkAlong(here: Dimension2): Pair<Double, Double>? {
        if (segmentHeight.value.inDefaultUnits == 0.0) {
            val shape = this.shape ?: return null
            return LineTo.checkAlong(shape, here, prevPart.point.value, point.value)
        }

        val angle = (here - center.value).angle()
        val rel = (angle - fromAngle.value).zeroToTau()

        if (sweep.value.radians < 0) {
            if (rel.radians < Math.PI * 2 + sweep.value.radians) return null
        } else {
            if (rel.radians > sweep.value.radians) return null
        }

        val dist = center.value + angle.unitVector() * radius.value - here

        val along = if (sweep.value.radians > 0)
            rel.radians / sweep.value.radians
        else
            (rel.radians - Math.PI * 2) / sweep.value.radians

        return Pair(dist.length().inDefaultUnits, along)
    }

    override fun copy(link: Boolean) = ArcTo(point.copy(link), segmentHeight.copy(link))

}
