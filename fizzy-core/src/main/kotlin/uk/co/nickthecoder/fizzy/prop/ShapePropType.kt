/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.methods.*

class ShapeExpression(formula: String, context: EvaluationContext = constantsContext)
    : PropExpression<Shape>(formula, Shape::class.java, context) {

    override val defaultValue: Shape = PrimitiveShapes.box

    override fun copy(link: Boolean): ShapeExpression = ShapeExpression(formula, context)

    override fun toFormula(value: Shape): String {
        throw RuntimeException("Cannot make a formula for a Shape")
    }
}

class ShapeChildrenPropType private constructor()
    : PropType<ShapeChildren>(ShapeChildren::class.java) {

    override fun findField(prop: Prop<ShapeChildren>, name: String): Prop<*>? {
        prop.value.forEach { property ->
            if (property.name.value == name) {
                return FindListItem("Child", prop.value, name)
            }
        }
        return super.findField(prop, name)
    }

    companion object {
        val instance = ShapeChildrenPropType()
    }
}


abstract class ShapePropType<T : Shape>(klass: Class<T>)
    : PropType<T>(klass) {

    override fun findField(prop: Prop<T>, name: String): Prop<*>? {

        if (name.startsWith("Shape")) {
            try {
                val id = name.substring(5).toInt()
                val shape = prop.value.findShape(id) ?: throw RuntimeException("Child shape $id not found")
                return PropValue(shape)
            } catch (e: NumberFormatException) {
                // Do nothing
            }
        }

        return when (name) {
            "ID" -> ConstantPropField("Shape.ID", prop) { it.value.id }
            "Name" -> PropField("Shape.Name", prop) { it.value.name }
            "Class" -> PropField("Shape.Class", prop) { it.value.themeClass }
            "Document" -> ConstantPropField("Shape.Document", prop) { it.value.document() }
            "Page" -> ConstantPropField("Shape.Page", prop) { it.value.page() }
            "Parent" -> ConstantPropField("Shape.Parent", prop) { it.value.parent }
            "Child" -> PropValue(prop.value.children)

            "ConnectionPoint" -> PropValue(prop.value.connectionPoints)
            "ControlPoint" -> PropValue(prop.value.controlPoints)
            "Snap" -> PropValue(prop.value.snapPoints)
            "Scratch" -> PropValue(prop.value.scratches)
            "CustomProperty" -> PropValue(CustomPropertyShortcut(prop.value.customProperties))
            "CustomProperties" -> PropValue(prop.value.customProperties)

            "LineWidth" -> PropField("Shape.LineWidth", prop) { it.value.lineWidth }
            "StrokeColor" -> PropField("Shape.StrokeColor", prop) { it.value.strokeColor }
            "LineColor" -> PropField("Shape.LineColor", prop) { it.value.strokeColor }
            "FillColor" -> PropField("Shape.FillColor", prop) { it.value.fillColor }
            "Rotation" -> PropField("Shape.Rotation", prop) { prop.value.transform.rotation }
            "Size" -> PropField("Shape.Size", prop) { it.value.size }

            "SmartConnectionType" -> PropField("Shape.SmartConnectionType", prop) { it.value.smartConnectionType }
            "Theme" -> PropDependency1<Theme, Shape>("Shape.Theme", prop) { it.page().theme }

            else -> super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<T>, name: String): PropMethod<in T, *>? {
        return when (name) {
            "findShape" -> FindShape(prop)

            "findCustomProperty" -> FindListItemDataMethod(prop, "CustomProperty") { shape -> shape.customProperties }
            "findScratch" -> FindListItemDataMethod(prop, "Scratch") { shape -> shape.scratches }
            "findControlPoint" -> FindListItemMethod(prop, "ControlPoint") { shape -> shape.controlPoints }

            "localToParent" -> LocalToParent(prop)
            "localToPage" -> LocalToPage(prop)
            "parentToLocal" -> ParentToLocal(prop)
            "pageToLocal" -> PageToLocal(prop)

            "connectTo" -> ConnectTo(prop, false) // Using a connection point
            "connectAround" -> ConnectAround(prop, false) // Around the Geometry of a Shape.
            "connectToShapePin" -> ConnectToShapePin(prop, false) // Connect to the pin (the center of rotation)

        // As above, but in local coordinates, rather than parent's
            "connectLocalTo" -> ConnectTo(prop, true)
            "connectLocalAround" -> ConnectAround(prop, true)
            "connectLocalToShapePin" -> ConnectToShapePin(prop, true)

            else -> super.findMethod(prop, name)
        }
    }

}

abstract class ShapeWithGeometryPropType<T : ShapeWithGeometry> protected constructor(klass: Class<T>)
    : ShapePropType<T>(klass) {

    override fun findField(prop: Prop<T>, name: String): Prop<*>? {
        return when (name) {
            "Geometry" -> PropValue(prop.value.geometry)
            "Geometry1" -> PropValue(prop.value.geometry) // For backwards compatibility

            "StrokeCap" -> PropField("Shape.StrokeCap", prop) { prop.value.strokeCap }
            "StrokeJoin" -> PropField("Shape.StrokeJoin", prop) { prop.value.strokeJoin }
            "CornerRadius" -> PropField("Shape.CornerRadius", prop) { prop.value.cornerRadius }
            "ConnectAround" -> PropField("Shape.ConnectAround", prop) { prop.value.connectAround }

            else -> super.findField(prop, name)

        }
    }
}

class Shape1dPropType private constructor()
    : ShapeWithGeometryPropType<Shape1d>(Shape1d::class.java) {

    override fun findField(prop: Prop<Shape1d>, name: String): Prop<*>? {
        return when (name) {
            "Start" -> PropField("Shape.Start", prop) { prop.value.start }
            "End" -> PropField("Shape.End", prop) { prop.value.end }
            "StartAngle" -> PropField("Shape.StartAngle", prop) { prop.value.startAngle }
            "EndAngle" -> PropField("Shape.EndAngle", prop) { prop.value.endAngle }
            "Length" -> PropField("Shape.Length", prop) { prop.value.length }
            "IsStartConnected" -> PropField("Shape.IsStartConnected", prop) { shape -> shape.value.isStartConnected }
            "IsEndConnected" -> PropField("Shape.IsEndConnected", prop) { shape -> shape.value.isEndConnected }
            "StartShape" -> PropField("Shape.StartConnection", prop) { shape -> shape.value.startConnectedShape }
            "EndShape" -> PropField("Shape.StartConnection", prop) { shape -> shape.value.endConnectedShape }
            "ExtraStartAngle" -> PropField("Shape.ExtraStartAngle", prop) { shape -> shape.value.extraStartAngle }
            "ExtraEndAngle" -> PropField("Shape.ExtraEndAngle", prop) { shape -> shape.value.extraEndAngle }
            "SmartStart" -> PropField("Shape.SmartStart", prop) { shape -> shape.value.smartStart }
            "SmartEnd" -> PropField("Shape.SmartEnd", prop) { shape -> shape.value.smartEnd }

            "StartMargin" -> PropField("Shape.StartMargin", prop) { it.value.startMargin }
            "EndMargin" -> PropField("Shape.EndMargin", prop) { it.value.endMargin }

            else -> super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<Shape1d>, name: String): PropMethod<in Shape1d, *>? {
        return when (name) {
            "connectToShape" -> ConnectToShape(prop) // Connect to the geometry of a 2d shape, using the other end of this line to determine the direction from the center.

            else -> super.findMethod(prop, name)
        }
    }

    companion object {
        val instance = Shape1dPropType()
    }
}

class Shape2dPropType private constructor()
    : ShapeWithGeometryPropType<Shape2d>(Shape2d::class.java) {

    override fun findField(prop: Prop<Shape2d>, name: String): Prop<*>? {
        return when (name) {
            "Pin" -> PropField("Shape.Pin", prop) { prop.value.transform.pin }
            "LocPin" -> PropField("Shape.LocPin", prop) { prop.value.transform.locPin }
            "Scale" -> PropField("Shape.Scale", prop) { prop.value.transform.scale }
            "FlipX" -> PropField("Shape.FlipX", prop) { prop.value.transform.flipX }
            "FlipY" -> PropField("Shape.FlipY", prop) { prop.value.transform.flipY }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = Shape2dPropType()
    }
}


class ShapeTextPropType private constructor()
    : ShapePropType<ShapeText>(ShapeText::class.java) {

    override fun findField(prop: Prop<ShapeText>, name: String): Prop<*>? {
        return when (name) {
            "Pin" -> PropField("Shape.Pin", prop) { prop.value.transform.pin }
            "LocPin" -> PropField("Shape.LocPin", prop) { prop.value.transform.locPin }
            "Scale" -> PropField("Shape.Scale", prop) { prop.value.transform.scale }
            "FlipX" -> PropField("Shape.FlipX", prop) { prop.value.transform.flipX }
            "FlipY" -> PropField("Shape.FlipY", prop) { prop.value.transform.flipY }

            "Text" -> PropField("Shape.Text", prop) { prop.value.text }
            "FontSize" -> PropField("Shape.Text", prop) { prop.value.fontSize }
            "AlignX" -> PropField("Shape.AlignX", prop) { prop.value.alignX }
            "AlignY" -> PropField("Shape.AlignY", prop) { prop.value.alignY }
            "Clip" -> PropField("Shape.Clip", prop) { prop.value.clip }

            "MarginTop" -> PropField("Shape.MarginTop", prop) { prop.value.marginTop }
            "MarginRight" -> PropField("Shape.MarginTop", prop) { prop.value.marginRight }
            "MarginBottom" -> PropField("Shape.MarginTop", prop) { prop.value.marginBottom }
            "MarginLeft" -> PropField("Shape.MarginTop", prop) { prop.value.marginLeft }
            "TextSize" -> PropField("Shape.TextSize", prop) { prop.value.textSize }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = ShapeTextPropType()
    }
}
