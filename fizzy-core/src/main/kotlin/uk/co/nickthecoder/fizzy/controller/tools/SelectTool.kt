/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.tools

import uk.co.nickthecoder.fizzy.controller.CMenuItem
import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.prop.PropVariable

class SelectTool(controller: Controller)
    : Tool(controller) {

    var mousePressedPoint = Dimension2.ZERO_mm

    override fun onContextMenu(event: CMouseEvent): List<CMenuItem> {
        val shapes = controller.findShapesAt(event.point)
        val latest = controller.selection.lastOrNull()
        val shape: Shape? = if (shapes.size > 1 && shapes.contains(latest) && event.isConstrain) {
            val i = shapes.indexOf(latest)
            if (i == 0) shapes.last() else shapes[i - 1]
        } else {
            shapes.lastOrNull()
        }

        val list = mutableListOf<CMenuItem>()

        if (controller.parent is Shape) {
            list.add(CMenuItem("Exit Shape", {
                controller.parent = (controller.parent as Shape).parent
                controller.selection.clear()
            }))

        }

        if (shape == null) {

            list.add(CMenuItem("Edit Document Sheet", { controller.otherActions.editDocumentSheet(controller.page.document) }))

        } else {

            list.add(CMenuItem("Enter Shape", {
                controller.parent = shape
                controller.selection.clear()
            }))

            list.add(CMenuItem("Edit Shape Sheet", { controller.otherActions.editShapeSheet(shape) }))

            if (shape.customProperties.isNotEmpty()) {
                shape.customProperties.filter { it.visible.value && it.isMenu.value }.forEach { cp ->
                    if (cp.data.value is Boolean) {
                        @Suppress("UNCHECKED_CAST")
                        val booleanData = cp.data as PropVariable<Boolean>
                        list.add(CMenuItem(cp.label.value, {
                            booleanData.value = !booleanData.value
                        }, booleanData))
                    }
                }
            }
        }

        return list
    }

    override fun onMousePressed(event: CMouseEvent) {

        mousePressedPoint = event.point

        controller.handles.forEach { handle ->
            if (handle.isAt(mousePressedPoint, event.scale)) {
                // If this is the beginning of dragging a handle, then leave the selection unchanged.
                return
            }
        }

        val shapes = controller.findShapesAt(event.point)
        if (shapes.isEmpty()) {
            if (!event.isAdjust) {
                controller.selection.clear()
            }
            val topShape = controller.findTopLevelShapesAt(event.point).firstOrNull()
            topShape?.let {
                if (topShape != controller.parent) {
                    controller.parent = controller.page
                    controller.selection.add(topShape)
                }
            }
        } else {
            val previouslyTopSelected = controller.selection.lastOrNull()
            val clickedShape: Shape?
            clickedShape = if (shapes.size > 1 && shapes.contains(previouslyTopSelected) && event.isConstrain) {
                val i = shapes.indexOf(previouslyTopSelected)
                if (i == 0) shapes.last() else shapes[i - 1]
            } else {
                shapes.lastOrNull()
            }

            if (clickedShape != null) {
                if (event.isAdjust) {
                    if (controller.selection.contains(clickedShape)) {
                        controller.selection.remove(clickedShape)
                    } else {
                        controller.selection.add(clickedShape)
                    }
                } else {
                    if (clickedShape !in controller.selection) {
                        controller.selection.clear()
                        controller.selection.add(clickedShape)
                    }
                }
            }
        }
    }

    override fun onDragDetected(event: CMouseEvent) {

        controller.handles.asReversed().forEach { handle ->
            if (handle.isAt(mousePressedPoint, event.scale)) {
                controller.tool = DragHandleTool(controller, handle, mousePressedPoint)
                return
            }
        }

        if (controller.selection.isEmpty()) {
            controller.tool = BoundingBoxTool(controller, event, mousePressedPoint)
            controller.tool.onMouseDragged(event)
        } else {
            controller.tool = MoveShapesTool(controller, mousePressedPoint)
            controller.tool.onMouseDragged(event)
        }
    }

}
