/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.handle

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.model.geometry.ArcTo
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions
import uk.co.nickthecoder.fizzy.view.DrawContext

class ArcToSegmentHeightHandle(
        private val shape: Shape,
        private val part: ArcTo,
        private val controller: Controller)

    : GeometryHandle() {

    override fun position(): Dimension2 {
        val mid = (part.point.value + part.prevPart.point.value) / 2.0
        val point = mid + ((part.point.value - part.prevPart.point.value).angle() + Angle.degrees(-90.0)).unitVector() * part.segmentHeight.value
        return shape.localToPage(point)
    }

    override fun isFor(shape: Shape) = shape === this.shape

    override fun dragTo(event: CMouseEvent, dragPoint: Dimension2) {

        val localPoint = shape.pageToLocal(dragPoint)
        val mid = (part.point.value + part.prevPoint.value) / 2.0
        val relative = (localPoint - mid)
        val angle = (part.point.value - part.prevPoint.value).angle() + Angle.degrees(-90.0)
        val height = relative.dot(angle.unitVector())

        shape.document().history.makeChange(
                ChangeExpressions(listOf(part.segmentHeight to height.toFormula()))
        )
        controller.dirty.value++
    }

    override fun fillColor() = Controller.GEOMETRY_HANDLE_FILL

    override fun strokeColor() = Controller.GEOMETRY_HANDLE_STROKE


    override fun draw(dc: DrawContext, dragging: Boolean) {
        dc.use {
            /*
            dc.beginPath()
            dc.moveTo(Dimension2.ZERO_mm)
            val m = shape.fromLocalToPage.value
            dc.lineTo(m * otherPoint.value - m * point.value)
            dc.endPath(true, false)
            */
            super.draw(dc, dragging)
        }
    }

    override fun toString() = "ArcToHeightHandle @ ${position()}"

}
