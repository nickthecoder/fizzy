/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.view.DrawContext


class MoveTo(point: Dimension2Expression)

    : AbstractGeometryPart(point) {

    constructor(pointFormula: String) : this(Dimension2Expression(pointFormula))

    constructor(point: Dimension2) : this(point.toFormula())

    constructor() : this(Dimension2.ZERO_mm)


    override fun isAlong(here: Dimension2, lineWidth: Dimension, minDistance: Dimension) = false

    override fun checkAlong(here: Dimension2): Pair<Double, Double>? = null

    override fun crossCount(here: Dimension2) = 0

    override fun pointAlong(along: Double) = Dimension2.ZERO_mm

    override fun tangentAlong(along: Double) = Angle.ZERO

    override fun copy(link: Boolean): GeometryPart = MoveTo(point.copy(link))

    override fun draw(dc: DrawContext) {
        dc.moveTo(point.value)
    }

    override fun toString() = "MoveTo point=${point.value}"
}
