/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListener
import java.util.regex.Pattern

abstract class AbstractFListPropType<T : FList<*>>(klass: Class<T>)
    : PropType<T>(klass) {

    override fun findField(prop: Prop<T>, name: String): Prop<*>? {
        @Suppress("UNCHECKED_CAST")
        return when (name) {
            "Size" -> FListSizeProp(prop as Prop<FList<Any>>) // Very bad style, the type is Prop<FList<?>>, but I couldn't compile it without this horrible bodge.
            else -> return findField2(prop, name) ?: super.findField(prop, name)
        }
    }

    private fun findField2(listProp: Prop<T>, name: String): Prop<*>? {

        // To make accessing items in an array easier, we can reference a property "Foo" of an item in the list using :
        // "myListProp.FooN" where N is the index into the list with base 1 (i.e. the 0th item is N=1).
        // For example, to get the point from geometry part row 3 (which is index 2) :
        // someGeometry.Point3
        //
        // Note, there is similar functionality in PropType, but it does something different!
        val matcher = nameNumber.matcher(name)
        if (matcher.matches()) {
            val fieldName = matcher.group(1)
            val row = matcher.group(2).toInt()
            if (row > 0 && row <= listProp.value.size) {
                @Suppress("UNCHECKED_CAST")
                return ListPropertyAccess(listProp as Prop<FList<Any>>, row - 1, fieldName)
            }
        }

        return null
    }

    companion object {
        private val nameNumber = Pattern.compile("(.*?)([0-9]+)")
    }
}

class FListPropType private constructor()
    : AbstractFListPropType<FList<*>>(FList::class.java) {

    companion object {
        val instance = FListPropType()
    }
}

class ListPropertyAccess(private val listProp: Prop<FList<Any>>, private val index: Int, private val fieldName: String)
    : PropCalculation<Any>("ListPropertyAcess", false), ListListener<Any>, MutableProp<Any> {

    override var value: Any
        get() = super.value
        set(v) {
            val found = findProp() ?: throw RuntimeException("Field $fieldName not found")
            if (found is MutableProp<*>) {
                @Suppress("UNCHECKED_CAST")
                (found as MutableProp<Any>).value = v
            } else {
                throw RuntimeException("Expected a MutableProp, but found $found")
            }
        }

    private var field: Prop<*>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    init {
        listProp.addListener(this)
        listProp.value.listeners.add(this)
    }

    override fun listChanged(list: FList<Any>, item: Any, index: Int, added: Boolean) {
        dirty = true
    }

    private fun findProp(): Prop<*>? {
        val item = listProp.value[index]
        // The "IF" is to allow for list lists of Props or lists of actual values.
        // I think we only use lists of values now, not lists of Props.
        return if (item is Prop<*>) {
            PropType.field(PropValue(item.value), fieldName)
        } else {
            PropType.field(PropValue(item), fieldName)
        }
    }

    override fun eval(): Any {
        field = findProp()
        return field?.value ?: throw RuntimeException("Field $fieldName not found")
    }

    override fun toString(): String {
        return "{ ListPropertyAccess cell $fieldName${index + 1} of $listProp => $field}"
    }
}


/**
 * NOTE. The type of Prop should be Prop<FList<*>>, but I couldn't create a listener for such an unknown
 * type. As we are not affecting the list in any way, this bodge should be safe.
 */
class FListSizeProp(private val prop: Prop<FList<Any>>)

    : PropCalculation<Double>("FListSizeProp", false), ListListener<Any> {

    init {
        prop.addListener(this)
    }

    private var list: FList<Any>? = null
        set(v) {
            if (field !== v) {
                field?.listeners?.add(this)
                field = v
                v?.listeners?.remove(this)
            }
        }

    override fun listChanged(list: FList<Any>, item: Any, index: Int, added: Boolean) {
        dirty(this)
    }

    override fun eval(): Double {
        list = prop.value
        return prop.value.size.toDouble()
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        prop.removeListener(this)
    }

    override fun toString() = "FListSizeProp ($prop)"
}

