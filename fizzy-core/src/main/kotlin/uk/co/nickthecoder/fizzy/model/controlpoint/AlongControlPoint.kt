/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.util.clamp

/**
 * A control point defined by a geometry part (such as LineTo), and the amount "along" that geometry part.
 * The amount "along" is always between 0 and 1, but can be constrained further via [min] and [max].
 *
 */
class AlongControlPoint(
        val part: GeometryPartExpression = GeometryPartExpression(),
        val along: DoubleExpression = DoubleExpression(0.5)
) : ControlPoint(ControlPointType.ALONG) {

    constructor(part: String, along: String) : this(GeometryPartExpression(part), DoubleExpression(along))

    /**
     * This property's value is incremented every time the Point may need to be re-evaluated.
     * This causes [point] to become dirty.
     */
    private val bodge = PropVariable<Int>(0)

    override val point: Prop<Dimension2> = PropCalculation3("AlongControlPoint.point", true, part, along, bodge) { vPart, vAlong, _ ->
        oldShape = part.value.geometry?.shape
        otherToMine(vPart.pointAlong(vAlong))
    }

    val tangent: Prop<Angle> by lazy {
        PropCalculation3("AlongControlPoint.tangent", true, part, along, bodge) { vPart, vAlong, _ ->
            val angle = vPart.tangentAlong(vAlong)
            vPart.geometry?.shape?.localToPage(angle) ?: angle
        }
    }

    val normal: Prop<Angle> by lazy {
        PropCalculation3("AlongControlPoint.normal", true, part, along, bodge) { vPart, vAlong, _ ->
            val angle = vPart.normalAlong(vAlong)
            vPart.geometry?.shape?.localToPage(angle) ?: angle
        }
    }

    val min = DoubleExpression("0.0")
    val max = DoubleExpression("1.0")

    /**
     * Whenever Shape changes, we invalidate [point], by incrementing [bodge].
     */
    private var shapeListener = object : ChangeListener<Shape> {
        override fun changed(item: Shape, changeType: ChangeType, obj: Any?) {
            bodge.value++
        }
    }

    /**
     * Every time we evaluate [point], we set [oldShape]. If it has changed since last time,
     * then we unlisten to the old shape, and listen to the new one.
     */
    private var oldShape: Shape? = null
        set(v) {
            if (field != v) {
                field?.changeListeners?.remove(shapeListener)
                field = v
                v?.changeListeners?.add(shapeListener)
                bodge.value++
            }
        }

    /**
     * Converts points
     * from the coordinate system of the geometry's shape
     * to the coordinate system of the shape that owns the ControlPoint.
     */
    private fun otherToMine(point: Dimension2): Dimension2 {
        val gShape = part.value.geometry?.shape
        val myShape = shape
        return if (myShape == null || gShape == null || gShape === shape) {
            point
        } else {
            myShape.pageToLocal(gShape.localToPage(point))
        }
    }

    /**
     * Converts points
     * from the coordinate system of the shape that owns the ControlPoint.
     * to the coordinate system of the geometry's shape,
     */
    private fun mineToOther(point: Dimension2): Dimension2 {
        val gShape = part.value.geometry?.shape
        val myShape = shape
        return if (myShape == null || gShape == null || gShape !== myShape) {
            point
        } else {
            gShape.pageToLocal(myShape.localToPage(point))
        }

    }

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        part.context = context
        along.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Part", part, "A")
        metaData.newCell("Along", along, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        val gPart = part.value
        val prevPart = gPart.prevPart
        val length = (gPart.point.value - prevPart.point.value).length().inDefaultUnits

        val draggedToOther = mineToOther(draggedTo)

        val pair = part.value.checkAlong(draggedToOther)

        val result = if (pair != null && (pair.first / length) < 0.25) {
            pair.second
        } else {
            if ((draggedToOther - gPart.point.value).length() < (draggedToOther - prevPart.point.value).length()) 1.0 else 0.0
        }

        along.value = result.clamp(min.value, max.value)
    }

    override fun copy(link: Boolean): AlongControlPoint {
        val result = AlongControlPoint(part.copy(link), along.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
