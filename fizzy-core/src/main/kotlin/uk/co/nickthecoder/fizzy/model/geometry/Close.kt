/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.view.DrawContext


class Close()

    : GeometryPart() {

    override val point = PropVariable(Dimension2.ZERO_mm)

    override fun isAlong(here: Dimension2, lineWidth: Dimension, minDistance: Dimension): Boolean {
        return LineTo.isAlong(shape, here, prevPart.point.value, point.value, lineWidth, minDistance)
    }

    override fun checkAlong(here: Dimension2): Pair<Double, Double>? {
        val shape = this.shape ?: return null
        return LineTo.checkAlong(shape, here, prevPart.point.value, point.value)
    }

    override fun crossCount(here: Dimension2) = LineTo.crossCount(here, prevPart.point.value, point.value)

    override fun pointAlong(along: Double): Dimension2 {
        return prevPart.point.value + (point.value - prevPart.point.value) * along
    }

    override fun tangentAlong(along: Double): Angle = (point.value - prevPart.point.value).angle()

    override fun copy(link: Boolean): GeometryPart = Close()

    override fun draw(dc: DrawContext) {
        dc.closePath()
    }

    override fun toString() = "Close"
}
