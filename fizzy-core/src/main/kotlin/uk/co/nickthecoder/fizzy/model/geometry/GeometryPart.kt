/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.geometry

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.util.ChangeListeners
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.util.HasChangeListeners
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * Used by everything except [Close], whose [point] is defined by the corresponding MoveTo.
 */
abstract class AbstractGeometryPart(final override val point: Dimension2Expression) : GeometryPart() {

    init {
        point.addListener(this)
    }

    override fun addEarlyMetaData(metaData: MetaData) {
        super.addEarlyMetaData(metaData)
        metaData.cells.add(MetaDataCell("Point", point))
    }

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        point.context = context
    }

}

abstract class GeometryPart

    : HasChangeListeners<GeometryPart>, PropListener, Named, MetaDataAware {

    val visible = BooleanExpression(true)

    override val name = PropVariable<String>("")

    abstract val point: Prop<Dimension2>

    internal var geometry: Geometry? = null
        set(v) {
            field = v
            if (v == null) {
                setContext(constantsContext)
            } else {
                setContext(v.shape.context)
            }
        }

    internal val prevPartProp = PropVariable<GeometryPart>(this)

    val prevPoint: Prop<Dimension2> = PropDependency1("GeometryPart.prefPoint", prevPartProp) { it.point }

    /**
     * The previous geometry part, or 'this' if there is no previous part.
     */
    val prevPart: GeometryPart
        get() = prevPartProp.value

    val shape: Shape?
        get() = geometry?.shape

    override val changeListeners = ChangeListeners<GeometryPart>()

    init {
        visible.addListener(this)
    }

    override fun metaData(): MetaData {
        val metaData = MetaData(null)
        addEarlyMetaData(metaData)
        addLateMetaData(metaData)
        return metaData
    }


    open fun addEarlyMetaData(metaData: MetaData) {
        metaData.cells.add(MetaDataCell("Name", name))
    }

    open fun addLateMetaData(metaData: MetaData) {
        metaData.cells.add(MetaDataCell("Visible", visible))
    }

    internal open fun setContext(context: EvaluationContext) {
        visible.context = context
    }

    fun index(): Int {
        return geometry?.parts?.indexOf(this) ?: -1
    }

    override fun dirty(prop: Prop<*>) {
        changeListeners.fireChanged(this, ChangeType.CHANGE, prop)
    }

    /**
     * Does the point touch the line given by this part of the geometry.
     * Note that [lineWidth] is in local coordinate system, whereas [minDistance] is in the page's coordinate system.
     * This lets us check if the point is within the thickness of the line and also check if the point is
     * "close enough", based on the zoom level of the view (even when the line width is very thin at that zoom level).
     *
     * @param here The point to test, in local coordinates.
     * @param lineWidth The thickness of the line in local coordinates
     * @param minDistance The distance in page coordinates that can also be considered touching (used when lineWidth is thin).
     */
    abstract fun isAlong(here: Dimension2, lineWidth: Dimension, minDistance: Dimension): Boolean

    /**
     * If the point is not along the line (e.g. it is passed the start/end points), then null is returned.
     * This is similar to [isAlong] returning false.
     * For non-null return values, return the distance away from the line, and the ratio of how far along the line.
     *
     * The distance is the minimum distance away from the line/curve in the page's coordinates.
     * The ratio should be in the range 0..1 and should be suitable to be passed into [pointAlong] for the 'along' parameter.
     *
     * @param here The point to test, in local coordinate system
     * @return distance in page coordinates , how far along the line in the range 0..1
     */
    abstract fun checkAlong(here: Dimension2): Pair<Double, Double>?

    /**
     * Used to test if point [here] is withing a polygon.
     * Does a horizontal ray starting at [here], heading in the positive X direction cross the this GeometryPart?
     *
     * If it crosses and EVEN number of times then return false.
     */
    abstract fun crossCount(here: Dimension2): Int

    abstract fun pointAlong(along: Double): Dimension2

    abstract fun tangentAlong(along: Double): Angle

    fun normalAlong(along: Double): Angle = tangentAlong(along) + Angle.radians(Math.PI / 2.0)

    abstract fun draw(dc: DrawContext)

    abstract fun copy(link: Boolean): GeometryPart

}
