/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.history

import uk.co.nickthecoder.fizzy.model.Shape1d
import uk.co.nickthecoder.fizzy.model.Shape2d
import uk.co.nickthecoder.fizzy.model.Shape2dTransform
import uk.co.nickthecoder.fizzy.model.ShapeWithGeometry

class MoveGeometryIntoChild(val parent: ShapeWithGeometry) : Change {

    override fun redo() {
        val child: ShapeWithGeometry = when (parent) {
            is Shape1d -> {
                val c = Shape1d.create(parent)
                c.start.formula = "(0mm,0mm)"
                c.end.formula = "(Parent.End - Parent.Start).Length"
                c
            }
            is Shape2d -> {
                val c = Shape2d.create(parent)
                c.size.formula = "Parent.Size"
                c
            }
            else -> throw IllegalStateException("Expected a Shape1d or Shape2d, but found ${parent.javaClass.name}")
        }
        child.name.value = "MovedGeometry"
        val transform = child.transform
        if (transform is Shape2dTransform) {
            transform.locPin.formula = "(0mm,0mm)"
        }
        parent.geometry.parts.forEach { part ->
            child.geometry.parts.add(part.copy(false))
        }
        parent.geometry.parts.clear()
        parent.children.add(0, child)
    }

    override fun undo() {
        val child = parent.children.firstOrNull() ?: throw IllegalStateException("Expected at least one child shape")
        if (child !is ShapeWithGeometry) throw IllegalStateException("Expected the child to be a ShapeWithGeometry")
        child.geometry.parts.forEach { part ->
            parent.geometry.parts.add(part)
        }
        parent.children.removeAt(0)
    }

}
