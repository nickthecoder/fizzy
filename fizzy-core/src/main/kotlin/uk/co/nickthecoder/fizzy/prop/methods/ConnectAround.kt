/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.model.geometry.Geometry
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation1
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType

class ConnectAround(prop: Prop<Shape>, val local: Boolean)
    : TypedMethod3<Shape, Geometry, Double, Dimension, Dimension2>("ConnectAround", prop, Geometry::class.java, Double::class.java, Dimension::class.java),
        Connection,
        ChangeListener<Geometry> {

    private var along: Double = 0.0

    private var myShape: Shape? = null
        set(v) {
            if (field !== v) {
                field?.transform?.fromLocalToPage?.removeListener(this)
                field = v
                if (local) {
                    v?.transform?.fromLocalToPage?.addListener(this)
                }
            }
        }

    private var connectTo: Geometry? = null
        set(v) {
            if (field !== v) {
                field?.changeListeners?.remove(this)
                field?.shape?.transform?.fromPageToLocal?.removeListener(this)
                field?.shape?.transform?.pin?.removeListener(this)

                field = v

                v?.changeListeners?.add(this)
                v?.shape?.transform?.fromPageToLocal?.addListener(this)
                // Why is this needed? The above should be sufficient?
                // Without it, the connection only works sporadically while dragging the box that the line is connected to.
                v?.shape?.transform?.pin?.addListener(this)

                // Force evaluation. I'm not sure why this is needed, but removing causes TestConnections.testConnectAlong to fail.
                v?.shape?.transform?.fromPageToLocal?.value
            }
        }

    override val angle = PropCalculation1("ConnectAround.angle", true, this) {
        val normal = connectTo?.normalAlong(along)
        val otherShape = connectTo?.shape

        if (normal == null || otherShape == null) {
            Angle.ZERO
        } else {
            otherShape.localToPage(normal).normalise()
        }
    }


    init {
        selfDependants(1) // angle
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()

        angle.noLongerNeeded()

        connectTo = null
        myShape = null
    }

    override fun changed(item: Geometry, changeType: ChangeType, obj: Any?) {
        dirty = true
    }

    override fun eval(a: Geometry, b: Double, c: Dimension): Dimension2 {
        myShape = prop.value
        connectTo = a
        along = b
        val margin = c
        var point = a.pointAlong(b)
        if (margin.inDefaultUnits != 0.0) {
            val angle = a.normalAlong(b)
            point += angle.unitVector() * margin
        }
        return if (local) {
            prop.value.pageToLocal(a.shape.localToPage(point))
        } else {
            prop.value.parent.pageToLocal(a.shape.localToPage(point))
        }
    }

    override fun connectedTo(): Shape {
        value // Force evaluation
        return connectTo?.shape ?: prop.value
    }

}
