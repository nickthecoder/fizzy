/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropVariable

/**
 * Used by [MetaDataAware] to create new rows, such as GeometryPart, CustomProperty etc.
 * [RowFactory] is not used while loading a document, when [MetaDataAware.createRow]
 * is used instead.
 *
 * Note. [aliases] isn't currently used while loading the document, so it isn't actually useful
 * other than for documentation purposes.
 *
 * Instead, [MetaDataAware.createRow] should test for old an new names of rows to maintain
 * backwards compatibility.
 */
class RowFactory(val label: String, val aliases: List<String> = emptyList(), val create: (index: Int) -> Unit)

/**
 * Holds references to all [PropExpression]s within a [Shape] in a generic hierarchy. This is used when saving a
 * document, for debugging, and also within the unit tests.
 */
class MetaData(val type: String?, val label: String? = null) {

    val cells = mutableListOf<MetaDataCell>()

    val sections = mutableMapOf<String, MetaData>()

    val rows = mutableListOf<MetaData>()

    val rowFactories = mutableListOf<RowFactory>()

    var rowRemoval: ((Int) -> Unit)? = null

    fun isEmpty() = cells.isEmpty() && sections.isEmpty() && rows.isEmpty()

    fun newCell(cellName: String, expression: Prop<*>, columnKey: String? = null, save: Boolean = true, display: Boolean = true): MetaDataCell {
        val cell = MetaDataCell(cellName, expression, columnKey, save, display)
        cells.add(cell)
        return cell
    }

    fun newSection(sectionName: String, label: String? = null): MetaData {
        val section = MetaData(sectionName, label)
        sections[sectionName] = section
        return section
    }

    fun newRow(type: String?, label: String? = null): MetaData {
        val row = MetaData(type, label)
        rows.add(row)
        return row
    }

    fun newRow(rowMetaData: MetaData) {
        rows.add(rowMetaData)
    }

    fun findCell(name: String): MetaDataCell? {
        cells.forEach { cell ->
            if (cell.cellName == name || cell.aliases.contains(name)) {
                return cell
            }
        }
        return null
    }

    fun findCell(other: MetaDataCell) = findCell(other.cellName)

    fun copyInto(into: MetaData, link: Boolean) {
        cells.filter { it.save }.forEach { cell ->
            val intoCell = into.findCell(cell)
            if (intoCell == null) {
                throw IllegalStateException("Cell $cell not found")
            } else {
                val intoProp = intoCell.cellProp
                val fromProp = cell.cellProp

                if (intoProp is PropExpression<*> && fromProp is PropExpression<*>) {
                    intoProp.copyFrom(fromProp, link)
                } else if (intoProp is PropVariable && fromProp is PropVariable) {
                    @Suppress("UNCHECKED_CAST")
                    (intoProp as PropVariable<Any>).value = fromProp.value
                }

            }
        }

        sections.forEach { sectionName, section ->
            val intoSection = into.sections[sectionName]
            if (intoSection != null) {
                section.copyInto(intoSection, link)
            }
        }

        rows.forEachIndexed { rowIndex, row ->
            if (rowIndex >= into.rows.size) throw IllegalStateException("Row $rowIndex not found")
            val intoRow = into.rows[rowIndex]
            row.copyInto(intoRow, link)
        }
    }

    override fun toString(): String {
        val buffer = StringBuffer()
        buildString(buffer, "")
        return buffer.toString()
    }

    private fun buildString(buffer: StringBuffer, padding: String) {
        type?.let { buffer.append(it).append('\n') }

        cells.forEach { cell ->
            buffer.append(padding)
            buffer.append(cell)
            buffer.append('\n')
        }
        rows.forEachIndexed { rowIndex, row ->
            buffer.append(padding).append("Row $rowIndex { ")
            row.buildString(buffer, padding + "    ")
            buffer.append(padding).append("}\n")
        }
        sections.forEach { _, section ->
            buffer.append(padding).append("Section { ")
            section.buildString(buffer, padding + "    ")
            buffer.append(padding).append("}\n")
        }
    }


    /**
     * Used when loading a document, we get the link names, and map them to their expression.
     * Note. an expression will be in the map multiple times if it has one or more aliases.
     *
     * Currently sections cannot have aliases, but this will be added if the need arises.
     */
    fun buildLinkToExpression(prefix: String): Map<String, PropExpression<*>> {
        val map = mutableMapOf<String, PropExpression<*>>()
        addLinkToExpression(map, prefix)
        return map
    }


    private fun addLinkToExpression(map: MutableMap<String, PropExpression<*>>, prefix: String) {
        cells.forEach { cell ->
            val exp = cell.cellProp
            if (exp is PropExpression<*>) {
                map[prefix + cell.cellName] = exp
                cell.aliases.forEach { map[prefix + it] = exp }
            }
        }
        sections.forEach { name, section ->
            section.addLinkToExpression(map, "$prefix$name.")
        }

        rows.forEachIndexed { index, row ->
            row.addLinkToExpression(map, "$prefix$index.")
        }
    }

    /**
     * Used when saving a document. For each expression, we map it to a link name.
     * This is the "primary" link name, i.e. aliases are ignored. Therefore this map
     * will be equal, or smaller than [buildLinkToExpression]'s map.
     */
    fun buildExpressionToLink(prefix: String): Map<PropExpression<*>, String> {
        val map = mutableMapOf<PropExpression<*>, String>()
        addExpressionToLink(map, prefix)
        return map
    }

    private fun addExpressionToLink(map: MutableMap<PropExpression<*>, String>, prefix: String) {
        cells.forEach { cell ->
            val exp = cell.cellProp
            if (exp is PropExpression<*>) {
                map[exp] = prefix + cell.cellName
            }
        }
        sections.forEach { name, section ->
            section.addExpressionToLink(map, "$prefix$name.")
        }

        rows.forEachIndexed { index, row ->
            row.addExpressionToLink(map, "$prefix$index.")
        }
    }
}
