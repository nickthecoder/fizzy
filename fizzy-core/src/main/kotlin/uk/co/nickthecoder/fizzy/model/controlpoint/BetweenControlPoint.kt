/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.DoubleExpression
import uk.co.nickthecoder.fizzy.prop.PropCalculation3
import uk.co.nickthecoder.fizzy.util.clamp

/**
 * A control point defined by two points, A and B, and the amount "along" that line.
 * The amount "along" is often between 0 and 1, but -ve value can be used, as well as values over 1.
 *
 * This used to be called LinearControlPoint, but was renamed when [LengthControlPoint] was created, as
 * both of them are linear in nature, but defined differently.
 */
class BetweenControlPoint(
        val start: Dimension2Expression = Dimension2Expression(Dimension2.ZERO_mm),
        val end: Dimension2Expression = Dimension2Expression(Dimension2.ZERO_mm),
        val along: DoubleExpression = DoubleExpression(0.5)
) : ControlPoint(ControlPointType.BETWEEN) {

    constructor(start: String, end: String, along: String) : this(Dimension2Expression(start), Dimension2Expression(end), DoubleExpression(along))

    override val point = PropCalculation3<Dimension2, Dimension2, Dimension2, Double>("BetweenControlPoint.point", true, start, end, along) { vStart, vEnd, vAlong ->
        vStart + (vEnd - vStart) * vAlong
    }

    val min = DoubleExpression("MIN_DOUBLE")
    val max = DoubleExpression("MAX_DOUBLE")

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        start.context = context
        end.context = context
        along.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Start", start, "A").addAliases("A")
        metaData.newCell("End", end, "B").addAliases("B")
        metaData.newCell("Along", along, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        val line = end.value - start.value
        val relative = (draggedTo - start.value) / line.length()
        along.value = relative.dot(line.normalise()).clamp(min.value, max.value)
    }

    override fun copy(link: Boolean): BetweenControlPoint {
        val result = BetweenControlPoint(start.copy(link), end.copy(link), along.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
