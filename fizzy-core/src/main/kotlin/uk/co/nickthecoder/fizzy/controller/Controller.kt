/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.controller.handle.*
import uk.co.nickthecoder.fizzy.controller.tools.SelectTool
import uk.co.nickthecoder.fizzy.controller.tools.Tool
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.history.*
import uk.co.nickthecoder.fizzy.prop.MutableProp
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.util.toFormula
import uk.co.nickthecoder.fizzy.view.DrawContext

/**
 * This is a non-gui class, which takes mouse events from GlassCanvas, and performs actions via Tools.
 * It has no dependencies on JavaFX, and can therefore be used easily from JUnit (i.e. bypassing GlassCanvas).
 */
class Controller(val page: Page, val singleShape: Shape? = null, val otherActions: OtherActions) {

    var scale = DEFAULT_SCALE

    var tool: Tool = SelectTool(this)
        set(v) {
            field.endTool(v)
            v.beginTool()
            field = v
            cursorProp.value = v.cursor
        }

    /**
     *  The parent of new shapes
     */
    var parent: ShapeParent = page // singleShape ?: page
        set(v) {
            field = v
            dirty.value++
        }

    val selection = MutableFList<Shape>()

    val cursorProp = PropVariable(tool.cursor)

    val handles = mutableListOf<Handle>()

    /**
     * The minimum distance away from a line for it to be considered close enough to select it.
     * This should be scaled with
     */
    val minDistance: Dimension
        get() = LINE_NEAR / scale

    /**
     * Listen to the currently selected shapes. When they change, we need to draw their handles and bounding box etc.
     */
    private val shapeListener = object : ChangeListener<Shape> {
        override fun changed(item: Shape, changeType: ChangeType, obj: Any?) {
            if (selection.contains(item)) {
                // createShapeHandles(item)
                dirty.value++
            }
        }
    }

    /**
     * When the selection changes, we need to draw, to update the bounding boxes and handles on display.
     */
    private val selectionListener = ListListenerLambda<Shape> { _, item, _, added ->
        if (added) {
            item.changeListeners.add(shapeListener)
            createShapeHandles(item)
        } else {
            item.changeListeners.remove(shapeListener)
            removeShapeHandles(item)
        }
    }


    /**
     * Whenever the handles, or the tool need redrawing, increment the counter, so that listeners
     * can redraw themselves.
     * GlassCanvas listens to this.
     * Several [Tool]s increment this
     */
    val dirty = PropVariable(0)

    /**
     * When set, the connection points of all shapes should be shown as tiny crosses.
     */
    val showConnectionPoints: Prop<Boolean> = PropVariable(singleShape != null)

    /**
     * When dragging a line end point, if it hovers over a connectible geometry, then
     * set this to it, and GlassCanvas will highlight it. Reset it back to [NO_SHAPE_HIGHLIGHT]
     * afterwards.
     */
    val highlightShape: PropVariable<ShapeHighlight> = PropVariable(ShapeHighlight.NO_SHAPE_HIGHLIGHT)

    init {
        tool.beginTool()
        selection.listeners.add(selectionListener)
        selection.forEach {
            createShapeHandles(it)
        }
    }

    fun onKeyTyped(event: CKeyEvent) {
        tool.onKeyTyped(event)
    }

    fun onKeyPressed(event: CKeyEvent) {
        tool.onKeyPressed(event)
    }

    fun onMousePressed(event: CMouseEvent) {
        tool.onMousePressed(event)
    }

    fun onMouseReleased(event: CMouseEvent) {
        tool.onMouseReleased(event)
    }

    fun onMouseMoved(event: CMouseEvent) {
        tool.onMouseMoved(event)
    }

    fun onMouseClicked(event: CMouseEvent) {
        tool.onMouseClicked(event)
    }

    fun onDragDetected(event: CMouseEvent) {
        tool.onDragDetected(event)
    }

    fun onMouseDragged(event: CMouseEvent) {
        tool.onMouseDragged(event)
    }

    fun copy() {
        Clipboard.copy(selection)
    }

    fun paste() {
        selection.clear()
        page.document.history.beginBatch()
        Clipboard.forEach { shape ->
            val copy = shape.copyInto(parent, false)
            selection.add(copy)
            page.document.history.makeChange(CreateShape(copy, parent))
        }
        page.document.history.endBatch()
    }

    fun cut() {
        Clipboard.copy(selection)
        page.document.history.beginBatch()
        selection.forEach { shape ->
            page.document.history.makeChange(DeleteShape(shape))
        }
        selection.clear()
        page.document.history.endBatch()
    }

    fun duplicate() {
        copy()
        paste()
    }

    fun findTopLevelShapesAt(pagePoint: Dimension2): List<Shape> {
        val min = minDistance
        val single = singleShape
        return if (single == null) {
            page.findShapesAt(pagePoint, min)
        } else {
            if (single.isAt(pagePoint, min)) {
                listOf(single)
            } else {
                emptyList()
            }
        }
    }

    fun findShapeAt(pagePoint: Dimension2) = findShapesAt(pagePoint).lastOrNull()

    fun findShapesAt(pagePoint: Dimension2): List<Shape> {

        val min = minDistance
        val p = parent

        // We have "entered" a shape, so only look at children of that shape
        if (p is Shape) {
            return parent.findShapesAt(pagePoint, min)
        }

        // We are NOT in an "entered" shape, but we may be viewing a single shape (such as when editing a Master Shape)
        return if (singleShape == null) {
            parent.findShapesAt(pagePoint, min)
        } else {
            if (singleShape.isAt(pagePoint, min)) {
                listOf(singleShape)
            } else {
                emptyList()
            }
        }
    }

    fun showConnectionPoints(value: Boolean) {
        if (singleShape == null) {
            (showConnectionPoints as MutableProp<Boolean>).value = value
        }
    }

    fun createShapeHandles(shape: Shape) {
        removeShapeHandles(shape)

        val transform = shape.transform
        when (transform) {
            is Shape2dTransform -> {
                if (!shape.locks.size.value) {
                    handles.add(Shape2dSizeHandle(transform, -1, -1))
                    handles.add(Shape2dSizeHandle(transform, 1, -1))
                    handles.add(Shape2dSizeHandle(transform, 1, 1))
                    handles.add(Shape2dSizeHandle(transform, -1, 1))

                    handles.add(Shape2dSizeHandle(transform, 0, -1))
                    handles.add(Shape2dSizeHandle(transform, 1, 0))
                    handles.add(Shape2dSizeHandle(transform, 0, 1))
                    handles.add(Shape2dSizeHandle(transform, -1, 0))
                }
                if (!shape.locks.rotation.value) {
                    handles.add(RotationHandle(shape, { Dimension(ROTATE_DISTANCE / scale) }))
                }
            }
            is Shape1dTransform -> {
                handles.add(Shape1dHandle(transform, this, false))
                handles.add(Shape1dHandle(transform, this, true))
            }
        }


        shape.controlPoints.forEach { cp ->
            handles.add(ControlPointHandle(shape, this, cp))
        }

    }

    fun removeShapeHandles(shape: Shape) {
        handles.removeIf { it.isFor(shape) }
    }

    fun shapeCorners(shape: Shape): Array<Dimension2> {
        return arrayOf<Dimension2>(
                shape.localToPage(Dimension2.ZERO_mm),
                shape.localToPage(shape.size.value * Vector2(1.0, 0.0)),
                shape.localToPage(shape.size.value * Vector2(1.0, 1.0)),
                shape.localToPage(shape.size.value * Vector2(0.0, 1.0))
        )
    }

    fun isWithin(shape: Shape, a: Dimension2, b: Dimension2): Boolean {
        shapeCorners(shape).forEach {
            if (((it.x < a.x) xor (it.x > b.x)) || ((it.y < a.y) xor (it.y > b.y))) {
                return false
            }
        }
        return true
    }

    fun flip(x: Boolean, y: Boolean) {
        page.document.history.beginBatch()
        if (x) {
            selection.forEach { shape ->
                val transform = shape.transform
                if (transform is Shape2dTransform) {
                    page.document.history.makeChange(ChangeExpressions(listOf(transform.flipX to (!transform.flipX.value).toFormula())))
                }
            }
        }
        if (y) {
            selection.forEach { shape ->
                val transform = shape.transform
                if (transform is Shape2dTransform) {
                    page.document.history.makeChange(ChangeExpressions(listOf(transform.flipY to (!transform.flipY.value).toFormula())))
                }
            }
        }
        page.document.history.endBatch()
    }

    fun deleteShapes(shapes: FList<Shape>) {
        page.document.history.let { history ->
            history.beginBatch()
            shapes.toList().forEach { shape ->
                history.makeChange(DeleteShape(shape))
                selection.remove(shape)
                removeShapeHandles(shape)
            }
            history.endBatch()
            dirty.value++
        }
    }

    /**
     * All of the zorder methods below assume that all selected shapes belong to the same parent.
     * If this isn't the case, then shapes with different parents will ignored.
     */
    private fun zOrderSelectionShapes(): List<Shape>? {
        val aShape = selection.firstOrNull() ?: return null
        return selection.filter { it.parent === aShape.parent }
    }

    /**
     * Moves the selection Shape(s) to the bottom of the z-order
     */
    fun zOrderBottom() {
        page.document.history.let { history ->
            val selection = zOrderSelectionShapes() ?: return

            val changes = selection.sortedBy { it.parent.children.indexOf(it) }.mapIndexed { index, shape -> ChangeZOrder(shape, index) }

            history.beginBatch()
            changes.forEach { history.makeChange(it) }
            history.endBatch()
            dirty.value++
        }
    }

    /**
     * Moves the selection Shape(s) to the top of the z-order
     */
    fun zOrderTop() {
        page.document.history.let { history ->
            val selection = zOrderSelectionShapes() ?: return

            val aShape = selection.firstOrNull() ?: return
            val lastIndex = aShape.parent.children.size - 1

            val changes = selection.sortedBy { -it.parent.children.indexOf(it) }.mapIndexed { index, shape -> ChangeZOrder(shape, lastIndex - index) }

            history.beginBatch()
            changes.forEach { history.makeChange(it) }
            history.endBatch()
            dirty.value++
        }
    }

    /**
     * Moves the selection Shape(s) down the z-order
     */
    fun zOrderDown() {
        page.document.history.let { history ->
            val selection = zOrderSelectionShapes() ?: return

            val shapes = selection.sortedBy { it.parent.children.indexOf(it) }
            var lowestIndex = 0

            val first = shapes.firstOrNull() ?: return
            val parent = first.parent
            if (parent.children.firstOrNull() === first && parent is ShapeWithGeometry && parent.geometry.parts.isNotEmpty()) {
                // We are trying to move the first child DOWN, so lets move the geometry of the parent into a new child.
                history.makeChange(MoveGeometryIntoChild(parent))
            }

            val changes = mutableListOf<Change>()
            shapes.forEach { shape ->
                val oldIndex = shape.parent.children.indexOf(shape)
                if (oldIndex != lowestIndex) {
                    changes.add(ChangeZOrder(shape, oldIndex - 1))
                    lowestIndex = oldIndex
                } else {
                    lowestIndex++
                }
            }

            history.beginBatch()
            changes.forEach { history.makeChange(it) }
            history.endBatch()
            dirty.value++
        }
    }

    /**
     * Moves the selection Shape(s) up the z-order
     */
    fun zOrderUp() {
        page.document.history.let { history ->
            val selection = zOrderSelectionShapes() ?: return

            val shapes = selection.sortedBy { -it.parent.children.indexOf(it) }
            val aShape = shapes.firstOrNull() ?: return

            var highestIndex = aShape.parent.children.size - 1

            val changes = mutableListOf<Change>()

            shapes.forEach { shape ->
                val oldIndex = shape.parent.children.indexOf(shape)
                if (oldIndex < highestIndex) {
                    changes.add(ChangeZOrder(shape, oldIndex + 1))
                    highestIndex = oldIndex
                } else {
                    highestIndex--
                }
            }

            history.beginBatch()
            changes.forEach { history.makeChange(it) }
            history.endBatch()
            dirty.value++
        }
    }

    /**
     * Calculates how much [delta] should be adjusted by to snap the shape.
     *
     * @param delta The change in position of shape in page coordinates
     * @return The adjustment to make to the shape, so that it snaps to something.
     * Returns [delta], if there are no suitable snapping points nearby.
     */
    fun calculateSnap(shape: Shape?, parent: ShapeParent, delta: Dimension2): Dimension2 {
        shape ?: return delta

        val maxSnap = 10 / scale

        var bestDeltaX = delta.x
        var bestScoreX = Double.MAX_VALUE

        var bestDeltaY = delta.y
        var bestScoreY = Double.MAX_VALUE

        fun maybeAdjustEither(diff: Dimension2) {
            val scoreX = Math.abs(diff.x.inDefaultUnits)
            if (scoreX < maxSnap && scoreX < bestScoreX) {
                bestScoreX = scoreX
                bestDeltaX = delta.x + diff.x
            }
            val scoreY = Math.abs(diff.y.inDefaultUnits)
            if (scoreY < maxSnap && scoreY < bestScoreY) {
                bestScoreY = scoreY
                bestDeltaY = delta.y + diff.y
            }
        }

        // TODO Add extra snap types, such as a Grid, Rulers etc.

        shape.snapPoints.forEach { sp ->
            val pageSP = shape.localToPage(sp.point.value) + delta

            parent.children.forEach { child ->
                if (child !in selection) {
                    child.snapPoints.forEach { childSP ->
                        val pageChildSP = child.localToPage(childSP.point.value)
                        maybeAdjustEither(pageChildSP - pageSP)
                    }
                }
            }
        }

        return Dimension2(bestDeltaX, bestDeltaY)
    }

    fun connectionData(pagePoint: Dimension2, shape: Shape, scale: Double, isStart: Boolean, local: Boolean): ConnectionData? {
        if (singleShape == null) {

            val localText = if (local) "Local" else ""
            val margin = if (isStart) "StartMargin" else "EndMargin"

            // Look for connection points
            shape.page().findNearestConnectionPoint(pagePoint, shape, !local)?.let { (connectionPoint, distance) ->
                val otherShape = connectionPoint.shape
                if (otherShape != null && distance < HANDLE_NEAR / scale) {
                    val index = otherShape.connectionPoints.indexOf(connectionPoint)
                    return ConnectionData("connect${localText}To( Page.Shape${otherShape.id}.ConnectionPoint${index + 1}, $margin )", otherShape, ConnectionType.CONTROL_POINT)
                }
            }

            // Look for geometries that can be connected to.
            shape.page().findNearestConnectionGeometry(pagePoint, shape, !local)?.let { (shape, distance, along) ->
                if (distance < HANDLE_NEAR / scale) {
                    return ConnectionData("connect${localText}Around( Page.Shape${shape.id}.Geometry, ${along.toFormula()}, $margin)", shape, ConnectionType.AROUND)
                }
            }

            shape.page().findShapesAt(pagePoint, Dimension.ZERO_mm).forEach { otherShape ->
                if (otherShape !== shape && otherShape is Shape2d && otherShape.smartConnectionType.value != SmartConnectionType.NONE.name) {
                    if (local) {
                        return ConnectionData("connectLocalToShapePin( Page.Shape${otherShape.id} )", otherShape, ConnectionType.PIN)
                    } else {
                        return ConnectionData("connectToShape( Page.Shape${otherShape.id}, ${isStart.toFormula()}, $margin)", otherShape, ConnectionType.SMART)
                    }
                }
            }

        }

        return null
    }

    fun connectionData(pagePoint: Dimension2, shape2dTransform: Shape2dTransform, scale: Double): ConnectionData? {
        if (singleShape == null) {
            val shape = shape2dTransform.shape

            shape.connectionPoints.forEachIndexed { n, mine ->
                val offset = shape.localToPage(mine.point.value) - shape.localToPage(shape.transform.locPin.value)
                val testPoint = pagePoint + offset

                // Look for connection points
                shape.page().findNearestConnectionPoint(testPoint, shape, true)?.let { (connectionPoint, distance) ->
                    connectionPoint.shape?.let { otherShape ->
                        if (distance < HANDLE_NEAR / scale) {
                            val index = otherShape.connectionPoints.indexOf(connectionPoint)
                            return ConnectionData("ConnectionPoint${n + 1}.connect2dTo( Page.Shape${otherShape.id}.ConnectionPoint${index + 1} )", otherShape, ConnectionType.CONTROL_POINT)
                        }
                    }
                }

                // Look for geometries that can be connected to.
                shape.page().findNearestConnectionGeometry(testPoint, shape, true)?.let { (otherShape, distance, along) ->
                    if (distance < HANDLE_NEAR / scale) {
                        return ConnectionData("ConnectionPoint${n + 1}.connect2dAround( Page.Shape${otherShape.id}.Geometry, ${along.toFormula()})", otherShape, ConnectionType.AROUND)
                    }
                }
            }

        }

        return null
    }

    companion object {

        val RED_BASE = Color.web("#cd3232")

        val GREEN_BASE = Color.web("#32cd32")
        val BLUE_BASE = Color.web("#72c2e9")
        val PURPLE_BASE = Color.web("#e972e8")

        val HANDLE_STROKE = BLUE_BASE.darker()
        val HANDLE_FILL = BLUE_BASE.brighter()

        val GEOMETRY_HANDLE_STROKE = GREEN_BASE.darker()
        val GEOMETRY_HANDLE_FILL = GREEN_BASE.brighter()

        val CONTROL_HANDLE_STROKE = PURPLE_BASE.darker()
        val CONTROL_HANDLE_FILL = PURPLE_BASE.brighter()

        val BOUNDING_STROKE = BLUE_BASE
        val BOUNDING_FILL = BLUE_BASE.transparent(0.3).brighter()

        val DEFAULT_SCALE = 96.0 / 25.4

        val HANDLE_SIZE = Dimension(3.0, Units.mm) // Half width (or height) of handles excluding the stroke.
        val HANDLE_NEAR = HANDLE_SIZE.inDefaultUnits + 2.0 // The size when testing if the mouse is at the handle
        val LINE_NEAR = Dimension(4.0) // The distance away from a line, and still be able to select it.

        val ROTATE_DISTANCE = 40.0

        private val ONE_mm = Dimension.ONE_mm

        val SQUARE = arrayOf(
                Dimension2(ONE_mm, ONE_mm),
                Dimension2(ONE_mm, -ONE_mm),
                Dimension2(-ONE_mm, -ONE_mm),
                Dimension2(-ONE_mm, ONE_mm)
        )

        val DIAMOND = arrayOf(
                Dimension2(ONE_mm, Dimension.ZERO_mm),
                Dimension2(Dimension.ZERO_mm, ONE_mm),
                Dimension2(-ONE_mm, Dimension.ZERO_mm),
                Dimension2(Dimension.ZERO_mm, -ONE_mm)
        )

        private val FORTY_FIVE = Dimension(Math.sin(Math.PI / 4.0), Units.mm)
        // A good enough approximation of a circle (its actually a regular octagon.
        val CIRCLE = arrayOf(
                Controller.DIAMOND[0],
                Dimension2(FORTY_FIVE, FORTY_FIVE),
                Controller.DIAMOND[1],
                Dimension2(-FORTY_FIVE, FORTY_FIVE),
                Controller.DIAMOND[2],
                Dimension2(-FORTY_FIVE, -FORTY_FIVE),
                Controller.DIAMOND[3],
                Dimension2(FORTY_FIVE, -FORTY_FIVE)
        )

        fun drawHandle(dc: DrawContext, points: Array<Dimension2>) {
            dc.beginPath()
            dc.moveTo(points.last())
            points.forEach { dc.lineTo(it) }
            dc.endPath(true, true)
        }


    }
}
