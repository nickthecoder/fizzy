/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.model.controlpoint.*

abstract class ControlPointPropType<T : ControlPoint>(klass: Class<T>)
    : PropType<T>(klass) {

    override fun findField(prop: Prop<T>, name: String): Prop<*>? {
        return when (name) {
            "Feedback" -> PropField("ControlPoint.Feedback", prop) { it.value.feedback }
            "Name" -> PropField("ControlPoint.Name", prop) { it.value.name }
            "Point" -> PropField("ControlPoint.Point", prop) { it.value.point }
            else -> super.findField(prop, name)
        }
    }

}

class StandardControlPointPropType private constructor()
    : ControlPointPropType<StandardControlPoint>(StandardControlPoint::class.java) {

    override fun findField(prop: Prop<StandardControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "Delta" -> PropField("StandardControlPoint.Delta", prop) { it.value.delta }
            "RelativeTo" -> PropField("StandardControlPoint.RelativeTo", prop) { it.value.relativeTo }
            "Min" -> PropField("StandardControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("StandardControlPoint.Max", prop) { it.value.max }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = StandardControlPointPropType()
    }
}

class WithinControlPointPropType private constructor()
    : ControlPointPropType<WithinControlPoint>(WithinControlPoint::class.java) {

    override fun findField(prop: Prop<WithinControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "A" -> PropField("WithinControlPoint.A", prop) { it.value.a }
            "B" -> PropField("WithinControlPoint.B", prop) { it.value.b }
            "Position" -> PropField("WithinControlPoint.Position", prop) { it.value.position }
            "Min" -> PropField("WithinControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("WithinControlPoint.Max", prop) { it.value.max }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = WithinControlPointPropType()
    }
}

class BetweenControlPointPropType private constructor()
    : ControlPointPropType<BetweenControlPoint>(BetweenControlPoint::class.java) {

    override fun findField(prop: Prop<BetweenControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "A" -> PropField("BetweenControlPoint.A", prop) { it.value.start } // Backwards compatibility only
            "Along" -> PropField("BetweenControlPoint.Along", prop) { it.value.along }
            "B" -> PropField("BetweenControlPoint.B", prop) { it.value.end } // Backwards compatibility only
            "End" -> PropField("BetweenControlPoint.End", prop) { it.value.end }
            "Start" -> PropField("BetweenControlPoint.Start", prop) { it.value.start }
            "Min" -> PropField("BetweenControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("BetweenControlPoint.Max", prop) { it.value.max }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = BetweenControlPointPropType()
    }
}

class LengthControlPointPropType private constructor()
    : ControlPointPropType<LengthControlPoint>(LengthControlPoint::class.java) {

    override fun findField(prop: Prop<LengthControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "A" -> PropField("LengthControlPoint.A", prop) { it.value.start } // Backwards compatibility
            "Angle" -> PropField("LengthControlPoint.Angle", prop) { it.value.angle }
            "Length" -> PropField("LengthControlPoint.Length", prop) { it.value.length }
            "Start" -> PropField("LengthControlPoint.Start", prop) { it.value.start }
            "Min" -> PropField("LengthControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("LengthControlPoint.Max", prop) { it.value.max }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = LengthControlPointPropType()
    }
}

class AngleControlPointPropType private constructor()
    : ControlPointPropType<AngleControlPoint>(AngleControlPoint::class.java) {

    override fun findField(prop: Prop<AngleControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "A" -> PropField("AngleControlPoint.A", prop) { it.value.start } // Backwards compatibility
            "Angle" -> PropField("AngleControlPoint.Angle", prop) { it.value.angle }
            "Radius" -> PropField("AngleControlPoint.Radius", prop) { it.value.radius }
            "Start" -> PropField("AngleControlPoint.Start", prop) { it.value.start }
            "Min" -> PropField("AngleControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("AngleControlPoint.Max", prop) { it.value.max }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = AngleControlPointPropType()
    }
}


class AlongControlPointPropType private constructor()
    : ControlPointPropType<AlongControlPoint>(AlongControlPoint::class.java) {

    override fun findField(prop: Prop<AlongControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "Part" -> PropField("AlongControlPoint.Part", prop) { it.value.part }
            "Along" -> PropField("AlongControlPoint.Length", prop) { it.value.along }
            "Min" -> PropField("AlongControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("AlongControlPoint.Max", prop) { it.value.max }
            "Tangent" -> PropDependency1("AlongControlPoint.Tangent", prop) { prop.value.tangent }
            "Normal" -> PropDependency1("AlongControlPoint.Normal", prop) { prop.value.normal }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = AlongControlPointPropType()
    }
}

class AroundControlPointPropType private constructor()
    : ControlPointPropType<AroundControlPoint>(AroundControlPoint::class.java) {

    override fun findField(prop: Prop<AroundControlPoint>, name: String): Prop<*>? {
        return when (name) {
            "AroundShape" -> PropField("AroundControlPoint.AroundShape", prop) { it.value.aroundShape }
            "Along" -> PropField("AroundControlPoint.Along", prop) { it.value.along }
            "Min" -> PropField("AroundControlPoint.Min", prop) { it.value.min }
            "Max" -> PropField("AroundControlPoint.Max", prop) { it.value.max }
            "Tangent" -> PropDependency1("AroundControlPoint.Tangent", prop) { prop.value.tangent }
            "Normal" -> PropDependency1("AroundControlPoint.Normal", prop) { prop.value.normal }
            else -> super.findField(prop, name)
        }
    }

    companion object {
        val instance = AroundControlPointPropType()
    }
}
