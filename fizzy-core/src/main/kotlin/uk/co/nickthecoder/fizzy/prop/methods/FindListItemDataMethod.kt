/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListener
import uk.co.nickthecoder.fizzy.model.NamedWithData
import uk.co.nickthecoder.fizzy.prop.Prop

/**
 * Finds a named item from a list, and returns its data value.
 * Used for the Shape's methods : "findCustomProperty" and "findScratch".
 *
 * The returned value is the CustomProperty's data, and not the CustomProperty object itself.
 */
class FindListItemDataMethod<S : Any, T : NamedWithData>(prop: Prop<S>, private val listName: String, private val listGetter: (S) -> FList<T>)
    : TypedMethod1<S, String, Any>("FindListItemValue", prop, String::class.java), ListListener<T> {

    override fun isConstant() = false

    override fun listChanged(list: FList<T>, item: T, index: Int, added: Boolean) {
        dirty = true
    }

    private var itemName: Prop<String>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    private var itemProp: Prop<*>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                v?.addListener(this)
                field = v
            }
        }

    private var list: FList<T>? = null
        set(v) {
            if (field !== v) {
                field?.listeners?.remove(this)
                field = v
                v?.listeners?.add(this)
            }
        }


    override fun noLongerNeeded() {
        super.noLongerNeeded()
        list?.listeners?.remove(this)
        itemName?.removeListener(this)
        itemProp?.removeListener(this)
    }

    override fun eval(a: String): Any {

        list = listGetter(prop.value)
        list?.forEach { item ->
            if (item.name.value == a) {
                itemName = item.name
                itemProp = item.data
                return item.data.value
            }
        }
        throw RuntimeException("Item $a not found in $listName")
    }

    override fun toString(): String {
        return "FindListItemDataMethod $listName"
    }
}
