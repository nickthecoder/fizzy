/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.Evaluator
import uk.co.nickthecoder.fizzy.evaluator.constantsContext

// Used to test primitive types in [evaluate].
private val alternativeTypes = mapOf(
        Double::class.java to java.lang.Double::class.java,
        Int::class.java to java.lang.Integer::class.java,
        Boolean::class.java to java.lang.Boolean::class.java,
        Byte::class.java to java.lang.Byte::class.java
)

fun Class<*>.isEitherInstance(obj: Any): Boolean {
    return this.isInstance(obj) || alternativeTypes[this]?.isInstance(obj) == true
}

fun Class<*>.assertIsInstance(obj: Any, prefix: String, suffix: String = "") {
    if (!isEitherInstance(obj)) throw RuntimeException("$prefix Expected $simpleName, but found ${obj.javaClass.simpleName} $suffix")
}

private fun <T : Any> evaluate(formula: String, klass: Class<T>, context: EvaluationContext): Prop<T> {

    val prop = Evaluator(formula, context).parse()
    val value = prop.value

    klass.assertIsInstance(value, "")

    @Suppress("UNCHECKED_CAST")
    return prop as Prop<T>

}

abstract class PropExpression<T : Any>(
        formula: String,
        val klass: Class<T>,
        var context: EvaluationContext = constantsContext)

    : MutableProp<T>, PropCalculation<T>("PropExpression", true) {

    constructor(other: PropExpression<T>, context: EvaluationContext = constantsContext)
            : this(other.formula, other.klass, context) {
        linkedTo = other
    }

    private val linkedListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            forceRecalculation()
        }
    }

    private var linkedTo: PropExpression<T>? = null
        set(v) {
            field?.removeListener(linkedListener)
            field = v
            calculatedProperty = null
            calculatedValue = null
            dirty = true
            v?.addListener(linkedListener)
        }

    private var exception: Exception? = null

    override var value: T
        get() = try {
            super.value
        } catch (e: Exception) {
            exception = e
            defaultValue
        }
        set(v) {
            val cp = calculatedProperty
            if (cp is Reference) {
                cp.value = v
            } else {
                formula = toFormula(v)
            }
        }

    var formula: String = formula
        get() = linkedTo?.formula ?: field
        set(v) {
            field = v
            linkedTo?.removeListener(this)
            linkedTo = null
            forceRecalculation()
        }

    abstract fun toFormula(value: T): String


    abstract val defaultValue: T

    private var calculatedProperty: Prop<T>? = null
        set(v) {
            field?.removeListener(this)
            field = v
            v?.addListener(this)
        }

    var debug = false

    override fun isConstant() = false

    fun isLinked() = linkedTo != null

    fun linkedTo(): PropExpression<T>? = linkedTo

    fun isError() = exception != null

    fun exception(): Exception? = exception

    fun calculatedProperty(): Prop<T>? {
        value // Ensure the expression has been calculated.
        return calculatedProperty
    }

    fun debug() {
        val cp = calculatedProperty
        println("\nPropExpression.debug")
        if (cp == null) {
            println("cp == null")
        } else {
            println("Formula '$formula'")
            println("cp # ${cp.hashCode()} type ${cp.javaClass.simpleName} = $calculatedProperty")
            println("Dirty? $dirty value = $value cp's value : ${cp.value}")
        }
        println()
    }


    override fun dirty(prop: Prop<*>) {
        if (debug) {
            println("Expression '$formula' made dirty")
        }
        super.dirty(prop)
    }

    fun copyFrom(other: PropExpression<*>, link: Boolean) {
        if (link) {
            if (other === this) throw IllegalArgumentException("Cannot link an expression to itself")
            if (klass !== other.klass) throw IllegalStateException("PropExpressions are not of the same type : ${this.javaClass} vs ${other.javaClass} ($this vs $other)")
            @Suppress("UNCHECKED_CAST")
            linkedTo = other as PropExpression<T>

            //other.propListeners.add(linkedListener)
            dirty = true
        } else {
            formula = other.formula
        }
    }

    override fun eval(): T {
        var cp = calculatedProperty

        if (cp == null) {
            try {
                cp = evaluate(formula, klass, context)
                calculatedProperty = cp
                exception = null
                return cp.value
            } catch (e: Exception) {
                exception = e
                return defaultValue
            }

        } else {
            return cp.value
        }
    }

    fun forceRecalculation() {
        calculatedProperty = null
        dirty = true
    }

    abstract fun copy(link: Boolean): PropExpression<T>

    override fun toString() = "='$formula'"
}

