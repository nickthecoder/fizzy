/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.MetaData
import uk.co.nickthecoder.fizzy.prop.AngleExpression
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.DimensionExpression
import uk.co.nickthecoder.fizzy.prop.PropCalculation3

class LengthControlPoint(
        val start: Dimension2Expression = Dimension2Expression(),
        val angle: AngleExpression = AngleExpression(),
        val length: DimensionExpression = DimensionExpression()
) : ControlPoint(ControlPointType.LENGTH) {

    constructor(a: String, angle: String, length: String) : this(Dimension2Expression(a), AngleExpression(angle), DimensionExpression(length))

    override val point = PropCalculation3<Dimension2, Dimension2, Angle, Dimension>("LengthControlPoint.point", true, start, angle, length) { vStart, vAngle, vLength ->
        vStart + (vAngle.unitVector() * vLength)
    }

    val min = DimensionExpression("MIN_DIMENSION")

    val max = DimensionExpression("MAX_DIMENSION")

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        start.context = context
        angle.context = context
        length.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Start", start, "A").addAliases("A")
        metaData.newCell("Angle", angle, "B")
        metaData.newCell("Length", length, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        val relative = (draggedTo - start.value)
        length.value = relative.dot(angle.value.unitVector()).clamp(min.value, max.value)
    }

    override fun copy(link: Boolean): LengthControlPoint {
        val result = LengthControlPoint(start.copy(link), angle.copy(link), length.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
