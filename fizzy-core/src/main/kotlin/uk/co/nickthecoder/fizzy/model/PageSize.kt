/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation2
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable

/**
 * Holds the scale of used by the document.
 * e.g. When drawing room plans, 1 cm on the printed output may represent 100 cm of the room,
 * in which case the scale is 1:100 i.e. 0.01@author
 *
 * This is NOT related to the zoom factor used by views.
 *
 * The main reason for this class (instead of storing a simple Double on Document), is to handle the special units
 * of length "pt" (points) and "px" (pixels).
 * When using a 1:1 DocumentScale, a point (pt) is 1/72 of an inch. However, when using a scaled document
 * (e.g. when drawing room plans), then a point will NOT be 1/72 of an inch. If it were, then the text would be
 * incredibly tiny!
 */
class PageSize {

    /**
     * The size that the diagram will be printed out (most commonly A4)
     */
    val paperSize = PropVariable<Dimension2>(Dimension2(Dimension(210.0, Units.mm), Dimension(297.0, Units.mm)))

    /**
     * The real-world size of the diagram. For simple charts, this will be the same as [paperSize], but for scaled drawings,
     * such as floor plans, then they will differ. For a floor plan, this will be the size of the room (plus margins).
     */
    val documentSize = PropVariable<Dimension2>(paperSize.value)

    /**
     * The scale of the diagram. For example, when drawing room plans, 1cm on paper may represent 100cm in the
     * real world, in which case the scale is 100.0
     *
     * A calculated value based on [paperSize] and [documentSize].
     * At a later date, I may allow this to be set to a custom size by the user.
     *
     * [DynamicUnits] use this property to adjust Dimensions dynamically. i.e. changing this value can change the size
     * of many objects (most commonly font sizes and line widths).
     */
    val scaleProp: Prop<Double> = PropCalculation2<Double, Dimension2, Dimension2>("PageSize.scaleProp", true, paperSize, documentSize) { paper, doc ->
        Math.max(doc.x / paper.x, doc.y / paper.y)
    }.apply { expectedMaxListeners = 1000 }

    private val scalePropListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            unitsMap["pt"] = DynamicUnits("pt", scaleProp, 25.4 / 72)
        }
    }

    val scale: Double
        get() = scaleProp.value

    init {
        //scaleProp.propListeners.expectedMaxSize = 10000
        scaleProp.addListener(scalePropListener)
    }

    private val unitsMap = mutableMapOf<String, Units>(
            "mm" to Units.mm,
            "cm" to Units.cm,
            "m" to Units.m,
            "km" to Units.km,
            "inch" to Units.inch,
            "ft" to Units.ft,
            "yard" to Units.yard,
            "pt" to DynamicUnits("pt", scaleProp, 25.4 / 72) // For documents with a 1:1 scale
    )

    fun units(): Collection<Units> {
        return unitsMap.values
    }

    fun findUnits(name: String): Units? = unitsMap[name]

    fun getUnits(name: String): Units = unitsMap[name] ?: throw IllegalArgumentException("Unknown units : $name")
}
