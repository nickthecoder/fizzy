/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.geometry.MoveTo
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType
import uk.co.nickthecoder.fizzy.util.clamp

/**
 * A control point which move around the geometry of a given shape.
 * The amount "along" is always between 0 and 1, but can be constrained further via [min] and [max].
 */
class AroundControlPoint(
        val aroundShape: ShapeExpression = ShapeExpression("this"),
        val along: DoubleExpression = DoubleExpression(0.5)
) : ControlPoint(ControlPointType.AROUND) {

    constructor(shape: String, along: String) : this(ShapeExpression(shape), DoubleExpression(along))

    /**
     * This property's value is incremented every time the Point may need to be re-evaluated.
     * This causes [point] to become dirty.
     */
    private val bodge = PropVariable<Int>(0)

    override val point: Prop<Dimension2> = PropCalculation3("AroundControlPoint.point", true, aroundShape, along, bodge) { vShape, vAlong, _ ->
        oldShape = vShape
        if (vShape is ShapeWithGeometry) {
            otherToMine(vShape.geometry.pointAlong(vAlong))
        } else {
            Dimension2.ZERO_mm
        }
    }

    val tangent: Prop<Angle> by lazy {
        PropCalculation3("AroundControlPoint.tangent", true, aroundShape, along, bodge) { vShape, vAlong, _ ->
            if (vShape is ShapeWithGeometry) {
                vShape.localToPage(vShape.geometry.tangentAlong(vAlong))
            } else {
                Angle.ZERO
            }
        }
    }

    val normal: Prop<Angle> by lazy {
        PropCalculation3("AroundControlPoint.normal", true, aroundShape, along, bodge) { vShape, vAlong, _ ->
            if (vShape is ShapeWithGeometry) {
                vShape.localToPage(vShape.geometry.normalAlong(vAlong))
            } else {
                Angle.ZERO
            }
        }
    }

    val min = DoubleExpression("0.0")
    val max = DoubleExpression("1.0")

    /**
     * Whenever Shape changes, we invalidate [point], but incrementing [bodge].
     */
    private var shapeListener = object : ChangeListener<Shape> {
        override fun changed(item: Shape, changeType: ChangeType, obj: Any?) {
            bodge.value++
        }
    }

    /**
     * Every time we evaluate [point], we set [oldShape]. If it has changed since last time,
     * then we unlisten to the old shape, and listen to the new one.
     */
    private var oldShape: Shape? = null
        set(v) {
            if (field != v) {
                field?.changeListeners?.remove(shapeListener)
                field = v
                v?.changeListeners?.add(shapeListener)
                bodge.value++
            }
        }

    /**
     * Converts points
     * from the coordinate system of the geometry's shape
     * to the coordinate system of the shape that owns the ControlPoint.
     */
    private fun otherToMine(point: Dimension2): Dimension2 {
        val otherShape = aroundShape.value
        val myShape = shape

        return if (myShape != null && otherShape !== myShape) {
            myShape.pageToLocal((otherShape.localToPage(point)))
        } else {
            point
        }
    }

    /**
     * Converts points
     * from the coordinate system of the shape that owns the ControlPoint.
     * to the coordinate system of the geometry's shape,
     */
    private fun mineToOther(point: Dimension2): Dimension2 {
        val otherShape = aroundShape.value
        val myShape = shape

        return if (myShape != null && otherShape !== myShape) {
            otherShape.pageToLocal((myShape.localToPage(point)))
        } else {
            point
        }
    }

    override fun setContext(context: EvaluationContext) {
        super.setContext(context)
        aroundShape.context = context
        along.context = context
        min.context = context
        max.context = context
    }

    override fun addMetaData(metaData: MetaData) {
        metaData.newCell("Shape", aroundShape, "A")
        metaData.newCell("Along", along, "Value")
        metaData.newCell("Min", min)
        metaData.newCell("Max", max)
    }

    override fun dragTo(draggedTo: Dimension2) {
        val otherShape = aroundShape.value
        if (otherShape !is ShapeWithGeometry) return

        val draggedToOther = mineToOther(draggedTo)

        val nonMoveCount = otherShape.geometry.parts.count { it.visible.value && it !is MoveTo }
        val interval = 1.0 / nonMoveCount
        var moveToCount = 0

        var nearestDistance = Double.MAX_VALUE
        var nearestAlong = 0.0

        otherShape.geometry.parts.forEachIndexed { index, part ->
            if (part.visible.value) {
                if (part is MoveTo) {
                    moveToCount++
                } else {

                    part.checkAlong(draggedToOther)?.let { (dist, along) ->
                        if (dist < nearestDistance) {
                            nearestDistance = dist
                            nearestAlong = ((index - moveToCount) + along) * interval
                        }
                    }
                }
            }
        }

        along.value = nearestAlong.clamp(min.value, max.value)
    }

    override fun copy(link: Boolean): AroundControlPoint {
        val result = AroundControlPoint(aroundShape.copy(link), along.copy(link))
        result.min.copyFrom(min, link)
        result.max.copyFrom(max, link)
        return result
    }

}
