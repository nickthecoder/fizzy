/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.Angle
import java.text.DecimalFormat

class AnglePropType private constructor()
    : PropType<Angle>(Angle::class.java) {

    override fun findField(prop: Prop<Angle>, name: String): Prop<*>? {
        return when (name) {
            "Degrees" -> ConstantPropField("Degrees", prop) { it.value.degrees }
            "Radians" -> ConstantPropField("Radians", prop) { it.value.radians }
            else -> super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<Angle>, name: String): PropMethod<Angle, *>? {
        return when (name) {
            "unitVector" -> PropMethod0("Angle.unitVector", prop) { prop.value.unitVector() }
            "format" -> PropMethod1("Angle.format", prop, String::class.java) { format -> DecimalFormat(format).format(prop.value.degrees) + "°" }
            "sin" -> PropMethod0("Angle.sin", prop) { Math.sin(prop.value.radians) }
            "cos" -> PropMethod0("Angle.cos", prop) { Math.cos(prop.value.radians) }
            "tan" -> PropMethod0("Angle.tan", prop) { Math.tan(prop.value.radians) }

            "sinh" -> PropMethod0("Angle.sinh", prop) { Math.sinh(prop.value.radians) }
            "cosh" -> PropMethod0("Angle.cosh", prop) { Math.cosh(prop.value.radians) }
            "tanh" -> PropMethod0("Angle.tanh", prop) { Math.tanh(prop.value.radians) }
            "clamp" -> PropMethod2("Angle.clamp", prop, Angle::class.java, Angle::class.java) { min, max -> prop.value.clamp(min, max) }
            else -> null
        }
    }

    companion object {
        val instance = AnglePropType()

        fun create(a: Prop<Double>, degrees: Boolean): Prop<Angle> {
            return if (a.isConstant()) {
                if (degrees) {
                    PropConstant(Angle.degrees(a.value))
                } else {
                    PropConstant(Angle.radians(a.value))
                }
            } else {
                if (degrees) {
                    PropCalculation1("Angle.createDegrees", false, a) { av -> Angle.degrees(av) }
                } else {
                    PropCalculation1("Angle.createRadians", false, a) { av -> Angle.radians(av) }
                }
            }
        }
    }
}


class AngleExpression
    : PropExpression<Angle> {

    constructor(formula: String, context: EvaluationContext = constantsContext) : super(formula, Angle::class.java, context)

    constructor(value: Angle = Angle.ZERO, context: EvaluationContext = constantsContext) : this(value.toFormula(), context)

    constructor(other: AngleExpression) : super(other)

    override val defaultValue = Angle.ZERO

    override fun toFormula(value: Angle) = value.toFormula()

    override fun copy(link: Boolean) = if (link) AngleExpression(this) else AngleExpression(formula)

    override fun valueString() = value.toFormula()
}

fun degConversion(a: Prop<*>): Prop<*> {
    if (a.value is Double) {
        @Suppress("UNCHECKED_CAST")
        return AnglePropType.create(a as Prop<Double>, degrees = true)
    }
    return throwExpectedType("Double", a)
}

fun radConversion(a: Prop<*>): Prop<*> {
    if (a.value is Double) {
        @Suppress("UNCHECKED_CAST")
        return AnglePropType.create(a as Prop<Double>, degrees = false)
    }
    return throwExpectedType("Double", a)
}
