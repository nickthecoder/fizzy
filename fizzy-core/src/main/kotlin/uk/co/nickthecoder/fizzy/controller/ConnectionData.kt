/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller

import uk.co.nickthecoder.fizzy.model.Shape

/**
 * When dragging a Shape1d start or end point, it may connect to a ConnectionPoint,
 * connect along the geometry of another shape, smart-connect to another shape,
 * or not be connected at all.
 *
 * This class the data communicated between Controller and the Shape1dHandle to connect
 * in all these ways.
 */
data class ConnectionData(
        val connectionPointFormula: String,
        val connectedToShape: Shape,
        val type: ConnectionType
)

enum class ConnectionType {
    CONTROL_POINT, AROUND, SMART, PIN
}
