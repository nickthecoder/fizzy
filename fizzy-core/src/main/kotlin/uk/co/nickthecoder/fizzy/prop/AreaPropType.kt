/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.model.Area
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Units

class AreaPropType private constructor()
    : PropType<Area>(Area::class.java) {

    override fun findField(prop: Prop<Area>, name: String): Prop<*>? {
        return when (name) {
            "mm" -> ConstantPropField("Area.mm", prop) { it.value.mm }
            "cm" -> ConstantPropField("Area.cm", prop) { it.value.cm }
            "m" -> ConstantPropField("Area.m", prop) { it.value.m }
            "km" -> ConstantPropField("Area.km", prop) { it.value.km }
            else -> return super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<Area>, name: String): PropMethod<in Area, *>? {
        return when (name) {
            "toString" -> PropMethod0("Area.toString", prop) { prop.value.toFormula() }
            "format" -> PropMethod1("Area.format", prop, String::class.java) { format -> prop.value.format(format) }
            "units" -> PropMethod0("Area.units", prop) { prop.value.units }
            "inUnits" -> PropMethod1("Area.inUnits", prop, Units::class.java) { units -> prop.value.inUnits(units) }
            "toUnits" -> PropMethod1("Area.toUnits", prop, Units::class.java) { units -> Dimension(prop.value.inUnits(units), units) }
            else -> super.findMethod(prop, name)
        }
    }

    companion object {
        val instance = AreaPropType()
    }
}
