/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.tools

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.ShapeHighlight
import uk.co.nickthecoder.fizzy.model.Color
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape1d
import uk.co.nickthecoder.fizzy.model.history.CreateShape
import uk.co.nickthecoder.fizzy.prop.DimensionExpression

/**
 * Adds a Shape1d to the document by dragging.
 */
class GrowShape1dTool(
        controller: Controller,
        val masterShape: Shape1d,
        val strokeColor: Color? = null,
        val fillColor: Color? = null,
        val lineWidthExpression: DimensionExpression? = null)
    : Tool(controller) {

    override val cursor = ToolCursor.GROW

    var startPoint: Dimension2? = null
    var newShape: Shape1d? = null

    init {
        controller.selection.clear()
    }

    override fun beginTool() {
        if (controller.singleShape == null) {
            controller.showConnectionPoints(true)
        }
    }

    override fun endTool(replacement: Tool) {
        controller.showConnectionPoints(false)
        controller.highlightShape.value = ShapeHighlight.NO_SHAPE_HIGHLIGHT
    }

    override fun onMousePressed(event: CMouseEvent) {
        startPoint = event.point
        controller.page.document.history.beginBatch()
    }

    override fun onDragDetected(event: CMouseEvent) {
        startPoint?.let { startPoint ->
            val makeLocal = controller.page.document !== masterShape.document()
            val parentStart = controller.parent.pageToLocal(startPoint)
            val parentEnd = controller.parent.pageToLocal(event.point)

            val newShape = controller.page.document.copyMasterShape(masterShape, controller.parent, makeLocal) as Shape1d

            // Either a connection to another shape, or the point of the onMousePressed event.
            val connectionData = controller.connectionData(startPoint, newShape, event.scale, true, false)

            newShape.start.formula = connectionData?.connectionPointFormula ?: parentStart.toFormula()

            newShape.end.formula = parentEnd.toFormula()

            strokeColor?.let { newShape.strokeColor.formula = it.toFormula() }
            fillColor?.let { newShape.fillColor.formula = it.toFormula() }
            lineWidthExpression?.let { newShape.lineWidth.formula = it.formula }

            this.newShape = newShape

            controller.page.document.history.makeChange(
                    CreateShape(newShape, controller.parent)
            )
        }
    }

    override fun onMouseDragged(event: CMouseEvent) {
        newShape?.let {

            // If we are constraining, then ensure the end point is either vertical, or horizontal from the start.
            // Otherwise let it be where the mouse is.
            val pageEnd = if (event.isConstrain) {
                if (event.isAdjust) {
                    // Snap along X or Y using the local coordinates.
                    // For new top-level shapes this is the same as the code below,
                    // but for child shapes, it will be different if the parent is rotated.
                    val localMouse = it.parent.pageToLocal(event.point)
                    val dx = Math.abs((localMouse.x - it.start.value.x).inDefaultUnits)
                    val dy = Math.abs((localMouse.y - it.start.value.y).inDefaultUnits)
                    val localEnd = if (dx > dy) {
                        Dimension2(localMouse.x, it.start.value.y)
                    } else {
                        Dimension2(it.start.value.x, localMouse.y)
                    }
                    it.parent.localToPage(localEnd)
                } else {
                    // Snap horizontally or vertically within the page.
                    val pageStart = it.parent.localToPage(it.start.value)
                    val dx = Math.abs((event.point.x - pageStart.x).inDefaultUnits)
                    val dy = Math.abs((event.point.y - pageStart.y).inDefaultUnits)
                    if (dx > dy) {
                        Dimension2(event.point.x, pageStart.y)
                    } else {
                        Dimension2(pageStart.x, event.point.y)
                    }
                }
            } else {
                event.point
            }
            val parentEnd = controller.parent.pageToLocal(pageEnd)

            // Either a connection to another shape, or the point of the mouse event.
            val connectionData = controller.connectionData(pageEnd, it, event.scale, false, false)

            controller.highlightShape.value = ShapeHighlight.create(connectionData)

            // Note, we don't need to change the end point using a Change, because the Batch will only be completed
            // when the drag is completed. At which point the end point is set correctly.
            it.end.formula = connectionData?.connectionPointFormula ?: parentEnd.toFormula()
        }
    }

    override fun onMouseReleased(event: CMouseEvent) {
        controller.highlightShape.value = ShapeHighlight.NO_SHAPE_HIGHLIGHT
        newShape = null
        controller.page.document.history.endBatch()
    }
}
