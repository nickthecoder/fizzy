/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model.controlpoint

import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.prop.methods.Connection
import uk.co.nickthecoder.fizzy.util.ChangeListeners
import uk.co.nickthecoder.fizzy.util.HasChangeListeners
import uk.co.nickthecoder.fizzy.view.DrawContext

abstract class ControlPoint(type: ControlPointType)
    : HasChangeListeners<ControlPoint>, PropListener, Named, MetaDataAware {

    override val changeListeners = ChangeListeners<ControlPoint>()

    override val name = PropVariable("")

    open var shape: Shape? = null
        set(v) {
            field = v
            setContext(v?.context ?: constantsContext)
        }

    abstract val point: Prop<Dimension2>

    val type = PropConstant(type)

    val visible = BooleanExpression(true)

    /**
     * Appears in the GUI while the control point is being moved.
     */
    val feedback = StringExpression()

    /**
     * Setting this expression will drag the control point. Using an expression (such as connectTo),
     * will change the control point whenever its dependencies become dirty.
     * This is a local coordinate.
     */
    val dragTo = Dimension2Expression()

    private val dragListener = PropListenerLambda {
        dragTo(dragTo.value)
    }


    init {
        dragTo.addListener(dragListener)
    }

    open fun setContext(context: EvaluationContext) {
        feedback.context = context
        visible.context = context
        dragTo.context = context
    }

    override fun dirty(prop: Prop<*>) {
        changeListeners.fireChanged(this)
    }

    override fun metaData(): MetaData {
        val md = MetaData(type.value.code, type.value.label)
        md.newCell("Name", name)
        md.newCell("Point", point, save = false)
        addMetaData(md)
        md.newCell("Visible", visible)
        md.newCell("Feedback", feedback)
        // Only save the dragTo when it is a Connection, otherwise, it is just a constant value of the previous
        // drag point.
        val dragConnection = dragTo.calculatedProperty() is Connection
        md.newCell("DragTo", dragTo, display = true, save = dragConnection)
        return md
    }

    abstract fun addMetaData(metaData: MetaData)

    fun index(): Int {
        shape?.let { shape ->
            shape.controlPoints.forEachIndexed { index, cp ->
                if (cp === this) {
                    return index
                }
            }
        }
        return -1
    }

    abstract fun dragTo(draggedTo: Dimension2)

    open fun draw(dc: DrawContext, dragging: Boolean = false) {
        drawHandle(dc, dragging)
        if (dragging) {
            val label = feedback.value
            if (label.isNotBlank()) {
                dc.save()
                dc.translate(Dimension(4.0, Units.mm), Dimension.ZERO_mm)
                val font = FFontDetails.find("Sans Serif", "Regular").createFont(4.0)
                dc.multiLineText(MultiLineText(label, font, 0.0, 0.5))
                dc.restore()
            }
        }
    }

    protected open fun drawHandle(dc: DrawContext, dragging: Boolean) {
        Controller.drawHandle(dc, Controller.DIAMOND)
    }

    abstract fun copy(link: Boolean): ControlPoint

    open fun shapeResized(oldSize: Dimension2, newSize: Dimension2) {}
}

/**
 * [code] is used when loading and saving documents.
 * [aliases] are old codes, and are used for backwards compatibility when loading old documents.
 * [label] is the label used within the ShapeSheetView
 */
enum class ControlPointType(val code: String, val label: String, val factory: () -> ControlPoint, val aliases: List<String> = emptyList()) {

    STANDARD("StandardControlPoint", "Standard", { StandardControlPoint() }),
    WITHIN("WithinControlPoint", "Within", { WithinControlPoint() }, aliases = listOf("ControlPoint", "FreeControlPoint")),
    BETWEEN("BetweenControlPoint", "Between", { BetweenControlPoint() }, aliases = listOf("LinearControlPoint")),
    LENGTH("LengthControlPoint", "Length", { LengthControlPoint() }),
    ANGLE("AngleControlPoint", "Angle", { AngleControlPoint() }),
    ALONG("AlongControlPoint", "Along", { AlongControlPoint() }),
    AROUND("AroundControlPoint", "Around", { AroundControlPoint() })
}
