/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListener
import uk.co.nickthecoder.fizzy.model.NamedWithData
import uk.co.nickthecoder.fizzy.prop.methods.FindListItemDataMethod

/**
 * Looks through an [FList], looking for the item with a particular name.
 * If found, then return the primary data value.
 *
 * This is used with CustomProperty and Scratch, in expressions like the following :
 *
 *     CustomProperty.foo
 *     Scratch.bar
 *
 * where foo is the name of a CustomProperty, and bar is the name of a Scratch.
 *
 * Note that it is the data that is returned, not the CustomProperty object, nor the Scratch object.
 * This is similar to [FindListItemDataMethod], except that one is a [PropMethod], which can be passed the item name,
 * whereas this uses a fixed item name.
 */
class FindListItemData<T : NamedWithData>(private val listName: String, private val list: FList<T>, val name: String)
    : PropCalculation<Any>("FindListItemData", false), MutableProp<Any>, ListListener<T> {

    override var value: Any
        get() = super.value
        set(v) {
            list.forEach { item ->
                if (item.name.value == name) {
                    val prop = item.data
                    @Suppress("UNCHECKED_CAST")
                    (prop as MutableProp<Any>).value = v
                    return
                }
            }
            throw RuntimeException("$listName $name not found")
        }

    /**
     * Listen to the name, so that if it changes, we become dirty (and will throw when re-evaluated).
     */
    private var itemName: Prop<String>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    private var itemProp: Prop<*>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }


    init {
        list.listeners.add(this)
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        list.listeners.remove(this)
        itemName?.removeListener(this)
        itemProp?.removeListener(this)
    }

    /**
     * If the list item is removed, we need to become dirty!
     * If the name has been added back again, become dirty (probably not needed, but it won't hurt).
     */
    override fun listChanged(list: FList<T>, item: T, index: Int, added: Boolean) {
        if (item.name.value == name) {
            dirty = true
        }
    }

    override fun eval(): Any {
        list.forEach { item ->
            if (item.name.value == name) {
                itemProp = item.data
                itemName = item.name
                return item.data.value
            }
        }
        throw RuntimeException("$listName $name not found")
    }

}
