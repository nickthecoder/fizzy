/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

abstract class PropCalculation<T : Any>(val description: String, permanent: Boolean)

    : CountingProp<T>(permanent), PropListener {

    var dirty: Boolean = true
        set(v) {
            if (field != v) {
                field = v
                if (v) {
                    notifyListeners()
                }
            }
        }

    protected var calculatedValue: T? = null
        get() {
            if (dirty || field == null) {
                if (isCalculating) {
                    throw RuntimeException("Recursive evaluation of $this")
                }
                if (noLongerNeeded) {
                    throw RuntimeException("Recalculating a value which is no longer needed. $this")
                }

                isCalculating = true
                try {
                    val v = eval()
                    field = v
                    dirty = false
                    return v
                } finally {
                    isCalculating = false
                }
            } else {
                return field
            }
        }

    override fun dirty(prop: Prop<*>) {
        dirty = true
    }

    private var isCalculating = false

    override val value: T
        get() {
            // We COULD return the value, but it is better to know that the code has a bug as early as possible.
            if (noLongerNeeded) throw RuntimeException("Attempting to get a value, which has been marked as noLongerNeeded. $this")

            return calculatedValue ?: throw RuntimeException("Failed to calculate : $this")
        }

    abstract fun eval(): T

    private fun listenTo(prop: Prop<*>) {
        if (!prop.isConstant()) {
            prop.addListener(this)
        }
    }

    private fun unlistenTo(prop: Prop<*>) {
        prop.removeListener(this)
    }

    override fun toString(): String = description
}

class PropCalculation1<T : Any, A : Any>(
        description: String,
        permanent: Boolean,
        private val a: Prop<A>,
        private val lambda: (A) -> T
) : PropCalculation<T>(description, permanent) {

    init {
        if (!a.isConstant()) a.addListener(this)
    }

    override fun eval(): T = lambda(a.value)


    override fun noLongerNeeded() {
        super.noLongerNeeded()
        a.removeListener(this)
    }
}

class PropCalculation2<T : Any, A : Any, B : Any>(
        description: String,
        permanent: Boolean,
        private val a: Prop<A>,
        private val b: Prop<B>,
        private val lambda: (A, B) -> T
) : PropCalculation<T>(description, permanent) {

    init {
        if (!a.isConstant()) a.addListener(this)
        if (!b.isConstant()) b.addListener(this)
    }

    override fun eval(): T = lambda(a.value, b.value)

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        a.removeListener(this)
        b.removeListener(this)
    }

}

class PropCalculation3<T : Any, A : Any, B : Any, C : Any>(
        description: String,
        permanent: Boolean,
        private val a: Prop<A>,
        private val b: Prop<B>,
        private val c: Prop<C>,
        private val lambda: (A, B, C) -> T
) : PropCalculation<T>(description, permanent) {

    init {
        if (!a.isConstant()) a.addListener(this)
        if (!b.isConstant()) b.addListener(this)
        if (!c.isConstant()) c.addListener(this)
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        a.removeListener(this)
        b.removeListener(this)
        c.removeListener(this)
    }

    override fun eval(): T = lambda(a.value, b.value, c.value)
}

class PropCalculation4<T : Any, A : Any, B : Any, C : Any, D : Any>(
        description: String,
        permanent: Boolean,
        private val a: Prop<A>,
        private val b: Prop<B>,
        private val c: Prop<C>,
        private val d: Prop<D>,
        private val lambda: (A, B, C, D) -> T
) : PropCalculation<T>(description, permanent) {

    init {
        if (!a.isConstant()) a.addListener(this)
        if (!b.isConstant()) b.addListener(this)
        if (!c.isConstant()) c.addListener(this)
        if (!d.isConstant()) d.addListener(this)
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        a.removeListener(this)
        b.removeListener(this)
        c.removeListener(this)
        d.removeListener(this)
    }

    override fun eval(): T = lambda(a.value, b.value, c.value, d.value)
}


class PropCalculation6<T : Any, A : Any, B : Any, C : Any, D : Any, E : Any, F : Any>(
        description: String,
        permanent: Boolean,
        private val a: Prop<A>,
        private val b: Prop<B>,
        private val c: Prop<C>,
        private val d: Prop<D>,
        private val e: Prop<E>,
        private val f: Prop<F>,
        private val lambda: (A, B, C, D, E, F) -> T
) : PropCalculation<T>(description, permanent) {

    init {
        if (!a.isConstant()) a.addListener(this)
        if (!b.isConstant()) b.addListener(this)
        if (!c.isConstant()) c.addListener(this)
        if (!d.isConstant()) d.addListener(this)
        if (!e.isConstant()) e.addListener(this)
        if (!f.isConstant()) f.addListener(this)
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        a.removeListener(this)
        b.removeListener(this)
        c.removeListener(this)
        d.removeListener(this)
        e.removeListener(this)
        f.removeListener(this)
    }

    override fun eval(): T = lambda(a.value, b.value, c.value, d.value, e.value, f.value)
}
