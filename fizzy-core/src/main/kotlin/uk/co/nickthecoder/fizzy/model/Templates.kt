/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.util.FizzyJsonReader
import java.io.File

object Templates {
    /**
     * The list of directories in which templates are stored.
     * Adding a directory to this list will automatically scan the directory for
     * files with extension ".ftemplate", and add those templates to the
     * [templates] list.
     */
    val directories = MutableFList<File>()

    /**
     * A list of [Document]s which are used as "starting points" for new documents.
     */
    val templates = MutableFList<Document>()


    private val directoriesListener = ListListenerLambda<File> { _, item, _, added ->
        if (added) addTemplates(item) else removeTemplates(item)
    }

    init {
        directories.listeners.add(directoriesListener)
        directories.add(File(File(System.getProperty("user.home"), "fizzy"), "templates"))
        findStockDirectory()?.let { directories.add(File(it, "templates")) }
    }

    private fun addTemplates(directory: File) {
        val files = directory.listFiles()
        files?.forEach { file ->
            if (file.isFile && file.extension == "fizzyt") {
                try {
                    val doc = FizzyJsonReader(file).load()
                    templates.add(doc)
                } catch (e: Exception) {
                }
            }
        }
    }

    private fun removeTemplates(directory: File) {
        templates.removeAll(templates.filter { it.file?.startsWith(directory) == true })
    }

}
