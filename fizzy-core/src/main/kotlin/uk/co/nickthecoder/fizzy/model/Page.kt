/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropListener
import uk.co.nickthecoder.fizzy.prop.PropVariable
import uk.co.nickthecoder.fizzy.util.ChangeAndListListener
import uk.co.nickthecoder.fizzy.util.ChangeListeners
import uk.co.nickthecoder.fizzy.util.HasChangeListeners
import uk.co.nickthecoder.fizzy.util.runLater

class Page internal constructor(val document: Document, add: Boolean)
    : ShapeParent, FindsUnits, HasChangeListeners<Page>, MetaDataAware, PropListener {

    constructor(document: Document) : this(document, true)

    override val changeListeners = ChangeListeners<Page>()

    override val children = ShapeChildren()

    private val childrenListener = ChangeAndListListener(this, children,
            onAdded = { item, _ -> if (item.page() != this@Page) throw IllegalStateException("Added $item to a different page") }
    )

    val pageSize = PageSize()

    val theme = PropVariable<Theme>(DefaultTheme(this)).apply { expectedMaxListeners = 500 }

    private val themeName = PropVariable(theme.value.name)

    private val themeListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            themeName.value = theme.value.name
        }
    }

    private val themeNameListener = object : PropListener {
        override fun dirty(prop: Prop<*>) {
            if (themeName.value != theme.value.name) {
                if (themeName.value == "DEFAULT") {
                    theme.value = DefaultTheme(this@Page)
                } else {
                    Themes.useTheme(themeName.value, this@Page)
                }
            }
        }
    }

    init {
        themeName.addListener(themeNameListener)
        theme.addListener(themeListener)

        if (add) {
            document.pages.add(this)
        }
        listenTo(pageSize.paperSize, pageSize.documentSize, theme)
    }

    override fun findUnits(name: String) = pageSize.findUnits(name)

    /**
     * Listens to the expression, so that when it changes, Shape's listeners are informed.
     * The expression's [EvaluationContext] is also set.
     */
    fun listenTo(vararg expressions: Prop<*>) {
        expressions.forEach { expression ->
            expression.addListener(this)
        }
    }

    private var dirty = false
        set(v) {
            if (field != v) {
                field = v
                if (v) {
                    runLater("Page is dirty") {
                        dirty = false
                        changeListeners.fireChanged(this)
                    }
                }
            }
        }

    override fun dirty(prop: Prop<*>) {
        dirty = true
    }

    override fun document() = document

    override fun page() = this

    override fun localToParent(localPoint: Dimension2) = localPoint
    override fun localToPage(localPoint: Dimension2) = localPoint
    override fun parentToLocal(parentPoint: Dimension2) = parentPoint
    override fun pageToLocal(pagePoint: Dimension2) = pagePoint

    override fun pageToLocal(pageAngle: Angle) = pageAngle
    override fun parentToLocal(parentAngle: Angle) = parentAngle
    override fun localToPage(localAngle: Angle) = localAngle
    override fun localToParent(localAngle: Angle) = localAngle

    override fun findChild(name: String): Shape? = children.firstOrNull { it.name.value == name }

    override fun findShape(name: String): Shape? {
        children.forEach { shape ->
            val found = shape.findShape(name)
            if (found != null) return found
        }
        return null
    }

    fun findShape(id: Int): Shape? {
        children.forEach { shape ->
            val found = shape.findShape(id)
            if (found != null) return found
        }
        return null
    }

    fun findShapeAt(pagePoint: Dimension2, minDistance: Dimension): Shape? {
        children.forEach { shape ->
            if (shape.isAt(pagePoint, minDistance)) {
                return shape
            }
        }
        return null
    }

    override fun metaData(): MetaData {
        val metaData = MetaData(null)

        metaData.newCell("paperSize", pageSize.paperSize)
        metaData.newCell("documentSize", pageSize.documentSize)
        metaData.newCell("themeName", themeName)

        return metaData
    }
}
