/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.view

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.geometry.GeometryPart
import uk.co.nickthecoder.fizzy.model.geometry.MoveTo

abstract class View(val dc: DrawContext) {

    abstract fun draw()

    fun drawShape(shape: Shape) {
        dc.use {

            shape.transform.apply(dc)

            dc.lineWidth(shape.lineWidth.value)
            dc.lineColor(shape.strokeColor.value)
            dc.fillColor(shape.fillColor.value)

            if (shape is ShapeWithGeometry) {

                dc.strokeJoin(shape.strokeJoin.value)
                dc.strokeCap(shape.strokeCap.value)
                dc.cornerRadius(shape.cornerRadius.value)

                if (shape.processedDashes.value.isNotEmpty()) {
                    dc.lineDashes(* shape.processedDashes.value)
                } else {
                    dc.lineDashes()
                }

                dc.beginPath()
                shape.geometry.parts.filter { it.visible.value }.forEach { it.draw(dc) }
                dc.endPath(shape.strokeColor.value.isVisible(), shape.fillColor.value.isVisible())


                val startMarker = if (shape is Shape1d) shape.startLineMarkerShape.value else NO_SHAPE
                val midMarker = if (shape is Shape1d) shape.midLineMarkerShape.value else NO_SHAPE
                val endMarker = if (shape is Shape1d) shape.endLineMarkerShape.value else NO_SHAPE

                if (startMarker !== NO_SHAPE || midMarker !== NO_SHAPE || endMarker !== NO_SHAPE) {

                    val scale = shape.lineWidth.value.inDefaultUnits

                    dc.fillColor(shape.strokeColor.value) // The FILL color for the line ending is this shape's LINE color.

                    var pending: GeometryPart? = null
                    shape.geometry.parts.filter { it.visible.value }.forEach { part ->
                        if (part is MoveTo) {
                            pending?.let {
                                // A new section has started, so draw the end marker on the previous section
                                if (it !is MoveTo) {
                                    drawLineMarker(dc, it, endMarker, it.tangentAlong(1.0), scale)
                                }
                            }
                        } else {
                            pending?.let {
                                if (it is MoveTo) {
                                    drawLineMarker(dc, it, startMarker, Angle.PI + part.tangentAlong(0.0), scale)
                                } else {
                                    drawLineMarker(dc, it, midMarker, it.tangentAlong(1.0), scale)
                                }
                            }
                        }
                        pending = part
                    }
                    pending?.let {
                        if (it !is MoveTo) {
                            drawLineMarker(dc, it, endMarker, it.tangentAlong(1.0), scale)
                        }
                    }
                }

            }


            if (shape is ShapeText) {
                val x = shape.textSize.value.x * shape.alignX.value + shape.marginLeft.value
                val y = shape.textSize.value.y * shape.alignY.value + shape.marginTop.value
                dc.translate(Dimension2(x, y))
                if (shape.clip.value) {
                    dc.multiLineText(shape.multiLineText.value, stroke = shape.strokeColor.value.isVisible(), fill = shape.fillColor.value.isVisible(),
                            clipStart = -shape.transform.locPin.value, clipSize = shape.size.value)
                } else {
                    dc.multiLineText(shape.multiLineText.value, stroke = shape.strokeColor.value.isVisible(), fill = shape.fillColor.value.isVisible())
                }
            }

            shape.children.forEach { child ->
                drawShape(child)
            }

        }
    }

    /**
     * Lines can have arrows (or other symbols) at the start and ends of the lines, and also at the joints
     * between two lines. This draws a single symbols at the end of the [part].
     */
    private fun drawLineMarker(dc: DrawContext, part: GeometryPart, shape: Shape2d, angle: Angle, scale: Double) {
        if (shape === NO_SHAPE) return

        dc.use {
            dc.cornerRadius(shape.cornerRadius.value)
            dc.translate(part.point.value)
            dc.rotate(angle)
            dc.scale(scale)
            dc.translate(-shape.transform.locPin.value)
            dc.beginPath()
            shape.geometry.parts.filter { it.visible.value }.forEach { it.draw(dc) }
            dc.endPath(shape.strokeColor.value.isVisible(), shape.fillColor.value.isVisible())
        }
    }

}
