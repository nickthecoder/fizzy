/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.collection.ListListener
import uk.co.nickthecoder.fizzy.model.Named
import uk.co.nickthecoder.fizzy.model.Scratch
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.prop.Prop

/**
 * A method for getting to the value of a [Scratch]. To use this, the syntax is :
 *   this.findScratch( "NameOfScratch" )
 *
 * This is cumbersome, and there is now an easier syntax :
 *   this.Scratch.NameOfScratch
 *
 * This replacement uses FindScratchField and a special type for the scratch list, which can then
 * have its own ScratchListPropType.
 */
class FindListItemMethod<T : Named>(shape: Prop<Shape>, private val listName: String, private val listGetter: (Shape) -> FList<T>)
    : TypedMethod1<Shape, String, Any>("FindListItem", shape, String::class.java), ListListener<T> {

    override fun isConstant() = false

    override fun listChanged(list: FList<T>, item: T, index: Int, added: Boolean) {
        dirty = true
    }

    private var itemName: Prop<String>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(this)
                field = v
                v?.addListener(this)
            }
        }

    private var list: FList<T>? = null
        set(v) {
            if (field !== v) {
                field?.listeners?.remove(this)
                field = v
                v?.listeners?.add(this)
            }
        }

    override fun noLongerNeeded() {
        super.noLongerNeeded()
        list?.listeners?.remove(this)
        itemName?.removeListener(this)
    }

    override fun eval(a: String): Any {

        list = listGetter(prop.value)
        list?.forEach { item ->
            if (item.name.value == a) {
                itemName = item.name
                return item
            }
        }
        throw RuntimeException("Item $a not found in $listName")
    }

    override fun toString(): String {
        return "FindListItemMethod $listName"
    }
}
