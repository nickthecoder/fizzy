/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.util

import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * The handler for [runLater]. The default is to run the actions immediately in the current thread.
 * When using JavaFX, this should be set using :
 *
 *    runLaterHandler = { Platform.runLater(it) }
 *
 */
var runLaterHandler: ((() -> Unit) -> Unit) = { it() }

/**
 * This is the maximum depth acceptable for RunLater.
 * i.e. If "runLater" code creates another "runLater", which creates another... then at some point
 * this will be an issue, if it never ends. (the GUI will block, and the CPU will be under heavy load).
 */
var RUN_LATER_LIMIT = 10

private val runLaterQueue = ConcurrentLinkedQueue<() -> Unit>()

private var running = false

/**
 * Runs an action at a later time.
 * If no [runLaterHandler] has been set, then the action is ignored.
 */
fun runLater(reason: String, action: () -> Unit) {

    val wasEmpty = runLaterQueue.isEmpty()
    runLaterQueue.add(action)

    if (!running && wasEmpty) {
        runLaterHandler {
            running = true
            var counter = 0
            var expectedIterations = runLaterQueue.size

            try {

                while (runLaterQueue.isNotEmpty()) {
                    try {
                        runLaterQueue.poll()()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    expectedIterations--

                    if (expectedIterations < 0) {
                        expectedIterations = runLaterQueue.size
                        counter++
                        if (counter > RUN_LATER_LIMIT) {
                            throw RuntimeException("RunLater limit exceeded. ($counter). Initial reason : $reason")
                        }
                    }
                    //println("RL : expected $expectedIterations  Counter $counter")

                }

            } finally {
                running = false
            }
        }
    }
}


/**
 * Used for debugging,to hunt down code that intermittently takes a long time to run
 */
fun <T> timeValue(label: String, action: () -> T): T {
    val before = System.currentTimeMillis()
    val result = action()
    val duration = System.currentTimeMillis() - before
    val max = maxDurations[label]
    if (max == null || max < duration) {
        println("$label took $duration ms")
        maxDurations[label] = duration
    }
    return result
}

/**
 * Used for debugging,to hunt down code that intermittently takes a long time to run
 */
fun timeAction(label: String, action: () -> Unit): Boolean {

    val before = System.currentTimeMillis()
    action()
    val duration = System.currentTimeMillis() - before
    val max = maxDurations[label]
    if (max == null || max < duration) {
        println("$label took $duration ms")
        maxDurations[label] = duration
        return true
    }
    return false
}

/**
 * Used for debugging, to hunt down code that intermittently takes a long time to run.
 */
private val maxDurations = HashMap<String, Long>()
