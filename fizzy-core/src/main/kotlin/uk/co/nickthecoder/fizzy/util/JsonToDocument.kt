/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.util

import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import uk.co.nickthecoder.fizzy.collection.FList
import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.MutableProp
import uk.co.nickthecoder.fizzy.prop.PropExpression
import uk.co.nickthecoder.fizzy.prop.PropVariable

class JsonToDocument(private val jRoot: JsonObject) {

    var version: Double = 0.0

    private val pathToExp = mutableMapOf<String, PropExpression<*>>()

    /**
     * An evaluation context to use when parsing PropVariables, such as those in CustomProperties.
     */
    private var context: EvaluationContext = constantsContext

    fun toDocument(): Document {
        version = jRoot.getDouble("version", 0.0)

        return loadDocument(jRoot)
    }

    private fun cacheLinks(parent: ShapeParent) {
        parent.children.forEach { shape ->
            shape.metaData().buildLinkToExpression("${shape.id}.").forEach { path, exp ->
                pathToExp[path] = exp
            }
            cacheLinks(shape)
        }
    }

    private fun loadDocument(jRoot: JsonObject): Document {
        val jDocument = jRoot.get("document") as JsonObject

        val id = jDocument.getString("id", Document.generateDocumentId())
        val document = Document(id)

        loadMetaData(jDocument, document, null, document.metaData())

        loadLocalMasters(jDocument, document)
        cacheLinks(document.localMasterShapes)

        val jPages = jDocument.optionalArray("pages")
        jPages.forEach { it ->
            val jPage = it.asObject()
            val page = loadPage(jPage, document)
            cacheLinks(page)
        }

        if (document.pages.isEmpty()) {
            Page(document)
        }

        // Set Shape.loaded for all shapes.
        fun loaded(shape: Shape) {
            shape.loaded = true
            shape.children.forEach { loaded(it) }
        }
        document.pages.forEach { page -> page.children.forEach { loaded(it) } }
        document.localMasterShapes.children.forEach { loaded(it) }

        document.unchanged.value = true
        // Hmm, this isn't good. However, some changes delay calling their listeners to avoid duplicate updates,
        // but this means that document.unchanged will be set to false AFTER the previous line has run.
        runLater("Loaded document") { document.unchanged.value = true }
        return document
    }

    private fun loadLocalMasters(jDocument: JsonObject, document: Document) {
        val jLocalMasters = jDocument.optionalArray("localMasters")
        jLocalMasters.forEach {
            val jLocalMaster = it.asObject()
            val id = jLocalMaster.get("id").asString()
            val jShape = jLocalMaster.get("shape").asObject()
            val shape = loadShape(jShape, document.localMasterShapes)
            document.localMasterShapes.children.add(shape)
            document.masterToLocalCopy[id] = shape
        }

    }

    private fun loadPage(jPage: JsonObject, document: Document): Page {
        val page = Page(document)

        loadMetaData(jPage, page, null, page.metaData())

        val jShapes = jPage.optionalArray("shapes")
        jShapes.forEach {
            val jShape = it.asObject()
            val shape = loadShape(jShape, page)
            page.children.add(shape)
        }
        return page
    }

    private fun loadShape(jShape: JsonObject, parent: ShapeParent): Shape {
        val id = jShape.get("id").asInt()
        val type = jShape.get("type").asString()
        val linkedFromId = jShape.get("linkedFrom")

        val linkedFrom: Shape? = if (linkedFromId == null)
            null
        else
            parent.document().localMasterShapes.findShape(linkedFromId.asInt())

        val shape = when (type) {
            "Shape1d" -> Shape1d.create(parent, linkedFrom, id)
            "Shape2d" -> Shape2d.create(parent, linkedFrom, id)
            "ShapeText" -> ShapeText.create(parent, linkedFrom, id)
            else -> throw IllegalStateException("Unknown shape type $type")
        }
        shape.loaded = false

        val oldContext = context
        context = shape.context

        loadMetaData(jShape, shape, null, shape.metaData())

        jShape.optionalArray("children").forEach {
            val jChild = it.asObject()
            shape.children.add(loadShape(jChild, shape))
        }

        context = oldContext
        return shape
    }

    private fun loadMetaData(jParent: JsonObject, parent: MetaDataAware, parentSectionName: String?, metaData: MetaData) {

        var metaDataVar = metaData

        jParent.optionalArray("cells").forEach {
            val jCell = it.asObject()
            val name = jCell.get("name").asString()
            val cell = metaDataVar.findCell(name)
            if (cell == null) {
                error("Unexpected cell name $name.Ignoring")
            } else {
                loadCell(jCell, cell)
                // In Scratch, when the type changes, the expression changes (from one type of PropExpression to another)
                // e.g. from DoubleExpression to DimensionExpression.
                // In which case the metaData will refer to the OLD expression, so lets refresh the metaData.
                // CustomProperty's meta data also needs refreshing due to the possible addition of "IsMenu".
                if ((parentSectionName == "Scratch" || parentSectionName == "CustomProperty") && name == "Type") {
                    metaDataVar = parent.metaData()
                }
            }
        }

        jParent.optionalArray("rows").forEach {
            val jRow = it.asObject()
            val type: String? = jRow.getString("type", parentSectionName)
            val dataTypeString: String? = jRow.getString("dataType", null)
            val dataType = if (dataTypeString == null) null else CellType.valueOf(dataTypeString)


            val (row, rowMetaData) =
                    try {
                        parent.createRow(type, dataType)
                    } catch (e: Exception) {
                        error("Failed to create a Row named $type.")
                        return
                    }
            if (dataType != null) {
                @Suppress("UNCHECKED_CAST")
                rowMetaData.findCell("Type")?.cellProp?.let { if (it is MutableProp<*>) (it as MutableProp<CellType>).value = dataType }
            }
            loadMetaData(jRow, row, parentSectionName, rowMetaData)
        }

        jParent.optionalArray("sections").forEach {
            val jSection = it.asObject()
            val sectionName = jSection.get("Section").asString()
            val (sectionObject, sectionMetaData) = getSection(parent, sectionName)
            if (sectionObject is MetaDataAware) {
                loadMetaData(jSection, sectionObject, sectionName, sectionMetaData)
            } else if (sectionObject is FList<*>) {
                loadMetaData(jSection, parent, sectionName, sectionMetaData)
            }
        }
    }

    private fun getSection(parent: MetaDataAware, sectioName: String): Pair<Any?, MetaData> {
        return try {
            parent.getSection(sectioName)
        } catch (e: Exception) {
            error("Section '$sectioName' not found in object $parent. Ignoring.")
            Pair(null, MetaData("none"))
        }
    }

    private fun loadCell(jCell: JsonObject, cell: MetaDataCell) {
        val prop = cell.cellProp
        if (prop is PropExpression<*>) {

            val valueString = jCell.get("v")
            if (valueString?.isString == true) {
                prop.formula = valueString.asString()
            }

            val link = jCell.get("l")
            if (link == null) {
                val formula = jCell.get("f").asString()
                prop.formula = formula
            } else {
                val exp = pathToExp[link.asString()]
                if (exp == null) {
                    error("Failed to link to ${link.asString()}")
                } else {
                    prop.copyFrom(exp, true)
                }
            }
        } else if (prop is PropVariable<*>) {
            when (prop.value) {
                is CellType -> {
                    @Suppress("UNCHECKED_CAST")
                    (prop as PropVariable<CellType>).value = try {
                        CellType.valueOf(jCell.getString("ct", CellType.STRING.name))
                    } catch (e: Exception) {
                        CellType.STRING
                    }
                }
                is String -> {
                    val str = jCell.getString("s", null)
                    if (str == null) {
                        error("Expected a string for cell $cell")
                    } else {
                        @Suppress("UNCHECKED_CAST")
                        (prop as PropVariable<String>).value = str
                    }
                }
                else -> {
                    val formula = jCell.get("f")
                    if (formula.isString) {
                        val exp = createExpression(prop.value.javaClass)
                        exp.formula = formula.asString()
                        exp.context = context // Ensures that "pt" conversions are correct. Without this "0 pt" would not work.
                        val value = exp.value
                        if (value.javaClass === prop.value.javaClass) {
                            @Suppress("UNCHECKED_CAST")
                            (prop as PropVariable<Any>).value = exp.value
                        } else {
                            error("Wrong type for cell $cell. ${value.javaClass.name} vs ${prop.value.javaClass.name}")
                            println("Formula = '${formula.asString()}' Exp : ${exp.formula} value=$value (${value.javaClass.simpleName}")
                        }
                    } else {
                        error("Expected a formula for cell $cell")
                    }
                }
            }
        } else {
            error("Unexpected property for cell $cell: $prop")
        }
    }

    // TODO Later, do something sensible with the errors. Also, count the errors, and abort when too many.
    private fun error(text: String) {
        println("JSON Error : $text")
    }
}

fun JsonObject.optionalArray(name: String): JsonArray {
    val jChild = this.get(name)
    return if (jChild == null) {
        JsonArray()
    } else {
        jChild.asArray()
    }
}