/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.evaluator

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropConstant
import uk.co.nickthecoder.fizzy.prop.PropType

interface EvaluationContext {

    fun findProp(name: String): Prop<*>?

    val thisProp: Prop<*>?
}

class CompoundEvaluationContext(val children: List<EvaluationContext>) : EvaluationContext {

    override fun findProp(name: String): Prop<*>? {
        children.forEach {
            val result = it.findProp(name)
            if (result != null) {
                return result
            }
        }
        return null
    }

    override val thisProp: Prop<*>?
        get() {
            children.forEach { child ->
                if (child.thisProp != null) {
                    return child.thisProp
                }
            }
            return null
        }
}

class SimpleEvaluationContext(properties: Map<String, Prop<*>> = emptyMap())

    : EvaluationContext {

    val properties = mutableMapOf<String, Prop<*>>()

    override val thisProp: Prop<*>? = null

    init {
        this.properties.putAll(properties)
    }

    override fun findProp(name: String) = properties[name]


    fun putProp(name: String, prop: Prop<*>) {
        properties[name] = prop
    }

    override fun toString(): String {
        return "SimpleEvaluationContext : ${properties.entries.map { "{it.key}->${it.value}" }.joinToString("\n")}"
    }
}

/**
 * All fields from a given property [me] are exposed as top-level properties.
 *
 * This is used by Shape, so that its expressions can reference part of the Shape
 * without the need for "this.".
 */
class ThisContext<T : Any>(val me: T, val type: PropType<T>)
    : EvaluationContext {

    override val thisProp = PropConstant(me)

    override fun findProp(name: String): Prop<*>? {
        if (name == "this") return thisProp
        return type.findField(thisProp, name)
    }
}


private inline fun <reified T : Enum<T>> enumMapValues(prefix: String = ""): List<Pair<String, PropConstant<T>>> {
    return enumValues<T>().map {
        prefix + it.name to PropConstant(it)
    }
}

private inline fun <reified T : Enum<T>> enumMapNames(prefix: String = ""): List<Pair<String, PropConstant<String>>> {
    return enumValues<T>().map {
        prefix + it.name to PropConstant(it.name)
    }
}

private val constantsMap = mutableMapOf(
        "true" to PropConstant(true),
        "false" to PropConstant(false),

        "PI" to PropConstant(Math.PI),
        "TAU" to PropConstant(Math.PI * 2),
        "E" to PropConstant(Math.E),
        "MAX_DOUBLE" to PropConstant(Double.MAX_VALUE),
        "MIN_DOUBLE" to PropConstant(-Double.MAX_VALUE), // Note, this is NOT the same as the badly named Java Double.MIN_VALUE
        "SMALLEST_DOUBLE" to PropConstant(Double.MIN_VALUE),
        "NaN" to PropConstant(Double.NaN),

        "MIN_DIMENSION" to PropConstant(Dimension(-Double.MAX_VALUE, Units.defaultUnits)),
        "MAX_DIMENSION" to PropConstant(Dimension(Double.MAX_VALUE, Units.defaultUnits)),

        "MIN_DIMENSION2" to PropConstant(Dimension2(Dimension(-Double.MAX_VALUE, Units.defaultUnits), Dimension(-Double.MAX_VALUE, Units.defaultUnits))),
        "MAX_DIMENSION2" to PropConstant(Dimension2(Dimension(Double.MAX_VALUE, Units.defaultUnits), Dimension(Double.MAX_VALUE, Units.defaultUnits))),

        "MIN_VECTOR2" to PropConstant(Vector2(-Double.MAX_VALUE, -Double.MAX_VALUE)),
        "MAX_VECTOR2" to PropConstant(Vector2(Double.MAX_VALUE, Double.MAX_VALUE)),

        "BLACK" to PropConstant(Color.BLACK),
        "WHITE" to PropConstant(Color.WHITE),
        "TRANSPARENT" to PropConstant(Color.TRANSPARENT)

).apply { putAll(enumMapValues<StrokeJoin>("STROKE_JOIN_")) }
        .apply { putAll(enumMapValues<StrokeCap>("STROKE_CAP_")) }
        .apply { putAll(enumMapNames<SmartConnectionType>("SMART_CONNECTION_")) }

val constantsContext = SimpleEvaluationContext(constantsMap)
