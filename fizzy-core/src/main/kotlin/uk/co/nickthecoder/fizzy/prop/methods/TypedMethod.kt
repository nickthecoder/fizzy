/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.evaluator.ArgList
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropMethod
import uk.co.nickthecoder.fizzy.prop.assertIsInstance

abstract class TypedMethod1<T : Any, A : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val aClass: Class<A>
) : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {

        aClass.assertIsInstance(arg.value, description)

        this.arg = arg
        @Suppress("UNCHECKED_CAST")
        return eval(arg.value as A)
    }

    protected abstract fun eval(a: A): R
}

abstract class TypedMethod2<T : Any, A : Any, B : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val aClass: Class<A>,
        private val bClass: Class<B>
) : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {
        if (arg is ArgList && arg.value.size == 2) {
            this.arg = arg

            val a = arg.value[0].value
            val b = arg.value[1].value

            aClass.assertIsInstance(a, description, "Argument #1")
            bClass.assertIsInstance(b, description, "Argument #2")

            @Suppress("UNCHECKED_CAST")
            return eval(a as A, b as B)
        }
        throw RuntimeException("Expected 2 arguments")
    }

    protected abstract fun eval(a: A, b: B): R
}

abstract class TypedMethod3<T : Any, A : Any, B : Any, C : Any, R : Any>(
        description: String,
        prop: Prop<T>,
        private val aClass: Class<A>,
        private val bClass: Class<B>,
        private val cClass: Class<C>
) : PropMethod<T, R>(description, prop) {

    override fun eval(arg: Prop<*>): R {
        if (arg is ArgList && arg.value.size == 3) {
            this.arg = arg

            val a = arg.value[0].value
            val b = arg.value[1].value
            val c = arg.value[2].value

            aClass.assertIsInstance(a, description, "Argument #1")
            bClass.assertIsInstance(b, description, "Argument #2")
            cClass.assertIsInstance(c, description, "Argument #3")

            @Suppress("UNCHECKED_CAST")
            return eval(a as A, b as B, c as C)
        }
        throw RuntimeException("Expected 3 arguments")
    }

    protected abstract fun eval(a: A, b: B, c: C): R
}
