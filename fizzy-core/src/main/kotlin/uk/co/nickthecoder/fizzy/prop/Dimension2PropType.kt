/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.Angle
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Units

class Dimension2PropType private constructor()
    : PropType<Dimension2>(Dimension2::class.java) {

    override fun findField(prop: Prop<Dimension2>, name: String): Prop<*>? {

        return when (name) {
            "X" -> ConstantPropField("Dimension2.X", prop) { it.value.x }
            "Y" -> ConstantPropField("Dimension2.Y", prop) { it.value.y }
            "Length" -> ConstantPropField("Dimension2.Length", prop) { it.value.length() }
            "Angle" -> ConstantPropField("Dimension2.Angle", prop) { it.value.angle() }
            else -> return super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<Dimension2>, name: String): PropMethod<Dimension2, *>? {
        return when (name) {
            "normalise" -> PropMethod0("Dimension2.normalise", prop) { prop.value.normalise() }
            "rotate" -> PropMethod1("Dimension2.rotate", prop, Angle::class.java) { prop.value.rotate(it) }
            "toString" -> PropMethod0("Dimension2.toString", prop) { prop.value.toFormula() }
            "format" -> PropMethod1("Dimension2.format", prop, String::class.java) { format -> prop.value.format(format) }
            "toUnits" -> PropMethod1("Dimension2.toUnits", prop, Units::class.java) { units -> Dimension2(Dimension(prop.value.x.inUnits(units), units), Dimension(prop.value.y.inUnits(units), units)) }
            "clamp" -> PropMethod2("Dimension2.clamp", prop, Dimension2::class.java, Dimension2::class.java) { min, max -> prop.value.clamp(min, max) }
            "min" -> PropMethod1("Dimension2.min", prop, Dimension2::class.java) { other -> prop.value.min(other) }
            "max" -> PropMethod1("Dimension2.max", prop, Dimension2::class.java) { other -> prop.value.max(other) }
            else -> null
        }
    }

    companion object {
        val instance = Dimension2PropType()
    }
}

class Dimension2Expression
    : PropExpression<Dimension2> {

    constructor(expression: String, context: EvaluationContext = constantsContext) : super(expression, Dimension2::class.java, context)

    constructor(pointValue: Dimension2 = Dimension2.ZERO_mm, context: EvaluationContext = constantsContext) : this(pointValue.toFormula(), context)

    constructor(other: Dimension2Expression) : super(other)

    override val defaultValue = Dimension2.ZERO_mm

    override fun toFormula(value: Dimension2) = value.toFormula()

    override fun copy(link: Boolean) = if (link) Dimension2Expression(this) else Dimension2Expression(formula)

    override fun valueString() = value.toFormula()
}
