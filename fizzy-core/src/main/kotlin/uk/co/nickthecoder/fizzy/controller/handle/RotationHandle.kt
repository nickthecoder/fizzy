/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.handle

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.history.ChangeProp
import uk.co.nickthecoder.fizzy.view.DrawContext

class RotationHandle(shape: Shape, private val offset: () -> Dimension)
    : ShapeHandle(shape) {

    override fun position() = shape.localToPage(Dimension2(shape.size.value.x / 2.0, -offset()))

    override fun draw(dc: DrawContext, dragging: Boolean) {
        Controller.drawHandle(dc, Controller.CIRCLE)
    }

    override fun dragTo(event: CMouseEvent, dragPoint: Dimension2) {
        val local: Dimension2 = shape.pageToLocal(dragPoint)
        val transform = shape.transform
        if (transform is Shape2dTransform) {
            var degrees = transform.rotation.value.degrees + (local - transform.locPin.value).angle().degrees - 90.0
            if (!event.isConstrain) {
                degrees -= (degrees + 360.0).rem(15.0)
            }
            shape.document().history.makeChange(ChangeProp(
                    transform.rotation,
                    (Angle.degrees(degrees))))
        }
    }
}
