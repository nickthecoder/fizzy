/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

/**
 * A factory method to create [FFontDetails].
 * The default factory creates [MockFFontDetails], however, in a JavaFX environment,
 * change this using :
 *
 *     fontDetailsFactory = { family,style -> JavaFXFFonDetails(family,style) }
 */
var fontDetailsFactory: (String, String) -> FFontDetails = { family, style -> MockFFontDetails(family, style) }

interface FFontDetails {

    val fontFamily: String

    val fullName: String

    val style: String

    fun svgStyle(): String {
        val v = style.toLowerCase()
        return when {
            v.contains("italic") -> "italic"
            v.contains("oblique") -> "oblique"
            else -> "normal"
        }
    }

    fun weight(): Int {
        val v = style.toLowerCase()
        return when {
            v.contains("thin") -> 100
            v.contains("extra light") -> 200
            v.contains("ultra light") -> 200
            v.contains("light") -> 300
            v.contains("medium") -> 500
            v.contains("semi bold") -> 600
            v.contains("demi bold") -> 600
            v.contains("extra bold") -> 800
            v.contains("ultra bold") -> 800
            v.contains("black") -> 900
            v.contains("bold") -> 700
            else -> 400
        }
    }

    fun createFont(fontSize: Double): FFont

    companion object {

        fun find(family: String, variant: String) = fontDetailsFactory(family, variant)
    }
}

interface FFont {

    val fontDetails: FFontDetails

    val lineHeight: Dimension

    val fontSize: Double

    fun textSize(str: String): Dimension2
}

abstract class AbstractFFontDetails(
        override val fontFamily: String,
        override val style: String
) : FFontDetails {

    override val fullName
        get() = "$fontFamily $style"

}

class MockFFontDetails(override val fontFamily: String, override val style: String) : FFontDetails {

    override val fullName: String = "$fontFamily $style"

    override fun createFont(fontSize: Double) = MockFFont(this, fontSize)
}

class MockFFont(override val fontDetails: FFontDetails, override val fontSize: Double) : FFont {

    override val lineHeight: Dimension = FONT_HEIGHT * fontSize

    override fun textSize(str: String): Dimension2 {
        val lines = str.split("\n")
        val maxWidth = lines.maxBy { it.length }?.length ?: 0
        return Dimension2(FONT_WIDTH * (fontSize * maxWidth), FONT_HEIGHT * (fontSize * lines.size))
    }

    companion object {
        val FONT_WIDTH = Dimension(10.0)
        val FONT_HEIGHT = Dimension(10.0)
    }
}
