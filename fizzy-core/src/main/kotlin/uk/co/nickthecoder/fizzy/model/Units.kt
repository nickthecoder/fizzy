/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation2
import uk.co.nickthecoder.fizzy.prop.dimensionConversion

abstract class Units(val name: String) {

    abstract val scale: Double

    abstract fun conversion(a: Prop<*>): Prop<*>

    companion object {
        val mm = FixedUnits("mm", 1.0)
        val cm = FixedUnits("cm", 10.0)
        val m = FixedUnits("m", 1000.0)
        val km = FixedUnits("km", 1000000.0)
        val inch = FixedUnits("inch", 25.4) // Note, cannot use "in" because that is a Kotlin keyword
        val ft = FixedUnits("ft", 25.4 * 12)
        val yard = FixedUnits("yard", 25.4 * 36)

        val defaultUnits = mm
    }
}

/**
 * Units of length measurements.
 *
 * The scale is relative to the default units.
 * The default units are currently mm. However, this may change, so do NOT assume that mm are the default,
 * and therefore do NOT use scale without comparing it to another scale.
 *
 * These are real-world units, so if you are drawing a plan of a room, then units such as mm will refer to
 * the sizes within the room and NOT the size that items appear on the page.
 */
open class FixedUnits(name: String, override val scale: Double)
    : Units(name) {

    override fun conversion(a: Prop<*>) = dimensionConversion(a, this)
}

/**
 * Dynamic units change, depending on [proportionsProp].
 * Currently [proportionsProp] is a calculated value based on the paper size and the document's real world
 * size. However, at a later date, this many become user editable (so that they can choose the scaling factor
 * for the dynamic units).
 *
 * Dynamic units, such as "pt" (point), are designed to measure sizes as they appear on the page.
 * For example, the font size of labels added to the document don't care about real world units,
 * and are therefore specified in "pt" (points), which are dynamically scaled.
 * It is also common to use points for line widths, as the widths of lines often do not refer to real-world units.
 */
class DynamicUnits(name: String, private val proportionsProp: Prop<Double>, private val fixedScale: Double)
    : Units(name) {

    override fun conversion(a: Prop<*>): Prop<*> {
        return PropCalculation2("DynamicUnits.conversion", true, a, proportionsProp) { _, _ ->
            dimensionConversion(a, this).value
        }
    }

    override val scale: Double
        get() = fixedScale * proportionsProp.value

}

interface FindsUnits {
    fun findUnits(name: String): Units?
}
