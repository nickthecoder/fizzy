/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.prop.*
import uk.co.nickthecoder.fizzy.util.ChangeListeners
import uk.co.nickthecoder.fizzy.util.HasChangeListeners

/**
 * An empty class definition, subclassing MutableFList<Scratch>.
 * See [ScratchListPropType] for details on why this exists.
 */
class ScratchList : MutableFList<Scratch>()

class Scratch(name: String, override var data: PropExpression<*>, cellType: CellType, comment: String = "")
    : HasChangeListeners<Scratch>, PropListener, MetaDataAware, NamedWithData {

    constructor(cellType: CellType) : this("", cellType.expression(), cellType, "")


    override val name = PropVariable(name)

    override val changeListeners = ChangeListeners<Scratch>()

    /**
     * Used only as documentation of the Master Shape, and is NOT available for use in formulas.
     */
    var comment = PropVariable(comment)

    val type = PropConstant(cellType)

    override fun metaData(): MetaData {
        val md = MetaData(null)
        addMetaData(md)
        return md
    }

    fun addMetaData(metaData: MetaData) {
        metaData.newCell("Name", name)
        metaData.newCell("Type", type)
        metaData.newCell("Data", data).addAliases("Expression")
        metaData.newCell("Comment", comment)
    }

    fun setContext(context: EvaluationContext) {
        data.context = context
    }

    override fun dirty(prop: Prop<*>) {
        changeListeners.fireChanged(this)
    }

    fun copy(link: Boolean) = Scratch(name.value, data.copy(link), type.value, comment.value)
}
