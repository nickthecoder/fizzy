/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.util.terse
import uk.co.nickthecoder.fizzy.util.toFormula
import java.text.DecimalFormat

/**
 * Holds an Area as a number with a given unit of measures.
 * For example, 1.5 square meters could be stored as 1.5m² or 15000cm².
 */
class Area : Comparable<Area>, FizzyType {

    /**
     * The size stored in default units. Currently the default units are mm, but this may change, and therefore
     * applications should tend NOT to use this property directly.
     */
    val inDefaultUnits: Double

    val units: Units

    constructor(number: Double, units: Units) {
        inDefaultUnits = number * units.scale * units.scale
        this.units = units
    }

    /**
     * This constructor is useful (more efficient) when performing maths on Dimensions, but would be confusing to
     * expose, and is therefore private.
     */
    private constructor(units: Units, inDefaultUnits: Double) {
        this.inDefaultUnits = inDefaultUnits
        this.units = units
    }

    /**
     * Creates a dimension using default units.
     */
    internal constructor(inDefaultUnits: Double) {
        this.inDefaultUnits = inDefaultUnits
        this.units = Units.defaultUnits
    }

    val mm get() = inDefaultUnits
    val cm get() = inUnits(Units.cm)
    val m get() = inUnits(Units.m)
    val km get() = inUnits(Units.km)
    val inch get() = inUnits(Units.inch)
    val ft get() = inUnits(Units.ft)
    val yard get() = inUnits(Units.yard)

    fun inUnits(units: Units): Double = inDefaultUnits / units.scale / units.scale

    operator fun unaryMinus(): Dimension = Dimension(-inUnits(units), units)

    operator fun plus(b: Area): Area {
        return Area(units, inDefaultUnits + b.inDefaultUnits)
    }

    operator fun minus(b: Area): Area {
        return Area(units, inDefaultUnits - b.inDefaultUnits)
    }

    operator fun times(b: Double): Area {
        return Area(units, inDefaultUnits * b)
    }

    operator fun div(b: Double): Area {
        return Area(units, inDefaultUnits / b)
    }

    operator fun div(b: Area): Double {
        return inDefaultUnits / b.inDefaultUnits
    }

    operator fun div(b: Dimension): Dimension {
        return Dimension(inDefaultUnits / b.inDefaultUnits, units)
    }

    fun sqrt(): Dimension {
        return Dimension(Math.sqrt(inUnits(units)), units)
    }

    /**
     * Return in a format suitable for a formula.
     */
    override fun toFormula() = "(${inUnits(units).toFormula()} ${units.name} * 1 ${units.name})"


    override fun hashCode(): Int = 17 * inDefaultUnits.hashCode()

    override fun equals(other: Any?): Boolean {
        if (other is Area) {
            return inDefaultUnits == other.inDefaultUnits
        }
        return false
    }

    override fun compareTo(other: Area): Int {
        return inDefaultUnits.compareTo(other.inDefaultUnits)
    }

    fun min(other: Area) = if (other < this) other else this

    fun max(other: Area) = if (other > this) other else this

    override fun toString() = "${inUnits(units).terse()} ${units.name}"

    fun format(format: String) = "${DecimalFormat(format).format(inUnits(units))} ${units.name}²"

    companion object {
        val ZERO_square_mm = Area(0.0, Units.mm)
    }
}
