/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.handle

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape
import uk.co.nickthecoder.fizzy.model.ShapeWithGeometry
import uk.co.nickthecoder.fizzy.model.geometry.GeometryPart
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression

abstract class GeometryHandle : Handle()

abstract class AbstractGeometryHandle(val shape: Shape, val geometryPart: GeometryPart, val point: Dimension2Expression, val controller: Controller)
    : GeometryHandle() {

    override fun position() = shape.localToPage(point.value)

    override fun isFor(shape: Shape) = shape === this.shape

    override fun dragTo(event: CMouseEvent, dragPoint: Dimension2) {

        val localPoint = shape.pageToLocal(dragPoint)

        shape.document().history.makeChange(
                ChangeExpressions(listOf(point to localPoint.toFormula()))
        )
        controller.dirty.value++
    }

    override fun fillColor() = Controller.GEOMETRY_HANDLE_FILL

    override fun strokeColor() = Controller.GEOMETRY_HANDLE_STROKE

    override fun toString() = "GeoHandle @ ${position()}"

}

class StandardGeometryHandle(val shapeWithGeometry: ShapeWithGeometry, geometryPart: GeometryPart, point: Dimension2Expression, controller: Controller)
    : AbstractGeometryHandle(shapeWithGeometry, geometryPart, point, controller)
