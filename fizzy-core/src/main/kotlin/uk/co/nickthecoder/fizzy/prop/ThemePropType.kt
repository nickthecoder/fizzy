/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.model.Theme

class ThemePropType private constructor()
    : PropType<Theme>(Theme::class.java) {

    override fun findMethod(prop: Prop<Theme>, name: String): PropMethod<in Theme, *>? {
        return when (name) {
            "lineWidth" -> PropMethod1("Theme.lineWidth", prop, String::class.java) { vClass -> prop.value.lineWidth(vClass) }
            "strokeColor" -> PropMethod1("Theme.strokeColor", prop, String::class.java) { vClass -> prop.value.strokeColor(vClass) }
            "fillColor" -> PropMethod1("Theme.fillColor", prop, String::class.java) { vClass -> prop.value.fillColor(vClass) }
            "strokeCap" -> PropMethod1("Theme.strokeCap", prop, String::class.java) { vClass -> prop.value.strokeCap(vClass) }
            "strokeJoin" -> PropMethod1("Theme.strokeJoin", prop, String::class.java) { vClass -> prop.value.strokeJoin(vClass) }
            "dashes" -> PropMethod1("Theme.dashes", prop, String::class.java) { vClass -> prop.value.dashes(vClass) }
            "cornerRadius" -> PropMethod1("Theme.cornerRadius", prop, String::class.java) { vClass -> prop.value.cornerRadius(vClass) }
            "startMargin" -> PropMethod1("Theme.startMargin", prop, String::class.java) { vClass -> prop.value.startMargin(vClass) }
            "endMargin" -> PropMethod1("Theme.endMargin", prop, String::class.java) { vClass -> prop.value.endMargin(vClass) }
            "lineMarking" -> PropMethod2("Theme.lineMarking", prop, String::class.java, String::class.java) { vType, vClass -> prop.value.lineMarking(vType, vClass) }

            else -> super.findMethod(prop, name)
        }
    }

    companion object {
        val instance = ThemePropType()
    }
}
