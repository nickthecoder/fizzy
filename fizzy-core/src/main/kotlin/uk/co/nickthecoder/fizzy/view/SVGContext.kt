/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.view

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.util.SimpleXML

/**
 * Used to export a diagram to SVG.
 *
 * The SVG 1.1 Spec : https://www.w3.org/TR/SVG11/
 */
class SVGContext : DrawContext {

    private val stateStack = mutableListOf<SVGState>()

    private var transformGroupCount = 0


    private var state = SVGState()

    private var path: StringBuffer? = null

    private val xml = SimpleXML()

    /**
     * Used by [roundedCorner] to calculate the details of the circle.
     * Set by each path method ([moveTo], [lineTo], [bezierCurveTo], [roundedCorner])
     */
    private var prevPoint = Dimension2.ZERO_mm

    // Styles

    override fun lineColor(paint: Paint) {
        state.strokeColor(paint)
    }

    override fun fillColor(paint: Paint) {
        state.fillColor(paint)
    }

    override fun lineWidth(width: Double) {
        state.strokeWidth(width)
    }

    override fun lineWidth(width: Dimension) {
        state.strokeWidth(width.inDefaultUnits)
    }

    override fun lineDashes(vararg dashes: Double) {
        state.lineDashes(*dashes)
    }

    override fun cornerRadius(radius: Dimension) {
    }

    override fun strokeCap(strokeCap: StrokeCap) {
        state.strokeCap(strokeCap)
    }

    override fun strokeJoin(strokeJoin: StrokeJoin) {
        state.strokeJoin(strokeJoin)
    }


    // Transformations
    override fun rotate(by: Angle) {
        state.rotate(by)
    }

    override fun scale(by: Vector2) {
        state.scale(by)
    }

    override fun scale(by: Double) {
        state.scale(by)
    }

    override fun translate(by: Dimension2) {
        state.translate(by.x.inDefaultUnits, by.y.inDefaultUnits)
    }

    override fun translate(x: Dimension, y: Dimension) {
        state.translate(x.inDefaultUnits, y.inDefaultUnits)
    }

    // Paths

    override fun beginPath() {
        state.useTransformation()
        if (path != null) {
            throw RuntimeException("Already within a path")
        }
        xml.beginTag("path")
        xml.attribute("style", state.getStyle())
        path = StringBuffer()
    }

    override fun endPath(stroke: Boolean, fill: Boolean) {
        val p = path ?: throw RuntimeException("Not within a path")
        xml.attribute("d", p.toString())
        path = null
        xml.endTag()
    }

    private fun appendPath(item: String) {
        val p = path ?: throw RuntimeException("Attempted to draw outside of beginPath() .. endPath()")
        if (!p.isEmpty()) p.append(" ")
        p.append(item)
    }

    override fun moveTo(point: Dimension2) {
        appendPath("M ${point.x.inDefaultUnits},${point.y.inDefaultUnits}")
        prevPoint = point
    }

    override fun closePath() {
        appendPath("Z")
    }

    override fun lineTo(point: Dimension2) {
        appendPath("L ${point.x.inDefaultUnits},${point.y.inDefaultUnits}")
        prevPoint = point
    }

    override fun bezierCurveTo(c1: Dimension2, c2: Dimension2, end: Dimension2) {
        appendPath("C ${c1.x.inDefaultUnits},${c1.y.inDefaultUnits} ${c2.x.inDefaultUnits},${c2.y.inDefaultUnits} ${end.x.inDefaultUnits},${end.y.inDefaultUnits}")
        prevPoint = end
    }

    override fun roundedCorner(mid: Dimension2, point: Dimension2, radius: Dimension) {
        
        val a = (mid - prevPoint).normalise()
        val b = (point - mid).normalise()
        val theta = (a.angle() - b.angle()) / 2.0

        val midV = Vector2(mid.x.inDefaultUnits, mid.y.inDefaultUnits)
        val opp = radius.inDefaultUnits * Math.tan(theta.radians)
        // The points where the line touches the circle
        val t1 = midV - a * opp
        val t2 = midV + b * opp

        appendPath("L ${t1.x},${t1.y} ")
        appendPath("A ${radius.inDefaultUnits} ${radius.inDefaultUnits} 0 0 1 ${t2.x},${t2.y}")

        prevPoint = Dimension2(Dimension(t2.x), Dimension(t2.y))
    }

    // Text


    override fun multiLineText(multiLineText: MultiLineText, stroke: Boolean, fill: Boolean, clipStart: Dimension2, clipSize: Dimension2) {
        // TODO Clip???
        multiLineText(multiLineText, stroke, fill)
    }

    override fun multiLineText(multiLineText: MultiLineText, stroke: Boolean, fill: Boolean) {
        state.useTransformation()

        xml.beginTag("text", true)
        xml.attribute("style", state.getTextStyle(multiLineText.font, stroke, fill))
        multiLineText.lines.forEach { line ->
            xml.beginTag("tspan", true)
            xml.attribute("x", line.dx.inDefaultUnits.toString())
            xml.attribute("y", line.dy.inDefaultUnits.toString())
            xml.attribute("dy", "1em")
            xml.text(line.text)
            xml.endTag("tspan", true)
        }
        xml.endTag("text")
    }


    // Misc

    override fun clear() {
        // Does nothing.
    }

    override fun save() {
        stateStack.add(state.copy())
    }

    override fun restore() {
        state = stateStack.removeAt(stateStack.size - 1)

        while (transformGroupCount > state.currentTransformGroupCount) {
            transformGroupCount--
            xml.endTag("g")
        }
    }

    fun begin() {
        xml.beginTag("svg")
        xml.attribute("width", "210mm")
        xml.attribute("height", "297mm")
        xml.attribute("viewBox", "0 0 210 297")
        xml.attribute("version", "1.1")
    }

    fun end(): String {
        while (transformGroupCount > 0) {
            transformGroupCount--
            xml.endTag("g")
        }
        xml.endTag("svg")

        return xml.toString()
    }


    // State


    inner class SVGState {

        private var transformations = StringBuffer()

        private var pendingTranslation = Vector2.ZERO

        var currentTransformGroupCount = transformGroupCount

        private var strokeCap = StrokeCap.ROUND

        private var strokeJoin = StrokeJoin.ROUND

        private var strokeColor: Paint = Color.TRANSPARENT

        private var fillColor: Paint = Color.TRANSPARENT

        private var strokeWidth = 0.0

        private val dashes = mutableListOf<Double>()

        fun copy(): SVGState {

            val copy = SVGState()

            copy.currentTransformGroupCount = transformGroupCount
            //copy.transformations = transformations
            copy.strokeCap = strokeCap
            copy.strokeJoin = strokeJoin
            copy.strokeColor = strokeColor
            copy.fillColor = fillColor
            copy.strokeWidth = strokeWidth
            copy.dashes.addAll(dashes)

            return copy
        }

        // Transformations

        fun rotate(by: Angle) {
            addTransformation("rotate(${-by.degrees})")
        }

        fun scale(by: Vector2) {
            if (by.x != 1.0 || by.y != 1.0) {
                addTransformation("scale(${by.x} ${by.y})")
            }
        }

        fun scale(by: Double) {
            if (by != 1.0) {
                addTransformation("scale($by)")
            }
        }

        fun translate(x: Double, y: Double) {
            if (x != 1.0 || y != 1.0) {
                pendingTranslation += Vector2(x, y)
            }
        }

        private fun addTransformation(item: String) {
            if (transformations.isNotBlank()) transformations.append(" ")
            if (pendingTranslation != Vector2.ZERO) {
                transformations.append("translate(${pendingTranslation.x} ${pendingTranslation.y}) ")
                pendingTranslation = Vector2.ZERO
            }
            transformations.append(item)
        }

        fun useTransformation() {

            if (pendingTranslation != Vector2.ZERO) {
                addTransformation("")
            }
            if (transformations.isNotEmpty()) {
                xml.beginTag("g")
                xml.attribute("transform", transformations.toString())
                transformations = StringBuffer()

                transformGroupCount++
                currentTransformGroupCount = transformGroupCount
            }
        }


        // Styles

        fun strokeColor(paint: Paint) {
            this.strokeColor = paint
        }

        fun fillColor(paint: Paint) {
            this.fillColor = paint
        }

        fun strokeWidth(width: Double) {
            this.strokeWidth = width
        }

        fun lineDashes(vararg dashes: Double) {
            this.dashes.clear()
            dashes.forEach { this.dashes.add(it) }
        }

        fun strokeCap(strokeCap: StrokeCap) {
            this.strokeCap = strokeCap
        }

        fun strokeJoin(strokeJoin: StrokeJoin) {
            this.strokeJoin = strokeJoin
        }

        fun getStyle(): String {
            val buffer = StringBuffer()
            buffer.append("fill-rule:evenodd;")
            buffer.append("stroke-width:${strokeWidth};")
            buffer.append("stroke-linecap:${strokeCap.name.toLowerCase()};")
            buffer.append("stroke-linejoin:${strokeJoin.name.toLowerCase()};")
            val sc = strokeColor
            if (sc is Color) {
                buffer.append("stroke:${sc.toHashRGB()};")
                buffer.append("stroke-opacity:${sc.opacity};")
            }
            if (sc.isVisible() && dashes.isNotEmpty()) {
                buffer.append("stroke-dasharray: ${dashes.joinToString(separator = ",") { it.toString() }};")
            }
            val fc = fillColor
            if (fc is Color) {
                buffer.append("fill:${fc.toHashRGB()};")
                buffer.append("fill-opacity:${fc.opacity};")
            }

            return buffer.toString()
        }

        fun getTextStyle(font: FFont, stroke: Boolean, fill: Boolean): String {
            val fontDetails = font.fontDetails
            val buffer = StringBuffer()

            buffer.append("font-family:${fontDetails.fontFamily};")
            buffer.append("font-style:${fontDetails.svgStyle()};")
            buffer.append("font-weight:${fontDetails.weight()};")
            buffer.append("font-size:${font.fontSize};")

            if (stroke) {
                buffer.append("stroke-width:${strokeWidth};")
                buffer.append("stroke-linecap:${strokeCap.name.toLowerCase()};")
                buffer.append("stroke-linejoin:${strokeJoin.name.toLowerCase()};")
                val sc = strokeColor
                if (sc is Color) {
                    buffer.append("stroke:${sc.toHashRGB()};")
                    buffer.append("stroke-opacity:${sc.opacity};")
                }
            } else {
                buffer.append("stroke:none;")
            }
            if (fill) {
                val fc = fillColor
                if (fc is Color) {
                    buffer.append("fill:${fc.toHashRGB()};")

                }
            } else {
                buffer.append("fill:none;")
            }

            return buffer.toString()

        }
    }
}
