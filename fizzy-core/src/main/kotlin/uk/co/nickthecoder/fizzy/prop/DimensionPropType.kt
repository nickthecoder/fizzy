/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop

import uk.co.nickthecoder.fizzy.evaluator.EvaluationContext
import uk.co.nickthecoder.fizzy.evaluator.constantsContext
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Units

class DimensionPropType private constructor()
    : PropType<Dimension>(Dimension::class.java) {

    override fun findField(prop: Prop<Dimension>, name: String): Prop<*>? {
        return when (name) {
            "mm" -> ConstantPropField("Dimension.mm", prop) { it.value.mm }
            "cm" -> ConstantPropField("Dimension.cm", prop) { it.value.cm }
            "m" -> ConstantPropField("Dimension.m", prop) { it.value.m }
            "km" -> ConstantPropField("Dimension.km", prop) { it.value.km }
            else -> return super.findField(prop, name)
        }
    }

    override fun findMethod(prop: Prop<Dimension>, name: String): PropMethod<in Dimension, *>? {
        return when (name) {
            "toString" -> PropMethod0("Dimension.toString", prop) { prop.value.toFormula() }
            "format" -> PropMethod1("Dimension.format", prop, String::class.java) { format -> prop.value.format(format) }
            "units" -> PropMethod0("Dimension.units", prop) { prop.value.units }
            "inUnits" -> PropMethod1("Dimension.inUnits", prop, Units::class.java) { units -> prop.value.inUnits(units) }
            "toUnits" -> PropMethod1("Dimension.toUnits", prop, Units::class.java) { units -> Dimension(prop.value.inUnits(units), units) }
            "clamp" -> PropMethod2("Dimension.clamp", prop, Dimension::class.java, Dimension::class.java) { min, max -> prop.value.clamp(min, max) }
            "min" -> PropMethod1("Dimension.min", prop, Dimension::class.java) { other -> prop.value.min(other) }
            "max" -> PropMethod1("Dimension.max", prop, Dimension::class.java) { other -> prop.value.max(other) }

            else -> super.findMethod(prop, name)
        }
    }

    companion object {
        val instance = DimensionPropType()

        init {
            PropType.put(instance)
        }

        fun create(a: Prop<Double>, units: Units): Prop<Dimension> {
            return if (a.isConstant()) {
                PropConstant(Dimension(a.value, units))
            } else {
                PropCalculation1("Dimension.create", false, a) { av -> Dimension(av, units) }
            }
        }
    }
}

class DimensionExpression
    : PropExpression<Dimension> {

    constructor(expression: String, context: EvaluationContext = constantsContext) : super(expression, Dimension::class.java, context)

    constructor(value: Dimension = Dimension.ZERO_mm, context: EvaluationContext = constantsContext) : this(value.toFormula(), context)

    constructor(other: DimensionExpression) : super(other)

    override val defaultValue = Dimension.ZERO_mm

    override fun toFormula(value: Dimension) = value.toFormula()

    override fun copy(link: Boolean) = if (link) DimensionExpression(this) else DimensionExpression(formula)

    override fun valueString() = value.toFormula()
}

fun dimensionConversion(a: Prop<*>, units: Units): Prop<*> {
    val v = a.value

    return when (v) {
        is Double -> {
            @Suppress("UNCHECKED_CAST")
            //return PropCalculation1(a as Prop<Double>) { d -> Dimension(d, units) }
            return DimensionPropType.create(a as Prop<Double>, units)
        }
        is Dimension -> {
            @Suppress("UNCHECKED_CAST")
            return PropCalculation1("Dimension conversion", false, a as Prop<Dimension>) { dim ->
                Dimension(dim.inUnits(units), units)
            }
        }
        is Dimension2 -> {
            @Suppress("UNCHECKED_CAST")
            return PropCalculation1("Dimension2 conversion", false, a as Prop<Dimension2>) { dim2 ->
                Dimension2(Dimension(dim2.x.inUnits(units), units), Dimension(dim2.y.inUnits(units), units))
            }
        }
        else ->
            throwExpectedType("Double or Dimension", a)
    }
}

fun mmConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.mm)
fun cmConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.cm)
fun mConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.m)
fun kmConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.km)
fun inConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.inch)
fun ftConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.ft)
fun yardConversion(a: Prop<*>): Prop<*> = dimensionConversion(a, Units.yard)
