/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.evaluator

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.*

abstract class Operator(val str: String, val precedence: Int) {

    abstract fun apply(values: MutableList<Prop<*>>): Prop<*>

    /**
     * Is there is a unary version of this operator?
     * For example MinusOperator is a BinaryOperator, but also has a unary version (UnaryMinusOperator).
     */
    open val prefixOperator: Operator? = null

    fun expectsValue(): Boolean = true

    override fun toString() = "${this.javaClass.simpleName} $str"

    companion object {

        val operators = mutableMapOf<String, Operator>()

        val UNARY_MINUS = UnaryMinusOperator(20)
        val NOT = NotOperator(20)

        val DOT = DotOperator(19)

        val CLOSE_BRACKET = CloseBracketOperator(18)

        val POW = PowerOperator(17)

        val TIMES = TimesOperator(15)
        val DIV = DivOperator(15)
        val REMAINDER = RemainderOperator(15)

        val PLUS = PlusOperator(14)
        val MINUS = MinusOperator(14)

        val EQUALS = EqualsOperator(6)
        val NOT_EQUALS = NotEqualsOperator(6)
        val GREATER_THAN = GreaterThanOperator(6)
        val GREATER_THAN_EQUALS = GreaterThanEqualsOperator(6)
        val LESS_THAN = LessThanOperator(6)
        val LESS_THAN_EQUALS = LessThanEqualsOperator(6)

        val AND = AndOperator(5)

        val OR = OrOperator(4)
        val XOR = XorOperator(4)

        val COMMA = CommaOperator(3)

        val APPLY = ApplyOperator(2)

        val START_LIST = StartListOperator(2)

        val END_LIST = EndListOperator(2)

        val OPEN_BRACKET = OpenBracketOperator(1)

        val NONE = NoOperator(0)

        fun add(vararg ops: Operator) {
            ops.forEach { operators[it.str] = it }
        }

        init {
            add(OPEN_BRACKET, COMMA, OR, XOR, AND,
                    EQUALS, NOT_EQUALS, GREATER_THAN, GREATER_THAN_EQUALS, LESS_THAN, LESS_THAN_EQUALS,
                    PLUS, MINUS, TIMES, DIV, REMAINDER, POW, CLOSE_BRACKET, DOT, NOT, START_LIST, END_LIST)
        }

        fun find(str: String): Operator? = operators[str]

        fun isOperatorChar(c: Char) = "()+*^/%-!=><|&.,#{}".contains(c)
    }
}

class NoOperator(precedence: Int) : Operator("", precedence) {
    override fun apply(values: MutableList<Prop<*>>): Prop<*> {
        throw RuntimeException("Unexpected use of the NoOperator")
    }
}

interface OpenBracket

class OpenBracketOperator(precedence: Int) : UnaryOperator("(", precedence), OpenBracket {
    override fun apply(a: Prop<*>): Prop<*> {
        throw RuntimeException("Cannot apply '('")
    }
}

class CloseBracketOperator(precedence: Int) : Operator(")", precedence) {
    override fun apply(values: MutableList<Prop<*>>): Prop<*> {
        throw RuntimeException("Cannot apply ')'")
    }
}

class ApplyOperator(precedence: Int) : BinaryOperator("(", precedence), OpenBracket {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return if (a is PropMethod<*, *>) {
            a.applyArgs(b)
            // Make sure that the value can be evaluated, so that if it fails, then the error is reported at
            // the correct place.
            a.value
            return if (a.isConstant() && b.isConstant()) {
                //println("Apply Operator Making constant :$a  Args : $b")
                PropConstant(a.value)
            } else {
                a
            }
        } else {
            cannotApply(a, b)
        }
    }
}

class StartListOperator(precedence: Int) : UnaryOperator("{", precedence) {
    override fun apply(a: Prop<*>): Prop<*> {
        return if (a is ArgList) {
            a
        } else {
            val newList = ArgList()
            newList.add(a)
            newList
        }
    }
}

class EndListOperator(precedence: Int) : UnaryOperator("}", precedence) {
    override fun apply(a: Prop<*>): Prop<*> {
        return if (a is ArgList) {
            a
        } else {
            val newList = ArgList()
            newList.add(a)
            newList
        }
    }

}

class DotOperator(precedence: Int) : BinaryOperator(".", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        if (b is FieldOrMethodName) {
            PropType.field(a, b.value)?.let { field ->
                return field
            }
        }
        throw RuntimeException("Expected a field or method name, but found ${b.value}")
    }
}

/**
 * Unary operators, which precede their operands, such as unary minus (e.g. -1)
 */
abstract class UnaryOperator(str: String, precedence: Int) : Operator(str, precedence) {

    override fun apply(values: MutableList<Prop<*>>): Prop<*> {
        if (values.size < 1) {
            throw RuntimeException("Operator $str must have an operand")
        }
        val a = values.removeAt(values.size - 1)
        val result = apply(a)
        return if (a.isConstant() && result !is ArgList) {
            //println("Unary Op making constant  A: $a [${a.javaClass.simpleName}] of ${a.value.javaClass.simpleName}")
            PropConstant(result.value)
        } else {
            result
        }
    }

    protected fun cannotApply(a: Prop<*>): Prop<*> {
        throw RuntimeException("Cannot apply $str to ${a.value}")
    }

    abstract fun apply(a: Prop<*>): Prop<*>
}

/**
 * Binary operators have two operands, one is before the operator, the other after it. e.g. 1 + 2
 */
abstract class BinaryOperator(str: String, precedence: Int) : Operator(str, precedence) {

    override fun apply(values: MutableList<Prop<*>>): Prop<*> {
        if (values.size < 2) {
            throw RuntimeException("Operator $str must have two operands")
        }
        val b = values.removeAt(values.size - 1)
        val a = values.removeAt(values.size - 1)
        val result = apply(a, b)

        return if (a.isConstant() && b.isConstant() && result !is ArgList) {
            //println("Binary Op making constant  A: $a   B : $b")
            PropConstant(result.value)
        } else {
            result
        }
    }

    protected fun cannotApply(a: Prop<*>, b: Prop<*>): Prop<*> {
        throw RuntimeException("Cannot apply $str to ${a.value} and ${b.value}. Types are ${a.value.javaClass.simpleName} and ${b.value.javaClass.simpleName}")
    }

    abstract fun apply(a: Prop<*>, b: Prop<*>): Prop<*>

}

class PowerOperator(precedence: Int) : BinaryOperator("^", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        if (a.value is Double && b.value is Double) {
            return PropCalculation2("PowerOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> Math.pow(av, bv) }
        }

        return cannotApply(a, b)
    }
}

class NotOperator(precedence: Int) : UnaryOperator("!", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>): Prop<*> {
        if (a.value is Boolean) {
            return PropCalculation1("Not Operator", false, a as Prop<Boolean>) { !it }
        }
        return cannotApply(a)
    }

    override val prefixOperator: Operator? = this
}

class OrOperator(precedence: Int) : BinaryOperator("||", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        if (a.value is Boolean && b.value is Boolean) {
            return PropCalculation2("OrOperator", false, a as Prop<Boolean>, b as Prop<Boolean>) { av, bv -> av || bv }
        }
        return cannotApply(a, b)
    }
}

class AndOperator(precedence: Int) : BinaryOperator("&&", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        if (a.value is Boolean && b.value is Boolean) {
            return PropCalculation2("AndOperator", false, a as Prop<Boolean>, b as Prop<Boolean>) { av, bv -> av && bv }
        }
        return cannotApply(a, b)
    }
}

class XorOperator(precedence: Int) : BinaryOperator("xor", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        if (a.value is Boolean && b.value is Boolean) {
            return PropCalculation2("XorOperator", false, a as Prop<Boolean>, b as Prop<Boolean>) { av, bv -> av xor bv }
        }
        return cannotApply(a, b)
    }
}

class PlusOperator(precedence: Int) : BinaryOperator("+", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {

        if (a.value is Double && b.value is Double) {
            return PropCalculation2("PlusOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av + bv }

        } else if (a.value is Angle && b.value is Angle) {
            return PropCalculation2("PlusOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av + bv }

        } else if (a.value is Dimension && b.value is Dimension) {
            return PropCalculation2("PlusOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av + bv }

        } else if (a.value is Dimension2 && b.value is Dimension2) {
            return PropCalculation2("PlusOperator", false, a as Prop<Dimension2>, b as Prop<Dimension2>) { av, bv -> av + bv }

        } else if (a.value is Vector2 && b.value is Vector2) {
            return PropCalculation2("PlusOperator", false, a as Prop<Vector2>, b as Prop<Vector2>) { av, bv -> av + bv }

        } else if (a.value is String && b.value is String) {
            return PropCalculation2("PlusOperator", false, a as Prop<String>, b as Prop<String>) { av, bv -> av + bv }

        } else if (a is ArgList && b is ArgList) {
            val newList = ArgList()
            a.value.forEach { newList.add(it) }
            b.value.forEach { newList.add(it) }
            return newList
        }

        return cannotApply(a, b)
    }
}

@Suppress("UNCHECKED_CAST")
class UnaryMinusOperator(precedence: Int) : UnaryOperator("-", precedence) {
    override fun apply(a: Prop<*>): Prop<*> {

        return when {
            a.value is Double -> PropCalculation1("UnaryMinusOperator", false, a as Prop<Double>) { -it }
            a.value is Angle -> PropCalculation1("UnaryMinusOperator", false, a as Prop<Angle>) { -it }
            a.value is Dimension -> PropCalculation1("UnaryMinusOperator", false, a as Prop<Dimension>) { -it }
            a.value is Dimension2 -> PropCalculation1("UnaryMinusOperator", false, a as Prop<Dimension2>) { -it }
            a.value is Vector2 -> PropCalculation1("UnaryMinusOperator", false, a as Prop<Vector2>) { -it }
            else -> cannotApply(a)
        }

    }

}

@Suppress("UNCHECKED_CAST")
class EqualsOperator(precedence: Int) : BinaryOperator("==", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return PropCalculation2("EqualsOperator", false, a as Prop<Any>, b as Prop<Any>) { av, bv -> av == bv }
    }
}

@Suppress("UNCHECKED_CAST")
class NotEqualsOperator(precedence: Int) : BinaryOperator("!=", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return PropCalculation2("NotEqualsOperator", false, a as Prop<Any>, b as Prop<Any>) { av, bv -> av != bv }
    }
}


@Suppress("UNCHECKED_CAST")
class GreaterThanOperator(precedence: Int) : BinaryOperator(">", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return if (a.value is Double && b.value is Double) {
            PropCalculation2("GreaterThanOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av > bv }
        } else if (a.value is Dimension && b.value is Dimension) {
            PropCalculation2("GreaterThanOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av.inDefaultUnits > bv.inDefaultUnits }
        } else if (a.value is Angle && b.value is Angle) {
            PropCalculation2("GreaterThanOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av.radians > bv.radians }
        } else {
            cannotApply(a, b)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class GreaterThanEqualsOperator(precedence: Int) : BinaryOperator(">=", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return if (a.value is Double && b.value is Double) {
            PropCalculation2("GreaterThanEqualsOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av >= bv }
        } else if (a.value is Dimension && b.value is Dimension) {
            PropCalculation2("GreaterThanEqualsOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av.inDefaultUnits >= bv.inDefaultUnits }
        } else if (a.value is Angle && b.value is Angle) {
            PropCalculation2("GreaterThanEqualsOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av.radians >= bv.radians }
        } else {
            cannotApply(a, b)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class LessThanOperator(precedence: Int) : BinaryOperator("<", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return if (a.value is Double && b.value is Double) {
            PropCalculation2("LessThanOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av < bv }
        } else if (a.value is Dimension && b.value is Dimension) {
            PropCalculation2("LessThanOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av.inDefaultUnits < bv.inDefaultUnits }
        } else if (a.value is Angle && b.value is Angle) {
            PropCalculation2("LessThanOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av.radians < bv.radians }
        } else {
            cannotApply(a, b)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class LessThanEqualsOperator(precedence: Int) : BinaryOperator("<=", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return if (a.value is Double && b.value is Double) {
            PropCalculation2("LessThanEqualsOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av <= bv }
        } else if (a.value is Dimension && b.value is Dimension) {
            PropCalculation2("LessThanEqualsOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av.inDefaultUnits <= bv.inDefaultUnits }
        } else if (a.value is Angle && b.value is Angle) {
            PropCalculation2("LessThanEqualsOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av.radians <= bv.radians }
        } else {
            cannotApply(a, b)
        }
    }
}

@Suppress("UNCHECKED_CAST")
class MinusOperator(precedence: Int) : BinaryOperator("-", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {

        if (a.value is Double && b.value is Double) {
            return PropCalculation2("MinusOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av - bv }

        } else if (a.value is Angle && b.value is Angle) {
            return PropCalculation2("MinusOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av - bv }

        } else if (a.value is Dimension && b.value is Dimension) {
            return PropCalculation2("MinusOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av - bv }

        } else if (a.value is Dimension2 && b.value is Dimension2) {
            return PropCalculation2("MinusOperator", false, a as Prop<Dimension2>, b as Prop<Dimension2>) { av, bv -> av - bv }

        } else if (a.value is Vector2 && b.value is Vector2) {
            return PropCalculation2("MinusOperator", false, a as Prop<Vector2>, b as Prop<Vector2>) { av, bv -> av - bv }

        }
        return cannotApply(a, b)
    }

    override val prefixOperator: Operator? get() = Operator.UNARY_MINUS
}


@Suppress("UNCHECKED_CAST")
class TimesOperator(precedence: Int) : BinaryOperator("*", precedence) {
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        if (a.value is Double) {
            if (b.value is Double) {
                return PropCalculation2("TimesOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Vector2) {
                return PropCalculation2("TimesOperator", false, b as Prop<Vector2>, a as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Angle) {
                return PropCalculation2("TimesOperator", false, b as Prop<Angle>, a as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Dimension) {
                return PropCalculation2("TimesOperator", false, b as Prop<Dimension>, a as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Dimension2) {
                return PropCalculation2("TimesOperator", false, b as Prop<Dimension2>, a as Prop<Double>) { av, bv -> av * bv }
            }

        } else if (a.value is Angle && b.value is Double) {
            return PropCalculation2("TimesOperator", false, a as Prop<Angle>, b as Prop<Double>) { av, bv -> av * bv }

        } else if (a.value is Dimension) {
            if (b.value is Double) {
                return PropCalculation2("TimesOperator", false, a as Prop<Dimension>, b as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Vector2) {
                return PropCalculation2("TimesOperator", false, a as Prop<Dimension>, b as Prop<Vector2>) { av, bv -> av * bv }

            } else if (b.value is Dimension) {
                return PropCalculation2("TimesOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av * bv }
            }


        } else if (a.value is Dimension2) {
            if (b.value is Double) {
                return PropCalculation2("TimesOperator", false, a as Prop<Dimension2>, b as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Vector2) {
                return PropCalculation2("TimesOperator", false, a as Prop<Dimension2>, b as Prop<Vector2>) { av, bv -> av * bv }
            }

        } else if (a.value is Vector2) {
            if (b.value is Vector2) {
                return PropCalculation2("TimesOperator", false, a as Prop<Vector2>, b as Prop<Vector2>) { av, bv -> av * bv }

            } else if (b.value is Double) {
                return PropCalculation2("TimesOperator", false, a as Prop<Vector2>, b as Prop<Double>) { av, bv -> av * bv }

            } else if (b.value is Dimension2) {
                return PropCalculation2("TimesOperator", false, b as Prop<Dimension2>, a as Prop<Vector2>) { av, bv -> av * bv }

            } else if (b.value is Dimension) {
                return PropCalculation2("TimesOperator", false, a as Prop<Vector2>, b as Prop<Dimension>) { av, bv -> av * bv }
            }

        }

        return cannotApply(a, b)
    }
}

class DivOperator(precedence: Int) : BinaryOperator("/", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {

        if (a.value is Double && b.value is Double) {
            return PropCalculation2("DivOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av / bv }

        } else if (a.value is Angle) {
            if (b.value is Double) {
                return PropCalculation2("DivOperator", false, a as Prop<Angle>, b as Prop<Double>) { av, bv -> av / bv }

            } else if (b.value is Angle) {
                return PropCalculation2("DivOperator", false, a as Prop<Angle>, b as Prop<Angle>) { av, bv -> av / bv }
            }

        } else if (a.value is Dimension) {
            if (b.value is Dimension) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> av / bv }

            } else if (b.value is Double) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension>, b as Prop<Double>) { av, bv -> av / bv }

            } else if (b.value is Vector2) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension>, b as Prop<Vector2>) { av, bv -> av / bv }
            }

        } else if (a.value is Dimension2) {
            if (b.value is Dimension2) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension2>, b as Prop<Dimension2>) { av, bv -> av / bv }

            } else if (b.value is Double) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension2>, b as Prop<Double>) { av, bv -> av / bv }

            } else if (b.value is Dimension) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension2>, b as Prop<Dimension>) { av, bv -> av / bv }

            } else if (b.value is Vector2) {
                return PropCalculation2("DivOperator", false, a as Prop<Dimension2>, b as Prop<Vector2>) { av, bv -> av / bv }
            }

        } else if (a.value is Vector2) {
            if (b.value is Vector2) {
                return PropCalculation2("DivOperator", false, a as Prop<Vector2>, b as Prop<Vector2>) { av, bv -> av / bv }

            } else if (b.value is Double) {
                return PropCalculation2("DivOperator", false, a as Prop<Vector2>, b as Prop<Double>) { av, bv -> av / bv }
            }

        } else if (a.value is Area) {
            if (b.value is Area) {
                return PropCalculation2("DivOperator", false, a as Prop<Area>, b as Prop<Area>) { av, bv -> av / bv }

            } else if (b.value is Dimension) {
                return PropCalculation2("DivOperator", false, a as Prop<Area>, b as Prop<Dimension>) { av, bv -> av / bv }
            }
        }

        return cannotApply(a, b)
    }
}

class RemainderOperator(precedence: Int)
    : BinaryOperator("%", precedence) {

    @Suppress("UNCHECKED_CAST")
    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {

        if (a.value is Double && b.value is Double) {
            return PropCalculation2("RemainderOperator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> av.rem(bv) }
        }
        return cannotApply(a, b)

    }
}

class CommaOperator(precedence: Int)
    : BinaryOperator(",", precedence) {

    override fun apply(a: Prop<*>, b: Prop<*>): Prop<*> {
        return if (a is ArgList) {
            a.add(b)
            a
        } else {
            val list = ArgList()
            list.add(a)
            list.add(b)
            list
        }
    }

    /**
     * The comma operator can be used in three ways.
     *
     * * Builds up a list of arguments into an ArgList
     * * Forms a list inside [ and ].
     * * Acts as a convenience for creating Vector2 or Dimension2 from a pair of Doubles or Dimensions.
     *
     * The two cases are distinguished by looking at [nextOperator], which is the top-most operator
     * on the stack after the CommaOperator has been removed.
     * If it is an ApplyOperator, then we build an ArgList from the two arguments.
     * Otherwise, we check for pairs of Doubles or Dimensions and return a Vector2 or Dimension2.
     * If the arguments are NOT Doubles or Dimensions, then an exception is thrown.
     */
    fun applySpecial(values: MutableList<Prop<*>>, nextOperator: Operator?): Prop<*> {

        if (values.size < 2) {
            throw RuntimeException("Operator $str must have two operands")
        }
        val b = values.removeAt(values.size - 1)
        val a = values.removeAt(values.size - 1)

        if (nextOperator is ApplyOperator) {

            return apply(a, b)

        } else if (nextOperator is StartListOperator) {

            return apply(a, b)

        } else {
            if (a.value is Double && b.value is Double) {
                @Suppress("UNCHECKED_CAST")
                return PropCalculation2("Vector2Creator", false, a as Prop<Double>, b as Prop<Double>) { av, bv -> Vector2(av, bv) }
            } else if (a.value is Dimension && b.value is Dimension) {
                @Suppress("UNCHECKED_CAST")
                return PropCalculation2("Dimension2Createor", false, a as Prop<Dimension>, b as Prop<Dimension>) { av, bv -> Dimension2(av, bv) }
            } else {
                throw RuntimeException("Operator $str. Expected a pair of numbers or a pair of Dimensions")
            }
        }
    }
}
