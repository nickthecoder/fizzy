/*
Fizzy
Copyright (C) 2019 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.view

import uk.co.nickthecoder.fizzy.model.*
import java.util.*

/**
 * Rounds corners by intercepting all path methods, and replacing [lineTo] calls
 * with [roundedCorner] calls.
 *
 * This process is made more complex, because the initial [moveTo] may be part of
 * a rounded corner (if [closePath] is called later). Therefore we don't know how
 * to process a [moveTo] until all of the path elements are complete.
 * So each path element is added to a list, and processed when [endPath] is called.
 *
 * If the [moveTo] is part of a rounded corner, then the mid point between the
 * [moveTo] and the first [lineTo] is used as the actual start of the path.
 *
 * Note. [cornerRadius] should NOT be changed between calls to [beginPath] and [endPath].
 */
class RoundedDrawContext(private val inner: DrawContext) : DrawContext {

    private var cornerRadius = Dimension.ZERO_mm

    /**
     * Used by save and restore to hold all information that isn't held by [gc].
     * Currently,this is only the [cornerRadius], and is therefore a stack of Dimension.
     */
    private val stack = Stack<Dimension>()

    private var firstPoint: Dimension2? = null

    /**
     * When rounding corners, holds the previous lineTo point, so that subsequent lineTo
     * can be replaced by an arcTo.
     */
    private var prevLineToPoint: Dimension2? = null
        set(v) {
            if (v == null) {
                field?.let { pathOperations.add { inner.lineTo(it) } }
            }
            field = v
        }

    private val pathOperations = mutableListOf<() -> Unit>()

    private var moveTo = Dimension2.ZERO_mm

    private var closePath = false

    // State

    override fun save() {
        inner.save()
        stack.push(cornerRadius)
    }

    override fun restore() {
        inner.restore()
        cornerRadius = stack.pop()
    }

    override fun clear() {
        inner.clear()
    }

    override fun fillColor(paint: Paint) {
        inner.fillColor(paint)
    }

    override fun lineColor(paint: Paint) {
        inner.lineColor(paint)
    }

    override fun lineDashes(vararg dashes: Double) {
        inner.lineDashes(*dashes)
    }

    override fun cornerRadius(radius: Dimension) {
        cornerRadius = radius
        inner.cornerRadius(radius)
    }

    override fun lineWidth(width: Dimension) {
        inner.lineWidth(width)
    }

    override fun lineWidth(width: Double) {
        inner.lineWidth(width)
    }

    override fun strokeCap(strokeCap: StrokeCap) {
        inner.strokeCap(strokeCap)
    }

    override fun strokeJoin(strokeJoin: StrokeJoin) {
        inner.strokeJoin(strokeJoin)
    }


    // Transformations

    override fun translate(by: Dimension2) {
        inner.translate(by)
    }

    override fun translate(x: Dimension, y: Dimension) {
        inner.translate(x, y)
    }

    override fun rotate(by: Angle) {
        inner.rotate(by)
    }

    override fun scale(by: Double) {
        inner.scale(by)
    }

    override fun scale(by: Vector2) {
        inner.scale(by)
    }

    // Paths

    override fun beginPath() {
        closePath = false
        pathOperations.clear()
        inner.beginPath()
        prevLineToPoint = null
    }

    override fun endPath(stroke: Boolean, fill: Boolean) {
        prevLineToPoint = null
        pathOperations.forEach { it() }
        pathOperations.clear()
        inner.endPath(stroke, fill)
        closePath = false
    }


    override fun moveTo(point: Dimension2) {
        pathOperations.forEach { it() }
        pathOperations.clear()

        prevLineToPoint = null
        moveTo = point
        firstPoint = null
        closePath = false
        pathOperations.add {
            val first = firstPoint
            if (closePath && cornerRadius.inDefaultUnits != 0.0 && first != null) {
                inner.moveTo((point + first) / 2.0)
            } else {
                inner.moveTo(point)
            }
        }
    }

    override fun lineTo(point: Dimension2) {
        if (firstPoint == null) firstPoint = point

        val prev = prevLineToPoint
        if (cornerRadius.inDefaultUnits != 0.0) {
            if (prev != null) {
                pathOperations.add { inner.roundedCorner(prev, point, cornerRadius) }
            }
            prevLineToPoint = point
        } else {
            pathOperations.add { inner.lineTo(point) }
            prevLineToPoint = null
        }
    }

    override fun bezierCurveTo(c1: Dimension2, c2: Dimension2, end: Dimension2) {
        if (firstPoint == null) firstPoint = moveTo
        prevLineToPoint = null
        pathOperations.add { inner.bezierCurveTo(c1, c2, end) }
    }

    override fun roundedCorner(mid: Dimension2, point: Dimension2, radius: Dimension) {
        if (firstPoint == null) firstPoint = mid
        prevLineToPoint = point
        pathOperations.add { inner.roundedCorner(mid, point, radius) }
    }

    override fun closePath() {
        val prev = prevLineToPoint
        if (cornerRadius.inDefaultUnits != 0.0) {
            if (prev != null) {
                pathOperations.add { inner.roundedCorner(prev, moveTo, cornerRadius) }
            }
        }
        val first = firstPoint
        if (cornerRadius.inDefaultUnits != 0.0 && first != null) {
            val last = (moveTo + first) / 2.0
            pathOperations.add { inner.roundedCorner(moveTo, last, cornerRadius) }
            prevLineToPoint = last
        }
        prevLineToPoint = null
        pathOperations.add { inner.closePath() }
        closePath = true
    }


    override fun multiLineText(multiLineText: MultiLineText, stroke: Boolean, fill: Boolean) {
        inner.multiLineText(multiLineText, stroke, fill)
    }

    override fun multiLineText(multiLineText: MultiLineText, stroke: Boolean, fill: Boolean, clipStart: Dimension2, clipSize: Dimension2) {
        inner.multiLineText(multiLineText, stroke, fill, clipStart, clipSize)
    }

}
