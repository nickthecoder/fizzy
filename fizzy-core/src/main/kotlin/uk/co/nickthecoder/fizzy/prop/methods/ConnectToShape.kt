/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.model.geometry.Geometry
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation
import uk.co.nickthecoder.fizzy.prop.PropCalculation1
import uk.co.nickthecoder.fizzy.util.ChangeListener
import uk.co.nickthecoder.fizzy.util.ChangeType

/**
 * Connects a (Shape1d) line's start or end to a (Shape2d) box in an intelligent manner.
 *
 * It uses the same concept as [ConnectAround], i.e. the point is connected to a part of the box's
 * geometry.
 * However, unlike [ConnectAround], this connection isn't permanently connected to a particular place
 * around the box.
 *
 * Instead, it starts at the box's pin location, and calculates where around the geometry
 * it should stick to based on the angle of the line.
 *
 * Unlike [ConnectAround] the result of this Prop is the shape's center, and to obtain the position
 * around the edge of the box, the line must use [Shape1d.smartStart] / [Shape1d.smartEnd].
 * Both are in the line's local coordinate system.
 *
 * Line's may assign a non-zero value to [Shape1d.extraStartAngle] and/or [Shape1d.extraEndAngle],
 * which adjusts the connection point slightly. This is particularly useful when connecting
 * more than 1 line between the same pair of boxes.
 * For example, if the line is a bezier curve, then [Shape1d.extraStartAngle] would be the
 * angle of the first control point.
 *
 * NOTE. Unlike other [Connection]s, there is no "local" boolean. This always returns coordinates in the parent's
 * coordinates, as it is only used by a line's start and end.
 *
 * [ConnectToShapePin] is an alternative to this, which merely connects to the shape's center of rotation,
 * i.e. has none of this class's cleverness.
 */
class ConnectToShape(prop: Prop<Shape1d>)
    : TypedMethod3<Shape1d, Shape2d, Boolean, Dimension, Dimension2>("ConnectToShape", prop, Shape2d::class.java, Boolean::class.java, Dimension::class.java),
        Connection,
        ChangeListener<Geometry> {

    private var along: Double = 0.0

    private var connectTo: Geometry? = null
        set(v) {
            if (field !== v) {
                field?.changeListeners?.remove(this)
                field?.shape?.transform?.fromPageToLocal?.removeListener(this)
                field?.shape?.smartConnectionType?.removeListener(this)
                field?.shape?.transform?.pin?.removeListener(this)

                field = v

                v?.changeListeners?.add(this)
                v?.shape?.transform?.fromPageToLocal?.addListener(this)
                v?.shape?.smartConnectionType?.addListener(this)
                // Why is this needed? The above should be sufficient?
                // Without it, the connection only works sporadically while dragging the box that the line is connected to.
                v?.shape?.transform?.pin?.addListener(this)

                // Force evaluation. I'm not sure why this is needed, but removing causes TestConnections.testConnectAlong to fail.
                v?.shape?.transform?.fromPageToLocal?.value
            }
        }

    private var connectionPoint: ConnectionPoint? = null
        set(v) {
            if (field !== v) {
                field?.point?.removeListener(this)
                field?.angle?.removeListener(angle)
                field = v
                v?.point?.addListener(this)
                v?.angle?.addListener(angle)
            }
        }

    /**
     * Either [Shape1d.extraStartAngle] or [Shape1d.extraEndAngle] (of the line).
     */
    private var extraAngle: PropCalculation<Angle>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(smartPosition)
                field = v
                v?.addListener(smartPosition)
            }
        }

    /**
     * Either [Shape1d.start] or [Shape1d.end] of the line.
     */
    private var otherEnd: PropCalculation<Dimension2>? = null
        set(v) {
            if (field !== v) {
                field?.removeListener(smartPosition)
                field?.removeListener(angle)
                field = v
                v?.addListener(smartPosition)
                v?.addListener(angle)
            }
        }

    override val angle = PropCalculation1("ConnectToShape.angle", false, this) {
        // Ensure smartPosition has been calculated, as that is what sets 'along'
        smartPosition.value

        val box = connectTo?.shape
        if (box != null) {
            when (box.smartConnectionType.value) {

                SmartConnectionType.AROUND.name -> {
                    val normal = connectTo?.normalAlong(along)
                    val otherShape = connectTo?.shape

                    if (normal == null || otherShape == null) {
                        Angle.ZERO
                    } else {
                        //println("BB : along=$along. Normal=${normal.degrees} ${otherShape.localToPage(normal).normalise().degrees}")
                        otherShape.localToPage(normal).normalise()
                    }
                }

                SmartConnectionType.CONNECTION_POINT.name -> {
                    val cp = connectionPoint
                    val otherShape = cp?.shape

                    if (cp == null || otherShape == null) {
                        Angle.ZERO
                    } else {
                        //println("AA : ${otherShape.localToPage(cp.angle.value).normalise().degrees}")
                        otherShape.localToPage(cp.angle.value).normalise()
                    }
                }

                else -> {
                    Angle.ZERO
                }
            }
        } else {
            Angle.ZERO
        }
    }

    private val smartPosition = PropCalculation1("ConnectToShape.smartPosition", false, this) {

        val box = connectTo?.shape
        val line = prop.value

        if (box != null) {
            val extraAngle = if (isStart) line.extraStartAngle else line.extraEndAngle
            this.extraAngle = extraAngle

            val otherEnd = if (isStart) line.end else line.start
            val otherEndCP = otherEnd.calculatedProperty()
            val otherEndValue = if (otherEndCP != null && otherEndCP is ConnectToShape) otherEndCP.connectedTo().transform.pin.value else otherEnd.value

            val angle = (line.parent.localToPage(otherEndValue) - box.localToPage(box.transform.locPin.value)).angle() + extraAngle.value

            when (box.smartConnectionType.value) {
                SmartConnectionType.AROUND.name -> {
                    var min = box.transform.locPin.value
                    var max = min + angle.unitVector() * 1.5 * box.size.value.length()
                    val chops = 10
                    val minDist = (max - min).length().inDefaultUnits / Math.pow(2.0, (chops - 4).toDouble())

                    // Use a binary chop to home in on outside edge of the box the line shall connect to.
                    along = 0.0
                    for (i in 0..chops) {
                        val mid = (min + max) / 2.0
                        val pair = box.geometry.checkAlong(mid)
                        if (pair != null) {
                            along = pair.second
                            if (pair.first < minDist) {
                                break
                            }
                        }

                        // Missed
                        if (box.geometry.isAt(mid, Dimension.ZERO_mm, Dimension.ZERO_mm)) {
                            // Inside, so increase min
                            min = mid
                        } else {
                            max = mid
                        }
                    }

                    var point = box.geometry.pointAlong(along)
                    if (margin.inDefaultUnits != 0.0) {
                        val normal = box.geometry.normalAlong(along)
                        point += normal.unitVector() * margin
                    }
                    prop.value.pageToLocal(box.localToPage(point))
                }

                SmartConnectionType.CONNECTION_POINT.name -> {
                    var closestRadians = Double.MAX_VALUE
                    var closestConnectionPoint: ConnectionPoint? = null
                    for (cp in box.connectionPoints) {
                        val diff = Math.abs(Angle.radiansNegPiToPi(box.localToPage((cp.point.value - box.transform.locPin.value).angle()).radians - angle.radians))
                        if (diff < closestRadians) {
                            closestRadians = diff
                            closestConnectionPoint = cp
                        }
                    }
                    connectionPoint = closestConnectionPoint
                    if (closestConnectionPoint == null) {
                        Dimension2.ZERO_mm
                    } else {
                        var point = closestConnectionPoint.point.value
                        if (margin.inDefaultUnits != 0.0) {
                            point += closestConnectionPoint.angle.value.unitVector() * margin
                        }
                        prop.value.pageToLocal(box.localToPage(point))
                    }
                }

                else -> {
                    prop.value.pageToLocal(box.transform.pin.value)
                }
            }

        } else {
            Dimension2.ZERO_mm
        }
    }

    fun smartPosition(): Prop<Dimension2> = smartPosition

    override fun changed(item: Geometry, changeType: ChangeType, obj: Any?) {
        smartPosition.dirty = true
    }

    private var isStart = true

    private var margin = Dimension.ZERO_mm


    init {
        selfDependants(2) // angle and smartPosition.
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()

        angle.noLongerNeeded()
        smartPosition.noLongerNeeded()

        extraAngle?.removeListener(smartPosition)

        otherEnd?.removeListener(smartPosition)
        otherEnd?.removeListener(angle)

        connectionPoint?.point?.removeListener(this)
        connectionPoint?.angle?.removeListener(angle)

        connectTo?.let { ct ->
            ct.changeListeners.remove(this)
            ct.shape.transform.fromPageToLocal.removeListener(this)
            ct.shape.smartConnectionType.removeListener(this)
            ct.shape.transform.pin.removeListener(this)
        }
    }

    override fun eval(a: Shape2d, b: Boolean, c: Dimension): Dimension2 {

        val box = a
        val line = prop.value
        connectTo = a.geometry
        isStart = b
        margin = c
        otherEnd = if (isStart) line.end else line.start

        val pagePoint = box.parent.localToPage(box.transform.pin.value)

        return line.parent.pageToLocal(pagePoint)
    }

    override fun connectedTo(): Shape {
        value // Force evaluation
        return connectTo?.shape ?: prop.value
    }

}
