/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.handle

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.controller.Controller
import uk.co.nickthecoder.fizzy.controller.ShapeHighlight
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape1d
import uk.co.nickthecoder.fizzy.model.Shape1dTransform
import uk.co.nickthecoder.fizzy.model.history.ChangeExpression

class Shape1dHandle(val transform: Shape1dTransform, val controller: Controller, val isEnd: Boolean)
    : ShapeHandle(transform.shape) {

    override fun position(): Dimension2 {
        return shape.parent.localToPage((if (isEnd) transform.end.value else transform.start.value))
    }

    override fun beginDrag(startPoint: Dimension2) {
        if (controller.singleShape == null) {
            controller.showConnectionPoints(true)
        }
    }

    override fun dragTo(event: CMouseEvent, dragPoint: Dimension2) {
        val startEnd = if (isEnd) transform.end else transform.start
        val other = if (isEnd) transform.start else transform.end

        val pagePoint = if (event.isConstrain) {
            if (event.isAdjust) {
                // Snap along X or Y using the local coordinates.
                // For top-level shapes this is the same as the code below,
                // but for child shapes, it will be different if the parent is rotated.
                val localDragPoint = shape.parent.pageToLocal(dragPoint)
                val dx = Math.abs((localDragPoint.x - other.value.x).inDefaultUnits)
                val dy = Math.abs((localDragPoint.y - other.value.y).inDefaultUnits)
                val local = if (dx > dy) {
                    Dimension2(localDragPoint.x, other.value.y)
                } else {
                    Dimension2(other.value.x, localDragPoint.y)
                }
                shape.parent.localToPage(local)
            } else {
                // Snap horizontally or vertically within the page.
                val pageOther = shape.parent.localToPage(other.value)
                val dx = Math.abs((dragPoint.x - pageOther.x).inDefaultUnits)
                val dy = Math.abs((dragPoint.y - pageOther.y).inDefaultUnits)
                if (dx > dy) {
                    Dimension2(dragPoint.x, pageOther.y)
                } else {
                    Dimension2(pageOther.x, dragPoint.y)
                }
            }
        } else {
            dragPoint
        }

        val connectionData = controller.connectionData(pagePoint, shape as Shape1d, event.scale, !isEnd, false)
        val changes = if (connectionData == null) {
            controller.highlightShape.value = ShapeHighlight.NO_SHAPE_HIGHLIGHT
            val parentPoint = shape.parent.pageToLocal(pagePoint)
            ChangeExpression(startEnd, parentPoint.toFormula())
        } else {
            controller.highlightShape.value = ShapeHighlight.create(connectionData)
            ChangeExpression(startEnd, connectionData.connectionPointFormula)
        }

        shape.document().history.makeChange(changes)
    }

    override fun endDrag() {
        controller.showConnectionPoints(false)
        controller.highlightShape.value = ShapeHighlight.NO_SHAPE_HIGHLIGHT
    }
}
