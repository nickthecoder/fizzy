/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.controller.handle

import uk.co.nickthecoder.fizzy.controller.CMouseEvent
import uk.co.nickthecoder.fizzy.model.Dimension
import uk.co.nickthecoder.fizzy.model.Dimension2
import uk.co.nickthecoder.fizzy.model.Shape2dTransform
import uk.co.nickthecoder.fizzy.model.history.ChangeExpressions

/**
 * Resize a Shape2d.
 * Note, this can affect the Pin as well as the Size
 *
 * dx and dy are either -1, 0 or 1.
 * 0 means is should not affect that axis.
 * -1 means adjust the left/top edge.
 * 1 means the right/bottom edge.
 */
class Shape2dSizeHandle(val transform: Shape2dTransform, val dx: Int, val dy: Int)
    : ShapeHandle(transform.shape) {

    var aspectRatio = 1.0

    override fun position(): Dimension2 {
        val x = if (dx < 0) {
            Dimension.ZERO_mm
        } else if (dx > 0) {
            shape.size.value.x
        } else {
            shape.size.value.x / 2.0
        }

        val y = if (dy < 0) {
            Dimension.ZERO_mm
        } else if (dy > 0) {
            shape.size.value.y
        } else {
            shape.size.value.y / 2.0
        }

        return shape.localToPage(Dimension2(x, y))
    }

    override fun beginDrag(startPoint: Dimension2) {
        aspectRatio = shape.size.value.aspectRatio()
    }

    override fun dragTo(event: CMouseEvent, dragPoint: Dimension2) {
        val local = shape.pageToLocal(dragPoint)

        // Calculate how much the shape's SIZE will increase by
        var deltaX = when (dx) {
            1 -> local.x - shape.size.value.x
            -1 -> -local.x
            else -> Dimension.ZERO_mm
        }

        var deltaY = if (dy == 1) {
            local.y - shape.size.value.y
        } else if (dy == -1) {
            -local.y
        } else {
            Dimension.ZERO_mm
        }

        if (event.isConstrain) {
            if ((shape.size.value.x + deltaX) / aspectRatio > shape.size.value.y + deltaY) {
                deltaX = (shape.size.value.y + deltaY) * aspectRatio - shape.size.value.x
            } else {
                deltaY = (shape.size.value.x + deltaX) / aspectRatio - shape.size.value.y
            }
        }

        // Calculate how much the shape will MOVE by
        val moveX = if (dx == 1) {
            deltaX * (shape.transform.locPin.value.x / shape.size.value.x)
        } else if (dx == -1) {
            -deltaX * (shape.transform.locPin.value.x / shape.size.value.x)
        } else {
            Dimension.ZERO_mm
        }

        val moveY = if (dy == 1) {
            deltaY * (shape.transform.locPin.value.y / shape.size.value.y)
        } else if (dy == -1) {
            -deltaY * (shape.transform.locPin.value.y / shape.size.value.y)
        } else {
            Dimension.ZERO_mm
        }


        val newPin =
                shape.localToParent(
                        (shape.parentToLocal(shape.transform.pin.value) + Dimension2(moveX, moveY)))
        val oldSize = shape.size.value
        val newSize = Dimension2(
                shape.size.value.x + deltaX,
                shape.size.value.y + deltaY)

        shape.document().history.makeChange(ChangeExpressions(listOf(
                shape.size to newSize.toFormula(),
                transform.pin to newPin.toFormula()
        )))

        // Move the control points.
        shape.controlPoints.forEach { cp ->
            cp.shapeResized(oldSize, newSize)
        }


    }

}
