/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation1

class ConnectTo(prop: Prop<Shape>, val local: Boolean)
    : TypedMethod2<Shape, ConnectionPoint, Dimension, Dimension2>("ConnectTo", prop, ConnectionPoint::class.java, Dimension::class.java),
        Connection {

    private var myShape: Shape? = null
        set(v) {
            if (field !== v) {
                field?.transform?.fromPageToLocal?.removeListener(this)
                field = v
                v?.transform?.fromPageToLocal?.addListener(this)
            }
        }

    /**
     * Whenever the connection point changes, we need to change too.
     */
    private var connectionPoint: ConnectionPoint? = null
        set(v) {
            if (field != v) {
                field?.shape?.transform?.fromLocalToPage?.removeListener(this)
                field?.point?.removeListener(this)
                field?.angle?.removeListener(angle)
                field = v
                v?.shape?.transform?.fromLocalToPage?.addListener(this)
                v?.point?.addListener(this)
                v?.angle?.addListener(angle)
            }
        }

    override val angle = PropCalculation1("ConnectTo.angle", false, this) {
        val cp = connectionPoint
        val otherShape = cp?.shape

        if (cp == null || otherShape == null) {
            Angle.ZERO
        } else {
            otherShape.localToPage(cp.angle.value).normalise()
        }
    }

    init {
        selfDependants(2) // angle and prop.value.parent.transform.fromPageToLocal
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()

        angle.noLongerNeeded()
        connectionPoint = null
        myShape == null
    }

    override fun eval(a: ConnectionPoint, b: Dimension): Dimension2 {
        myShape = prop.value
        connectionPoint = a
        val margin = b

        val otherShape = a.shape ?: throw RuntimeException("ConnectionPoint does not have a Shape")
        var point = a.point.value
        if (margin.inDefaultUnits != 0.0) {
            point += a.angle.value.unitVector() * margin
        }

        return if (local) {
            prop.value.pageToLocal(otherShape.localToPage(point))
        } else {
            prop.value.parent.pageToLocal(otherShape.localToPage(point))
        }
    }

    override fun connectedTo(): Shape {
        value // Force evaluation
        return connectionPoint?.shape ?: prop.value
    }
}
