/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.model.controlpoint.WithinControlPoint
import uk.co.nickthecoder.fizzy.model.geometry.BezierCurveTo
import uk.co.nickthecoder.fizzy.model.geometry.LineTo
import uk.co.nickthecoder.fizzy.model.geometry.MoveTo
import uk.co.nickthecoder.fizzy.prop.Dimension2Expression
import uk.co.nickthecoder.fizzy.prop.Vector2Expression
import uk.co.nickthecoder.fizzy.util.toFormula

/**
 * Primitive shapes that are available without needing to load a stencil. Its master shapes are defined programmatically.
 */
object PrimitiveShapes {

    val document = Document()

    val page = Page(document)

    val box = createBox(page)

    val line = createLine(page)

    val poly3 = createPolygon(page, 3)
    val poly4 = createPolygon(page, 4)
    val poly5 = createPolygon(page, 5)
    val poly6 = createPolygon(page, 6)
    val poly7 = createPolygon(page, 7)
    val poly8 = createPolygon(page, 8)

    val star3 = createPolygon(page, 3, star = true)
    val star4 = createPolygon(page, 4, star = true)
    val star5 = createPolygon(page, 5, star = true)
    val star6 = createPolygon(page, 6, star = true)

    val pentangle = createPolygon(page, 5, star = true)

    init {
        (pentangle.controlPoints[0] as WithinControlPoint).position.formula = (pentangle.geometry.parts[4].point.value / pentangle.size.value).toFormula()
    }

    fun createBox(
            parent: ShapeParent,
            size: String = "(100mm,100mm)",
            at: String = "(0mm,0mm)",
            fillColor: String? = "WHITE")
            : Shape2d {

        val box = Shape2d.create(parent)
        box.size.formula = size
        box.transform.pin.formula = at

        box.geometry.parts.add(MoveTo("Size * Vector2(0,0) + (LineWidth/2, LineWidth/2)"))
        box.geometry.parts.add(LineTo("Size * Vector2(1,0) + (-LineWidth/2, LineWidth/2)"))
        box.geometry.parts.add(LineTo("Size * Vector2(1,1) + (-LineWidth/2, -LineWidth/2)"))
        box.geometry.parts.add(LineTo("Size * Vector2(0,1) + (LineWidth/2, -LineWidth/2)"))
        box.geometry.parts.add(LineTo("Geometry1.Point1"))
        box.connectAround.formula = "true" // Allow connections along

        if (fillColor != null) {
            box.fillColor.formula = fillColor
        }

        return box
    }


    fun createPolygon(
            parent: ShapeParent,
            sides: Int,
            radius: Dimension = Dimension(60.0, Units.mm),
            star: Boolean = false,
            fillColor: String? = "WHITE",
            at: String = "(0mm,0mm)")
            : Shape2d {

        val poly = Shape2d.create(parent)
        poly.transform.pin.formula = at
        poly.transform.locPin.formula = Dimension2.ZERO_mm.toFormula()

        val unit = Dimension2(radius, Dimension.ZERO_mm)

        // Create points around (0,0)
        poly.geometry.parts.add(LineTo("(${radius.toFormula()},0mm)"))
        for (i in 1..sides) {
            val point = unit.rotate(Angle.TAU * (i.toDouble() / sides))
            poly.geometry.parts.add(LineTo(point))
        }

        val minX = poly.geometry.parts.minBy { it.point.value.x }
        val minY = poly.geometry.parts.minBy { it.point.value.y }

        val maxX = poly.geometry.parts.maxBy { it.point.value.x }
        val maxY = poly.geometry.parts.maxBy { it.point.value.y }

        if (minX != null && minY != null && maxX != null && maxY != null) {

            val shapeSize =
                    Dimension2(maxX.point.value.x, maxY.point.value.y) -
                            Dimension2(minX.point.value.x, minY.point.value.y)

            poly.size.formula = shapeSize.toFormula()

            val translate = Dimension2(-minX.point.value.x, -minY.point.value.y)
            poly.scratches.add(Scratch("MagicRatio", Vector2Expression(
                    Vector2(1.0, (maxY.point.value.y - minY.point.value.y).ratio(maxX.point.value.x - minX.point.value.x))),
                    CellType.VECTOR2))

            poly.transform.locPin.formula = "Size * ${translate.ratio(shapeSize).toFormula()}"

            // Translate them, so they are all +ve
            // Also, make them reference Size, so that resizing works as expected.
            poly.geometry.parts.forEach { part ->
                val ratio = (part.point.value + translate).ratio(shapeSize)
                if (part.point is Dimension2Expression) {
                    (part.point as Dimension2Expression).formula = "Size * ${ratio.toFormula()}"
                }
            }
        }

        if (star) {

            // Add extra geometries for the inner vertices of the star
            for (i in 0 until sides) {
                poly.geometry.parts.add(i * 2 + 1, LineTo("LocPin + ( (ControlPoint.Point1 - LocPin) / Size * Scratch.MagicRatio ).rotate( ($i / $sides * TAU) rad ) * Size / Scratch.MagicRatio"))
            }

            // Create a control point between 1st two outer points
            poly.controlPoints.add(WithinControlPoint("(Geometry1.Point1 + Geometry1.Point3)/2/Size"))

        }

        poly.fillColor.formula = fillColor ?: Color.TRANSPARENT.toFormula()

        // Create connection points at each vertex
        for (i in 0..poly.geometry.parts.size - 2) {
            val cp = ConnectionPoint("Geometry1.Point${i + 1}")
            poly.connectionPoints.add(cp)
        }

        //println(poly.metaData().joinToString(separator = "\n"))
        return poly
    }


    fun createText(
            parent: ShapeParent,
            str: String = "",
            fontFamily: String = "Sans",
            fontVariant: String = "Regular",
            fontSize: String = "46pt",
            at: String = "(0mm,0mm)",
            alignX: Double = 0.5,
            alignY: Double = 0.5
    ): ShapeText {

        val text = ShapeText.create(parent)
        text.text.formula = str.toFormula()
        text.fontSize.formula = fontSize
        text.fontFamily.formula = fontFamily.toFormula()
        text.fontVariant.formula = fontVariant.toFormula()
        text.fillColor.formula = "BLACK"
        text.alignX.formula = alignX.toFormula()
        text.alignY.formula = alignY.toFormula()

        if (parent is Shape2d) {
            text.size.formula = "Parent.Size - Dimension2( MarginLeft + MarginRight, MarginTop + MarginBottom )"
            text.transform.pin.formula = "Size * Vector2(AlignX, AlignY) + Dimension2( MarginTop, MarginLeft )"
            text.clip.formula = true.toFormula()
            //text.transform.locPin.formula = "Size / 2"
        } else {
            text.transform.pin.formula = at
        }

        return text
    }

    // TODO We need to do something more sensible for the lineWidth.
    // Maybe when styles are in place, it will use a default style, rather than a hard coded value.
    fun createLine(
            parent: ShapeParent,
            start: String = "(0mm,0mm)",
            end: String = "(10mm,10mm)",
            lineWidth: String = "2mm",
            bezier: Boolean = false)
            : Shape1d {

        val line = Shape1d.create(parent)

        line.start.formula = start
        line.end.formula = end
        line.lineWidth.formula = lineWidth

        line.geometry.parts.add(MoveTo("(0mm,LineWidth/2)"))
        if (bezier) {
            line.geometry.parts.add(BezierCurveTo("(Length/3,LineWidth/2)", "(Length*2/3,LineWidth/2)", "(Length,LineWidth/2)"))
        } else {
            line.geometry.parts.add(LineTo("(Length,LineWidth/2)"))
        }

        return line
    }
}
