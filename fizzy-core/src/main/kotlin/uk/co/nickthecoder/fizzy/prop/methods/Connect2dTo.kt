/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.prop.methods

import uk.co.nickthecoder.fizzy.model.*
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation1

/**
 * Connects a Shape2d or ShapeText to another shape via their ConnectionPoints.
 *
 * Used by the expressions [Shape2dTransform.pin].
 */
class Connect2dTo(prop: Prop<ConnectionPoint>)
    : TypedMethod1<ConnectionPoint, ConnectionPoint, Dimension2>("Connect2dTo", prop, ConnectionPoint::class.java),
        Connection {

    private var myShape: Shape? = null
        set(v) {
            if (field !== v) {
                (field?.transform as? Shape2dTransform)?.let {
                    it.flipX.removeListener(this)
                    it.flipY.removeListener(this)
                    it.scale.removeListener(this)
                    it.rotation.removeListener(this)
                }

                field = v
                (v?.transform as? Shape2dTransform)?.let {
                    it.flipX.addListener(this)
                    it.flipY.addListener(this)
                    it.scale.addListener(this)
                    it.rotation.addListener(this)
                }
            }
        }

    /**
     * Whenever the connection point changes, we need to change too.
     */
    private var connectionPoint: ConnectionPoint? = null
        set(v) {
            if (field != v) {
                field?.shape?.transform?.fromLocalToPage?.removeListener(this)
                field?.point?.removeListener(this)
                field?.angle?.removeListener(angle)
                field = v
                v?.shape?.transform?.fromLocalToPage?.addListener(this)
                v?.point?.addListener(this)
                v?.angle?.addListener(angle)
            }
        }

    override val angle = PropCalculation1("ConnectTo.angle", false, this) {
        val cp = connectionPoint
        val otherShape = cp?.shape

        if (cp == null || otherShape == null) {
            Angle.ZERO
        } else {
            otherShape.localToPage(cp.angle.value).normalise()
        }
    }

    init {
        myShape = prop.value.shape
        selfDependants(2) // angle and myShape.transform.fromPageToLocal
    }

    override fun noLongerNeeded() {
        super.noLongerNeeded()

        angle.noLongerNeeded()
        connectionPoint?.shape?.transform?.fromLocalToPage?.removeListener(this)
        connectionPoint?.point?.removeListener(this)
        connectionPoint?.angle?.removeListener(angle)

        (myShape?.transform as? Shape2dTransform)?.let {
            it.flipX.removeListener(this)
            it.flipY.removeListener(this)
            it.scale.removeListener(this)
            it.rotation.removeListener(this)
        }
    }

    override fun eval(a: ConnectionPoint): Dimension2 {
        myShape = prop.value.shape
        connectionPoint = a

        val otherShape = a.shape ?: throw RuntimeException("ConnectionPoint does not have a Shape")
        if (otherShape === myShape) throw RuntimeException("$otherShape is connected to itself")

        // The other shape's connection point in page coordinates
        val pagePoint = otherShape.localToPage(a.point.value)

        // The control point relative to locPin in local coordinates.
        val offset = prop.value.point.value - myShape!!.transform.locPin.value

        // Adjust offset for rotation, scale, flip etc. Cannot use localToParent, or localToPage, because
        // that would result in recursion, as it uses pin
        var matrix = Matrix33.identity
        val t = myShape!!.transform
        when (t) {
            is Shape2dTransform -> {
                if (t.flipX.value || t.flipY.value) matrix *= Matrix33.flip(t.flipX.value, t.flipY.value)
                if (t.rotation.value.radians != 0.0) matrix *= Matrix33.rotate(t.rotation.value)
                if (t.scale.value.x != 1.0 || t.scale.value.y != 1.0) matrix *= Matrix33.scale(t.scale.value)
            }
            is Shape1dTransform -> {
                if (t.rotation.value.radians != 0.0) matrix *= Matrix33.rotate(t.rotation.value)
            }
        }

        return myShape!!.parent.pageToLocal(pagePoint - matrix * offset)
    }

    override fun connectedTo(): Shape {
        value // Force evaluation
        return connectionPoint?.shape ?: prop.value.shape ?: myShape!!
    }
}
