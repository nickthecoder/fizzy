/*
Fizzy
Copyright (C) 2018 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fizzy.model

import uk.co.nickthecoder.fizzy.collection.ListListenerLambda
import uk.co.nickthecoder.fizzy.collection.MutableFList
import uk.co.nickthecoder.fizzy.prop.Prop
import uk.co.nickthecoder.fizzy.prop.PropCalculation
import uk.co.nickthecoder.fizzy.util.FizzyJsonReader
import java.io.File

/**
 * A Stencil is a collection of Shapes, that are used as Master Shapes, and copied
 * when used. The copy's expressions will LINK to the master shape's expressions,
 * so that changes made to the original affect all of the copies.
 *
 * The stencils are stored in a page of a [Document], and the only difference between
 * a Stencil and a regular Document is the file extension, and how they are used.
 * The data structures are identical.
 *
 * When a stencil is used within a regular document for the first time, TWO copies
 * are made, one copy is placed in the Document's "Local Masters", and the other copy
 * is a linked copy of the local master.
 * In this way, a document is no long dependent on the original Stencil.
 *
 * [Stencils] holds a list of all of the stencil objects (as described above these
 * are actually of type [Document]).
 *
 */
object Stencils {

    /**
     * The list of directories in which stencils are saved.
     * Adding a directory to this list will automatically scan the directory for
     * files with extension ".fstencil", and add those stencils to the
     * [stencils] list.
     */
    val directories = MutableFList<File>()

    /**
     * A list of [Document]s containing Master Shapes.
     */
    val stencils = MutableFList<Document>()

    private val directoriesListener = ListListenerLambda<File> { _, item, _, added ->
        if (added) addStencils(item) else removeStencils(item)
    }


    init {
        directories.listeners.add(directoriesListener)
        directories.add(File(File(System.getProperty("user.home"), "fizzy"), "stencils"))
        findStockDirectory()?.let { directories.add(File(it, "stencils")) }
    }


    private fun addStencils(directory: File) {
        val files = directory.listFiles()
        files?.forEach { file ->
            if (file.isFile && file.extension == "fizzys") {
                try {
                    val doc = FizzyJsonReader(file).load()
                    stencils.add(doc)
                } catch (e: Exception) {
                }
            }
        }
    }

    private fun removeStencils(directory: File) {
        stencils.removeAll(stencils.filter { it.file?.startsWith(directory) == true })
    }

    fun findLineMarker(name: String): Shape? {
        stencils.forEach { stencil ->
            val found = stencil.pages.firstOrNull()?.findChild(name)
            if (found != null && found.getStencilShapeInfo().isLineMarking.value) return found
        }
        return null
    }

}

class FindLineMarker(permanent: Boolean, private val name: Prop<String>) : PropCalculation<Shape2d>("FindLineMarker", permanent) {

    init {
        if (!name.isConstant()) name.addListener(this)
    }

    override fun eval(): Shape2d = if (name.value.isBlank()) {
        NO_SHAPE
    } else {
        Stencils.findLineMarker(name.value) as? Shape2d ?: NO_SHAPE
    }
}

/**
 * Does a "best effort" attempt to find the "stock" directory, which contains sub directories for
 * stencils and templates.
 * It does this by finding the jar's location, and walking up the file's hierarchy, looking for a
 * "stock" directory at each level.
 * If it isn't found, or a security exception is thrown, then null is returned.
 */
fun findStockDirectory(): File? {
    try {
        var dir = File(Stencils::class.java.protectionDomain?.codeSource?.location?.toURI() ?: return null)

        while (dir.exists()) {
            val stock = File(dir, "stock")
            if (stock.exists()) {
                return stock
            }
            // When running from my IDE, and also from "gradle run", dir is initially ./out/production/classes,
            // and our stock directory is in ./src/dist/stock
            val stock2 = File(File(File(dir, "src"), "dist"), "stock")
            if (stock2.exists()) {
                return stock2
            }
            dir = dir.parentFile ?: break
        }
        return null
    } catch (e: Exception) {
        return null
    }
}
