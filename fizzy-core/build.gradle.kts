// fizzy-core

plugins {
    kotlin("jvm") version "1.9.10"
    id("maven-publish")
    id("org.openjfx.javafxplugin") version "0.1.0"
    application
}

val jsonVersion: String by project
val log4jVersion: String by project
val junitVersion: String by project

dependencies {
    implementation("com.eclipsesource.minimal-json:minimal-json:${jsonVersion}")
    implementation("org.apache.logging.log4j:log4j-api:${log4jVersion}")
    implementation("org.apache.logging.log4j:log4j-core:${log4jVersion}")

    testImplementation("junit:junit:${junitVersion}")
    //testCompile 'org.jetbrains.kotlin:kotlin-test'
}

publishing {
    repositories {

        maven {
            // 9097003 is the GitLab project ID of project nickthecoder/fizzy
            url = uri("https://gitlab.com/api/v4/projects/9097003/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = providers.gradleProperty("gitLabPrivateToken").get()
                // The password is stored in ~/.gradle/gradle.properties
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
